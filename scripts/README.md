# SCRIPT om/script

Ensemble de script système permettant la plupart du temps d'installé un service et de le sécurisé, mais aussi d'automatiser certaines tâches courante d'administration système et de personnalisation.
C'est ici qu'on retrouve les deux scripts principaux pour personnaliser son environnement et pour configurer son système pour le compléter d'outils pratiques en ligne de commande.
Certains scripts sont aussi destiné à une utilisation en mode graphique. 
Pour l'installation de service, un script peut être couplé de son équivalent en client à déployer sur les postes clients du service installé.

Les scripts ont tous un même mode de fonctionnement et d'écriture. Ils sont structurés pareil au niveau du code source, et il structure le système de la même manière. ex : les services sont installés et cofiguré pour utiliser le dossier /srv/nom-du-service. Ainsi on peut installer plusieurs script sur la même machine (comme le serveur git et un serveur web).

## Usage

La plupart des scripts doivent être exécuter dans leur dossier d'origine et sont lié à la structure du projet om car il font appel à des template de fichier de configuration présent dans om/config.
Il convient aussi d'avoir préalablement lancé le script de configuration du système utilisé qui s'apelle par exemple sudo_config_ubuntudesktop.sh ou root_config_debian.sh. Cela apporte un cadre au système sur lequel se reposent l'ensemble des scripts du projet Om. Par exemple, de nombreux scripts supposent que sipcalc est déjà installé pour calculer les plages d'adresses IP.

Le convention de nommage utilisé permet une meilleur lecture :
- sudo/root/user pour définir sous quel utilisateur il faut lancer le script (un script commenant par user ne demande aucune autorisation spéciale)
- install/config/... est l'action principal à savoir qu'install consister à attribué un nouveau service à la machine tandis que config ne requiert pas néccessairement d'installation. les scripts xxx_config_nom-systeme-exploitation.sh permette de configurer le système pour utilisé efficacement le projet Om.
- service est le nom du service qui sera installé
- option propre niveau d'installation du service. ex : sudo_install_web_http-only.sh n'installera qu'un seveur web sans base de donnée ni https
- suffixe indiquant le système pour lequel il est destiné : rhel7 fedora debian ubuntu centos freebsd
- -extension .sh .py .pl pour rappeler en quel langage il est écrit. Cela aide aussi Vim à établir sa coloration syntaxique. 

## Contributing

1. Fork it!
2. Create your feature branch: `git checkout -b my-new-feature`
3. Commit your changes: `git commit -am 'Add some feature'`
4. Push to the branch: `git push origin my-new-feature`
5. Submit a pull request :D

## Credits

Kyprio CenKeneno

## License

GPLv3

## Code erreur des scripts

20 : fonction om_testuser, l'utilisteur n'est pas celui attendu pour executer le script 
21 : le fichier base_script.sh n'est pas present dans le dossier courant du script
