echo IN PROGRESS && exit 7
#!/bin/bash
# DEBUT TEST SUDO
clear
om_echotitle "DEL USER"
[ $UID -eq 0 ] && echo -e $red"NE PAS EXECUTER AVEC ROOT\n" && exit 1
echo -e $yellow"mot de passe du super-user $USER :"$white
read -s mdp
sudo -k
( echo $mdp; echo $mdp echo $mdp ) | sudo -S echo -e $whitebig"mot de passe correct"$white 2> /dev/null
if [ ! $? -eq 0 ]; then
	echo -e $red"MOT DE PASSE INCORECT OU $USER N'EST PAS UN SUPER-USER"
	exit 2
fi
# FIN TEST SUDO

om_echoinfo "\n## DEL USER ##\n"

om_echoinfo "Liste des utilisateurs actuels :"
getent passwd | grep -P 1[0-9]{3} | cut -d: -f1
om_readtext liste_users "\nListe des nom d'utilisateurs à supprimer (séparés par un espace) :"

for user in ${liste_users[@]}; do
	sudo deluser --remove-home $user
done
 
om_theend
