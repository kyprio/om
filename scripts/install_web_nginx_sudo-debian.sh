echo IN PROGRESS && exit 7
#!/bin/bash
# option debug
#set -euo pipefail
#IFS=$'\n\t'
source "$HOME/om/scripts/base_script.sh" 2> /dev/null

# DEBUT TEST SUDO
clear
om_echotitle "INSTALL WEB NGINX"
[ $UID -eq 0 ] && whiptail --title "TEST SUDO" --msgbox "NE PAS EXECUTER AVEC ROOT" 20 70 && exit 1
mdp=$(whiptail --title "TEST SUDO" --passwordbox "Mot de passe du super-user $USER :" 20 70 3>&1 1>&2 2>&3)
sudo -k
( echo $mdp; echo $mdp echo $mdp ) | sudo -S echo -e $whitebig"mot de passe correct"$white 2> /dev/null
if [ ! $? -eq 0 ]; then
	echo -e $red"MOT DE PASSE INCORECT OU $USER N'EST PAS UN SUPER-USER"
	exit 2
fi
# FIN TEST SUDO

# PRECONFIGURATION
##################

# INSTALLATION
##############

sudo apt install php7.0-fpm nginx-light mariadb-server php7.0mysql
#sudo apt install nginx-full

# POSTCONFIGURATION
###################


# création des dossiers
sudo mkdir -p /srv/web /srv/mariadb
#sudo mkdir -p /srv/ssl

# changement du dossier racine web de lighttpd
sudo sed -i 's#root /var/www/html;#root /srv/web;#' /etc/nginx/sites-available/default 
sudo sh -c "echo '<?php phpinfo(); ?>' > /srv/web/index.php"
sudo chown -R www-data:www-data /srv/web
sudo chmod -R 770 /srv/web

# rajout de l'utilisateur $USER à  www-data
sudo adduser $USER www-data

# déplacement base de donnée
sudo sed -i "s#/var/lib/mysql#/srv/mariadb#" /etc/mysql/mariadb.conf.d/50-server.cnf 

# redémarrage du service lighttpd
sudo service nginx restart
sudo service mysql restart
 
om_theend
