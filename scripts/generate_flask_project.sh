#!/bin/bash

if [[ -n "$1" ]]; then
  pretty_project="$1"
else
  read -p "Nom du Projet (Majuscule et espace acceptée) : " pretty_project
fi
project=$(echo "${pretty_project,,}" | sed 's/ /_/g')

if [[ -n "$2" ]]; then
  pj="$2"
else
  read -p "initiale projet (minuscle sans espace) pour les tables de la DB : " pj
fi


cp -r ${HOME}/om/lang/python/templates/flask_project "${project}"

cd "${project}" ; mv flask_project "${project}"

find . -type f -exec sed -i "s/flask_project/${project}/" {} \;
find . -type f -exec sed -i "s/Flask Project/${pretty_project}/" {} \;
find . -type f -exec sed -i "s/fp_/${pj}_/" {} \;

#python3 -m venv venv
#echo "execute :"
#echo ". test.sh"
#echo ". untest.sh"
