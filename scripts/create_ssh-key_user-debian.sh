echo IN PROGRESS && exit 7
#!/bin/bash
# option debug
#set -euo pipefail
source "$HOME/om/scripts/base_script.sh" 2> /dev/null

om_echotitle "CREATE SSH-KEY"

# PRECONFIGURATION
##################


dest=$(whiptail --title "CREATE SSH-KEY _ destination" --inputbox "Saisir le user@destination pour l'envoi de la futur clés ssh\nécrire NO pour ne pas envoyer la clés" 20 70 "$USER@IP" 3>&1 1>&2 2>&3)


# EXECUTION
###########

ssh-keygen -t rsa -C echo "$USER@$HOSTNAME"

[ "$dest" == "NO" ] && echo -e $yellow"Pas d'envoi de clés"$white && exit
ssh-copy-id $dest

# POSTCONFIGURATION
###################


 
om_theend
