#!/bin/bash
source "$HOME/om/scripts/base_script.sh" || ( echo "base_script.sh manquant" && exit 1 )
om_testuser root

# installation
om_echoinfo "NE PAS INSTALLER OPENVPN SUR UN CONTAINER...cf internet"
om_echoinfo "installation d'Openvn\nprévoir de répondre YES pour la config d'iptables"
sleep 2
om_readtext ethwan "Nom de l'interface réseau coté WAN (internet)"
def_vpnport=1194
om_readtext vpnport "port VPN de la box internet (coté WAN)"
[[ -z "$vpnport" ]] && vpnport=$def_vpnport
apt-get -y remove ipmitool
apt-get install -y openvpn iptables-persistent easy-rsa curl
ipwan="$(curl v4.ifconfig.co)"

# PRECONFIGURATION

cp -a /usr/share/easy-rsa /etc/openvpn/
mv /etc/openvpn/easy-rsa/openssl-1.0.0.cnf /etc/openvpn/easy-rsa/openssl.cnf
gunzip -c /usr/share/doc/openvpn/examples/sample-config-files/server.conf.gz > /etc/openvpn/server.conf

sed -i 's/;user nobody/user nobody/' /etc/openvpn/server.conf
sed -i 's/;group nogroup/group nogroup/' /etc/openvpn/server.conf
sed -i 's/^ca ca.crt/ca \/etc\/openvpn\/easy-rsa\/keys\/ca.crt/' /etc/openvpn/server.conf
sed -i 's/^cert server.crt/cert \/etc\/openvpn\/easy-rsa\/keys\/server.crt/' /etc/openvpn/server.conf
sed -i 's/^key server.key/key \/etc\/openvpn\/easy-rsa\/keys\/server.key/' /etc/openvpn/server.conf
sed -i 's/^dh .*/dh \/etc\/openvpn\/easy-rsa\/keys\/dh2048.pem/' /etc/openvpn/server.conf
sed -i 's/^tls-auth ta.key/tls-auth \/etc\/openvpn\/easy-rsa\/keys\/ta.key/' /etc/openvpn/server.conf
sed -i 's/;push \"redirect-gateway def1 bypass-dhcp\"/push \"redirect-gateway def1 bypass-dhcp\"/' /etc/openvpn/server.conf
sed -i 's/;compress lz4/;compress lz4/' /etc/openvpn/server.conf
sed -i 's/;push "compress/push "compress/' /etc/openvpn/server.conf

sed -i 's/#net.ipv4.ip_forward=1/net.ipv4.ip_forward=1/' /etc/sysctl.conf

cd /etc/openvpn/easy-rsa/
sed -i 's/COUNTRY=\"US\"/COUNTRY=\"FR\"/' vars
sed -i 's/PROVINCE=\"CA\"/PROVINCE=\"IDF\"/' vars
sed -i 's/CITY=\"SanFrancisco\"/CITY=\"Bretigny\"/' vars
sed -i 's/ORG=\"Fort-Funston\"/ORG=\"KYPRIO-LIBRE\"/' vars
sed -i 's/EMAIL=\"me@myhost.mydomain\"/EMAIL=\"admin@kyprio-libre.fr\"/' vars
sed -i 's/OU=\"MyOrganizationalUnit\"/OU=\"HOME\"/' vars

source /etc/openvpn/easy-rsa/vars
bash /etc/openvpn/easy-rsa/clean-all

# création des certificats serveur

om_echoinfo "Création des certificats serveur"
om_echoinfo "Création du CA"
om_echoinfo "il est possible de modifier les valeurs par défauts"

bash /etc/openvpn/easy-rsa/build-ca

om_echoinfo "création de la clés Diffie-Hellman en 2048 bit"

bash /etc/openvpn/easy-rsa/build-dh

om_echoinfo "création du certificat du serveur"$whitebig" server"
om_echoinfo "il est conseillé d'ajuster les valeurs par défauts"
om_echoinfo "répondez"$whitebig" y "$yellow" aux questions"

bash /etc/openvpn/easy-rsa/build-key-server server

# creation du ta.key
openvpn --genkey --secret /etc/openvpn/easy-rsa/keys/ta.key

# création des certificats client

om_echoinfo "Création des certificats client"
om_echoinfo "il est conseillé d'ajuster les valeurs par défauts"
om_echoinfo "répondez"$whitebig" y "$yellow" aux questions"

cd /etc/openvpn/easy-rsa/
bash /etc/openvpn/easy-rsa/build-key client

mkdir -p ~/client_vpn
cp /etc/openvpn/easy-rsa/keys/client.* ~/client_vpn/
cp /etc/openvpn/easy-rsa/keys/ca.crt ~/client_vpn/
cp /etc/openvpn/easy-rsa/keys/ta.key ~/client_vpn/
cd ~/client_vpn
cp /usr/share/doc/openvpn/examples/sample-config-files/client.conf .
sed -i "s/^remote my-server-1 1194/remote ${ipwan} $vpnport/" client.conf
tar -zcf ~/client_vpn.tar.gz ~/client_vpn

# iptables

iptables -t nat -A POSTROUTING -o $ethwan -j MASQUERADE
iptables -t nat -A POSTROUTING -s 10.8.0.0/24 -o $ethwan -j MASQUERADE
#iptables -t nat -A POSTROUTING -s 10.8.0.0/24 -o wlan0 -j MASQUERADE

iptables-save > /etc/iptables/rules.v4
ip6tables-save > /etc/iptables/rules.v6

# activation openvpn
systemctl enable openvpn
systemctl start openvpn
systemctl status openvpn

om_theend
