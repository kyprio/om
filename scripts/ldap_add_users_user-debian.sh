echo IN PROGRESS && exit 7
#!/bin/bash

source "$HOME/om/scripts/base_script.sh" 2> /dev/null
om_echotitle "LDAP ADD USER"

# Script pour créer un fichier .ldif pour ajouter des utilisateurs à un annuaire ldap3


# PRECONFIGURATION
##################


def_dngroup="cn=users,ou=groups,dc=labo,dc=lan"
def_uid="5001"
tmp=tmp_ldap_add_user.ldif

whiptail --title "LDAP ADD USER" --yesno "Supprimer l'ancien fichier $tmp ?" 20 70 && rm -f $tmp

dngroup=$(whiptail --title "LDAP ADD USER" --inputbox "DistinguishedName du Group ou de l'OrganizationalUnit\npar défaut : $def_dngroup" 20 70 3>&1 1>&2 2>&3) || exit 1
[ -z "$dngroup" ] && dngroup=$def_dngroup

uid=$(whiptail --title "LDAP ADD USER" --inputbox "UID Number de la première personne à ajouter\npar défaut$def_uid" 20 70 3>&1 1>&2 2>&3) || exit 1
[ -z "$uid" ] && uid=$def_uid

liste_users=$(whiptail --title "LDAP ADD USER" --inputbox "Liste des utilisateurs à inscrire dans l'annuaire séparé par un espace" 20 70 3>&1 1>&2 2>&3) || exit 1


# MODIFICATION
##############


touch $tmp

for user in ${liste_users[@]}; do

	echo "\
# Entrycn=$user,$dngroup
dn: cn=$user,$dngroup
cn:  $user
gidnumber: 100
homedirectory: /home/$user
loginshell: /bin/bash
objectclass: inetOrgPerson
objectclass: posixAccount
objectclass: top
sn: $user
uid: $user
uidnumber: $uid
userpassword: {MD5}K+rHwgsBDl0x8Vl5Qj69oA==

" >> $tmp
	(( uid++ ))
done


# POSTCONFIGURATION
###################


echo -e $yellow"Le mot de passe par défaut est"$whitebig"@test@"$white
echo -e $yellow"La commande a exécuter sera du type :\n"$whitebig"sudo ldapadd -c -x -D \"cn=admin,dc=labo,dc=lan\" -W -f $tmp"$white
 
om_theend
