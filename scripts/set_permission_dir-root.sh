echo IN PROGRESS && exit 7
#!/bin/bash
ls 
source "$HOME/om/scripts/base_script.sh" || echo "base_script.sh manquant" && exit 1
testuser root
read -p "Dossier" dir
read -p "Groupe" group

cd $dir

chgrp -R $group .

find -type f -exec setfacl -m mask:rw {} \;
find -type d -exec setfacl -m mask:rwx {} \;

find -type d -exec chmod g+s {} \;
find -type d -exec setfacl -m default:mask:rwx {} \;
find -type d -exec setfacl -m default:other:‑‑‑ {} \;
 
om_theend
