#!/bin/bash
source "$HOME/om/scripts/base_script.sh" || ( echo "base_script.sh manquant" && exit 1 )
om_testuser root

cd /etc/openvpn/easy-rsa

source vars

# reset 
bash clean-all

# création des certificats serveur

om_echoinfo "Création des certificats serveur"
om_echoinfo "Création du CA"
om_echoinfo "il est possible de modifier les valeurs par défauts"

bash build-ca

om_echoinfo "création de la clés Diffie-Hellman en 2048 bit"

bash build-dh

om_echoinfo "création du certificat du serveur"$whitebig" server"
om_echoinfo "il est conseillé d'ajuster les valeurs par défauts"
om_echoinfo "répondez"$whitebig" y "$yellow" aux questions"

bash build-key-server server

# creation du ta.key
openvpn --genkey --secret /etc/openvpn/easy-rsa/keys/ta.key

# rechargement d'openvpn
systemctl restart openvpn

om_theend
