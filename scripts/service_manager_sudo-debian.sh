echo IN PROGRESS && exit 7
#!/bin/bash
# option debug
#set -euo pipefail
#IFS=$'\n\t'
source "$HOME/om/scripts/base_script.sh" 2> /dev/null

# DEBUT TEST SUDO
clear
om_echotitle "SERVICE MANAGER"
[ $UID -eq 0 ] && whiptail --title "TEST SUDO" --msgbox "NE PAS EXECUTER AVEC ROOT" 20 70 && exit 1
mdp=$(whiptail --title "TEST SUDO" --passwordbox "Mot de passe du super-user $USER :" 20 70 3>&1 1>&2 2>&3)
sudo -k
( echo $mdp; echo $mdp echo $mdp ) | sudo -S echo -e $whitebig"mot de passe correct"$white 2> /dev/null
if [ ! $? -eq 0 ]; then
	echo -e $red"MOT DE PASSE INCORECT OU $USER N'EST PAS UN SUPER-USER"
	exit 2
fi
# FIN TEST SUDO

# PRECONFIGURATION
##################


liste_action=("restart" "start" "stop" "status" "reload" "force-reload" "check" "flush") 
liste_service=(\
apache2 \
lighttpd \
nginx \
networking \
network-manager \
isc-dhcp-server \
bind9 \
postfix \
dovecot \
exim \
ssh \
sshd \
nfs-kernel-server \
tftpd-hpa \
squid \
samba \
openvpn \
git \
slapd \
)

long_action=`echo ${#liste_action[@]}`
menu_action="whiptail --title 'SERVICE MANAGER' --radiolist 'action' $(( $long_action + 10 )) 70 $long_action "
for i in ${liste_action[@]};  do echo $i; menu_action="$menu_action  '$i' '' OFF" ; done
menu_action='action=$('"$menu_action"' 3>&1 1>&2 2>&3)'

long_service=`echo ${#liste_service[@]}`
menu_service="whiptail --title 'SERVICE MANAGER' --radiolist 'service' $(( $long_service + 10 )) 70 $long_service "
for i in ${liste_service[@]};  do echo $i; menu_service="$menu_service  '$i' '' OFF" ; done
menu_service='choix=$('"$menu_service"' 3>&1 1>&2 2>&3)'

# EXECUTION
###########

eval $menu_action
eval $menu_service
clear
sudo service $choix $action
sudo service $choix status
 
om_theend
