echo IN PROGRESS && exit 7
#!/bin/bash
# option debug
source "$HOME/om/scripts/base_script.sh" 2> /dev/null

om_echotitle "RPI BUTTON PLAY"

# PRECONFIGURATION
##################

nGPIO=4
source user_rpi_gpio_control.sh


# EXECUTION
###########

gpio_control $nGPIO in get

while true do
	until grep 0 /sys/class/gpio/gpio$nGPIO/value >> /dev/null do
		sleep .1
	done
	play ~/10670.wav
done
# POSTCONFIGURATION
###################


 
om_theend
