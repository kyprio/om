#!/bin/bash

# HEADER
##################################################

#set -o errexit
set -o nounset
set -o pipefail
#set -o xtrace
#set -v

source "base_script.sh"  2> /dev/null || (echo "base_script.sh manquant" ; exit 1)

readonly scriptName="$(basename ${0})"
readonly scriptVersion="0.1" # 04/12/17 by kyprio for the project om

function usage {
  local returnCode="${1:-0}"
 
  om_echoinfo "${scriptName}\tconfigure the acl for the group on the specify directory"

  om_echoreturn "${scriptName}
  -x $Y # run the script with interactive mode if necessary $B
  [-v] $Y # version $B
  [-h] $Y # help $B"
  om_echoreturn "${scriptName}
  -d  $Y #  $B 
  -g  $Y #  $B
  -s  $Y #  $B"

  exit "${returnCode}"
}

om_header -t "CONFIG ACL ON DIRECTORY" -v "${scriptVersion}" -u sudo

[[ "${#}" -eq 0 ]] && (om_echoerror "At least, please run the script with the -x parameter" ; usage "4") 

# PRECONFIGURATION
##################################################

om_echosubtitle "PRECONFIGURATION"

pri_group=''     # mandatory :  
dir=''     # mandatory :  
while getopts ":g:d:s:xhv" opt; do
  case "${opt}" in
    x) ;;
    h) usage ;;
    v) om_version "${scriptVersion}" ;;
    \?) om_echoerror "Invalid option: -${OPTARG}" ; exit 4 ;;
    :) om_echoerror "missing value to -${OPTARG}" ; exit 4 ;;
    g) pri_group="${OPTARG}" ;; 
    d) dir="${OPTARG}" ;; 
    s) sec_group="${OPTARG}" ;; 
  esac
done

while [[ -z "${pri_group}" ]]; do om_readtext pri_group "pri_group" ; done 
[[ -z "${sec_group}" ]] && om_readtext sec_group "sec_group (optional)"
while [[ -z "${dir}" ]]; do om_readtext dir "dir" ; done 

om_debugvar pri_group sec_group dir 

# EXECUTION
##################################################

om_step "EXECUTION"

# droit posix

sudo chgrp -R "${pri_group}" "${dir}"
sudo find "${dir}" -type d -exec sudo chmod g+s {} \;

# droit acl rw(x) pour pri_group et r(x) pour sec_group

sudo find "${dir}" -type f -exec sudo setfacl -m "group:${pri_group}:rw" {} \;
sudo find "${dir}" -type d -exec sudo setfacl -m "group:${pri_group}:rwx" {} \;
sudo find "${dir}" -type d -exec sudo setfacl -m "default:group:${pri_group}:rwx" {} \;
for group in ${sec_group} ; do
  sudo find "${dir}" -type f -exec sudo setfacl -m "group:${group}:r" {} \;
  sudo find "${dir}" -type d -exec sudo setfacl -m "group:${group}:rx" {} \;
  sudo find "${dir}" -type d -exec sudo setfacl -m "default:group:${group}:rx" {} \;
done

# droits pour other et mask

sudo find "${dir}" -exec sudo setfacl -m mask:rwx {} \;
sudo find "${dir}" -type d -exec sudo setfacl -m default:mask:rwx {} \;
sudo find "${dir}" -exec sudo setfacl -m other::--- {} \;
sudo find "${dir}" -type d -exec sudo setfacl -m default:other:--- {} \;

# POSTCONFIGURATION
##################################################

om_echosubtitle "POSTCONFIGURATION"

om_theend
