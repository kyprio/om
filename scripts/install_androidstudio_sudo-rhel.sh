echo IN PROGRESS && exit 7
#!/bin/bash

# options shell
set -o errexit
set -o nounset
set -o pipefail
set -o xtrace

source "$HOME/om/scripts/base_script.sh"  2> /dev/null || ( echo "base_script.sh manquant" ; exit 1 

# information script

readonly scriptName="$(basename ${0})"
readonly scriptVersion="0.1" # 04/01/2017 by kyprio for the project om

om_echotitle "INSTALL ANDROID STUDIO"
om_version "${scriptVersion}"
om_testuser sudo
om_inprogress

function usage {
  om_version "${scriptVersion}"
  om_echoinfo "${scriptName} -s androidstudio.zip [-v] [-h]"
  om_echoinfo "${scriptName} extrait et install Android Studio\ncompatible Android Studio version 2.3"
}


# PRECONFIGURATION
##################
om_echosubtitle "PRECONFIGURATION"

readonly pkgToInstall="zlib.i686 ncurses-libs.i686 bzip2-libs.i686"

# informations attendues
source=''
while getopts ":s:hv" opt; do
  case "${opt}" in
    h)
      usage
      exit 0
      ;;
    v)
      om_version "${scriptVersion}"
      exit 0
      ;;
    a)
      source="${OPTARG}"
      echo source=${source}
      ;;
    \?)
      om_echoerror "Invalid option: -${opt}"
      exit 4
      ;;
    :)
      om_echoerror "Missing value to -${opt}"
      exit 4
      ;;
  esac
done

[ -n "${source}" ]

# EXECUTION
###########
om_step "EXECUTION"

om_rhelinstall lib6:i686 

# POSTCONFIGURATION
###################
om_echosubtitle "POSTCONFIGURATION"

om_theend
