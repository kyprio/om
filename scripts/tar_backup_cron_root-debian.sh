#!/bin/bash

bk_path="/var/backups/backup_let_conf/"
bk_name="backup_let_conf_$(date +%F).tar.gz"

# cron a 2h
mkdir -p ${bk_path}
echo 0 > ${bk_path}status && \
chown backup:backup ${bk_path}status && \
tar -zcf ${bk_path}${bk_name} /root/backup && \
chown backup:backup ${bk_path}${bk_name} && \
echo "${bk_name}" > ${bk_path}status
