#!/bin/bash

#---------
#- utilisation d'un dossier /etc/hosts.d
#- contenant l'ensemble des fichiers à concaténer
#- nommés de façon ordonné :
#- 001-localhost
#- 002-lan
#- ...
#---------

HOSTS_DIR="/etc/hosts.d/"

if [ ! -d "${HOSTS_DIR}" ] || [ "$(ls "${HOSTS_DIR}" | wc -l )" == 0 ] ; then
  echo "la destination ${HOSTS_DIR} n'existe pas ou est vide"
  exit 1
fi


cat "${HOSTS_DIR}"* > /etc/hosts
