#!/bin/bash

/sbin/iptables-restore < /etc/iptables/rules.v4

cd /root/backup/script/firewall
for i in * ; do
  bash ${i}
done
