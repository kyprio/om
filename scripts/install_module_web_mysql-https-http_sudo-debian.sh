echo IN PROGRESS && exit 7
#!/bin/bash

# PRE CONFIGURATION
###################

# INSTALLATION
##############

sudo debconf-set-selections <<< "mysql-server mysql-server/root_password password $mdpsrv"
sudo debconf-set-selections <<< "mysql-server mysql-server/root_password_again password $mdpsrv"
sudo apt-get install -y unzip lynx vim screen lighttpd php5-cgi php5-gd imagemagick mysql-server php5-mysql openssl libcurl3 php5-curl 

# POST CONFIGURATION
####################

# création des dossiers
sudo mkdir /srv/ssl /srv/web /srv/mysql

# création du certificat SSL
sudo openssl req -new -x509 -keyout /srv/ssl/$cn\.pem -out /srv/ssl/$cn\.pem -days 3650 -nodes -subj "/C=$pays/ST=$region/L=$ville/O=$entreprise/OU=$entreprise/CN=$cn"

# restriction des droits sur le dossier /srv/ssl
sudo chmod -R 400 /srv/ssl/

# changement du dossier racine web de lighttpd
sudo sed -i 's/\/var\/www\/html/\/srv\/web/' /etc/lighttpd/lighttpd.conf
sudo sh -c "echo '<?php phpinfo(); ?>' > /srv/web/index.php"
sudo chown -R www-data:www-data /srv/web
sudo chmod -R 775 /srv/web

# PRECONFIGURATION du ssl dans lighhtpd
sudo sed -i "s/\/etc\/lighttpd\/server.pem/\/srv\/ssl\/$cn\.pem/" /etc/lighttpd/conf-available/10-ssl.conf

# activation des modules php ssl pour http https
sudo service lighttpd start
sudo lighty-enable-mod fastcgi-php
sudo lighty-enable-mod ssl

# rajout de l'utilisateur $USER à  www-data
sudo adduser $USER www-data

# backup des configurations du web
sudo cp -rp /etc/mysql /srv/backup/
sudo cp -rp /etc/lighttpd /srv/backup/

# création de liste détaillée avant déplacement
liste_dossier=('/' '/etc/ssl' '/var/lib')
for dossier in ${liste_dossier[@]}; do
	file_dossier=`echo $dossier | tr '/' '_'`
	sudo sh -c "ls $dossier > /srv/backup/$file_dossier"
done

# creation/déplacement du dossier /var/lib/mysql vers /srv/mysql
sudo cp -rp /var/lib/mysql /srv/
sudo rm -r /var/lib/mysql
sudo ln -s /srv/mysql /var/lib/mysql

# redémarrage du service lighttpd
sudo service lighttpd restart
sudo service mysql restart
 
om_theend
