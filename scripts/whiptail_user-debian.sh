echo IN PROGRESS && exit 7
#!/bin/bash
#set -euo pipefail
IFS=$'\n\t'
source "$HOME/om/scripts/base_script.sh" 2> /dev/null

om_echotitle "WHIPTAIL"

# PRE CONFIGURATION
###################


path=`pwd`/tmp_whiptail
continu=y
list_type=('inputbox' 'passwordbox' 'msgbox' 'yesno')

echo -e $yellow"Suppression de l'ancien fichier tmp_whiptail ? CTRL+C pour annuler"$whitebig
read -n 1 suppr
rm -f tmp_whiptail


# MODIFICATION
##############


while [ "$continu" == "y" ]; do
	om_readtext titre "TITRE DE LA BOITE DE DIALOGUE"
	om_readtext type "type : 0=inputbox/1=passwordbox/2=msgbox ?"
	om_readtext content "Contenu texte :"
	type=${list_type[$type]}
	case $type in
		"msgbox")
			echo "whiptail --title \"$titre\" --$type \"$content\" 20 70 3>&1 1>&2 2>&3 || exit 1" >> $path
			;;
		*)
			om_readtext var "nom de la variable récupérant le contenu :"
			echo "$var=\$(whiptail --title \"$titre\" --$type \"$content\" 20 70 3>&1 1>&2 2>&3) || exit 1" >> $path
			;;
	esac


	om_echoinfo "le fichier est :\n$path"
	echo -e $yellow"Ajouter de nouveau élément ? y/N"
	read continu && continu=`echo $continu | tr 'A-Z' 'a-z'`
	
	om_echoinfo "\nle fichier est :\n$path"
done

 
om_theend
