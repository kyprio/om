#!/bin/bash 

DIA_DIR="${HOME}/dia_decomposed"
COMMAND=$( if [[ -n "$1" ]]; then echo "$1" ; else exit 1 ; fi )
DIA_FILE=$( if [[ -n "$2" ]]; then echo "$(realpath $2)" ; else echo "${DIA_DIR}/dia_generated" ; fi )

case "$COMMAND" in 
  "init")
    mkdir -p ${DIA_DIR}/{skel,object}
    ;;
  "extract"|"x")
    cp -i $DIA_FILE $DIA_DIR 2> /dev/null
    cd $DIA_DIR
    DIA_FILE=$(basename ${DIA_FILE})
    rm -f ${DIA_FILE%.dia}
    rm -f ${DIA_DIR}/{skel,object}/*
    gunzip -kS .dia $DIA_FILE
    DIA_FILE=${DIA_FILE%.dia}
    sed -n '/<\/dia:layer/,$p' $DIA_FILE > skel/foot
    sed -i '/<\/dia:layer/,$d' $DIA_FILE
    sed -n '0,/<dia:layer/p' $DIA_FILE > skel/head
    sed -i '0,/<dia:layer/d' $DIA_FILE
    obj=1
    obj_lines=($(awk '/dia:object/ {print FNR}' $DIA_FILE ))
    for (( i=0; i<=$((${#obj_lines[@]} - 1)); i=$((i+2)) )); do
      d=${obj_lines[$i]}
      f=${obj_lines[$((i+1))]}
      sed -n "${d},${f}p" $DIA_FILE > object/$obj
      ((obj++))
    done
    rm -f $DIA_FILE
    ;;
  "generate"|"gen")
    cd $DIA_DIR
    cat skel/head object/* skel/foot > $DIA_FILE
    rm -f ${DIA_FILE}.gz
    gzip $DIA_FILE
    mv ${DIA_FILE}.gz ${DIA_FILE}.dia
    DIA_FILE=${DIA_FILE}.dia
    dia $DIA_FILE &
    ;;
esac
echo done


