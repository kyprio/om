echo IN PROGRESS && exit 7
#!/bin/bash

pathCA="$(ls CA/ca_*.crt)"
pathConf='openssl_users.cnf'

test -e ${pathCA} || exit 1
test -e ${pathConf} || exit 2

if [ -z "${*}" ]; then
	echo "prenom_nom en argument"
	exit 3
fi

for USERL in $(echo "${*}"); do

	pathKey="key/user-${USERL}.pem"
	pathReq="req/user-${USERL}.req"
	dirCert='cert'
	destCert="${dirCert}/$(date +%Y%m%d)_user-${USERL}.crt"
	pathOldCert="${dirCert}/$(ls ${dirCert} | sort -r | grep -m 1 ${USERL})"
	destP12="${USERL}.p12"
	
	# DEBUG
	test -e "${pathKey}" || exit 4
	test -e "${pathReq}" || exit 5
	test -e "${dirCert}" || exit 6
	test -e "${pathOldCert}" || exit 7

	echo -e  "\n"${USERL}"\n"
	if [ "$(openssl verify -CAfile "${pathCA}" "${pathOldCert}" | wc -l )" -gt 1 ]; then
		openssl ca -config "${pathConf}" -revoke "${pathOldCert}" && \
	openssl ca -config "${pathConf}" -out "${destCert}" -in "${pathReq}" && \
 	openssl pkcs12 -export -in "${destCert}" -inkey "${pathKey}" -certfile "${pathCA}" -out "${destP12}"
	else
		echo "certificat encore valide"
	fi

done


# vérifier si un certificat expire avant 3mois
# openssl x509 -noout -in "${pathOldCert}" -checkend 7862400 || echo "expire avant 3 mois"
#
# vérifier si un certificat est issue du CA
# openssl verify -CAfile "${pathCA}" "${pathOldCert}"
#
# vérifier si un certificat est expiré
# openssl verify -CAfile "${pathCA}" "${pathOldCert}"
