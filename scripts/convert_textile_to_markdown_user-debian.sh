#!/bin/bash
# option debug
#set -euo pipefail
#IFS=$'\n\t'
source "$HOME/om/scripts/base_script.sh" 2> /dev/null

om_echotitle "CONVERT TEXTILE TO MARKDOWN"

# PRECONFIGURATION
##################

textile=${1}
echo $textile
md=`echo ${textile} | sed 's/\.textile$/.md/'`

CR=$(printf '\n')

# EXECUTION
###########

cat $textile \
| sed 's/{{>toc}}//' \
| sed 's/{{lastupdated_at}}//' \
| sed 's/^h1\. /#  /'  \
| sed 's/^h2\. /##    /' \
| sed 's/^h3\. /###      /' \
| sed 's/^h4\. /####        /' \
| sed 's/^h5\. /#####          /' \
| sed 's/^h6\. /######            /' \
| sed 's/<pre>$/\`\`\`/' \
| sed 's/<\/pre>$/\`\`\`/' \
| sed 's/<pre>/\`/' \
| sed 's/<\/pre>/\`/' \
| sed 's/bq. %{color:.*}/\`\`\`\n/' \
| sed 's/bq. /\`\`\`\n/' \
| sed 's/%$/\n\`\`\`/' \
> $md

# LIEN

# POSTCONFIGURATION
###################


 
om_theend
