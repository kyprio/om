echo IN PROGRESS && exit 7
#!/bin/bash
# ./create_vm name_vm name_lvm size_vm  [mode]

# NON FONCTIONNEL
# Objectif : $4 permet de choisir mode graphique ou mode console
exit

name=${1}
lvm=${2}
size=${3}

# mode
case ${4} in
	"vnc")
		graphical="vnc"
		extraargs="none"
		;;
	"console"|"")
		graphical="none"
		extraargs="console=ttyS0,115200n8 serial"
		;;
esac
	
	
lvcreate -n ${name} -L "$"g ${lvm}
#mkdir /dev/${lvm}/${name}
virt-install \
        --name ${name} \
        --ram 512 \
        --disk path=/dev/${lvm}/${name} \
        --vcpus 2 \
        --mac 54:52:00:00:00:2F \
        --network bridge:vbr0 \
        --graphics ${graphical} \
        --console pty,target_type=serial \
        --os-variant debianwheezy \
        --os-type linux \
        --location 'http://ftp.fr.debian.org/debian/dists/stable/main/installer-amd64/' \
        --extra-args 'console=ttyS0,115200n8 serial' \
        --debug \
        --force

lvcreate -n $m -L "$t"g $n
#mkdir /dev/$n/$m
virt-install \
        --name $m \
        --ram 1024 \
        --disk path=/dev/$n/$m \
        --vcpus 2 \
        --mac 54:52:00:00:00:3F \
        --network bridge:vbr0 \
        --graphics vnc \
        --os-variant debianwheezy \
        --os-type linux \
        --location 'http://ftp.fr.debian.org/debian/dists/stable/main/installer-amd64/' \
        --debug \
        --force

#--console pty,target_type=serial \
#--extra-args 'console=ttyS0,115200n8 serial' \
 
om_theend
