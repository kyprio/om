#!/bin/bash
source "$HOME/om/scripts/base_script.sh" 2> /dev/null

# pre configuration

ip_def=`hostname -I`
if [ $(echo $ip_def | wc -w) -gt 1 ]; then
	echo -e $yellow"Vous avez plusieurs IP :"
	echo -e $whitebig"$ip_def" | tr " " "\n" | grep -v ^$
	om_readtext ip "Quel IP choisir pour le service SLAPD ?"
	[ ! $ip ] && ip=`echo $ip_def | cut -d " " -f 1`
fi

base_def=`hostname -d | sed 's/^/dc=/' | sed 's/\./,dc=/'`
echo -e $yellow"Base de l'annuaire de type dc=example,dc=com..."
om_readtext base "Dans votre cas, ce pourrait-être " $base_def " :"
[ ! $base ] && base=$base_def
dn=`echo $base | sed 's/dc=//g' | sed 's/,/./'`
DN=`echo $dn | tr 'a-z' 'A-Z'`

echo -e $yellow"Mot de passe pour le compte admin du LDAP :"$white
read -s mdp_ldap

om_readtext orga "Un nom d'entreprise,association ou organisation" 

# installation
apt install -y debconf-utils

echo "
slapd slapd/password1 password $mdp_ldap
slapd slapd/password2 password $mdp_ldap
slapd slapd/no_configuration boolean false
slapd slapd/domain string $dn
slapd shared/organization string $orga
slapd slapd/backend select MDB
slapd slapd/purge_database boolean true
slapd slapd/move_old_database boolean true
slapd slapd/allow_ldap_v2 boolean false
" | debconf-set-selections

#apt install -y slapd ldap-utils phpldapadmin
apt install -y slapd ldap-utils

# post configuration

sed -i "s/#BASE.*/BASE\t$base/" /etc/ldap/ldap.conf
sed -i "s/#URI.*/URI\t ldap:\/\/$ip/" /etc/ldap/ldap.conf

#cd /etc/phpldapadmin
#sed -i "s/dc=example,dc=com/$base/g" config.php
#sed -i "s/example\.com/$dn/g" config.php
#sed -i "s/EXAMPLE\.COM/$DN/g" config.php

om_echoinfo "FIN"
 
om_theend
