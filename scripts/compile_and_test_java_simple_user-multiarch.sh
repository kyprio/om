#!/bin/bash

rm -rf bin
mkdir -p bin
javac -Xlint -g -deprecation -d bin src/*.java
rsync src/* --exclude="*.java" bin/
cd bin
java Main
cd ..
rm -rf bin
