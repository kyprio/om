echo IN PROGRESS && exit 7
#!/bin/bash
source "$HOME/om/scripts/base_script.sh" 2> /dev/null

om_echotitle "CONNECT SSH-SFTP"

# s'utilise avec des alias de type xssh, xsftp, xsshlocale, xsftplocale ou :
# $1 = [ssh|sftp]
# $2 = [ip|racine_ip|"ip_public -p port_redirection"]
# $3 = [vide|fin_ip]

om_echoinfo "$1 sysadmin@$2$3\n"
$1 sysadmin@$2$3
 
om_theend
