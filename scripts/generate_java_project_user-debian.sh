#!/bin/bash
source $HOME/om/scripts/base_script.sh || ( echo "base_script.sh manquant" && exit 1 )

om_testuser user
om_echotitle "CREATION CLASSE JAVA 1 to Many"

# PRECONFIGURATION
##################

om_readtext "project" "NOM_DU_PROJET"

# EXECUTION
###########

mkdir -p ${project}
cd ${project}
mkdir -p src
cd src
mkdir -p domain main services

# POSTCONFIGURATION
###################

cd domain
om_echoinfo "`pwd`\nPrêt à créer une classe"

