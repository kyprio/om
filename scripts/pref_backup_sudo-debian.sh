echo IN PROGRESS && exit 7
#!/bin/bash

source "$HOME/om/scripts/base_script.sh" 2> /dev/null
# DEBUT TEST SUDO
#clear
om_echotitle "PREFERENCE BACKUP"
#om_echotitle "PREFERENCE BACKUP"
#[ $UID -eq 0 ] && echo -e $red"NE PAS EXECUTER AVEC ROOT\n" && exit 1
#echo -e $yellow"mot de passe du super-user $USER :"$white
#read -s mdp
#sudo -k
#echo $mdp | sudo -S echo -e $whitebig"mot de passe correct"$white 2> /dev/null
#if [ ! $? -eq 0 ]; then
#	echo -e $red"MOT DE PASSE INCORECT OU $USER N'EST PAS UN SUPER-USER"
#	exit 2
#fi
# FIN TEST SUDO

# PRECONFIGURATION
dossier="/home/$USER/.pref_backup"
liste_fichier=("/etc/bash.bashrc" "/etc/vim/vimrc" "/home/$USER/.bashrc" "/home/$USER/.vimrc")

# sauvegarde des fichiers configuration
if [ -d "/home/$USER/.pref_backup/" ]; then
	om_echoerror "dossier \".pref_backup\" existe déjà"
else
	#création du dossier .pref_backup
	sudo mkdir $dossier
	for fichier in ${liste_fichier[@]}; do
		dest_fichier=`echo $fichier | tr '/' '_'`
		sudo sh -c "cat $fichier > $dossier/$dest_fichier"
	done
fi
 
om_theend
