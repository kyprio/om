#!/bin/bash

# HEADER
##################################################

set -o errexit
set -o nounset
set -o pipefail
#set -o xtrace

source "${HOME}/om/scripts/base_script.sh"  2> /dev/null || (echo "base_script.sh manquant" ; exit 1)

readonly scriptName="$(basename ${0})"
readonly scriptVersion="0.3" # 04/08/2017 by kyprio for the project om

function usage {
  local returnCode="${1:-0}"

  om_echoinfo "${scriptName} : Creation de script bash"

  om_echoreturn "\n${scriptName}
  -x $Y # run the script in interactive mode if necessary $B
  [-v] $Y # version $B
  [-h] $Y # help $B\n"
  om_echoreturn "${scriptName}
  -n 'base_script_name'
  -u 'user'|'sudo'|'root'
  -a 'multiarch'|'debian'|'ubuntu'|'raspbian'|'rhel[n]'|'centos[n]'|'fedora'
  [-V 'script_version']
  -t 'SCRIPT TITLE'
  -i 'information about this script'
  [-c] $Y # place the script in the current directory $B"

  exit "${returnCode}"
}

om_header -t "CREATE BASH SCRIPT" -v "${scriptVersion}" -u user

if [[ "${#}" -eq 0 ]] ; then
  om_echoerror "Please, specify at least the mandatory arguments"
  usage "4"
fi

#om_inprogress

# PRECONFIGURATION
##################################################

om_echosubtitle "PRECONFIGURATION"

name=''		# mandatory : base_script_name
typeUser=''	# mandatory : user|sudo|root
arch=''		# mandatory : architecture/distribution
version='' 	# optional : (0.1)
title=''	# mandatory : TITLE
info=''		# mandatory : information about the script
dir='' 		# optional : current directory if -c
filename=''	# internal : full name of the script
date=''		# internal : format %D
while getopts ":n:u:a:V:t:i:cxvh" opt; do
  case "${opt}" in
    x) ;;
    h) usage ;;
    v)  m_version "${scriptVersion}" ;;
    n)  name="${OPTARG/ /_/}" ;;
    u)  typeUser="${OPTARG}" ;;
    a)  arch="${OPTARG}" ;;
    V)  version="${OPTARG}" ;;
    t)  title="${OPTARG}" ;;
    i)  info="${OPTARG}" ;;
    c)  dir="$(pwd)" ;;
    \?) om_echoerror "Invalid option: -${OPTARG}" ; exit 4 ;;
    :) om_echoerror "missing value to -${OPTARG}" ; exit 4 ;;
  esac
done

while [[ -z "${name}" ]]; do
  om_readtext name  "Basename of the script (install_foo|config_foo|action_foo|pkgname_action_foo)"
done

while [[ -z "${typeUser}" ]]; do
  om_readtext typeUser  "Type of user who should use the script (user|sudo|root)"
done

while [[ -z "${arch}" ]]; do
  om_readtext arch  "Architecture/distribution (multiarch|debian|ubuntu|rhel[n]|centos[n]|fedora)" "?"
done

version="${version:-0.1}"

while [[ -z "${title}" ]]; do
  om_readtext title  "TITLE"
  title="${title^^}"
done

while [[ -z "${info}" ]]; do
  om_readtext info  "Information about the new script"
done

dir="${dir:-${HOME}/om/scripts}"

filename="${dir}/${name}_${typeUser}-${arch}.sh"
date="$(date '+%D')"

om_debugvar name typeUser arch version title info dir filename date

# EXECUTION
##################################################

om_step "EXECUTION"

cp ${HOME}/om/lang/bash/templates/template_script.sh ${filename}

sed -i "2d ; s/TYPE_USER/${typeUser}/ ; s/VERSION/${version}/ ; s#DATE#${date}# ; s/SCRIPT_TITLE/${title}/ ; s#INFORMATION_SCRIPT#${info}#" ${filename}

chmod 771 "${filename}"

# POSTCONFIGURATION
##################################################

om_echosubtitle "POSTCONFIGURATION"

vim +33 -c 'startinsert' "${filename}"

om_theend
