echo IN PROGRESS && exit 7
#!/bin/bash
source "$HOME/om/scripts/base_script.sh" 2> /dev/null

# DEBUT TEST SUDO
clear
om_echotitle "INSTALL AUTH LDAP"
[ $UID -eq 0 ] && echo -e $red"NE PAS EXECUTER AVEC ROOT\n" && exit 1
echo -e $yellow"mot de passe du super-user $USER :"$white
read -s mdp
sudo -k
echo $mdp | sudo -S echo -e $whitebig"mot de passe correct"$white 2> /dev/null
if [ ! $? -eq 0 ]; then
	echo -e $red"MOT DE PASSE INCORECT OU $USER N'EST PAS UN SUPER-USER"
	exit 2
fi
# FIN TEST SUDO

# pre configuration

om_readtext ip_ldap "\nIP du serveur SLDAP :"

base_def=`hostname -d | sed 's/^/dc=/' | sed 's/\./,dc=/'`
echo -e $yellow"Base requise"$whitebig
echo -e "Dans votre cas, ce pourrait-être " $base_def " :"$whitebig
read base
[ ! $base ] && base=$base_def

om_readsecret mdp_ldap "mot de passe de l'admin LDAP cn=admin,$base :"

# installation

echo "
libnss-ldap libnss-ldap/rootbindpw password $mdp_ldap
libnss-ldap libnss-ldap/bindpw password $mdp_ldap
libnss-ldap libnss-ldap/rootbinddn string cn=admin,$base
libnss-ldap shared/ldapns/base-dn string $base
libnss-ldap libnss-ldap/dbrootlogin boolean false
libnss-ldap libnss-ldap/nsswitch note
libnss-ldap libnss-ldap/binddn string cn=admin,$base
libnss-ldap libnss-ldap/confperm boolean true
libnss-ldap shared/ldapns/ldap_version select 3
libnss-ldap shared/ldapns/ldap-server string ldap://$ip_ldap
libnss-ldap libnss-ldap/override boolean true
libnss-ldap libnss-ldap/dblogin boolean false
libpam-ldap libpam-ldap/rootbindpw password $mdp_ldap
libpam-ldap libpam-ldap/bindpw password $mdp_ldap
libpam-ldap shared/ldapns/ldap-server string ldap://$ip_ldap
libpam-ldap libpam-ldap/dblogin boolean false
libpam-ldap libpam-ldap/pam_password select crypt
libpam-ldap libpam-ldap/override boolean true
libpam-ldap libpam-ldap/dbrootlogin boolean false
libpam-ldap libpam-ldap/binddn string cn=admin,$base
libpam-ldap shared/ldapns/ldap_version select 3
libpam-ldap libpam-ldap/rootbinddn string cn=admin,$base
libpam-ldap shared/ldapns/base-dn string $base
" | sudo debconf-set-selections

sudo apt-get install -y libnss-ldap libpam-ldap ldap-utils

# post configuration

sudo sed -i 's/compat/compat ldap/' /etc/nsswitch.conf
sudo sed -i '$asession optional pam_mkhomedir.so skel=/etc/skel' /etc/pam.d/common-session

om_echoinfo "Un redémarrage est nécessaire. L'ordinateur redémarrera automatiquement"
read -p "entrée pour continuer / CTRL-C pour fermer" continu
sudo reboot
 
om_theend
