#echo IN PROGRESS && exit 7
#!/bin/bash
testuser user
source "$HOME/om/scripts/base_script.sh" 

echoinfo "\n## CREATE WIKI ##\n"

racine=$(pwd)
readtxt nomWiki "Le nom du wiki sera"
wiki="${racine}/""${nomWiki}"
readtxt titre "LE TITRE SERA"

spaces=$(echo ${#titre} + 2 | bc )
end=$(echo 70-$spaces-6 | bc)

cadre=$(printf "%0.s#" $(seq 1 6) ; printf "%0.s@" $(seq 1 $spaces) ; printf "%0.s#" $(seq 1 $end))
gdTitre=$(printf "#@@@@@@";printf "$titre";printf "%0.s@" $(seq 1 $end) ; printf "#")

echo "$cadre" | tr "@" " " >> "${wiki}"
echo >> "${wiki}"
echo $gdTitre | tr "@" " " | tr [:lower:] [:upper:] >> "${wiki}"
echo >> "${wiki}"
echo "$cadre" | tr "@" " " >> "${wiki}"
echo "

######################################################################

# TITRE #

######################################################################

# TITRE
#######

# titre

texte

######################################################################

# TITRE #

######################################################################" >> "${wiki}"
vi + -c 'startinsert' "${wiki}"
 
om_theend
