echo IN PROGRESS && exit 7
#!/bin/bash
# option debug
source "$HOME/om/scripts/base_script.sh" 2> /dev/null

om_echotitle "CONVERT SCRIPT TO HTML"

# PRECONFIGURATION
##################


if [ -n "$1" -a -n "$2" ]; then
	sc="$1"
	ht="$2"
else
	echo -e -$red "\n"'$1 et $2 manquant'"\nMerci de renseigner le script à convertir espacé du fichier html de destination"$white
	exit 1
fi

# EXECUTION
###########

# script sed de suppression et remplacement d'élément

sed -re '
# remplacement des < et >
s/>/\&gt;/g ; s/</\&lt;/g
# supprime le shebang et la source design
1d ; /source modele/d
# supprime le test utilisateur
/\# DEBUT TEST/,/\# FIN TEST/d
# suppression des variables de couleur
s/(\$yellow)|(\$red)|(\$whitebig)|(\$white)//g
' $sc > $ht



# remplacement des commentaire et placement des balises

sed -i -e '
s/[ ]*#.*/<p>&<pre>/
s/<p># /<p>/
s#<p>#</pre></p><p>#
' $ht

# repositionnement des balises </pre> et <pre>

sed -i -e '
s#</pre></p>#</pre></p>\n#
s#<pre>#\n<pre>#
' $ht


# POSTCONFIGURATION
###################

echo -e $yellow"ATTENTION LA CONVERTION N'EST PAS ENCORE COMPLETE :
- supprimez la toute première balise </pre></p>
- si du code n'est pas précédé d'un commentaire, il ne sera pas complètement balisé"$white
 
om_theend
