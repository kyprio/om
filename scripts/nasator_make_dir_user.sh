echo IN PROGRESS && exit 7
#!/bin/bash
# DEBUT TEST SUDO
clear
om_echotitle "MAKE DIR"
[ $UID -eq 0 ] && echo -e $red"NE PAS EXECUTER AVEC ROOT\n" && exit 1
echo -e $yellow"mot de passe du super-user $USER :"$white
read -s mdp
sudo -k
( echo $mdp; echo $mdp echo $mdp ) | sudo -S echo -e $whitebig"mot de passe correct"$white 2> /dev/null
if [ ! $? -eq 0 ]; then
	echo -e $red"MOT DE PASSE INCORECT OU $USER N'EST PAS UN SUPER-USER"
	exit 2
fi
# FIN TEST SUDO

om_echoinfo "\n## MAKE DIR ##\n"


# PREFCONFIGURATION
###################


iface=`sudo ifconfig | grep HW | cut -d " " -f 1`
iface=`echo $iface | cut -d " " -f 1`
network=`sipcalc $iface | grep "Network address" | tr -d " " | cut -d "-" -f 2`
mask=`sipcalc $iface | grep "Network mask (bits)" | tr -d " " | cut -d "-" -f 2`

ipldap=`sudo grep "^uri ldap://" /etc/pam_ldap.conf | cut -d "/" -f3`
base=`sudo grep "^base " /etc/pam_ldap.conf | cut -d " " -f2`


users_sys=`getent passwd | grep -P 1[0-9]{3} | cut -d: -f1`
users_ldap=`ldapsearch -x -LLL -b dc=labo,dc=lan cn -H ldap://$ipldap | grep ^cn | cut -d " " -f 2 2> /dev/null`

groups_actuel=`getent group | cut -d: -f1`
users_actuel=`echo -e "$users_sys\n$users_ldap"`
echo $users_actuel


# EXECUTION
###########


om_echoinfo "Liste des dossiers partagés existants :"
ls /srv/stockage/

echo -e $yellow"\nListe des dossiers partagés à créer (séparés par un espace) :\n"$red"pas de caracères spéciaux, majuscules ou d'espaces dans les noms"$whitebig
read liste_dossiers

# chaque dossier correspond à un groupe
# Un dossier n'appartient pas à un ou plusieurs groupe d'utilisateur, il n'appartient qu'à son propre groupe
# si le groupe existe deja pour un dossier, alors il faut relancer la commande et nommer différemment le dossier à créer
for  dossier  in ${liste_dossiers[@]}; do
	for group in ${groups_actuel[@]}; do
		if [ $group == $dossier ]; then
			existant=true
			echo -e $whitebig"$dossier"$red" est un nom réservé !\nannulation pour $dossier"$white
			break
		else
			existant=false
		fi
	done
	# création du dossier, création du groupe, affectation des utilisateurs existants à ce groupe
	if [ $existant == false ]; then
		path="/srv/stockage/"$dossier"/"
		echo -e $yellow"\vCONFIGURATION POUR "$whitebig"$dossier\v"$white
#		echo -e $yellow"Informations concernant ce dossier partagé :"$whitebig
#		read comment

		om_echoinfo "Liste des utilisateurs actuels :"
		echo -e $users_actuel

		om_readtext liste_users ; [ -z "$liste_users" ] && liste_users=`echo $users_actuel` "écrire la liste des utilisateurs autorisés pour ce dossier séparés par un espace (tous par défaut):"

		om_readtext w "Possibilité d'écriture dans le dossier (sinon lecture seule) ? yes/NO:"

		# si les utilisateurs listés peuvent modifier, alors ils peuvent aussi lire
		# si les utilisateurs listés ne peut pas modifier alors ils ne peuvent pas lire
		case $w in
			n|no|NO|"")
				r="no"
				w="no"
				rw="ro"
				;;
			y|yes|YES)
				r="yes"
				w="yes"
				rw="rw"
				;;
		esac

		# création du dossier et des fichiers config
		sudo mkdir $path
		sudo addgroup $dossier
		sudo chown -R root:$dossier $path
		sudo chmod -R 770 $path
		sudo setfacl -m d:g:$dossier:rwx $path
#		cd /srv/config/samba
#		cat add_dossier | sed "s#DOSSIER#$dossier# ;s#CHEMIN#$path# ; s#ryesno#$r# ; s#wyesno#$w# ; s#utilisateurs#$liste_users# ; s#COMMENTAIRE#$comment#" | sudo tee dossiers/$dossier
		echo "/srv/stockage/$dossier $network/$mask($rw,sync,anonuid=1000,anongid=100,no_subtree_check)" | sudo tee -a /etc/exports
	
		for user in ${liste_users[@]}; do
			om_readtext rwx ; [ -z "$rwx" ] && rwx=rwx "droits \"rwx\" à attribuer pour $user (x obligatoire accéder au dossier)(par défaut rwx):"
			sudo adduser $user $dossier
			sudo setfacl -Rm u:$user:$rwx $path
		done

	fi
done
 
om_theend
