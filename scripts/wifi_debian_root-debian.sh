echo IN PROGRESS && exit 7
#!/bin/bash
source "$HOME/om/scripts/base_script.sh" || echo "base_script.sh manquant" && exit 1
testuser root


om_echoinfo "Ce script indique les étapes à suivre pour l'installation et la configuration du wifi sur un Debian vierge avec l'utilisateur root\n\n"

echo -e $yellow"repérez le nom du chipset avec la commande :"
echo -e $whitebig"lspci | grep 'Network controller'\n"
echo -e $yellow"et rechercher ce nom sur la page :"
echo -e $whitebig"https://wiki.debian.org/WiFi#PCI_Devices\n"
echo -e $yellow"ou plus globalement :"
echo -e $whitebig"https://wiki.debian.org/WiFi\n"
echo -e $yellow"puis référez vous à la 'help page' associée\n"
echo -e $yellow"Il faudra généralement ajouter la ligne suivante à la sources.list pour récupérer les paquets non libres:"
echo -e $whitebig"deb http://http.debian.net/debian/ jessie main contrib non-free\n"
echo -e $yellow"ensuite il faut effectuer un update"
echo -e $whitebig"apt-get update\n"
echo -e $yellow"installer le package correspondant aux modules référencés"
echo -e $yellow"et suivre les dernières lignes de commande pour stopper les conflits avec certains modules"
echo -e $yellow"et activer le module précédemment installé\n\n"

echo -e $yellow"par exemple pour les modules Broadcom suivants :"
echo -e $yellow"BCM4311, BCM4312, BCM4313, BCM4321, BCM4322, BCM43224, BCM43225, BCM43227, BCM43228 :"
echo -e $whitebig"echo 'deb http://http.debian.net/debian/ jessie main contrib non-free' >> /etc/apt/sources.list"
echo -e $whitebig"apt-get update"
echo -e $whitebig"apt-get install -q broadcom-sta-dkms"
echo -e $whitebig"modprobe -r b44 b43 b43legacy ssb brcmsmac"
echo -e $whitebig"modprobe wl\n\n"
echo -e $yellow"FIN"
 
 
om_theend
