echo IN PROGRESS && exit 7
#!/bin/bash
source "$HOME/om/scripts/base_script.sh" || (echo "base_script.sh manquant" && exit 1)
om_testuser root



# PRECONFIGURATION
##################

url_mirror=$(whiptail --title "CONFIG DEBIAN" --inputbox "URL complet du miroir Debian\nLaissez vide pour garder celui par défaut :" 20 70 3>&1 1>&2 2>&3)

# INSTALLATION
##############


# reconfiguration des locales avant installation pour résoudre les problèmes lors des installation depuis SSH
locale-gen "fr_FR.UTF-8" "en_US.UFT-8"

# sources.list
[ -n "$url_mirror" ] && sed "s#http://ftp.fr.debian.org/debian/#$url_mirror#" ../config/_etc_apt_sources.list
cp ../config/sources.list/debian_etc_apt_sources.list /etc/apt/sources.list

# mise à jour
apt-get update
apt-get dist-upgrade -y

# installation
apt-get install -y vim openssh-server sudo rsync tcpdump lynx screen sipcalc talk talkd debconf-utils nfs-common whois git-core nmap


# POST CONFIGURATION
####################


# sysadmin en sudoer
adduser sysadmin sudo

# test_utilisateur_existe en ROOT
if [[ ! `getent passwd | grep sysadmin` ]] ; then
	useradd -m sysadmin -s /bin/bash -p `mkpasswd -m sha-512 @sys@`
	usermod -G sysadmin,cdrom,floppy,sudo,audio,dip,video,plugdev,users,netdev sysadmin
else
	adduser sysadmin sudo
fi

# désactive le bip du PC speaker 
echo "blacklist pcspkr" > /etc/modprobe.d/nobeep.conf
rmmod pcspkr

# changement de la résolution par default
sed -i 's/#GRUB_GFXMODE=640x480/GRUB_GFXMODE=1024x768/' /etc/default/grub
sed -i '/GRUB_GFXMODE/aGRUB_GFXPAYLOAD_LINUX=keep' /etc/default/grub
update-grub
 
om_theend
