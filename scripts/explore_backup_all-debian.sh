#!/bin/bash

set -o errexit
set -o nounset
set -o pipefail
#set -o xtrace
#set -v


BK_DIR="${HOME}/backup/${1}"
version_dir="${1}"

if [ ! -d "${BK_DIR}" ] || [ "$(ls "${BK_DIR}" | wc -l )" == 0 ] ; then
  echo "la destination ${BK_DIR} n'existe pas ou est vide"
  exit 1
fi

# initialisation du listing
cd "${BK_DIR}"
echo -e explore
field=2
list=('')
sel=0
continu=true

# Lister chaque niveau de fichiers sélectionnés
while  "${continu}" == "true" ; do
  # mise à jour du listing listing
  list=( $(ls | egrep ^${list[${sel}]} | cut -d _ -f 1-${field} | uniq) )
  long="${#list[@]}"
  menu="whiptail --title 'LISTE FICHIER' --radiolist 'Quel fichier afficher ' $(( long + 5 )) 70 ${long} "
  for (( i=0; i<=$((${#list[@]} - 1)); i++ )); do
    menu="${menu} '${i}' '${list[${i}]}' OFF"
  done
  menu='sel=$('"${menu}"' 3>&1 1>&2 2>&3)'
  eval $menu
  if [ -e "${list[${sel}]}" ] || [ -L "${list[${sel}]}" ]; then
    action=$(whiptail --title "Action" --radiolist "Que faire avec ${list[0]} ? " 20 70 7 \
"restore" "Restaurer ce backup" OFF \
"backup" "Mettre à jour ce backup" OFF \
"compare" "editer et comparer" OFF \
"delete_bk" "supprimer le backup" OFF \
"delete_src" "supprimer le fichier source" OFF \
"delete_all" "supprimer le backup et le fichier source" OFF \
"exit" "Quitter" ON 3>&1 1>&2 2>&3)
   "${HOME}"/om/scripts/manage_backup_all-debian.sh "${version_dir}" "${action}" "${list[${sel}]}"
    continu=false
  else
    continu=true
    ((field++))
  fi
done
