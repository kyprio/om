#!/bin/bash

source "$HOME/om/scripts/base_script.sh" 
om_echotitle "CREATE SCRIPT SUDO"

# PRECONFIGURATION
##################

userprin=`getent passwd | grep -P 1000 | cut -d: -f1`
cd /home/$userprin/om/scripts
racine=`pwd`

if [ -n "$1" ]; then
	nomProg="$1"
else
	echo -en $white"Le nom du script sera"$yellow
	read nomProg
fi
prog="$racine/sudo_$nomProg".sh

# EXECUTION
###########

echo '#!/bin/bash
# option debug
#set -euo pipefail
'"#IFS=$'\n\t'"'
source "$HOME/om/scripts/base_script.sh" 2> /dev/null

# DEBUT TEST SUDO
clear
om_echotitle "'`echo $nomProg | tr 'a-z' 'A-Z' | tr '_' ' '`'"
[ $UID -eq 0 ] && whiptail --title "TEST SUDO" --msgbox "NE PAS EXECUTER AVEC ROOT" 20 70 && exit 1
mdp=$(whiptail --title "TEST SUDO" --passwordbox "Mot de passe du super-user $USER :" 20 70 3>&1 1>&2 2>&3)
sudo -k
( echo $mdp; echo $mdp echo $mdp ) | sudo -S echo -e $whitebig"mot de passe correct"$white 2> /dev/null
if [ ! $? -eq 0 ]; then
	echo -e $red"MOT DE PASSE INCORECT OU $USER N'"'"'EST PAS UN SUPER-USER"
	exit 2
fi
# FIN TEST SUDO

# PRECONFIGURATION
##################

#om_echoerror "OPTION DEBUG set -euo pipefail ACTIVEE"

# INSTALLATION
##############

# POSTCONFIGURATION
###################

' > $prog
chmod +x $prog
vi +27 -c 'startinsert' $prog

 
om_theend
