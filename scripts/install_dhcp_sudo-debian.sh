echo IN PROGRESS && exit 7
#!/bin/bash

source "$HOME/om/scripts/base_script.sh" 2> /dev/null
# DEBUT TEST SUDO
clear
om_echotitle "INSTALL DHCP"
[ $UID -eq 0 ] && echo -e $red"NE PAS EXECUTER AVEC ROOT\n" && exit 1
echo -e $yellow"mot de passe du super-user $USER :"$white
read -s mdp
sudo -k
echo $mdp | sudo -S echo -e $whitebig"mot de passe correct"$white 2> /dev/null
if [ ! $? -eq 0 ]; then
	echo -e $red"MOT DE PASSE INCORECT OU $USER N'EST PAS UN SUPER-USER"
	exit 2
fi
# FIN TEST SUDO

# vérification d'éxécution du script

echo -en $yellow"\nvérification du contenu de /etc/network/interfaces : "$white
[ ! $((`grep "static" /etc/network/interfaces | wc -l`)) -eq 1 ] && echo -e $red"Plusieurs ou aucune IP en static : le script ne peut pas configurer automatiquement le fichier /etc/dhcp/dhcpd.conf"$white && exit 1
echo -e $whitebig"OK"$white

# pre-configuration

iface=`grep "static" /etc/network/interfaces | cut -d" " -f2`
ip=`grep -A 1 "static" /etc/network/interfaces | grep address | cut -d" " -f2`
subnet=`echo $iface| sipcalc - | grep "Network address" | awk -F "- " '{print $2}'`
netmask=`echo $iface | sipcalc - | grep "mask" | grep -vP "bits|hex" | awk -F "- " '{print $2}'`
old_begin=`echo $iface | sipcalc - | grep "Usable range" | awk -F "- " '{print $2}'`
old_end=`echo $iface | sipcalc - | grep "Usable range" | awk -F "- " '{print $3}'`
gw=`grep -A 3 "static" /etc/network/interfaces | grep "gateway" | cut -d" " -f2`
old_dn=`hostname -d` ; [ -z "$old_dn" ] && old_dn="labo.lan"
old_dns1="8.8.8.8"
old_dns2="8.8.4.4"

# PRECONFIGURATION

echo -e $yellow"ip serveur dhcp\t"$whitebig"$ip"$yellow"\nmasque\t"$whitebig"$netmask"$yellow"\nplage\t"$whitebig"$old_begin -> $old_end"$white
[ -n $gw ] && echo -e $yellow"gateway\t"$whitebig"$gw\n"$yellow"DNS1\t"$whitebig"$old_dns1\n"$yellow"DNS2\t"$whitebig"$old_dns2"$white

om_readsecret default "\nAppliquer les valeurs par défaut ? Y/n :"
case $default in
	Y|y|"")
		dn=$old_dn
		dns1=$old_dns1
		dns2=$old_dns2
		begin=$old_begin
		end=$old_end
		;;
	*)
		om_readtext dn "Nom de domaine désiré ($old_dn) :"
		[ -z "$dn" ] && dn=$old_dn
		om_readtext dns1 "IP de serveur DNS primaire ($old_dns1)"
		[ -z "$dns1" ] && dns1=$old_dns1
		om_readtext dns2 "IP serveur DNS secondaire ($old_dns2) :"
		[ -z "$dn2" ] && dns2=$old_dns2
		om_readtext begin "Première adresse desservie, ($old_begin) "
		[ -z "$begin" ] && begin=$old_begin
		om_readtext end "Dernière adresse desservie, ($old_end) "
		[ -z "$end" ] && end=$old_end
		;;
esac

# installation

sudo apt-get install -y isc-dhcp-server

# backup des fichier de configuration

list_conf=("/etc/dhcp/dhcpd.conf /etc/default/isc-dhcp-server")
for file in ${list_conf[@]}; do
	if [ -f "$file\_bak" ]; then
		om_echoinfo "le backup $file\_bak existe déjà"
	else
		sudo mv $file $file\_bak && echo -e $yellow"\nbackup de "$whitebig"$file"$yellow" vers "$whitebig"$file\_bak\n"$white
	fi
done

# modification du fichier /etc/dhcp/dhcp.conf

echo -e "\
log-facility local7;
ddns-update-style none;

subnet $subnet netmask $netmask {
	range $begin  $end;\
" | sudo tee /etc/dhcp/dhcpd.conf

[ -n "$gw" ] && echo "\
	option routers $gw;\
" | sudo tee -a /etc/dhcp/dhcpd.conf

echo "\
	option domain-name \"$dn\";
	option domain-name-servers $dns1, $dns2;
	default-lease-time 600;
	max-lease-time 7200;

	#host nom_machine_a_fixer { hardware ethernet 00:11:22:33:44:55; fixed-address 192.168.50.5;}
	#ici _nouveau_host
}\
" | sudo tee -a /etc/dhcp/dhcpd.conf

# modification du fichier /etc/default/isc-dhcp-server

echo "INTERFACES=$iface" | sudo tee -a /etc/default/isc-dhcp-server

# fin de configuration

sudo service isc-dhcp-server restart
[ ! $? == 0 ] && echo -e $red"ERREUR DE CONFIGURATION\nvoici le fichier /etc/dhcp/dhcpd.conf"$white && cat /etc/dhcp/dhcpd.conf

# liste des fichier conf

echo -e $yellow"Voici la liste des fichiers de configurations utilisés :"$whitebig
for file in ${list_conf[@]}; do echo $file
done; echo -e $white

# liste des fichier log

liste_log=("/var/log/syslog")
echo -e $yellow"Voici la liste des fichiers de log à vérifier :"$whitebig
for file in ${liste_log[@]}; do echo $file
done; echo -e $white
 
om_theend
