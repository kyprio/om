echo IN PROGRESS && exit 7
#!/bin/bash
# option debug
#set -euo pipefail
#IFS=$'\n\t'
source "$HOME/om/scripts/base_script.sh" 2> /dev/null

# DEBUT TEST SUDO
clear
om_echotitle "INSTALL MAIL"
[ $UID -eq 0 ] && whiptail --title "TEST SUDO" --msgbox "NE PAS EXECUTER AVEC ROOT" 20 70 && exit 1
mdp=$(whiptail --title "TEST SUDO" --passwordbox "Mot de passe du super-user $USER :" 20 70 3>&1 1>&2 2>&3)
sudo -k
( echo $mdp; echo $mdp echo $mdp ) | sudo -S echo -e $whitebig"mot de passe correct"$white 2> /dev/null
if [ ! $? -eq 0 ]; then
	echo -e $red"MOT DE PASSE INCORECT OU $USER N'EST PAS UN SUPER-USER"
	exit 2
fi
# FIN TEST SUDO

# PRECONFIGURATION
##################

#om_echoerror "OPTION DEBUG set -euo pipefail ACTIVEE"

# INSTALLATION
##############


sudo apt install -y postfix telnet dovecot-imapd


# POSTCONFIGURATION
###################

# utilisateur et dossier vmail
sudo useradd vmail -r -d /srv/vmail
sudo mkdir -p /srv/vmail 
sudo chown vmail:vmail /srv/vmail -R


# SOURCE
# https://github.com/etalab/etalab-support/blob/master/howtos/how-to-install-mail-server-with-imaps-smtps-caldav-carddav.rst
# https://www.tictech.info/post/mail_preparation
 
om_theend
