#!/bin/bash
IFS=$'\n\t'
source "$HOME/om/scripts/base_script.sh" 2> /dev/null

# DEBUT TEST SUDO
clear
om_echotitle "MOUNT NFS AUTO"
[ $UID -eq 0 ] && echo -e $red"NE PAS EXECUTER AVEC ROOT\n" && exit 1
echo -e $yellow"mot de passe du super-user $USER :"$white
read -s mdp
sudo -k
( echo $mdp; echo $mdp echo $mdp ) | sudo -S echo -e $whitebig"mot de passe correct"$white 2> /dev/null
if [ ! $? -eq 0 ]; then
	echo -e $red"MOT DE PASSE INCORECT OU $USER N'EST PAS UN SUPER-USER"
	exit 2
fi
# FIN TEST SUDO

# PRE CONFIGURATION
###################

whiptail --title "MOUNT NFS AUTO" --msgbox "Liste des partages existant\nauto.master :\n`grep '^/' /etc/auto.master`\nauto.nfs :\n`grep 'fstype=nfs' /etc/auto.nfs`" 20 110
ip=$(whiptail --title "MOUNT NFS AUTO" --inputbox "IP/dns source du partage :" 20 110 3>&1 1>&2 2>&3) || exit 1
dossier_partage=$(whiptail --title "MOUNT NFS AUTO" --inputbox "Chemin du dossier du partage (ex : /srv/stockage/partage) :" 20 110 3>&1 1>&2 2>&3) || exit 1
mnt_parent=$(whiptail --title "MOUNT AUTO NFS" --inputbox "Point de montage parent (ex : /mnt/nfs) :" 20 110 3>&1 1>&2 2>&3) || exit 1
mnt_enfant=$(whiptail --title "MOUNT AUTO NFS" --inputbox "dossier du partage enfant (ex : dossier pour /mnt/nfs/dossier) : " 20 110 3>&1 1>&2 2>&3) || exit 1

# INSTALLATION
###############

sudo apt-get install -y autofs

# POST CONFIGURATION
####################

echo "\
$mnt_parent /etc/auto.nfs --ghost,--timeout=60\
" | sudo tee -a /etc/auto.master

uniq=`sudo uniq /etc/auto.master` ; echo "$uniq" | sudo tee /etc/auto.master

echo "\
$mnt_enfant -fstype=nfs,rw,rsize=8192,wsize=8192 $ip:$dossier_partage\
" | sudo tee -a /etc/auto.nfs

sudo service autofs restart

whiptail --title "MOUNT NFS AUTO" --msgbox "Vous pouvez accéder à votre nouveau partage :\n$mnt_parent/$mnt_enfant" 20 110
 
om_theend
