#!/bin/bash

# HEADER
##################################################

set -o errexit
set -o nounset
set -o pipefail
#set -o xtrace

source "$HOME/om/scripts/base_script.sh"  2> /dev/null || (echo "base_script.sh manquant" ; exit 1)

readonly scriptName="$(basename ${0})"
readonly scriptVersion="0.1" # 04/09/17 by kyprio for the project om

function usage {
  local returnCode="${1:-0}"
 
  om_echoinfo "${scriptName}\tgenerate an output file containing usage/getopt/descriptions and test validations of the listed variable in the specified file"

  om_echoreturn "${scriptName}
  -x $Y # run the script with interactive mode if necessary $B
  [-v] $Y # version $B
  [-h] $Y # help $B"
  om_echoreturn "${scriptName}
  [-s source] $Y # file containing the list of variablesand their attribut/value info (input_listvar) $B
  [-d destination] $Y # file destination (may be the script itself) (output_listvar) $B"
  om_echoinfo 'The input file mut respect the format '$B'
mandatory:var1:1:tips1:description1:
optional:var2:2:tips2:description2:default_value
internal:var3:::description3:"$(command_value)"'
  om_echoinfo 'The output file will be like this '$B'
# mandatory:var1:-1:tips1:description1
-1 tips1 $Y # description1 $B
var1='"''"'	# mandatory : description1
1:
1) var1=$"{OPTARG}" ;;
while [[ -z "${var1}" ]]; do
  om_readtext var1 "description1"
done
om_debugvar var1

# optional:var2:-2:tips2:description2:default_value
-2 tips2 $Y # description2 (default_value) $B
var2='"''"'	# optional : description2
2:
2) var2=$"{OPTARG}" ;;
var2="${var2:-default_value}"
om_debugvar var2

# internal:var3:description3
var3='"''"'	# internal : description3
var3="$(command value)"
om_debugvar var3
'

  exit "${returnCode}"
}

om_header -t "BASHSCRIPT GENERATE VARIABLES" -v "${scriptVersion}" -u user

[[ "${#}" -eq 0 ]] &&  (om_echoerror "At least, please run the script with the -x parameter" ; usage "4")

#om_inprogress

# PRECONFIGURATION
##################################################

om_echosubtitle "PRECONFIGURATION"

sourceF=''	# optional : file containing the list of variablesand their attribut/value info
destF=''	# optional : file destination (may be the script itself)
destF=''	# internal : destination file
while getopts ":s:d:xhv" opt; do
  case "${opt}" in
    x) ;;
    h) usage ;;
    v) om_version "${scriptVersion}" ;;
    \?) om_echoerror "Invalid option: -${OPTARG}" ; exit 4 ;;
    :) om_echoerror "missing value to -${OPTARG}" ; exit 4 ;;
    s) sourceF="${OPTARG}" ;;
    d) destF="${OPTARG}" ;;
  esac
done

sourceF="${sourceF:-$HOME/om/scripts/input_listvar}"
destF="${destF:-$HOME/om/scripts/output_listvar}"

om_debugvar sourceF destF

# EXECUTION
##################################################

om_step "EXECUTION"
oIFS="echo -e $IFS"
IFS=''
while read "line" ; do
  while IFS=":" read type var arg tips desc val; do
    [[ "${line}" =~ "#${type}" ]] && \
    echo "$line" | \
    sed "s#VAR#${var}#g ; s#X#${arg}#g ; s#TIPS#${tips}#g ; s#DESCRIPTION#${desc}#g ; s#VALUE#${val}#g ; s#\#${type}##g " >> ${destF}
  done < "$HOME/om/scripts/input_listvar"
done < "$HOME/om/lang/bash/templates/bashscript_variable.sh"

# POSTCONFIGURATION
##################################################

om_echosubtitle "POSTCONFIGURATION"

om_theend
