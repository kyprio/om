echo IN PROGRESS && exit 7
#!/bin/bash

source "$HOME/om/scripts/base_script.sh" 2> /dev/null
# DEBUT TEST SUDO
#clear
om_echotitle "LINUX DEPLOY"
[ $UID -eq 0 ] && echo -e $red"NE PAS EXECUTER AVEC ROOT\n" && exit 1
echo -e $yellow"test de mot de passe du super-user $USER :"$white
sudo -k
read -s mdp
( echo $mdp ; echo $mdp ; echo $mdp ) | sudo -S echo -e $whitebig"mot de passe correct"$white 2> /dev/null
[ ! "$?" == "0" ] && echo -e $red"incorrect"$white && exit 1
# FIN TEST SUDO


om_echoinfo "LINUX DEPLOY DEBIAN"

# PRECONFIGURATION
liste_dest="/etc/skel /root /home/$USER"

# deploiement des fichiers preferences
cd /home/$USER/om/preferences

for file in * ; do
	for dest in ${liste_dest[@]}; do
		# initiation des variables
		file_dest="$dest"`echo $file | sed "s/_/\//g"`
		file_dest_bak="$file_dest""_bak"
		
		# test backup et envoi du fichier
		echo -e $yellow"$file_dest :"
		sudo mkdir -p `dirname $file_dest`
		if [ -f "$file_dest_bak" ]; then
			echo -e $whitebig"\tbackup existant"
		else
			sudo cp $file_dest $file_dest_bak 2> /dev/null && echo -e $whitebig"\tbackup OK" || echo -e $whitebig"\trien à sauvegarder"
		fi
		sudo cp $file $file_dest 2>/dev/null && echo -e $whitebig"\ttransfert OK"$white || echo -e $whitebig"\trien à remplacer"$white
		
		# affectation des droits
		case $dest in
			"/etc/skel"|"/root")
			user=root
			;;
			"/home/$USER")
			user=$USER
			;;
		esac
		sudo chown $user:$user $file_dest
	done
done
#source /home/$USER/om/scripts/user_linux_backup.sh

echo -e $whitebig"\vcd ~ ; source .bashrc .inputrc"$yellow" ou "$whitebig" sourcelinux"$white
 
om_theend
