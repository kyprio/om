echo IN PROGRESS && exit 7
#!/bin/bash
source "$HOME/om/scripts/base_script.sh" || ( echo "base_script.sh manquant" && exit 1 )
om_testuser user
om_echotitle "CREATION CLASSE JAVA 1 to Many"

# PRECONFIGURATION
##################
path1=`pwd`
cd ../..
path2=`pwd`
pkgName=$(echo "${path1#"${path2}"}")

om_readtext className "NomClasse"

templateHeader="""
package ${pkgName};
import java.util.ArrayList;

public class ${className}{
"""

templateBody="""
    private int id;
    private String prenom;
    private String nom;
    private String dateNaissance;
    private String adresse;
    
    public ${className}(int id, String prenom, String nom, String adresse, String dateNaissance){
        this.prenom = prenom;
        this.nom = nom;
        this.id = id;
        this.adresse = adresse;
        this.dateNaissance = dateNaissance;
    }

ArrayList<Contrat> contrats = new ArrayList<Contrat>();

	public ArrayList<Contrat> getContrat() {
	   return contrats;
	}
	
	public void setContrat(ArrayList<Contrat> contrats) {
		this.contrats = contrats;
}
    public int getId() {
        return id;
    }

    public String getPrenom() {
        return prenom;
    }

    public String getNom() {
        return nom;
    }

    public String getAdresse() {
        return adresse;
    }

public String getDateNaissance() {
        return dateNaissance;
    }


    public void setId(int id) {
        this.id = id;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public void setnom(String nom) {
        this.nom = nom;
    }

    public void setAdresse(String adresse) {
        this.adresse = adresse;
    }

    public void setDateNaissance(String dateNaissance) {
        this.dateNaissance = dateNaissance;
    }
    
    
}
"""

# EXECUTION
###########

# POSTCONFIGURATION
###################
