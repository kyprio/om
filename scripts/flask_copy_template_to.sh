#!/bin/bash

destination="$1"
while [[ -z "$destination" ]]; do read -p "nom relatif du fichier de destination : " destination ; done

currentpwd=$PWD
tplfilename='page_template.html.tpl'
tplfile=$(pwd | sed 's/\(.*\/templates\/\).*/\1/')${tplfilename}


echo "$tplfile -> ${currentpwd}/${destination}"
cp $tplfile ${currentpwd}/${destination}
