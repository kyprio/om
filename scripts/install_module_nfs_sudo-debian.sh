echo IN PROGRESS && exit 7
#!/bin/bash

# PRE CONFIGURATION
###################

list_conf=('/etc/exports'  '/etc/hosts.allow' '/etc/hosts.deny' '/etc/fstab' '/etc/default/nfs-kernel-server' '/etc/default/nfs-common')
port=2050

# INSTALLATION
##############

sudo apt-get install -y nfs-kernel-server

# POST CONFIGURATION
####################

# fixage du port rpcmountd dans /etc/default/nfs-kernel-server
sudo sed -i "s/RPCMOUNTDOPTS=\"--manage-gids\"/RPCMOUNTDOPTS=\"--manage-gids --port $port\"/" /etc/default/nfs-kernel-server

# backup
sudo mkdir -p /srv/backup/nfs
for file in ${list_conf[@]}; do sudo cp -p $file /srv/backup/nfs/ ; done

# liste des fichier conf
om_echoinfo "Voici la liste des fichiers de CONFIGURATIONs utilisés :"
for file in ${list_conf[@]}; do echo $file
done; echo -e $white

 
om_theend
