echo IN PROGRESS && exit 7
#!/bin/bash
source "$HOME/om/scripts/base_script.sh" || echo "base_script.sh manquant" && exit 1
om_testuser root
om_inprogress

# PRE CONFIGURATION
###################

list_conf=('/etc/exports'  '/etc/hosts.allow' '/etc/hosts.deny' '/etc/sysconfig/nfs' '/etc/modprobe.d/lockd.conf')
port_nfs=2049 # RFC
port_rpcbind=111 # RFC
port_mountd=20048
port_statd=662
port_lockd=2050

# INSTALLATION
##############

yum install -y nfs-utils nfs4-acl-tools

# POST CONFIGURATION
####################

# fixage des ports pour le NFSv3 et v4
sed -i "/^#MOUNTD_PORT=/cMOUNTD_PORT=${port_mountd}" /etc/sysconfig/nfs
sed -i "/^#STATD_PORT=/cSTATD_PORT=${port_statd}" /etc/sysconfig/nfs 
sed -i "/^#options lockd nlm_tcpport/coptions lockd nlm_tcpport=${port_lockd}" /etc/modprobe.d/lockd.conf
sed -i "/^#options lockd nlm_udpport/coptions lockd nlm_udpport=${port_lockd}" /etc/modprobe.d/lockd.conf

# ajout des services non connus et des regles au parefeu
echo """
<?xml version=\"1.0\" encoding=\"utf-8\"?>
<service>
 <short>nfs-lockd</short>
 <description>NFS LOCKD</description>
 <port protocol=\"tcp\" port=\"${port_lockd}\"/>
 <port protocol=\"udp\" port=\"${port_lockd}\"/>
</service>
""" > /etc/firewalld/services/nfs-lockd
echo """
<?xml version=\"1.0\" encoding=\"utf-8\"?>
<service>
 <short>nfs-statd</short>
 <description>NFS STATD</description>
 <port protocol=\"tcp\" port=\"${port_statd}\"/>
 <port protocol=\"udp\" port=\"${port_statd}\"/>
</service>
""" > /etc/firewalld/services/nfs-statd
firewall-cmd --permanent --zone=public --add-service=nfs
firewall-cmd --permanent --zone=public --add-service=rpc-bind
firewall-cmd --permanent --zone=public --add-service=mountd
firewall-cmd --permanent --zone=public --add-service=nfs-lockd
firewall-cmd --permanent --zone=public --add-service=nfs-statd
firewall-cmd --reload

om_readcontinu

# backup
om_backupexternesudo "/srv/backup/nfs" "${list_conf[@]}"

# liste des fichier conf
om_echolistconf ${list_conf[@]}
 
om_theend
