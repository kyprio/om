#echo IN PROGRESS && exit 7
#!/bin/bash -ve

source "$HOME/om/scripts/base_script.sh" 2> /dev/null
# DEBUT TEST SUDO
clear
om_echotitle "MOUNT NFS TEMPORAIRE"
[ $UID -eq 0 ] && echo -e $red"NE PAS EXECUTER AVEC ROOT\n" && exit 1
echo -e $yellow"mot de passe du super-user $USER :"$white
read -s mdp
sudo -k
echo $mdp | sudo -S echo -e $whitebig"mot de passe correct"$white 2> /dev/null
if [ ! $? -eq 0 ]; then
	echo -e $red"MOT DE PASSE INCORECT OU $USER N'EST PAS UN SUPER-USER"
	exit 2
fi
# FIN TEST SUDO

# vérification des outils néccessaires

om_echoinfo "\nCheck de la configuration minimale...\n"
if [ -n "`sudo dpkg -l | grep nfs-common`" ]; then
	echo -e $whitebig"NFS est bien supporté"$white
else
	echo -e $whitebig"nfs-common non installé sur le client"$white
	requis=false
fi
[ "$requis" == "false" ] && exit 1

# PRECONFIGURATION et liste

IFS=$'\n\t'

liste_ip=(192.168.1.16)
liste_ptmontage=(/media/distant/ ~/distant/ /mnt/)

# choix de la configuration

#choix_ip
om_echoinfo "choix d'une IP ou "A" autre"
for (( i=0; i<=$((${#liste_ip[@]} - 1)); i++ )); do echo "$i = ${liste_ip[$i]}";done
read -sn 1 choix_ip
case $choix_ip in
	A)
		echo -ne $yellow"IP = "$whitebig
		read ip
		;;
	"")
		ip=${liste_ip[0]}
		;;
	*)
		ip=${liste_ip[$choix_ip]}
		;;
esac

#choix_source
om_echoinfo "choix d'un dossier source"
liste_source=( $(sudo showmount -e --no-headers $ip | cut -d " " -f 1) )
for (( i=0; i<=$((${#liste_source[@]} - 1)); i++ )); do echo "$i = ${liste_source[$i]}";done
read -sn 1 choix_source
case $choix_source in
	"")
		source=${liste_source[0]}
		;;
	*)
		source=${liste_source[$choix_source]}
		;;
esac
dossier=`basename $source`
echo $dossier

#choix_ptmontage
om_echoinfo "choix d'un point de montage ou "A" autre"
for (( i=0; i<=$((${#liste_ptmontage[@]} - 1)); i++ )); do echo "$i = ${liste_ptmontage[$i]}$dossier";done
read -sn 1 choix_ptmontage
case $choix_ptmontage in
	A)
		echo -ne $yellow"chemin absolu du point de montage = "$whitebig
		read ptmontage
		;;
	"")
		ptmontage=${liste_ptmontage[0]}$dossier
		;;
	*)
		ptmontage=${liste_ptmontage[$choix_source]}$dossier
		;;
esac

# création du point de montage pour l'utilisateur en cours et montage du dossier distant

sudo mkdir -p $ptmontage
sudo chown $USER:$USER $ptmontage
sudo chmod 555 $ptmontage
sudo mount -t nfs $ip:$source $ptmontage
[ -n "`sudo dpkg -l | grep nautilus`" ] && nautilus $ptmontage || cd $ptmontage

# information

echo -e $yellow"executer ce fichier pout démonter et supprimer le point de montage :"

nomProg=`echo "umount_"${ip}_${ptmontage}" | tr '.' '-' | tr '/' '_'`
prog="$HOME/$nomProg"".sh"
echo -e $whitebig"$prog"$white
echo '#!/bin/bash' > $prog
echo -e 'echo -e $yellow"\\n## '`echo $nomProg | tr 'a-z' 'A-Z' | tr '_' ' '`' ##\\n"$white'"\n\n" >> $prog
echo "sudo umount $ptmontage" >> $prog
echo "sudo rm -r $ptmontage" >> $prog
echo "sudo rm $prog" >> $prog
chmod +x $prog

 
om_theend
