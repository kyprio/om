echo IN PROGRESS && exit 7
#!/bin/bash
source "$HOME/om/scripts/base_script.sh" 2> /dev/null

om_echotitle "OM WO BACKUP"

## PRECONFIGURATION
###################

# crontab à faire au moins toutes les 4h au plus toutes les 1h
# une seule sauvegarde est conservé

# backup à intervalle d'une heure
ombak=.om_bak_`date "+%d-%m_%H-%M"`
wobak=.wo_bak_`date "+%d-%m_%H-%M"`
ombigbak=.ombig_bak_`date "+%d-%m_%H-%M"`

# EXECUTION
###########

cd ~

test -d om && rsync -a --exclude=.* om/ ${ombak} && tar -zcf "${ombak}.tar.gz" ${ombak} && rm -r `ls -d .om_bak* | grep -v ${ombak}.tar.gz` && echo -e $whitebig"${ombak}"$white || om_echoerror "$ombak"
test -d wo && rsync -a --exclude=.* wo/ ${wobak} && tar -zcf "${wobak}.tar.gz" ${wobak} && rm -r `ls -d .wo_bak* | grep -v ${wobak}.tar.gz` && echo -e $whitebig"${wobak}"$white || om_echoerror "$wobak"
test -d ombig && rsync -a --exclude=.* ombig/ ${ombigbak} && tar -zcf "${ombigbak}.tar.gz" ${ombigbak} && rm -r `ls -d .ombig_bak* | grep -v ${ombigbak}.tar.gz` && echo -e $whitebig"${ombigbak}"$white || om_echoerror "$ombigbak"
 
om_theend
