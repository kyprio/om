echo IN PROGRESS && exit 7
#!/bin/bash

source "$HOME/om/scripts/base_script.sh" 2> /dev/null
# DEBUT TEST SUDO
clear
om_echotitle "INSTALL PXESERVER MODIF DHCP"
[ $UID -eq 0 ] && echo -e $red"NE PAS EXECUTER AVEC ROOT\n" && exit 1
echo -e $yellow"mot de passe du super-user $USER :"$white
read -s mdp
sudo -k
echo $mdp | sudo -S echo -e $whitebig"mot de passe correct"$white 2> /dev/null
if [ ! $? -eq 0 ]; then
	echo -e $red"MOT DE PASSE INCORECT OU $USER N'EST PAS UN SUPER-USER"
	exit 2
fi
# FIN TEST SUDO

# PRECONFIGURATION

localhost=`hostname -I | cut -d " " -f 1`

om_readtext ip "IP du serveur tftp/pxe ($localhost) :"
[ -z "$ip" ] && ip=$localhost

# modification du serveur DHCP isc-dhcp-server
sudo sed -i "/^subnet/a\\\tnext-server $ip;\n\tfilename \"pxelinux.0\";" /etc/dhcp/dhcpd.conf
sudo service isc-dhcp-server restart
[ ! $? == 0 ] && echo -e $red"ERREUR DE CONFIGURATION\nvoici le fichier /etc/dhcp/dhcpd.conf"$white && cat /etc/dhcp/dhcpd.conf

# liste des fichier log

liste_log=("/var/log/syslog /var/log/daemon.log")
om_echoinfo "Voici la liste des fichiers de log à vérifier :"
for file in ${liste_log[@]}; do echo $file
done; echo -e $white

om_echoinfo "FIN"
 
om_theend
