echo IN PROGRESS && exit 7
#!/bin/bash
# DEBUT TEST SUDO
clear
om_echotitle "RM DIR"
[ $UID -eq 0 ] && echo -e $red"NE PAS EXECUTER AVEC ROOT\n" && exit 1
echo -e $yellow"mot de passe du super-user $USER :"$white
read -s mdp
sudo -k
( echo $mdp; echo $mdp echo $mdp ) | sudo -S echo -e $whitebig"mot de passe correct"$white 2> /dev/null
if [ ! $? -eq 0 ]; then
	echo -e $red"MOT DE PASSE INCORECT OU $USER N'EST PAS UN SUPER-USER"
	exit 2
fi
# FIN TEST SUDO

om_echoinfo "\n## RM DIR ##\n"

echo -e $yellow"Liste des dossiers partagés existants :"$whitebig
ls /srv/stockage

om_readtext liste_dossiers "Liste des dossiers partagés à supprimer ?"

for dossier in ${liste_dossiers[@]}; do
	echo DOSSIER$dossier
	if [ $(sudo ls /srv/stockage/$dossier | wc -l) -gt 0 ]; then
		echo -e $whitebig"$dossier "$yellow" n'est pas vide. Confirmez la suppression yes/no"$whitebig
		read suppr
		case $suppr in
			y|yes|YES)
				sudo rm -r /srv/stockage/$dossier
				sudo sed -i "/$dossier/d" /etc/exports
				#sudo rm /srv/config/samba/dossiers/$dossier
				;;
			n|no|NO|*)
				om_echoinfo "Annulation de suppression pour $dossier"
				;;
		esac
	else
		sudo rm -vr /srv/stockage/$dossier
		#sudo rm -v /srv/config/samba/dossiers/$dossier
		sudo delgroup $dossier
	fi
done
 
om_theend
