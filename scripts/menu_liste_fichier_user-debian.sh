#!/bin/bash
# option debug
source "$HOME/om/scripts/base_script.sh" 2> /dev/null

om_echotitle "MENU LISTE FICHIER"

# PRECONFIGURATION
##################

dossier="/home/${USER}/om/config/"

# EXECUTION
###########

# création de la liste des fichiers
cd $dossier
liste=( $(ls $dossier) )
long=`echo ${#liste[@]}`


# forme de la commande à utiliser
# whiptail --title "radiolist"    --radiolist "Choix unique"    20 70 3    "tag1" "Description 1" ON    "tag2" "Description 2" OFF
# la commande doit être elle même placée dans une variable pour récupérer la valeur sélectionner
# variable=$( commande whiptail    3>&1 1>&2 2>&3 )
# le tout doit être placé aussi dans une variable pour permettre la création dynamique du contenu de la commande

# affectation des premiers termes de la commande menu dans une variable
menu="whiptail --title 'LISTE FICHIER' --radiolist 'Quel fichier afficher ' $(( $long + 5 )) 70 $long "

# parcours de la liste pour crée les tag de whiptails
# sous la forme "tag" "description" OFF
for i in ${liste[@]};  do
	echo $i
	menu="$menu  '$i' '' OFF"
done

menu='choix=$('"$menu"' 3>&1 1>&2 2>&3)'



eval $menu
clear
less $choix



 
om_theend
