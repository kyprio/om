echo IN PROGRESS && exit 7
#!/bin/bash
# option debug
#set -euo pipefail
#IFS=$'\n\t'
source "$HOME/om/scripts/base_script.sh" 2> /dev/null

om_echotitle "CONVERT HTML TO MARKDOWN"

# PRECONFIGURATION
##################

html=${1}
md=`echo ${html} | sed 's/\.html$/.md/'`

CR=$(printf '\r')

# EXECUTION
###########


cp ${html} ${md}
sed -i -e '
# STRUCTURE & META

/<!DOCTYPE html>/d ;
/<.*html>/d ; 
s/<.*head>/---/ ; 
/meta charset/d ; 
/link rel=/d ; 
/<.*header>/d ;
/<.*section>/d ;
/<.*footer>/d ;
/<.*article>/d ;
/<.*body>/d ;

# PARAGRAPHES

s/<.*p>// ;
s/<.*br>// ;

# TITRES

s/<h1>/#/ ;
s/<\/h1>// ;
s/<h2>/##/ ;
s/<\/h2>// ;
s/<h3>/###/ ;
s/<\/h3>// ;
s/<h4>/####/ ;
s/<\/h4>// ;
s/<h5>/#####/ ;
s/<\/h5>// ;
s/<h6>/######/ ;
s/<\/h6>// ;

# STYLE

s/<strong>/**/ ;
s/<\/strong>/**/ ;
s/<em>/_/ ;
s/<\/em>/_/ ;
s/<.*ul>// ;
s/<li>/+ / ;
s/<\/li>// ;
' ${md}

# LIEN

sed -i 's/<a href="\(.*\)">\(.*\)<\/a>/[\2](\1)/' $md



# POSTCONFIGURATION
###################


 
om_theend
