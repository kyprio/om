#!/bin/bash

if [[ -n "$1" ]]; then
  project="$1"
else
  read -p "projet : " project
fi


mkdir ${project} ; cd ${project}
cat << "EOF" > main.py
#!/usr/bin/python3 -B
# -*-coding:utf8 -*

EOF
chmod +x main.py
vi + main.py
