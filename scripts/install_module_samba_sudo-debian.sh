echo IN PROGRESS && exit 7
#!/bin/bash

# PRE CONFIGURATION
###################

# INSTALLATION
##############

sudo apt-get install -y samba

# POST CONFIGURATION
####################

# création dossier
sudo mkdir /srv/config/samba/dossiers

# creation des fichier config de base de samba
sed "/^#/d ; /^;/d ; /^$/d" /etc/samba/smb.conf | sed -n "/\[global\]/,/\[printers\]/p" | grep -v "\[printers\]" > /srv/config/samba/base_smb.conf
sed -i "1i######\n# Ce fichier est généré à partir du fichier /srv/config/samba/base_smb.conf\n# et des fichiers /srv/config/samba/dossiers/*.conf" /srv/config/samba/base_smb.conf
echo """
[DOSSIER]
   path=CHEMIN
   read only = ryesno
   writeable = wyesno
   valid users = utilisateurs
   comment = COMMENTAIRE
""" > /srv/config/samba/add_dossier

# activation des comptes utilisateurs de la machine à samba
(echo $mdproot ; echo $mdproot ) | sudo smbpasswd -sa root
(echo $mdp ; echo $mdp ) | sudo smbpasswd -sa $USER

# backup
sudo cp -rp /etc/samba /srv/backup/
 
om_theend
