#!/bin/bash

echo "WARNING : Cette version ne gère pas la copie de lien symbolique"

# PRECONFIGURATION

if [[ ! -n "$3" ]] ; then
  echo "$@ : Manque d'arguments à la commande"
  exit 1
fi

version_dir="${1}"
BK_DIR="${HOME}/backup/${version_dir}/"
if [[ ! -e $BK_DIR ]]; then
  echo "la destination ${BK_DIR} n'existe pas...création."
  mkdir -p ${BK_DIR}
fi
action="${2}"
shift 2


# FONCTION

# define FilePath
function defineFilePath {
  if [[ "$(pwd)/" =~ "${BK_DIR}"* ]] ; then
    # in backup dir
    bk_file="${BK_DIR}${1}"
    ori_file="$(echo ${1} | tr '_' '/' | sed 's#--#_#g')"
  
  else
    # out backup dir
    ori_file="$(realpath ${1})"
    bk_file="${BK_DIR}$(echo ${ori_file} | sed 's#_#--#g' | tr '/' '_')"
  fi
}

# copie
function copy {
  src=$1
  dest=$2
  if [ -e "${src}" ] || [ -L "${src}" ]; then
    cp -Pv ${src} ${dest}
  else
    echo "${src} doesn't exist"
  fi
}

# EXECUTION

for file in "${@}"; do
  defineFilePath $file
  case "${action}" in 
    'backup') 
      copy ${ori_file} ${bk_file} ;;
    'restore')
      copy ${bk_file} ${ori_file} ;;
    'compare')
      vimdiff ${bk_file} ${ori_file} ;;
    'delete_bk')
      rm -v ${bk_file};;
    'delete_src')
      rm -v ${ori_file};;
    'delete_all')
      rm -v ${bk_file} ${ori_file};;
    'exit') exit ;;
    *) exit 1;;
  esac
done
exit
