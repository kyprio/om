echo IN PROGRESS && exit 7
#!/bin/bash

source "$HOME/om/scripts/base_script.sh" 2> /dev/null
# DEBUT TEST SUDO
clear
om_echotitle "CONFIG UBUNTU SERVER"
[ $UID -eq 0 ] && echo -e $red"NE PAS EXECUTER AVEC ROOT\n" && exit 1
echo -e $yellow"mot de passe du super-user $USER :"$white
read -s mdp
sudo -k
echo $mdp | sudo -S echo -e $whitebig"mot de passe correct"$white 2> /dev/null
if [ ! $? -eq 0 ]; then
	echo -e $red"MOT DE PASSE INCORECT OU $USER N'EST PAS UN SUPER-USER"
	exit 2
fi
# FIN TEST SUDO

# INSTALLATIONdes paquets habituels
sudo apt-get update
sudo apt-get dist-upgrade -y
sudo apt-get install -y tcpdump lynx vim openssh-server screen debconf-utils nfs-common sipcalc whois nmap
 
om_theend
