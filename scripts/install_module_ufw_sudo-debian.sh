echo IN PROGRESS && exit 7
#!/bin/bash

# PRE CONFIGURATION
###################

# INSTALLATION
##############

sudo apt-get install -y ufw

# POST CONFIGURATION
####################

# modification des profiles d'application pour ajouter le port non standard 2050 à NFS
sudo sed -i "s#ports=2049,111/tcp|2049,111/udp#ports=2049,2050,111/tcp|2049,2050,111/udp#" /etc/ufw/applications.d/ufw-fileserver


# PRECONFIGURATION de ufw pour les services installés
sudo ufw allow WWW
sudo ufw allow "WWW Secure"
sudo ufw allow SSH
[ -n "`sudo dpkg -l | grep nfs-kernel-server`" ] && sudo ufw allow NFS
[ -n "`sudo dpkg -l | grep samba`" ] && sudo ufw allow CIFS
[ -n "`sudo dpkg -l | grep webmin`" ] && sudo ufw allow WEBMIN
sudo ufw --force enable
 
om_theend
