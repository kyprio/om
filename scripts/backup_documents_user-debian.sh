echo IN PROGRESS && exit 7
#!/bin/bash
source "$HOME/om/scripts/base_script.sh" 2> /dev/null

om_echotitle "DOC BACKUP"

# PRECONFIGURATION
path=~/Documents/
liste_dossiers=("Articles")

# execution du backup des documents
cd $path

for dossier in ${liste_dossiers[@]};  do
	dossiertar="."$dossier"_bak.tar.gz"
	dossierh=$dossiertar"_hourly_"
	dossierd=$dossiertar"_daily_"
	tar -hzcf $dossiertar $dossier
	# HOURLY
	h_old=`ls -A | grep "$dossierh*"`
	h_now=$dossierh`date +%D%H | tr "/" "-"`
	if [ "$h_old" != "$h_now" ]; then
		om_echoinfo "Mise à jour du bigbackup horaire"
		cp $dossiertar $h_now
		rm -rf $h_old
	else
		om_echoinfo "bigbackup horaire récent"
	fi

	# DAILY
	d_old=`ls -A | grep "$dossierd*"`
	d_now=$dossierd`date +%D | tr "/" "-"`
	if [ "$d_old" != "$d_now" ]; then
		om_echoinfo "Mise à jour du bigbackup journalier"
		cp $dossiertar $d_now
		rm -rf $d_old
	else
		om_echoinfo "bigbackup journalier récent"
	fi

done

# linuxpro
cp ~/.linuxpro.tar.gz ~/Documents/Articles/content/linux.tar.gz
 
om_theend
