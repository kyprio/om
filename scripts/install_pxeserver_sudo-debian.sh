echo IN PROGRESS && exit 7
#!/bin/bash
source "$HOME/om/scripts/base_script.sh" 2> /dev/null

# DEBUT TEST SUDO
clear
om_echotitle "INSTALL PXESERVER"
[ $UID -eq 0 ] && echo -e $red"NE PAS EXECUTER AVEC ROOT\n" && exit 1
echo -e $yellow"mot de passe du super-user $USER :"$white
read -s mdp
sudo -k
echo $mdp | sudo -S echo -e $whitebig"mot de passe correct"$white 2> /dev/null
if [ ! $? -eq 0 ]; then
	echo -e $red"MOT DE PASSE INCORECT OU $USER N'EST PAS UN SUPER-USER"
	exit 2
fi
# FIN TEST SUDO

# alerte : avoir un serveur DHCP en place sur la machine ou une autre

om_echoinfo "PREREQUIS : AVOIR UN SERVEUR DHCP EN PLACE\n"
read -p "entrée pour continuer / CTRL-C pour fermer" continu

# PRECONFIGURATION
##################


ip=`hostname -I | cut -d " " -f 1`

# INSTALLATION
##############

#installation du serveur tftp et du client tftp pour le test en interne

sudo apt-get install -y tftpd-hpa tftp-hpa

# backup des fichier de configuration

list_conf=("/etc/default/tftpd-hpa")
for file in ${list_conf[@]}; do
	if [ -f "$file\_bak" ]; then
		om_echoinfo "le backup $file\_bak existe déjà"
	else
		sudo mv $file $file\_bak && echo -e $yellow"\nbackup de "$whitebig"$file"$yellow" vers "$whitebig"$file\_bak\n"$white
	fi
done

# modification du fichier /etc/default/tftpd-hpa

echo "\
TFTP_USERNAME=\"tftp\"
TFTP_DIRECTORY=\"/srv/tftp\"
TFTP_ADDRESS=\"0.0.0.0:69\"
TFTP_OPTIONS=\"--secure\"
RUN_DAEMON="yes"\
" | sudo tee /etc/default/tftpd-hpa

# fin de la configuration

sudo service tftpd-hpa restart

# liste des fichier conf

om_echoinfo "Voici la liste des fichiers de configurations utilisés :"
for file in ${list_conf[@]}; do echo $file
done; echo -e $white

# liste des fichier log

liste_log=("/var/log/syslog")
om_echoinfo "Voici la liste des fichiers de log à vérifier :"
for file in ${liste_log[@]}; do echo $file
done; echo -e $white

# INSTALLATIONd'un serveur web minimal


[ ! -n "`sudo dpkg -l | grep lighttpd`" ] && source sudo_install_web_http_only.sh
sudo sed -i '/^server.port/adir-listing.activate        = "enable"' /etc/lighttpd/lighttpd.conf
sudo rm /srv/web/index.php

# récupération de l'installation Debian Netboot

cd /srv/tftp/
sudo wget http://ftp.nl.debian.org/debian/dists/jessie/main/installer-amd64/current/images/netboot/netboot.tar.gz
sudo tar -zxf netboot.tar.gz
sudo rm netboot.tar.gz

# copie des fichiers de config menant à un preseed standard

cd /home/$USER/om/config
sudo cp preseed/preseed-auto /srv/web/preseed-auto.txt
sudo chown www-data:www-data /srv/web/preseed-auto.txt
echo "\
######################################
# Installation automatisee de Debian #
######################################

rescue64 = (par default) installation manuelle
auto = Installation automatique de Debian 8 x64

" | sudo tee /srv/tftp/boot.msg

echo "\
prompt 1
timeout 300
display boot.msg
default rescue64

label rescue64
    kernel debian-installer/amd64/linux
    append priority=low rescue/enable=true vga=788 initrd=debian-installer/amd64/initrd.gz -- quiet

label auto
    kernel debian-installer/amd64/linux
    append auto=true vga=788 priority=critical  preseed/url=http://$ip/preseed-auto.txt initrd=debian-installer/amd64/initrd.gz -- quiet
" | sudo tee /srv/tftp/pxelinux.cfg/default

# changement de propriétaire de /srv/tftp pour modification par $USER

sudo chown -R $USER:$USER /srv/tftp


# FIN

om_echoinfo "FIN INSTALL PXESERVER"
 
om_theend
