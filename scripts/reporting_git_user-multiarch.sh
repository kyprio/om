#!/bin/sh

GIT_BASE="/srv/git"
EMAIL_TO="$1"
EMAIL_SENDER="git@librescargot.fr"


# Get all git repositories, without logs dir in path
GIT_HEADS=$(find ${GIT_BASE} -name HEAD 2>/dev/null | grep -v "/logs/")

for GIT_HEAD in ${GIT_HEADS}
do
  # Get repo directory
  GIT_REPO=$(dirname ${GIT_HEAD})
  # CD to repo directory
  cd ${GIT_REPO}

  # Get git log per branches
  GIT_BRANCHES="$( git branch | sed 's/[ \*]//' )"
  for GIT_BRANCH in $(echo "${GIT_BRANCHES}")
  do
    GIT_LOG=$(git log "${GIT_BRANCH}" --since=1.day --stat --reverse)

    # email
    if [ -n "${GIT_LOG}" ]
      then
        EMAIL_SUBJECT=$(echo ${GIT_REPO} \(${GIT_BRANCH}\) | sed -e "s#${GIT_BASE}#[GIT Report]#" -e "s#/.git##")

        # send email
        echo "${GIT_LOG}" | mail -s "${EMAIL_SUBJECT}" "${EMAIL_TO}"

        # Debug
        #echo "
        #${EMAIL_TO}
        #
        #${EMAIL_SUBJECT}
        #
        #${GIT_LOG}
        #
        #"
    fi
  done
done
