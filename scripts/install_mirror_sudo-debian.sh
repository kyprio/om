echo IN PROGRESS && exit 7
#!/bin/bash
IFS=$'\n\t'
source "$HOME/om/scripts/base_script.sh" 2> /dev/null

# DEBUT TEST SUDO
clear
om_echotitle "INSTALL MIRROR"
[ $UID -eq 0 ] && echo -e $red"NE PAS EXECUTER AVEC ROOT\n" && exit 1
echo -e $yellow"mot de passe du super-user $USER :"$white
read -s mdp
sudo -k
( echo $mdp; echo $mdp echo $mdp ) | sudo -S echo -e $whitebig"mot de passe correct"$white 2> /dev/null
if [ ! $? -eq 0 ]; then
	echo -e $red"MOT DE PASSE INCORECT OU $USER N'EST PAS UN SUPER-USER"
	exit 2
fi
# FIN TEST SUDO

# PRECONFIGURATION
##################

userftpsync="userftpsync"
whiptail --title "INSTALL MIRROR" --msgbox "Un miroir Debian prend environ 160Go pour les architectures courantes.\nIl faut prévoir 300Mo par jour de synchro par architures.\nComptez donc 500Go d'espace disque pour un miroir amd64 i386." 20 70
mdp=$(whiptail --title "INSTALL MIRROR" --passwordbox "Un nouvel utilisateur $userftpsync va être créer\nMot de passe de l'utilisateur $userftpsync pour le miroir :" 20 70 3>&1 1>&2 2>&3)
mdp=`mkpasswd $mdp`
arch=$(whiptail --title "INSTALL MIRROR" --checklist "Liste des architectures a exclure du miroir parmi la liste suivante\n(on supprimera généralement de la liste les architectures amd64 et i386)" 35 70 22 \
 amd64 "" OFF\
 i386 "" OFF\
 source "" ON\
 alpha "" ON\
 arm "" ON\
 arm64 "" ON\
 armel "" ON\
 armhf "" ON\
 hppa "" ON\
 hurd-i386 "" ON\
 ia64 "" ON\
 kfreebsd-amd64 "" ON\
 kfreebsd-i386 "" ON\
 m68k "" ON\
 mipsel "" ON\
 mips "" ON\
 powerpc "" ON\
 ppc64el "" ON\
 s390 "" ON\
 s390x "" ON\
 sh "" ON\
 sparc "" ON\
 3>&1 1>&2 2>&3)
arch=`echo $arch | sed 's/"//g'`

# INSTALLATION
##############

[ ! -n "`sudo dpkg -l | grep lighttpd`" ] && source sudo_install_web_http_only.sh && sudo sed -i '/^server.port/adir-listing.activate        = "enable"' /etc/lighttpd/lighttpd.conf

sudo apt-get install -y dpkg-dev
sudo rm /srv/web/index.php


# POST CONFIGURATION
####################

sudo useradd -m -p $mdp --shell /bin/bash $userftpsync
sudo adduser $userftpsync www-data

sudo mkdir -p /srv/web/mirrors/debian
sudo cp ../config/ftpsync.tar.gz /home/$userftpsync/
cd /home/$userftpsync/
sudo tar -zxf ftpsync.tar.gz
sudo rm ftpsync.tar.gz
sudo mv distrib ftpsync

cd ftpsync/etc/

sudo cp ftpsync.conf.sample ftpsync.conf

sudo sed -i "s/#TO=\"\/srv\/mirrors\/debian\/\"/TO=\"\/srv\/web\/mirrors\/debian\"/" ftpsync.conf
sudo sed -i "s/#RSYNC_PATH=\"debian\"/RSYNC_PATH=\"debian\"/" ftpsync.conf
sudo sed -i "s/#ARCH_EXCLUDE=\"\"/ARCH_EXCLUDE=\"$arch\"/" ftpsync.conf
sudo sed -i "s/#RSYNC_HOST=some\.mirror\.debian\.org/RSYNC_HOST=ftp2.fr.debian.org/" ftpsync.conf
sudo sed -i "s/#LOGDIR=\"\${BASEDIR}\/log\"/LOGDIR=\"\/home\/$userftpsync\/ftpsync\/log\"/" ftpsync.conf
sudo sed -i "s/#MIRRORNAME=\`hostname -f\`/MIRRORNAME=\`hostname -f\`/" ftpsync.conf

sudo chown $userftpsync:root -R /home/$userftpsync/ftpsync/
sudo chown www-data:www-data -R /srv/web/mirrors/
sudo chmod -R 770 /srv/web/mirrors

whiptail --title "INSTALL MIRROR" --msgbox "\
L'installation et la configuration du miroir sont terminées\n\
Pour utiliser le miroir utiliser, passer par l'utilisateur $userftpsync .\n\
Aller dans ~/ftpsync/bin et exécuter la commande :\n\
./ftpsync sync:all\n\
Celle-ci exécutera à la suite les deux étapes de synchronisation.\n\
Il est possible de lancer séparement les deux étapes :\n\
./ftpsync sync:stage1\n\
./ftpsync sync:stage2\
" 20 70

 
om_theend
