echo IN PROGRESS && exit 7
#!/bin/bash
# option debug
source "$HOME/om/scripts/base_script.sh" 2> /dev/null

om_echotitle "CONVERT CIDR TO NETMASK"

# PRECONFIGURATION
##################
function cidr2mask {
   set -- $(( 5 - ($1 / 8) )) 255 255 255 255 $(( (255 << (8 - ($1 % 8))) & 255 )) 0 0 0
   [ $1 -gt 1 ] && shift $1 || shift
   echo ${1-0}.${2-0}.${3-0}.${4-0}
}

function mask2cidr {
   local x=${1##*255.}
   set -- 0^^^128^192^224^240^248^252^254^ $(( (${#1} - ${#x})*2 )) ${x%%.*}
   x=${1%%$3*}
   echo $(( $2 + (${#x}/4) ))
}


 
om_theend
