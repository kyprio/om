echo IN PROGRESS && exit 7
#!/bin/bash
# DEBUT TEST SUDO
clear
om_echotitle "RECONF LIGHTTPD"
[ $UID -eq 0 ] && echo -e $red"NE PAS EXECUTER AVEC ROOT\n" && exit 1
echo -e $yellow"mot de passe du super-user $USER :"$white
read -s mdp
sudo -k
( echo $mdp; echo $mdp echo $mdp ) | sudo -S echo -e $whitebig"mot de passe correct"$white 2> /dev/null
if [ ! $? -eq 0 ]; then
	echo -e $red"MOT DE PASSE INCORECT OU $USER N'EST PAS UN SUPER-USER"
	exit 2
fi
# FIN TEST SUDO

om_echoinfo "\n## RECONF LIGHTTPD ##\n"

sudo chown -R www-data:www-data /srv/web
sudo chmod 775 -R /srv/web
sudo service lighttpd restart
 
om_theend
