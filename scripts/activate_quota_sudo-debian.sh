echo IN PROGRESS && exit 7
#!/bin/bash
# option debug
set -euo pipefail
IFS=$'\n\t'
source "$HOME/om/scripts/base_script.sh" 2> /dev/null

# DEBUT TEST SUDO
clear
om_echotitle "ACTIVATE QUOTA"
[ $UID -eq 0 ] && whiptail --title "TEST SUDO" --msgbox "NE PAS EXECUTER AVEC ROOT" 20 70 && exit 1
mdp=$(whiptail --title "TEST SUDO" --passwordbox "Mot de passe du super-user $USER :" 20 70 3>&1 1>&2 2>&3)
sudo -k
( echo $mdp; echo $mdp echo $mdp ) | sudo -S echo -e $whitebig"mot de passe correct"$white 2> /dev/null
if [ ! $? -eq 0 ]; then
	echo -e $red"MOT DE PASSE INCORECT OU $USER N'EST PAS UN SUPER-USER"
	exit 2
fi
# FIN TEST SUDO

# PRECONFIGURATION
##################

om_echoerror "OPTION DEBUG set -euo pipefail ACTIVEE"

# INSTALLATION
##############


sudo apt-get -y install quota


# POSTCONFIGURATION
###################

sudo sed -i 's/\/srv ext4 defaults/\/srv ext4 defaults,usrquota,grpquota' /etc/fstab

sudo mount -o remount /srv

# vérification de la taille du dossier et création des fichiers de quota
sudo quotacheck -cgum /srv/stockage

# activation du monitoring
sudo quotaon -avug


# info commande
whiptail --title "ACTIVATE QUOTA" --msgbox "sudo edquota -g nom_groupe\nsudo edquota -u nom_user" 20 70
whiptail --title "ACTIVATE QUOTA" --msgbox "
# si les fichier quota n'existe pas
sudo touch /home/aquota.group   #Création du fichier pour le groupe
sudo touch /home/aquota.user  #Création du fichier pour l'utilisateur
sudo chmod 600 /home/aquota.*
" 20 70

 
om_theend
