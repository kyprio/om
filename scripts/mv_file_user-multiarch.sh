#!/bin/bash

# HEADER
##################################################

set -o errexit
set -o nounset
set -o pipefail
#set -o xtrace
#set -v


source "$HOME/om/scripts/base_script.sh"  2> /dev/null || (echo "base_script.sh manquant" ; exit 1)

readonly scriptName="$(basename ${0})"
readonly scriptVersion="0.1" # 06/26/17 by kyprio for the project om

function usage {
  local returnCode="${1:-0}"
 
  om_echoinfo "${scriptName}\trename file with regex"

  om_echoreturn "${scriptName}
  -x $Y # run the script with interactive mode if necessary $B
  [-v] $Y # version $B
  [-h] $Y # help $B"
  [-c] $Y # description2 (default_value) $B 
  om_echoreturn "${scriptName}
  "

  exit "${returnCode}"
}

om_header -t "RENAME FILE WITH REGEX" -v "${scriptVersion}" -u user

[[ "${#}" -eq 0 ]] &&  (om_echoerror "At least, please run the script with the -x parameter" ; usage "4")

#om_inprogress

# PRECONFIGURATION
##################################################

om_echosubtitle "PRECONFIGURATION"

continu=''    # optional : description2 
pattern=''    # internal : pattern 
file=''       # internal : description3 

while getopts ":xhvc:" opt; do
  case "${opt}" in
    c) continu="${OPTARG}" ;; 
    x) ;;
    h) usage ;;
    v) om_version "${scriptVersion}" ;;
    \?) om_echoerror "Invalid option: -${OPTARG}" ; exit 4 ;;
    :) om_echoerror "missing value to -${OPTARG}" ; exit 4 ;;
  esac
done

continu="${continu:-yes}" 
pattern="" 
file="" 

om_debugvar continu pattern file 

# EXECUTION
##################################################

om_step "EXECUTION"

while [ ${continu} == yes ] ; do
  om_readtext file "Nom du fichier"
  om_readtext pattern "regex"
  if [ ! -f "${file}" ] ; then
    grep  "${pattern}" "${file}" && om_echook "${file} existe"
  fi


  
# POSTCONFIGURATION
##################################################

om_echosubtitle "POSTCONFIGURATION"

om_theend
