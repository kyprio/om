echo IN PROGRESS && exit 7
#!/bin/bash
IFS=$'\n\t'

# DEBUT TEST SUDO
clear
om_echotitle "INSTALL WEB"
[ $UID -eq 0 ] && echo -e $red"NE PAS EXECUTER AVEC ROOT\n" && exit 1
echo -e $yellow"mot de passe du super-user $USER :"$white
read -s mdp
sudo -k
echo $mdp | sudo -S echo -e $whitebig"mot de passe correct"$white 2> /dev/null
if [ ! $? -eq 0 ]; then
	echo -e $red"MOT DE PASSE INCORECT OU $USER N'EST PAS UN SUPER-USER"
	exit 2
fi
# FIN TEST SUDO

# PRE CONFIGURATION
###################

# variable pour mediawiki
#cms=$(whiptail --title "INSTALLATION SERVEUR WEB / CN" \
#	--inputbox "\n\nNom de la future base de donnée accueillant le mediawiki (en minuscule sans espaces)" \
#	20 70 3>&1 1>&2 2>&3) || exit 1

# variables pour le serveur
while true; do
	mdpsrv=$(whiptail --title "INSTALLATION SERVEUR WEB / MDP ROOT SRV" \
		--passwordbox "\n\nmot de passe de root des services (ex:mysql)" \
		20 70 3>&1 1>&2 2>&3) || exit 1
	mdpsrv2=$(whiptail --title "INSTALLATION SERVEUR WEB / MDP ROOT SRV CONFIRM" \
		--passwordbox "\n\nconfirmation mot de passe de root des services (ex:mysql)" \
		20 70 3>&1 1>&2 2>&3) || exit 1
	[ $mdpsrv == $mdpsrv2 ] && break
	whiptail --title "INSTALLATION SERVEUR WEB / MDP ROOT SRV ERROR" \
		--yesno "Erreur dans la saisie" --yes-button "Revenir" --no-button "Annuler" \
		20 70 3>&1 1>&2 2>&3 || exit 1
done
#while true; do
#	mdpcms=$(whiptail --title "INSTALLATION SERVEUR WEB / MDP ADMIN DB $cms" \
#		--passwordbox "\n\nmot de passe admin de la base de donnée $cms" \
#		20 70 3>&1 1>&2 2>&3) || exit 1
#	mdpcms2=$(whiptail --title "INSTALLATION SERVEUR WEB / MDP ADMIN DB $cms CONFIRM" \
#		--passwordbox "\n\nconfirmation mot de passe admin de la base de donnée $cms" \
#		20 70 3>&1 1>&2 2>&3) || exit 1
#	[ $mdpcms == $mdpcms2 ] && break
#	whiptail --title "INSTALLATION SERVEUR WEB / MDP ADMIN DB $cms ERROR" \
#		--yesno "Erreur dans la saisie" --yes-button "Revenir" --no-button "Annuler" \
#		20 70 3>&1 1>&2 2>&3 || exit 1
#done
cn=$(whiptail --title "INSTALLATION SERVEUR WEB / CN" \
	--inputbox "\n\nINFORMATION SUR LE SERVEUR\nne saisir que des caractères alphanumeriques sans espace\nnom du nas (Common Name/Fully Qualified Domain Name, ex: www.domain.lan )" \
	20 70 "`hostname -f`" 3>&1 1>&2 2>&3) || exit 1
pays=$(whiptail --title "INSTALLATION SERVEUR WEB / PAYS" \
	--inputbox "\n\nacronyme en 2 lettres du pays(Country)" \
	20 70 "FR" 3>&1 1>&2 2>&3) || exit 1
region=$(whiptail --title "INSTALLATION SERVEUR WEB / REGION" \
	--inputbox "\n\nregion (State)" \
	20 70 "IDF" 3>&1 1>&2 2>&3) || exit 1  
ville=$(whiptail --title "INSTALLATION SERVEUR WEB / VILLE" \
	--inputbox "\n\nville (Locality)" \
	20 70 "EVRY" 3>&1 1>&2 2>&3) || exit 1    
entreprise=$(whiptail --title "INSTALLATION SERVEUR WEB / ENTREPRISE" \
	--inputbox "\n\nsociete (Organization et Organization Unit)" \
20 70 "ITIS" 3>&1 1>&2 2>&3) || exit 1
whiptail --title "INSTALLATION SERVEUR WEB / DEBUT" \
	--msgbox "\n\n$USER sera le seul utilisateur à pouvoir parcourir et modifier /srv/web du NAS" \
	20 70

# INSTALLATION
##############

# importation des modules à installer
source sudo_install_module_ssh.sh
source sudo_install_module_web_mysql-https-http.sh
#source sudo_install_module_web_https-http.sh
#source sudo_install_module_web_http.sh
source sudo_install_module_ufw.sh


# POST CONFIGURATION
####################

## création de la base de donnée mysql
#mysql -u root -p$mdpcms -e "\
#CREATE DATABASE $cms CHARACTER SET  utf8; 
#GRANT ALL ON "$cms".* TO '$cms'@'localhost' IDENTIFIED BY '$mdpcms';"


## récupération de la dernière version connue à l'heure où j'écris ces lignes
#cd /srv/web
#sudo wget http://releases.wikimedia.org/mediawiki/1.26/mediawiki-1.26.2.tar.gz -O mediawiki.tar.gz
#sudo tar -zxf mediawiki.tar.gz
#sudo mv mediawiki-1.26.2 $cms
#sudo rm mediawiki.tar.gz
#sudo chown -R www-data:www-data /srv/web

# il est recommandé d'enlever le droit d'éxécution sur certains dossiers
#sudo chmod -x $cms/images -R

# ajout de l'utilisateur principal aux groupes www-data
sudo adduser $USER www-data
sudo chmod 775 -R /srv/web

# FIN
whiptail --title "INSTALLATION SERVEUR WEB / INFORMATION" \
	--msgbox "\n\n$USER sera le seul utilisateur à pouvoir parcourir et modifier /srv/web" \
	20 70
whiptail --title "INSTALLATION SERVEUR WEB / INFORMATION" \
	--msgbox "\n\nFIN D'INSTALLATION" \
	20 70
 
om_theend
