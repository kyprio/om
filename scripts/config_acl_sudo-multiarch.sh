#!/bin/bash

# HEADER
##################################################

function usage {
  echo -e "Affecte les droits posix/acl aux groupes sur le dossier.
  -x    # mode interactif
  [-v]  # version
  [-h]  # help
  -d    # dossier
  -g    # groupe_principal
  [-s]  # \"group_secondaire1 rwx group_secondaire2 rw\""
}
if [[ "${#}" -eq 0 ]] ; then
  echo -e "Lancer au moins le script avec l'option -x"
  usage
  exit 4
fi


# PRECONFIGURATION
##################################################

echo -e "\nPRECONFIGURATION\n"
pri_group=''
group=''
dir=''
while getopts ":g:d:s:xhv" opt; do
  case "${opt}" in
    x) ;;
    h) usage ;;
    \?) echo -e "Invalid option: -${OPTARG}" ; exit 4 ;;
    :) echo -e "missing value to -${OPTARG}" ; exit 4 ;;
    g) pri_group="${OPTARG}" ;; 
    s) group="${OPTARG}" ;; 
    d) dir="${OPTARG}" ;; 
  esac
done

# affectation des variables si vides
while [[ -z "${pri_group}" ]]; do read -p "nom du groupe principal : " pri_group ; done 
[[ -z "${group}" ]] && read -p "noms des groupes secondaires et droits r/rw (facultatif) (ex: group2 rw group3 r) : " group ;
while [[ -z "${dir}" ]] || [[ ! -d "${dir}" ]] ; do read -p "chemin complet et du dossier parent (doit exister) : " dir ; done 

# concaténation des groupes pour acl et convertion en array
# group=group1 rw group2 r group3 rw
group="$pri_group rw $group" 
x=0
e=''
for e in ${group}; do
  arr_group[${x}]=${e}
  ((x++))
done

# test validité groupe
y=0
for (( y=0; y<=$((${#arr_group[@]} - 1)); y=y+2 )); do
  if [[ ! $(getent group ${arr_group[$y]} ) ]] ; then
    echo -e "\n${arr_group[$y]} n'est pas un groupe valide\n"
    exit 5
  else
    echo -e "${arr_group[$y]} valide"
  fi
done


# EXECUTION
##################################################

echo -e "Modification à appliquer pour ${dir} :"
echo -e " - suppression d'ancienne ACL"
echo -e " - affectation droit posix rw(x)rw(s)--- pour root et ${pri_group}" 
echo -e " - affectation droit acl : ${group} (+x)"
echo -e " - affectation mask rw(x)"
echo -e  "\nEXECUTION\nappuyez sur ENTRER pour continuer\n"
read

# suppression ancienne acl

sudo setfacl -R -b ${dir} && \
echo -e "Suppression d'anciennes ACL OK"

# droit posix

sudo chown -R "root:${pri_group}" "${dir}" && \
sudo chmod -R u=rw,g=rw,o= ${dir} && \
sudo find "${dir}" -type d | xargs sudo chmod u+x,g+xs  && \
echo -e "Affectation droit posix rwxrws--- pour root et ${pri_group} (-x pour fichier) OK" 
[[ "$?" -ne 0 ]] && echo -e "Erreur ! vérifiez le code de retour" && exit  7

# droit acl pour les groupes

z=0
for (( z=0; z<=$((${#arr_group[@]} - 1)); z++ )); do
  g=${arr_group[$z]}
  ((z++))
  perm=${arr_group[$z]}
  sudo setfacl -R -m "group:${g}:${perm}x" ${dir} && \
  sudo find "${dir}" -type d | xargs sudo setfacl -m "default:group:${g}:${perm}x" && \
  echo -e "Affectation droit acl sur les fichiers : ${g} ${prem} (+x pour les dossiers) OK"
[[ "$?" -ne 0 ]] && echo -e "Erreur ! vérifiez le code de retour" && exit  8
done

# mask

sudo find "${dir}" -type f | xargs sudo setfacl -m "mask::${perm}" && \
sudo find "${dir}" -type d | xargs sudo setfacl -m "default:mask::rwx" && \

echo -e "\nFIN EXECUTION\n"

# POSTCONFIGURATION
##################################################

sleep 0.5
echo -e "\nresultat : getfacl ${dir} :\n"
getfacl "${dir}"
