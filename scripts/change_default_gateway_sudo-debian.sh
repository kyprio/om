echo IN PROGRESS && exit 7
#!/bin/bash
source "$HOME/om/scripts/base_script.sh" 2> /dev/null

# DEBUT TEST SUDO
clear
om_echotitle "CHANGE DEFAULT GATEWAY"
[ $UID -eq 0 ] && echo -e $red"NE PAS EXECUTER AVEC ROOT\n" && exit 1
echo -e $yellow"mot de passe du super-user $USER :"$white
read -s mdp
sudo -k
echo $mdp | sudo -S echo -e $whitebig"mot de passe correct"$white 2> /dev/null
if [ ! $? -eq 0 ]; then
	echo -e $red"MOT DE PASSE INCORECT OU $USER N'EST PAS UN SUPER-USER"
	exit 2
fi
# FIN TEST SUDO

# PRECONFIGURATION
om_readtext "Quel gateway voulez-vous utiliser par défault ?"
read gw

om_readtext "Avant :"
sudo route -n

sudo route del default
sudo route add default gw $gw
if [ ! $? -eq 0 ]; then echo -e $red"ERREUR DANS L'EXECUTION DE LA COMMANDE PRECEDENTE"$white; exit 5; fi

echo -e $yellow"Après :"$whitebig
sudo route -n
 
om_theend
