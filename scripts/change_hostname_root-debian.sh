echo IN PROGRESS && exit 7
#!/bin/bash
source "$HOME/om/scripts/base_script.sh" || (echo "base_script.sh manquant" && exit 21)
om_testuser root
om_echotitle "CHANGE HOSTNAME"

# script pour modifier le nom de la machine le nom de domaine
# ceci modifie /etc/hostname et /etc/hosts

# récupération des valeurs actuelles
# et préparation des couleurs de mise en évidence

old_machine=`hostname`
old_domain=`grep "127.0.1.1" /etc/hosts | cut -f 2 | cut -d "." -f 2,3`

# récupération des nouvelles valeurs

om_echoinfo "nom actuel de la machine : ${old_machine}"
om_echoinfo "nom actuel du domaine : $old_domain"
[ -z "$old_domain" ] && old_domain="labo.lan"

om_readtext machine "nouveau nom de la machine"
[ -z "$machine" ] && machine=$old_machine
om_readtext domain "nouveau nom du domain (${old_domain})"
[ -z "$domain" ] && domain=$old_domain

# enregistrement

echo $machine > /etc/hostname
sed -i "s/127.0.1.1.*/127.0.1.1\t${machine}.${domain}\t${machine}/" /etc/hosts

nw_machine=`cat /etc/hostname`
nw_domain=`grep "127.0.1.1" /etc/hosts | cut -f 2 | cut -d "." -f 2,3`
om_echoinfo "\nnouvelles valeurs (nom_machine.domaine) : "${whitebig}"${nw_machine}.${nw_domain}"

om_theend
om_reboot
