echo IN PROGRESS && exit 7
#!/bin/bash

source "$HOME/om/scripts/base_script.sh" 2> /dev/null
# DEBUT TEST SUDO
clear
om_echotitle "GIT NEW PROJECT"
[ $UID -eq 0 ] && echo -e $red"NE PAS EXECUTER AVEC ROOT\n" && exit 1
echo -e $yellow"mot de passe du super-user $USER :"$white
read -s mdp
sudo -k
echo $mdp | sudo -S echo -e $whitebig"mot de passe correct"$white 2> /dev/null
if [ ! $? -eq 0 ]; then
	echo -e $red"MOT DE PASSE INCORECT OU $USER N'EST PAS UN SUPER-USER"
	exit 2
fi
# FIN TEST SUDO

# SCRIPT PERMETANT LA CREATION ET L'INITIALISATION DE NOUVEAUX PROJETS GIT
# chaque ligne du fichier "liste_dossier" correspond à un nouveau projet à créer

om_echoinfo "Préalablement, ayez créé le fichier liste_dossier dans votre home\net renseignez dedans la liste des dossiers à créer\nséparés d'un retour à la ligne"

# continu

echo -e $yellow""
read -p "si cela est déjà fait , appuyez sur entrée pour continuer / CTRL-C pour fermer" continu

liste_dossier=`cat ~/liste_dossier`

cd /srv/git/
for dossier in $liste_dossier; do
	om_echoinfo "CREATION DE $dossier"
	sudo su -m -c "mkdir -p $dossier && cd $dossier && git init --bare" git
done

# vidage du fichier liste_dossier
cd  ~
echo "" > liste_dossier
 
om_theend
