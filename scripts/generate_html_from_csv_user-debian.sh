echo IN PROGRESS && exit 7
#!/bin/bash
# option debug
IFS=$'\n\t'
source "$HOME/om/scripts/base_script.sh" 2> /dev/null

om_echotitle "GENERATE HTML FROM CSV"


# PRECONFIGURATION
##################


path=~/Documents/Articles/
base=$path'base_whole.html'

if [ -n "$1" -a -n "$2" ]; then
	pre="$1"
	csv="$2"
else
	echo -e $red "indiquez en argument le préfixe des fichiers html généré puis le fichier source csv contenant pour chaque ligne :\nTitre de la page,nom_fichier"$white
	exit 1
fi

# EXECUTION
###########


for line in `cat $csv`; do
	cd $path
	titre=$(echo $line | cut -d"," -f1)
	file=$pre"_"$(echo $line | cut -d"," -f2)".html"
	cp $base $file
	sed -i "s#Titre1#${titre}#" $file
	echo -e $whitebig"$file = $titre"$white
done


# POSTCONFIGURATION
###################


 
om_theend
