#!/bin/bash
source "$HOME/om/scripts/base_script.sh" 2> /dev/null

om_echotitle "CONFIG RASPBIAN DOC"

echo -e $yellow"
1 expand Filesystem

4 Internationalisation Options
	T1 Change Locale
		en_GB_UTF-8
		fr_FR_UTF-8
		en_GB_UTF-8
	T2 Change Timezone
		Europe
		Paris
	T3 Change keyboard Layout
		generic 105-key
		french
		The default for the keyboard layout
		No compose key
		Yes

2 Changer User Password
7 Overclock
8 Advanced Options
	A2 Hostname
		raspbor
	A3 Memory Split
		16
	A4  SSH
		enable
Finish
Yes

sudo dpkg-reconfigure console-setup
	utf-8
	Guess optimal character set
	Let the system select a suitable font
	11x22
	
sudo passwd
exit
usermod pi -l sysadmin -d /home/sysadmin -m
groupmod pi -n sysadmin
passwd -l root
exit
"
 
om_theend
