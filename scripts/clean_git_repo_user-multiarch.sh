#!/bin/bash


echo """
# DEBUT CLEAN #
###############

dans $1

"""

# Compiled source #
###################
find "$1" -type f -name *.com -exec rm {} \;
find "$1" -type f -name *.class -exec rm {} \;
find "$1" -type f -name *.dll -exec rm {} \;
find "$1" -type f -name *.exe -exec rm {} \;
find "$1" -type f -name *.o -exec rm {} \;
find "$1" -type f -name *.so  -exec rm {} \;
find "$1" -name __pycache__ -exec rm -r {} \;
find "$1" -name *.egg-info -exec rm -r {} \;
find "$1" -name 'venv' -exec rm -r {} \;
find "$1" -name '.venv' -exec rm -r {} \;
find "$1" -type f -name *.jar -exec rm {} \;

# media #
#########
find "$1" -type f -name *.mkv -exec rm {} \;
find "$1" -type f -name *.avi -exec rm {} \;
find "$1" -type f -name *.mp3 -exec rm {} \;
find "$1" -type f -name *.ogg -exec rm {} \;
find "$1" -type f -name *.flac -exec rm {} \;
find "$1" -type f -name *.jpg -exec rm {} \;
find "$1" -type f -name *.png -exec rm {} \;


# Logs #
########
find "$1" -type f -name *.log -exec rm {} \;
find "$1" -type f -name log -exec rm {} \;
find "$1" -type f -name *.out -exec rm {} \;

# archives #
############
find "$1" -type f -name *.zip -exec rm {} \;
find "$1" -type f -name *.tar -exec rm {} \;
find "$1" -type f -name *.tar.gz -exec rm {} \;
find "$1" -type f -name *.rar -exec rm {} \;
find "$1" -type f -name *.gzip -exec rm {} \;

# bootstrap et framework CSS #
##############################
find "$1" -name bootstrap -exec rm -r {} \;


# OS generated files #
######################
find "$1" -type f -name *.swp -exec rm {} \;
find "$1" -type f -name ~* -exec rm {} \;
find "$1" -type f -name .DS_Store* -exec rm {} \;
find "$1" -type f -name ._* -exec rm {} \;
find "$1" -name .Spotlight-V100 -exec rm -r {} \;
find "$1" -name .Trashes -exec rm -r {} \;
find "$1" -name ehthumbs.db -exec rm -r {} \;
find "$1" -type f -name Thumbs.db -exec rm {} \;

# perso #
#########
find "$1" -type f -name _.bash-aliases-cryp -exec rm {} \;

echo """
# FIN   CLEAN #
###############
"""
