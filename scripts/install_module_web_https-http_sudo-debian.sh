echo IN PROGRESS && exit 7
#!/bin/bash

# PRE CONFIGURATION
###################

# INSTALLATION
##############

sudo apt-get install -y unzip lynx vim screen lighttpd

# POST CONFIGURATION
####################

# création des dossiers
sudo mkdir /srv/ssl /srv/web

# création du certificat SSL
sudo openssl req -new -x509 -keyout /srv/ssl/$cn\.pem -out /srv/ssl/$cn\.pem -days 3650 -nodes -subj "/C=$pays/ST=$region/L=$ville/O=$entreprise/OU=$entreprise/CN=$cn"

# restriction des droits sur le dossier /srv/ssl
sudo chmod -R 400 /srv/ssl/

# changement du dossier racine web de lighttpd
sudo sed -i 's/\/var\/www\/html/\/srv\/web/' /etc/lighttpd/lighttpd.conf
sudo sh -c "echo '<html>TEST</html>' > /srv/web/index.html"
sudo chown -R www-data:www-data /srv/web
sudo chmod -R 775 /srv/web

# PRECONFIGURATION du ssl dans lighhtpd
sudo sed -i "s/\/etc\/lighttpd\/server.pem/\/srv\/ssl\/$cn\.pem/" /etc/lighttpd/conf-available/10-ssl.conf

# activation des modules php ssl pour http https
sudo service lighttpd start
sudo lighty-enable-mod ssl

# rajout de l'utilisateur $USER à  www-data
sudo adduser $USER www-data

# backup des configurations du web
sudo cp -rp /etc/lighttpd /srv/backup/

# création de liste détaillée avant déplacement
liste_dossier=('/' '/etc/ssl' '/var/lib')
for dossier in ${liste_dossier[@]}; do
	file_dossier=`echo $dossier | tr '/' '_'`
	sudo sh -c "ls $dossier > /srv/backup/$file_dossier"
done

# redémarrage du service lighttpd
sudo service lighttpd restart
 
om_theend
