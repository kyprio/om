echo IN PROGRESS && exit 7
#!/bin/bash
# DEBUT TEST SUDO
clear
om_echotitle "ADD USER WEB"
[ $UID -eq 0 ] && echo -e $red"NE PAS EXECUTER AVEC ROOT\n" && exit 1
echo -e $yellow"mot de passe du super-user $USER :"$white
read -s mdp
sudo -k
( echo $mdp; echo $mdp echo $mdp ) | sudo -S echo -e $whitebig"mot de passe correct"$white 2> /dev/null
if [ ! $? -eq 0 ]; then
	echo -e $red"MOT DE PASSE INCORECT OU $USER N'EST PAS UN SUPER-USER"
	exit 2
fi
# FIN TEST SUDO


# PRECONFIGURATION
##################

ipldap=`sudo grep "^uri ldap://" /etc/pam_ldap.conf | cut -d "/" -f3`
base=`sudo grep "^base " /etc/pam_ldap.conf | cut -d " " -f2`
echo $ipldap
echo $base


# EXECUTION
###########


om_echoinfo "\n## ADD USER WEB ##\n"

om_echoinfo "Liste des utilisateurs actuels du système :"
getent passwd | grep -P 1[0-9]{3} | cut -d: -f1

om_echoinfo "Liste des utilisateurs actuels de l'annuaire LDAP :"
ldapsearch -x -LLL -b dc=labo,dc=lan cn -H ldap://$ipldap | grep ^cn | cut -d " " -f 2 2> /dev/null

om_readtext liste_users "\nListe des utilisateurs à ajouter au groupe www-data (séparés par un espace) :"

for user in ${liste_users[@]}; do
	sudo adduser $user www-data
done

 
om_theend
