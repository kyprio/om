#!/bin/bash

if [[ -n "$1" ]]; then
  script="$1"
else
  read -p "script : " script
fi


cp ${HOME}/om/lang/python/templates/class.py ${script}.py
sed -i "s/{{classe}}/${script}/g" ${script}.py
chmod +x ${script}.py
vi + ${script}.py
