echo IN PROGRESS && exit 7
#!/bin/bash

pathCA="$(ls CA/ca_*.crt)"
pathConf='openssl_users.cnf'

test -e ${pathCA} || exit 1
test -e ${pathConf} || exit 2

if [ -z "${*}" ]; then
	echo "prenom_nom en argument"
	exit
fi

for USERL in $(echo "${*}"); do

	destKey="key/user-${USERL}.pem"
	destReq="req/user-${USERL}.req"
	destCert="cert/$(date +%Y%m%d)_user-${USERL}.crt"
	destP12="${USERL}.p12"
	
	echo -e  "\n"${USERL}"\n"

	openssl req -config "${pathConf}" -newkey rsa:2048 -keyout "${destKey}" -out "${destReq}" && \
	openssl ca -config "${pathConf}" -out "${destCert}" -in "${destReq}" && \
	openssl pkcs12 -export -in "${destCert}" -inkey "${destKey}" -certfile "${pathCA}" -out "${destP12}"
	
done
