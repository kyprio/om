#!/bin/bash

# PRECONFIGURATION

action="${1}"
src="${2}"
dst="${3}"
dict="/etc/hosts"

function getIPlines {
regIP='(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?).(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?).(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?).(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)'

list_IP=( $( cat "${dict}" | egrep "${regIP}" | awk '{print $1}' ) )
list_URL=( $( cat "${dict}" | egrep "${regIP}" | awk '{print $2}' ) )

}

function showmap {
for (( i=0; i<=$((${#list_IP[@]} - 1)); i++ )); do
  echo ${list_URL[$i]} = ${list_IP[$i]}
done
}

function url2ip {
cp "${src}" "${dst}"
for (( i=0; i<=$((${#list_IP[@]} - 1)); i++ )); do
  sed -i -e "s/${list_URL[$i]}\([: ]\)/${list_IP[$i]}\1/" -e "s/${list_URL[$i]}$/${list_IP[$i]}/" "${dst}"
done
}

function ip2url {
cp "${src}" "${dst}"
for (( i=0; i<=$((${#list_IP[@]} - 1)); i++ )); do
  sed -i -e "s/${list_IP[$i]}\([: ]\)/${list_URL[$i]}\1/" -e "s/${list_IP[$i]}$/${list_URL[$i]}/" "${dst}"
done
}


getIPlines

eval "${action}"
