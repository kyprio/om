#!/bin/bash
source "$HOME/om/scripts/base_script.sh" 2> /dev/null

# DEBUT TEST SUDO
clear
om_echotitle "CONFIG RASPBIAN"
[ $UID -eq 0 ] && echo -e $red"NE PAS EXECUTER AVEC ROOT\n" && exit 1
echo -e $yellow"mot de passe du super-user $USER :"$white
read -s mdp
sudo -k
echo $mdp | sudo -S echo -e $whitebig"mot de passe correct"$white 2> /dev/null
if [ ! $? -eq 0 ]; then
	echo -e $red"MOT DE PASSE INCORECT OU $USER N'EST PAS UN SUPER-USER"
	exit 2
fi
# FIN TEST SUDO


# PRECONFIGURATION
##################
listPkg="$(cat ${OMDIR}/config/packages/default_install/debian_based_minimum) $(cat ${OMDIR}/config/packages/default_install/debian_based_rpi)"

# INSTALLATION
##############

# remplacement du fichier sources.list
#sudo cp ../config/sources.list/raspbian_etc_apt_sources.list /etc/apt/sources.list || exit 1

# mise à jour
sudo apt update
sudo apt dist-upgrade -y

# installation
sudo apt install -y ${listPkg}

# reconfiguration des locales avant installation pour résoudre les problèmes lors des installation depuis SSH
sudo locale-gen "fr_FR.UTF-8" "en_US.UF-8"


# POST CONFIGURATION
####################


# remplacement des references au user pi
sudo sed -i -e "s/pi user/sysadmin user/ ; s/ pi / sysadmin / ; s/'pi'/'sysadmin'/ ; s/user=pi/user=sysadmin/ ; s/(pi)/(sysadmin)/" /usr/bin/raspi-config

# activation de root et copie du script
echo root:#rtt# | sudo chpasswd 
sudo cp change_pi_to_sysadmin_root-raspbian.sh /root

# activer temporairement ssh pour root
sudo sed -i 's/#PermitRootLogin prohibit-password/PermitRootLogin yes/' /etc/ssh/sshd_config
sudo systemctl restart ssh

# info
whiptail --title "CONFIG RASPBIAN" --msgbox "Quittez votre session\nLogguez vous en root avec #rtt#\nExécutez le script change_pi_to_sysadmin_root-raspbian.sh" 20 90
 
om_theend
