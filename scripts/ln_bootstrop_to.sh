#!/bin/bash

dest="$PWD"
src="${HOME}/om/lang/css/examples/bootstrap/"

for s in $(realpath ${src}/*); do
  echo "ln -s $s ${dest}/"
  ln -s $s ${dest}/
done
