echo IN PROGRESS && exit 7
#!/bin/bash
source "$HOME/om/scripts/base_script.sh" || echo "base_script.sh manquant" && exit 1
om_testuser root
# option debug
#set -euo pipefail
#IFS=$'\n\t'


# PRECONFIGURATION
##################

#om_echoerror "OPTION DEBUG set -euo pipefail ACTIVEE"

# EXECUTION
###########



# droit sur l\'existant
chown damien.mussard:team_dev -R .
find . -exec setfacl -m group:team_dev:rwx {} \;
find . -exec setfacl -m group:apache:rwx {} \;
find . -type f -exec setfacl -m mask:rw {} \;
find . -type d -exec setfacl -m mask:rwx {} \;

# droit sur les futurs fichiers
find . -type d -exec setfacl -m default:mask:rwx {} \;
find . -type d -exec setfacl -m default:group:apache:rwx {} \;
find . -type d -exec setfacl -m default:group:team_dev:rwx {} \;
find . -type d -exec chmod g+s {} \;


##################


 
om_theend
