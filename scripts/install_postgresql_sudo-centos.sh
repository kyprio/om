#!/bin/bash

# HEADER
##################################################

set -o errexit
set -o nounset
set -o pipefail
#set -o xtrace
#set -v


source "$HOME/om/scripts/base_script.sh"  2> /dev/null || (echo "base_script.sh manquant" ; exit 1)

readonly scriptName="$(basename ${0})"
readonly scriptVersion="0.1" # 04/24/17 by kyprio for the project om

function usage {
  local returnCode="${1:-0}"
 
  om_echoinfo "${scriptName}\tinstall postgresql on centos"

  om_echoreturn "${scriptName}
  -x $Y # run the script with interactive mode if necessary $B
  [-v] $Y # version $B
  [-h] $Y # help $B"
  om_echoreturn "${scriptName}
  "

  exit "${returnCode}"
}

om_header -t "INSTALL POSTGRESQL" -v "${scriptVersion}" -u sudo

[[ "${#}" -eq 0 ]] &&  (om_echoerror "At least, please run the script with the -x parameter" ; usage "4")

#om_inprogress

# PRECONFIGURATION
##################################################

om_echosubtitle "PRECONFIGURATION"

while getopts ":xhv" opt; do
  case "${opt}" in
    x) ;;
    h) usage ;;
    v) om_version "${scriptVersion}" ;;
    \?) om_echoerror "Invalid option: -${OPTARG}" ; exit 4 ;;
    :) om_echoerror "missing value to -${OPTARG}" ; exit 4 ;;
  esac
done

om_debugvar $USER

# EXECUTION
##################################################

om_step "EXECUTION"

om_rheinstall https://yum.postgresql.org/9.6/redhat/rhel-7-x86_64/pgdg-centos96-9.6-3.noarch.rpm
om_rhelinstall postgresql96 postgresql96-server postgresql96-contrib

# POSTCONFIGURATION
##################################################

om_echosubtitle "POSTCONFIGURATION"

om_echoinfo "new user "$B"${postgresql}"$Y" created without password"

om_theend
