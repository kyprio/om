echo IN PROGRESS && exit 7
#!/bin/bash
IFS=$'\n\t'

# DEBUT TEST SUDO
clear
om_echotitle "INSTALL MULTISERVEUR"
[ $UID -eq 0 ] && echo -e $red"NE PAS EXECUTER AVEC ROOT\n" && exit 1
echo -e $yellow"mot de passe du super-user $USER :"$white
read -s mdp
sudo -k
echo $mdp | sudo -S echo -e $whitebig"mot de passe correct"$white 2> /dev/null
if [ ! $? -eq 0 ]; then
	echo -e $red"MOT DE PASSE INCORECT OU $USER N'EST PAS UN SUPER-USER"
	exit 2
fi
# FIN TEST SUDO

# PRE CONFIGURATION
###################

# variable pour mediawiki
wikiname=$(whiptail --title "INSTALLATION MEDIAWIKI / CN" \
	--inputbox "\n\nNom de la future base de donnée accueillant le mediawiki (en minuscule sans espaces)" \
	20 70 3>&1 1>&2 2>&3) || exit 1

# variables pour le serveur
while true; do
	mdpsrv=$(whiptail --title "INSTALLATION MEDIAWIKI / MDP ROOT SRV" \
		--passwordbox "\n\nmot de passe de root des services (ex:mysql)" \
		20 70 3>&1 1>&2 2>&3) || exit 1
	mdpsrv2=$(whiptail --title "INSTALLATION MEDIAWIKI / MDP ROOT SRV CONFIRM" \
		--passwordbox "\n\nconfirmation mot de passe de root des services (ex:mysql)" \
		20 70 3>&1 1>&2 2>&3) || exit 1
	[ $mdpsrv == $mdpsrv2 ] && break
	whiptail --title "INSTALLATION MEDIAWIKI / MDP ROOT SRV ERROR" \
		--yesno "Erreur dans la saisie" --yes-button "Revenir" --no-button "Annuler" \
		20 70 3>&1 1>&2 2>&3 || exit 1
done
while true; do
	mdpwiki=$(whiptail --title "INSTALLATION MEDIAWIKI / MDP ADMIN DB $wikiname" \
		--passwordbox "\n\nmot de passe admin de la base de donnée $wikiname" \
		20 70 3>&1 1>&2 2>&3) || exit 1
	mdpwiki2=$(whiptail --title "INSTALLATION MEDIAWIKI / MDP ADMIN DB $wikiname CONFIRM" \
		--passwordbox "\n\nconfirmation mot de passe admin de la base de donnée $wikiname" \
		20 70 3>&1 1>&2 2>&3) || exit 1
	[ $mdpwiki == $mdpwiki2 ] && break
	whiptail --title "INSTALLATION MEDIAWIKI / MDP ADMIN DB $wikiname ERROR" \
		--yesno "Erreur dans la saisie" --yes-button "Revenir" --no-button "Annuler" \
		20 70 3>&1 1>&2 2>&3 || exit 1
done
cn=$(whiptail --title "INSTALLATION MEDIAWIKI / CN" \
	--inputbox "\n\nINFORMATION SUR LE SERVEUR\nne saisir que des caractères alphanumeriques sans espace\nnom du nas (Common Name/Fully Qualified Domain Name, ex: www.domain.lan )" \
	20 70 "`hostname -f`" 3>&1 1>&2 2>&3) || exit 1
pays=$(whiptail --title "INSTALLATION MEDIAWIKI / PAYS" \
	--inputbox "\n\nacronyme en 2 lettres du pays(Country)" \
	20 70 "FR" 3>&1 1>&2 2>&3) || exit 1
region=$(whiptail --title "INSTALLATION MEDIAWIKI / REGION" \
	--inputbox "\n\nregion (State)" \
	20 70 "IDF" 3>&1 1>&2 2>&3) || exit 1  
ville=$(whiptail --title "INSTALLATION MEDIAWIKI / VILLE" \
	--inputbox "\n\nville (Locality)" \
	20 70 "EVRY" 3>&1 1>&2 2>&3) || exit 1    
entreprise=$(whiptail --title "INSTALLATION MEDIAWIKI / ENTREPRISE" \
	--inputbox "\n\nsociete (Organization et Organization Unit)" \
20 70 "ITIS" 3>&1 1>&2 2>&3) || exit 1
whiptail --title "INSTALLATION MEDIAWIKI / DEBUT" \
	--msgbox "\n\n$USER sera le seul utilisateur à pouvoir parcourir et modifier /srv/web du NAS" \
	20 70

# INSTALLATION
##############

# importation des modules à installer
source sudo_install_module_ssh.sh
source sudo_install_module_web_mysql-https-http.sh
source sudo_install_module_ufw.sh


# POST CONFIGURATION
####################

# ajout de l'utilisateur principal aux groupes www-data
sudo adduser $USER www-data

# création de la base de donnée mysql
mysql -u root -p$mdpwiki -e "\
CREATE DATABASE $wikiname CHARACTER SET  utf8; 
GRANT ALL ON "$wikiname".* TO '$wikiname'@'localhost' IDENTIFIED BY '$mdpwiki';"


# récupération de la dernière version connue à l'heure où j'écris ces lignes
cd /srv/web
sudo wget http://releases.wikimedia.org/mediawiki/1.26/mediawiki-1.26.2.tar.gz -O mediawiki.tar.gz
sudo tar -zxf mediawiki.tar.gz
sudo mv mediawiki-1.26.2 $wikiname
sudo rm mediawiki.tar.gz
sudo chown -R www-data:www-data /srv/web

# il est recommandé d'enlever le droit d'éxécution sur certains dossiers
sudo chmod 774 $wikiname/images -R

# FIN
whiptail --title "INSTALLATION MEDIAWIKI / INFORMATION" \
	--msgbox "\n\n$USER sera le seul utilisateur à pouvoir parcourir et modifier /srv/web du NAS" \
	20 70
whiptail --title "INSTALLATION MEDIAWIKI / INFORMATION" \
	--msgbox "\n\nFIN D'INSTALLATION" \
	20 70
 
om_theend
