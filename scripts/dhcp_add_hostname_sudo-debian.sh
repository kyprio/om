echo IN PROGRESS && exit 7
#!/bin/bash
source "$HOME/om/scripts/base_script.sh" 2> /dev/null

# DEBUT TEST SUDO
clear
om_echotitle "DHCP ADD HOSTNAME"
[ $UID -eq 0 ] && echo -e $red"NE PAS EXECUTER AVEC ROOT\n" && exit 1
echo -e $yellow"mot de passe du super-user $USER :"$white
read -s mdp
sudo -k
echo $mdp | sudo -S echo -e $whitebig"mot de passe correct"$white 2> /dev/null
if [ ! $? -eq 0 ]; then
	echo -e $red"MOT DE PASSE INCORECT OU $USER N'EST PAS UN SUPER-USER"
	exit 2
fi
# FIN TEST SUDO

# définition des variable de la boucle
continu=o

# récupération des nouvelles valeurs utilisateur grâce à une boucle
rm -f add_hostname_dhcp.list
until [ $continu == "n" ]; do
	# création du fichier .ldif si inexistant et affichade bref de son contenu
	touch add_hostname_dhcp.list

	# saisie des valeurs prenom nom mdp
	echo -ne $yellow"nom de la machine"$white
       	read hostname
	echo -ne $yellow"IP"$white
	read ip
	echo -ne $yellow"MAC"$white
	read mac
	mac=`echo $mac | tr " .-" ":::"`

	# incrémentation du fichier avec les valeurs saisies
	echo -e "\thost $hostname { hardware ethernet $mac; fixed-address $ip; }" >> add_hostname_dhcp.list

	# demande pour continuer ou non
	echo -ne $yellow"ajouter une nouvelle entrée ? o/n"$white
	read -n 1 continu
	clear
done

# affichage bref du contenu du fichier pour vérification visuelle

om_echoinfo "vérifiez les éventuels doublon :"
cat add_hostname_dhcp.list

# ajout des lignes dans le fichier /etc/dhcp/dhcpd.conf

sudo sed -i "/#ici_nouveau_host/r add_hostname_dhcp.list" /etc/dhcp/dhcpd.conf
echo -e $yellow"Nouveau contenu de /etc/dhcp/dhcpd.conf"$whitbig
cat /etc/dhcp/dhcpd.conf
sudo service isc-dhcp-server restart
om_echoinfo "FIN"

 
om_theend
