echo IN PROGRESS && exit 7
#!/bin/bash
# option debug
source "$HOME/om/scripts/base_script.sh" 2> /dev/null

om_echotitle "RPI GPIO CONTROL"

# PRECONFIGURATION
##################


echo -e $whitebig"# gpio_control():\n"$white"gpio_control NUMBER in|out 1|0|get|-"
function gpio_control {
	# gpio_control NUMBER in|out 1|0|get
	[ -d /sys/class/gpio/gpio$1 ] || echo $1 | tee /sys/class/gpio/export
	[ -w /sys/class/gpio/gpio$1/direction ] && echo $2 | tee /sys/class/gpio/gpio$1/direction
	case $3 in
		"1"|"0")
			[ "$2" != "in" ] && echo $3 | tee /sys/class/gpio/gpio$1/value
			;;
		"get"|"-")
			echo -n "GPIO$1 = "; cat /sys/class/gpio/gpio$1/value
		
	esac
}
 
om_theend
