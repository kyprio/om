echo IN PROGRESS && exit 7
#!/bin/bash
# option debug
#set -euo pipefail
#IFS=$'\n\t'
source "$HOME/om/scripts/base_script.sh" 2> /dev/null

# DEBUT TEST SUDO
clear
om_echotitle "INSTALL GIT"
[ $UID -eq 0 ] && whiptail --title "TEST SUDO" --msgbox "NE PAS EXECUTER AVEC ROOT" 20 70 && exit 1
mdp=$(whiptail --title "TEST SUDO" --passwordbox "Mot de passe du super-user $USER :" 20 70 3>&1 1>&2 2>&3)
sudo -k
( echo $mdp; echo $mdp echo $mdp ) | sudo -S echo -e $whitebig"mot de passe correct"$white 2> /dev/null
if [ ! $? -eq 0 ]; then
	echo -e $red"MOT DE PASSE INCORECT OU $USER N'EST PAS UN SUPER-USER"
	exit 2
fi
# FIN TEST SUDO

# PRECONFIGURATION
##################


list_conf=("/etc/git/git.conf /etc/git/gitd.conf")
liste_log=("/var/log/syslog /var/log/git/error.log")
homegit="/srv/git"


# INSTALLATION
##############


sudo apt-get install -y git whois

# création utilisateur git (login desactivé)
sudo useradd --shell /usr/bin/git-shell --base-dir ${homegit} git
sudo su -m -c "mkdir -p ${homegit}/.shh && touch ${homegit}/.ssh/authorized_keys" git


# POSTCONFIGURATION
###################

# création d'un dépot test
om_echoinfo "Création d'un dépot test dans ${homegit}/test/"
sudo su -m -c "mkdir -p ${homegit}/test && cd /srv/git/test && git init --bare"

# liste des fichier conf
list_conf=("/chemin/conf /chemin/conf")
echo -e $yellow"Voici la liste des fichiers de configurations utilisés :"$whitebig
for file in ${list_conf[@]}; do echo $file
done; echo -e $white

# liste des fichier log
liste_log=("/var/log/syslog")
echo -e $yellow"Voici la liste des fichiers de log à vérifier :"$whitebig
for file in ${liste_log[@]}; do echo $file
done; echo -e $white

#information complémentire
echo -e $yellow"Pour les clients :\n\
sudo apt install git\n\
git config --global user.email "client_email@mail.com"\n\
git config --global user.name "client name"\n"$white
 
om_theend
