echo IN PROGRESS && exit 7
#!/bin/bash

# PRE CONFIGURATION
###################

# INSTALLATION
##############

sudo apt-get install -y unzip lynx vim screen lighttpd

# POST CONFIGURATION
####################

# création des dossiers
sudo mkdir /srv/ssl /srv/web

# changement du dossier racine web de lighttpd
sudo sed -i 's/\/var\/www\/html/\/srv\/web/' /etc/lighttpd/lighttpd.conf
sudo sh -c "echo '<html>TEST</html>' > /srv/web/index.html"
sudo chown -R www-data:www-data /srv/web
sudo chmod -R 775 /srv/web

# activation des modules php ssl pour http https
sudo service lighttpd start

# rajout de l'utilisateur $USER à  www-data
sudo adduser $USER www-data

# backup des configurations du web
sudo cp -rp /etc/lighttpd /srv/backup/

# redémarrage du service lighttpd
sudo service lighttpd restart
 
om_theend
