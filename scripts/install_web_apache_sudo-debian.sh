echo IN PROGRESS && exit 7
#!/bin/bash
# option debug
#set -euo pipefail
#IFS=$'\n\t'
source "$HOME/om/scripts/base_script.sh" 2> /dev/null

# DEBUT TEST SUDO
clear
om_echotitle "INSTALL WEB APACHE"
[ $UID -eq 0 ] && whiptail --title "TEST SUDO" --msgbox "NE PAS EXECUTER AVEC ROOT" 20 70 && exit 1
mdp=$(whiptail --title "TEST SUDO" --passwordbox "Mot de passe du super-user $USER :" 20 70 3>&1 1>&2 2>&3)
sudo -k
( echo $mdp; echo $mdp echo $mdp ) | sudo -S echo -e $whitebig"mot de passe correct"$white 2> /dev/null
if [ ! $? -eq 0 ]; then
	echo -e $red"MOT DE PASSE INCORECT OU $USER N'EST PAS UN SUPER-USER"
	exit 2
fi
# FIN TEST SUDO

# PRECONFIGURATION
##################

#om_echoerror "OPTION DEBUG set -euo pipefail ACTIVEE"

# INSTALLATION
##############

sudo apt -y install apache2 mysql-server php5 php5-mysql libapache2-mod-php5

# POST CONFIGURATION
####################

# création des dossiers
sudo mkdir /srv/ssl /srv/web /srv/mysql

# création du certificat SSL
sudo openssl req -new -x509 -keyout /srv/ssl/$cn\.pem -out /srv/ssl/$cn\.pem -days 3650 -nodes -subj "/C=$pays/ST=$region/L=$ville/O=$entreprise/OU=$entreprise/CN=$cn"

# restriction des droits sur le dossier /srv/ssl
sudo chmod -R 400 /srv/ssl/

# changement du dossier racine web de lighttpd
#sudo sed -i 's/\/var\/www\/html/\/srv\/web/' /etc/lighttpd/lighttpd.conf
sudo sh -c "echo '<?php phpinfo(); ?>' > /srv/web/index.php"
sudo chown -R www-data:www-data /srv/web
sudo chmod -R 775 /srv/web

# PRECONFIGURATION du ssl dans lighhtpd
#sudo sed -i "s/\/etc\/lighttpd\/server.pem/\/srv\/ssl\/$cn\.pem/" /etc/lighttpd/conf-available/10-ssl.conf

# activation des modules php ssl pour http https
#sudo service lighttpd start
#sudo lighty-enable-mod fastcgi-php
sudo a2enmod ssl

# desservir un site

# rajout de l'utilisateur $USER à  www-data
sudo adduser $USER www-data

# backup des configurations du web
sudo cp -rp /etc/mysql /srv/backup/
sudo cp -rp /etc/apache2 /srv/backup/

# création de liste détaillée avant déplacement
liste_dossier=('/' '/etc/ssl' '/var/lib')
for dossier in ${liste_dossier[@]}; do
	file_dossier=`echo $dossier | tr '/' '_'`
	sudo sh -c "ls $dossier > /srv/backup/$file_dossier"
done

# creation/déplacement du dossier /var/lib/mysql vers /srv/mysql
sudo cp -rp /var/lib/mysql /srv/
sudo rm -r /var/lib/mysql
sudo ln -s /srv/mysql /var/lib/mysql

# redémarrage du service lighttpd
sudo service apache2 restart
sudo service mysql restart
 
om_theend
