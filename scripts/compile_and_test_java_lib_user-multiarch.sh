#!/bin/bash

lib="$(ls $(pwd)/lib/* | tr '\n' ':')"
rm -rf bin 
mkdir -p bin 
javac -Xlint -g -deprecation -d bin src/*.java -classpath "${lib}"
cd bin
java -cp "${lib}" Main
cd ..
rm -rf bin 
