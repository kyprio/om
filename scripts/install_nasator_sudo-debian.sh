echo IN PROGRESS && exit 7
#!/bin/bash
IFS=$'\n\t'

# DEBUT TEST SUDO
clear
om_echotitle "INSTALL NASATOR"
[ $UID -eq 0 ] && echo -e $red"NE PAS EXECUTER AVEC ROOT\n" && exit 1
echo -e $yellow"mot de passe du super-user $USER :"$white
read -s mdp
sudo -k
echo $mdp | sudo -S echo -e $whitebig"mot de passe correct"$white 2> /dev/null
if [ ! $? -eq 0 ]; then
	echo -e $red"MOT DE PASSE INCORECT OU $USER N'EST PAS UN SUPER-USER"
	exit 2
fi
# FIN TEST SUDO

# PRE CONFIGURATION
###################

while true; do
	mdpsrv=$(whiptail --title "INSTALLATION NASATOR / MDP ROOT SRV" \
		--passwordbox "\n\nmot de passe de root des services (ex:mysql)" \
		20 70 3>&1 1>&2 2>&3) || exit 1
	mdpsrv2=$(whiptail --title "INSTALLATION NASATOR / MDP ROOT SRV CONFIRM" \
		--passwordbox "\n\nconfirmation mot de passe de root des services (ex:mysql)" \
		20 70 3>&1 1>&2 2>&3) || exit 1
	[ $mdpsrv == $mdpsrv2 ] && break
	whiptail --title "INSTALLATION NASATOR / MDP ROOT SRV ERROR" \
		--yesno "Erreur dans la saisie" --yes-button "Revenir" --no-button "Annuler" \
		20 70 3>&1 1>&2 2>&3 || exit 1
done
cn=$(whiptail --title "INSTALLATION NASATOR / CN" \
	--inputbox "\n\nINFORMATION SUR LE SERVEUR\nne saisir que des caractères alphanumeriques sans espace\nnom du nas (Common Name/Fully Qualified Domain Name, ex: www.domain.lan )" \
	20 70 "`hostname -f`" 3>&1 1>&2 2>&3) || exit 1
pays=$(whiptail --title "INSTALLATION NASATOR / PAYS" \
	--inputbox "\n\nacronyme en 2 lettres du pays(Country)" \
	20 70 "FR" 3>&1 1>&2 2>&3) || exit 1
region=$(whiptail --title "INSTALLATION NASATOR / REGION" \
	--inputbox "\n\nregion (State)" \
	20 70 "IDF" 3>&1 1>&2 2>&3) || exit 1  
ville=$(whiptail --title "INSTALLATION NASATOR / VILLE" \
	--inputbox "\n\nville (Locality)" \
	20 70 "EVRY" 3>&1 1>&2 2>&3) || exit 1    
entreprise=$(whiptail --title "INSTALLATION NASATOR / ENTREPRISE" \
	--inputbox "\n\nsociete (Organization et Organization Unit)" \
20 70 "ITIS" 3>&1 1>&2 2>&3) || exit 1
whiptail --title "INSTALLATION NASATOR / DEBUT" \
	--msgbox "\n\n$USER sera le seul utilisateur à pouvoir parcourir et modifier /srv/web du NAS" \
	20 70

# INSTALLATION
##############

# dossier
sudo mkdir -p /srv/stockage /srv/backup

# lien symmbolique pour stockage
sudo ln -s /srv/stockage /etc/skel/stockage
sudo ln -s /srv/stockage /home/$USER/stockage

# modification des valeurs par défaut de la création d'un utilisateur
#sudo sed -e -i 's/SHELL=.*/SHELL=\/bin\/bash/ ; s/# GROUP=100/GROUP=100/' /etc/default/useradd
sudo sed -e -i 's/SHELL=.*/SHELL=\/bin\/bash/' /etc/default/useradd

# ajout de l'utilisateur principal au groupe users
sudo adduser $USER users

# backup de la configuaration
sudo cp -p /etc/default/useradd /srv/backup/

source sudo_install_module_ssh.sh
source sudo_install_module_nfs.sh
#source sudo_install_module_samba.sh
#source sudo_install_module_web_http.sh
source sudo_install_module_web_https-http.sh
#source sudo_install_module_web_mysql-https-http.sh
source sudo_install_module_ufw.sh


# POST CONFIGURATION
####################


# ajout de l'option d'indexation des fichiers si pas de index.html
sudo sed -i '/^server.port/adir-listing.activate        = "enable"' /etc/lighttpd/lighttpd.conf

# FIN
whiptail --title "INSTALLATION NASATOR / INFORMATION" \
	--msgbox "\n\n$USER sera le seul utilisateur à pouvoir parcourir et modifier /srv/web du NAS" \
	20 70
whiptail --title "INSTALLATION NASATOR / INFORMATION" \
	--msgbox "\n\nFIN D'INSTALLATION" \
	20 70
 
om_theend
