#!/bin/bash
# option debug
#set -euo pipefail
#IFS=$'\n\t'
source "$HOME/om/scripts/base_script.sh" 2> /dev/null

om_echotitle "CONVERT WIKI TO MARKDOWN"

# PRECONFIGURATION
##################

wiki=${1}
md="${wiki}.md"

echo $wiki $md
CR=$(printf '\r')

# EXECUTION
###########


cp ${wiki} ${md}

sed -ie "
s/#----------// ;
s/#- \(.*\)/> \1\n> / ;
s/# \([a-z].*\)/#####        \1/;
" ${md}

sed -i ':a;N;$!ba;s/# \([A-Z].*\)\n/###      \1/g' ${md}



# POSTCONFIGURATION
###################


 
om_theend
