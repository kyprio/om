echo IN PROGRESS && exit 7
#!/bin/bash

userprin=`getent passwd | grep -P 1000 | cut -d: -f1`
cd /home/$userprin/om/scripts/
racine=`pwd`"/python"

echo -en $white"Le nom du script python sera"$yellow
read nomProg
prog="$racine/$nomProg".py

echo "# $nomProg" > $prog
echo '#!/usr/bin/python3' > $prog
echo '# -*-coding:utf8 -*' >> $prog
chmod +x $prog

vi + -c 'startinsert' $prog
 
om_theend
