# Ajout de var de couleur et de fonction pour faciliter les scripts 

Y="\\033[1;33m" R="\\033[1;31m" W="\\033[0;37m" B="\\033[1;37m" G="\\033[1;32m"

# constante d'environnement

OMDIR="$HOME/om/"

# CODE DE RETOUR ET NOMS RESERVES
#################################

# code de retour

#----------
#- 1 general errors
#- 2 misus of shell builtins
#- 3 missing file
#- 4 missing/wrong argument
#- 5 return code > 1
#- 6 missing package
#- 7 beta version/work in progress
#- 10
#- 20 = type utilisateur
#- 126	Command invoked cannot execute
#- 127	"command not found"
#- 128	Invalid argument to exit
#- 128+n	Fatal error signal "n"	
#- 130	Script terminated by Control-C
#- 255*	Exit status out of range
#----------

# valeur de variable non définie

# var="${var:-empty}" string
# var="${var:-0}" int
# var="${var:-false}" bool

# FONCTIONS AFFICHAGE
#####################

function om_header {
#- om_header -t "SCRIPT TITLE" -u user|sudo|root
#- affiche le titre du script, sa version et execute le test de permission du type utilisateur
  local opt
  local OPTARG
  local OPTIND
  local OPTERR

  while getopts :t:v:u: opt ; do
    case $opt in
      t) om_echotitle "${OPTARG}" ;;
      u) om_testuser "${OPTARG}" ;;
      \?) om_echoerror "Invalid option -${opt}" ; exit 4 ;;
      :) om_echoerror "Missing value to -${opt}" ; exit 4 ;;
    esac
  done

}

function om_echotitle {
#- om_echotitle "SCRIPT TITLE"
#- affiche le titre
  local omTitle="${@}"
  local omLine1="#  ${omTitle}  #"
  local omLine2=$(printf '%0.s#' $(eval echo {1..${#omLine1}} ))
  
  echo -e $Y"\n${omLine2}\n\n${omLine1}\n\n${omLine2}\n"
}

function om_echosubtitle {
#- om_echosubtitle "SUBTITLE"
#- affiche un soustitre ou un titre de niveau 2 séparant les grandes parties d'un script
  local omLine1="#  ${@}"
  local omLine2=$(printf '%0.s#' $(eval echo {1..${#omLine1}} ))

  echo -e $Y"${omLine1}\n${omLine2}\n"
}

function om_echoinfo {
#- om_echoinfo "message"
#- affiche un simple message d'information
  local omInfo="${@}"

  echo -e ${Y}"${omInfo}"${W}
}

function om_echoerror {
#- om_echoerror "message"
#- affiche un message d'erreur sans quitter
  local omError="${@}"
  
  echo -e ${R}"\n${omError}\n"${W} >&2
}

function om_echook {
#- om_echook "message"
#- affiche une information positive
  local omOk="${@}"
  
  echo -e ${G}"${omOk}"${W}
}

function om_echoreturn {
#- om_echoreturn "message"
#- affiche une confirmation de saisie utilisateur ou un reponse du système
  local omReturn="${@}"

  echo -e ${B}"${omReturn}"${W}
}

function om_theend {
#- om_theend
#- fonction a placer tout à la fin du script pour s'assurer qu'il a fini jusqu'au bout

  om_echoinfo "Le script s'est terminé avec succès.\n\n## THE END ##"${W}
}

function om_echolist {
#- om_echoliste "element1" "element2"
#- affiche une liste d'élément
  local omList=("${@}")
  local e

  for e in "${omList[@]}";
    do om_echoreturn "${e}"
  done
  echo
}

function om_echolistconfig {
#- om_echolistconfig "/path/file1" "/path/file2"
#- affiche la  liste des fichiers de configuration utilisé par le service installé dans le script
  local omListConfig=("${@}")
  local f

  om_echoinfo 'Voici la liste des fichiers de configurations utilisés :'
  for f in "${omListConfig[@]}";
    do om_echoreturn "${f}"
  done
  echo
}

function om_echolistlog {
#- om_echolistlog "/path/log1" "/path/log2"
#- affiche la  liste des fichiers de log liés au service installé dans le script
  local omListLog=("${@}")
  local f

  om_echoinfo 'Voici la liste des fichiers de log utiles :'
  for f in "${omListLog[@]}"
    do om_echoreturn "${f}"
  done
  echo
}


# FONCTIONS TEST USER
#####################

function om_testuser {
#- om_testuser <user>|<sudo>|<root>
#- example : om_testuser sudo
#- test le type d'utilisateur néccessaire pour lancer le script
  local omTypeUser="${1}"
  local pwdSudo

  case "${omTypeUser}" in
    root)
      [[ ${UID} -ne 0 ]] && { om_echoerror 'ROOT ONLY CANT RUN THIS SCRIPT' ; exit 20 ; }
      ;;
    sudo)
      om_readsecret pwdSudo "password for the super-user $USER :"
      sudo -k
      [[ "$((echo ${pwdSudo}; echo ${pwdSudo} echo ${pwdSudo} ) | sudo -S whoami 2> /dev/null)" != "root" ]] \
      || \
      [[ ${UID} -eq 0 ]] \
      && \
      { om_echoerro 'RUN ONLY WITH A SUPER-USER' ; exit 20 ; }
      ;;
    user)
      ;;
    *)
      om_echoerror "Le script ne définit pas un type d'utilisateur valide"
        exit 20
      ;;
  esac
}


# FONCTIONS SAISIES
###################

function om_readtext {
#- om_readtxt varName "message"
#- example : om_readtxt name "lastname"
#- example : om_readtxt name "lastname" "?"
#- demande une saisie utilisateur avec $2 et la saisie dans la variable ouverte nommé par $1
#- $3 permet de modifier la ponctuation par defaut de la demande
  local omVarName="${1}"
  local omMessage="${2}"
  local omPnc="${3:-:}"

  echo -en ${Y}"${omMessage} ${omPnc} "${B}
  read "${omVarName}"
  echo -e ${W}
}

function om_readsecret {
#- om_readsecret varName "message"
#- example : om_readsecret pwdMysql "password"
#- demande une saisie d'un mot de passe
  local omVarName="${1}"
  local omMessage="${2}"
  
  om_echoinfo "${omMessage}"
  read -s "${omVarName}"
  echo 
}

function om_readyesno {
#- om_readyesno <y>'|'<n>' varName 'valeuryes' 'valeurno' 'message'
#- pose une question $5 dont la réponse est de type Oui/Non
#- la réponse par defaut est définit dans $1
#- cette question à pour but de remplir une variable $2
#- avec les valeurs définies en $3 et $4.
#- la variable nommée par $2 est volontairement accessible par le script appelant la fonction
  local omYesNo="${1}"
  omVarName="${2}"
  local omVarValue
  local omValueYes="${3}"
  local omValueNo="${4}"
  local omMessage="${5}"
  local c

  if [ "${omYesNo}" == 'y' ]; then
    om_readtext c "${omMessage} [Y/n]" '?'
    c="$( echo ${c} | tr [:upper:] [:lower:])"
    case "${c}" in
      'n'|'no') omVarValue="${omValueNo}" ;;
      'y'|'yes'|*) omVarValue="${omValueYes}" ;;
    esac
  elif [ "${omYesNo}" == 'n' ]; then
    om_readtext c "${omMessage} [y/N]" '?'
    c="$( echo ${c} | tr [:upper:] [:lower:])"
    case "${c}" in
      'y'|'yes') omVarValue="${omValueYes}" ;;
      'n'|'no'|*) omVarValue="${omValueNo}" ;;
    esac
  fi
  eval ${omVarName}="${omVarValue}"
  echo
}

function om_readyesnocommand {
#- om_readyesnocommand <y>|<n>''commandyes' 'commandno' 'message'
#- pose une question $4 dont la réponse est de type Oui/Non
#- la réponse par defaut est définit dans $1
#- cette question à pour but d'executer la command $3 ou $4
#- en fonction de la réponse
  local omYesNo="${1}"
  local omCommand
  local omCommandYes="${2}"
  local omCommandNo="${3}"
  local omMessage="${4}"
  local c

  if [ "${omYesNo}" == 'y' ]; then
    om_readtext c "${omMessage} [Y/n]" '?'
    c="${c,,}"
    case "${c}" in
      'n'|'no') ${omCommandNo} ;;
      'y'|'yes'|*) ${omCommandYes} ;;
    esac
  elif [ "${omYesNo}" == 'n' ]; then
    om_readtext c "${omMessage} [y/N]" '?'
    c="${c,,}"
    case "${c}" in
      'y'|'yes') ${omCommandYes} ;;
      'n'|'no'|*) ${omCommandNo} ;;
    esac
  fi
  echo
}

# FONCTIONS SYSTEMES
####################

function om_reboot {
#- om_reboot
#- reboot la machine après avertissement
  sleep 1
  om_echoinfo "reboot nécessaire\n3sec pour fermer avant reboot CTRL+C\n"
  sleep 3
  reboot
}

function om_openssh {
#- om_openssh "ARGUMENT"
#- permet la création et l'utilisation d'une même session ssh entre plusieurs processus du script
  local omReturnCode

  if [ -z "${OM_KEY}" ] || [ -z "${OM_HOST}" ]; then
    om_log "remote_open needs OM_KEY (${OM_KEY}) and HOST (${OM_HOST}) to be set"
    exit 1
  fi

  om_log "Setting up shared ssh connection"
  if [ -z "${OM_DRYRUN}" ]; then
    if [ -n "${OM_CONTROLDIR}" ] || [ -n "${OM_CONTROLOPTS}" ]; then
    om_log "SSH MASTER CONNECTION ALREADY UP: controldir : '${OM_CONTROLDIR}' controlopts : '${OM_CONTROLOPTS}'"
    return
    fi

    om_log "ssh [...] -C -i ${OM_KEY} admin@${OM_HOST} $@"
    OM_CONTROLDIR="$(mktemp -d)"

#- commande principale de lancement de la session ssh
    ssh ${SSH_OPTIONS} -o ControlMaster=auto -o "ControlPath=${OM_CONTROLDIR}/%r-%h:%p" -i "${OM_KEY}" "admin@${OM_HOST}" -Nf
    omReturnCode="${?}"
  
    OM_CONTROLOPTS="-o ControlMaster=auto -o ControlPath=${OM_CONTROLDIR}}/%r-%h:%p"
    om_log "SSH MASTER CONNECTION UP: controldir: '${OM_CONTROLDIR}' controlopts: ${OM_CONTROLOPTS}'"

    export OM_CONTROLOPTS
    export OM_CONTROLDIR

    return "${omReturnCode}"

  else
    om_log "OM_DRYRUN: ssh -q -o ControlMaster=auto -o ControlPath=<tmp>/%r-%h:%p -o ConnectTimeout=10 -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no -C -i ${OM_KEY} admin@{OM_HOST} ${@}"
  fi
}

function om_rhelinstall {
#- om_rhelinstal package1 [...]
#- will choose between DNF or YUM and lauch : yum install package1
#- 5 
  local omPkg="${@}"
  local e

  for e in 'yum' 'dnf'; do
    [[ -n "$(basename $(which ${e} ) 2> /dev/null)" ]] && omInstaller="${e}"
  done

  if [[ -z "${omInstaller}" ]] ; then
    om_echoerror "The OS is not a RHEL"
    exit 6
  fi
  
  sudo $omInstaller install "${omPkg}"
}

function om_rhelinstallroot {
#- om_rhelinstalroot package1 [...]
#- fonction a utiliser pour les scripts root
#- will choose between DNF or YUM and lauch : yum install package1
  local omPkg="${@}"
  local e

  for e in 'yum' 'dnf'; do
    [[ -n "$(basename $(which ${e} ) 2> /dev/null)" ]] && omInstaller="${e}"
  done

  if [[ -z "${omInstaller}" ]] ; then
    om_echoerror "Neither YUM nor DNF has been found"
    exit 6
  fi
  
  
  $omInstaller install "${omPkg}"
}

# FONCTIONS MATERIELS
#####################

function om_listether {
  #sudo ifconfig -a | grep HW | cut -d " " -f 2 | grep -v lo
  ip -o link show | sed 's/^.*: \(.*\): .*/\1/'
}




# FONCTIONS DEBUG ET DEROULEMENT SCRIPT
#######################################

function om_debugvar {
#- om_debugvar var1 [var2...]
#- print the value of each variable
#- do not write the $ before each variables
  local omVar=("${@}")

  for i in "${omVar[@]}"; do
    om_echoinfo "${i} = ${B}$(eval echo \${${i}})"
  done
  echo
}     


#function om_pipestatus {
##- om_pipestatus
##- affiche le code retour de chaque commande du précédent pipe
#  local n
#  local i=0
#
#  for n in "${PIPESTATUS[@]}" ; do
#    if [[ "${n}" -ne 0 ]]; then
#      om_echoerror "pipe ${i} : ${n}"
#    else
#      om_echoreturn "pipe ${i} : ${n}"
#    fi
#      ((i++))
#  done
#}

function om_log {
#- om_log "${OM_DEBUG}"
#- affiche les logs om_log si la variable OM_DEBUG est initialisée
  if [ -n ${OM_DEBUG} ] ; then
    om_echoinfo "[DEBUG] ${@}"
  fi
}

function om_continue {
#- om_continue
#- petite alerte demandant confirmation de continuer l'execution du script
#- il est utile de le placer avant que le script ne modifie vraiment des éléments du système

  om_echoinfo 'ENTER to continue or CTRL+C to exit'
  read 
}

function om_step {
#- om_step "TITLE"
#- appelle om_continue en indiquant à quel étape on est avec $1 mis en forme par om_echosubtitle
  local omSubtitle="${1}"

  om_echosubtitle "${omSubtitle}"
  om_continue
}

function om_inprogress {
#- om_inprogress
#- avertissement que ce script est en cours de rédaction et n'est donc pas fonctionnelle

  om_echoerror "WORK IN PROGRESS\nREAD THE SCRIPT BEFORE USING IT"
  om_continue
}

function om_checkreturn {
#- om_checkreturn "${?}" ['c'|'q'] ['information about the error'] 
#- test le retour de la commande précédente
#- si elle s'est mal terminée, affiche son code et le message prévu par $3
#- si pas de $3, le script se ferme en exit 5
  local omReturnCode="${1}"
  local omInfoError="${2:-q}"  
  local omError="${3:-ERREUR LORS DE L\'EXECUTION DE LA COMMANDE PRECEDENTE}"

  if [[ ! ${omReturnCode} -eq 0 ]]; then
    om_echoerror "${omError}\nerreur : "${B}"${omReturnCode}"
    if [[ "${omInfoError}" = 'q' ]]; then
      exit 5
    fi
  fi
}

function om_version {
#- om_version "0.1"
#- affiche le numero de version $1
  local omVersion="${1}"

  om_echoinfo "# $(basename "${0}") version "${B}" ${omVersion}"
  exit 0
}

# FONCTIONS GESTION DE FICHIER
##############################

function om_backup {
#- om_backupsudo '/etc/hosts' '/etc/bashrc'
#- fais une copie_bak à coté des fichiers envoyés en $@
  local omList="${@}"
  local f

  om_echoinfo "\nbackup des fichiers confs"
  for f in ${omList}; do
    if [ -f "${f}_bak" ]; then
      om_echoinfo 'le backup '${B}"${f}_bak"${Y}' existe déjà'
    else
      sudo cp -rp "${f}" "${f}_bak" && om_echoinfo 'backup de '${B}"${f}"${Y}' vers '${B}"${f}_bak"
    fi
  done
}

function om_backuproot {
#- om_backup '/etc/hosts' '/etc/bashrc'
#- fonction a utiliser pour les scripts root
#- fais une copie_bak à coté des fichiers envoyés en $@
  local omList="${@}"
  local f

  om_echoinfo "\nbackup des fichiers confs"
  for f in ${omList}; do
    if [ -f "${f}_bak" ]; then
      om_echoinfo 'le backup '${B}"${f}_bak"${Y}' existe déjà'
    else
      cp -rp "${f}" "${f}_bak" && om_echoinfo 'backup de '${B}"${f}"${Y}' vers '${B}"${f}_bak"
    fi
  done
}

function om_backupcentral {
#- om_backupcentral "ARGUMENT"
#- om_backupcentralsudo /srv/backup/ /etc/fichier1 [/etc/fichier2]
#- fais un copie centralisée dans un dossier ${dernierargument} des fichiers $@
  local omDest="${1}"
  local omList=("${@}")
  local omNbDest="${#}"
  local f
  
  om_echoinfo "\nbackup centralisé dans"${B}" ${omDest}/"
  sudo mkdir -p ${omDest}
  for ((i=1; i<"${omNbDest}"; i++)); do
    sudo cp -p "${omList[${i}]}" "${omDest}/"
    om_echoreturn "${omList[${i}]}"
  done
}

function om_backupcentralroot {
#- om_backupcentral /srv/backup/ /etc/fichier1 [/etc/fichier2]
#- fonction a utiliser pour les scripts root
#- fais un copie centralisée dans un dossier ${dernierargument} des fichiers $@
  local omDest="${1}"
  local omList=("${@}")
  local omNbDest="${#}"
  local f
  
  om_echoinfo "\nbackup centralisé dans"${B}" ${omDest}/"
  mkdir -p ${omDest}
  for ((i=1; i<"${omNbDest}"; i++)); do
    cp -p "${omList[${i}]}" "${omDest}/"
    om_echoreturn "${omList[${i}]}"
  done
}

# FONCTION GESTION VARIABLES ET ARGUMENTS
#########################################

function om_unifyargs {
#- om_unifyargs "argment" [argument...]
#- liste dans dans ordre croissant et unique les arguments envoyés dans une variable global omUnifyArgs
  local a
  local b
  for a in "${@}" ; do
    b="${b}\n${a}"
  done

  omUnifyArgs="$(echo -e ${b} | sort | uniq | sed -n '2,$'p)"
}
