echo IN PROGRESS && exit 7
#!/bin/bash
source "$HOME/om/scripts/base_script.sh" || echo "base_script.sh manquant" && exit 1
om_testuser root
om_echotitle "CHANGE IP"


# ce script permet de fixer ou non des adresse ip par rapport à son interface réseau
# ce script écrase le fichier /etc/network/interfaces
om_echoinfo "liste des interfaces disponibles :"
listDev=`ifconfig -a | grep HW | cut -d " " -f 1 | grep -v lo`
ifconfig -a | grep HW | cut -d " " -f 1 | grep -v lo

# pre-génération du fichier /etc/network/interfaces
echo "# boucle locale
auto lo
dev lo inet loopback
"> /etc/network/interfaces

# PRECONFIGURATION et génération du fichier pour chaque interface trouvée
for dev in ${listDev[@]}; do
	
	om_echoinfo "Config actuelle pour $dev :"
	om_echoreturn "`ifconfig $dev`"
	om_readtext fixe "Nouvelle configuration de $dev : static/dhcp/off ?"

	case $fixe in
		"static")
			oldIp=`ifconfig eth0 | awk '/inet ad/{print substr($2,6)}'`
			oldMask=`ifconfig eth0 | awk '/inet ad/{print substr($4,6)}'`
			oldGw=`route -n | grep 'UG[ \t]' | awk '{print $2}'`
			oldDn=`hostname -d`
			
			om_readtext newIp "adresse ip pour $dev ($oldIp)"
			[ -z "$newIp" ] && newIp=$oldIp
			om_readtext newMask "newMask ($oldMask)"
			[ ! $newMask ] && newMask=$oldMask
			om_readtext newGw "default gateway (remplir s'il s'agit de l'accès à internet) ($oldGw)"

			echo -e "\n# interface $dev\nauto $dev\nallow-hotplug $dev\ndev $dev inet static\n\taddress $newIp\n\tnewMask $newMask" >> /etc/network/interfaces
			if [ ! -z $newGw ]; then
				om_readtext newDn "nom du domaine actuel de type labo.lan ($oldDn)"
				[ -z "$newDn" ] && newDn=$oldDn ; [ -z "$newDn" ] && newDn="labo.lan"
				om_readtext dns1 "ip du serveur DNS primaire (8.8.8.8)"
				[ -z "$dns1" ] && dns1="8.8.8.8"
				om_readtext dns2 "ip du serveur DNS secondaire (8.8.4.4)"
				[ -z "$dns2" ] && dns2="8.8.4.4"
				echo -e "\tgateway $newGw\n\tdns-nameservers $dns1 $dns2" >> /etc/network/interfaces
				echo -e "# resolv.conf écrit par root_change_ip.sh `date`\nsearch $newDn\nnameserver $dns1\nnameserver $dns2" > /etc/resolv.conf
			fi
			;;
		"dhcp")
			echo -e "\n# interface $dev\nauto $dev\nallow-hotplug $dev\ndev $dev inet dhcp" >> /etc/network/interfaces
			;;
		"off"|"")
			om_echoreturn "$dev ne sera pas renseignée : off"
	esac

done

# vidage des adresses ip et démarrage du service
for dev in ${listDev[@]}; do
	ip addr flush dev $dev
done
service networking restart
systemctl daemon-reload

om_chechreturn "${?}" "Le service n'a pas pu redémarrer\nreboot conseillé pour appliquer les changements" "retour de la commande ifconfig :\n`ifconfig`"

om_theend
