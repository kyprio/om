#echo IN PROGRESS && exit 7
#!/bin/bash
source "$HOME/om/scripts/base_script.sh" 2> /dev/null

om_echotitle "DEPLOY OM"


# backup et création d'un lien symbolique vers om/pref/_.bashrc
test ! -L ~/.bashrc && mv ~/.bashrc ~/.bashrc_bak 2> /dev/null
test ! -L ~/.inputrc && mv ~/.bashrc ~/.inputrc_bak 2> /dev/null
ln -fs ~/om/pref/_.bashrc ~/.bashrc
ln -fs ~/om/pref/_.inputrc ~/.inputrc

# idem pour vimrc
test ! -L ~/.vim/vimrc && mv ~/.vim/vimrc ~/.vim/vimrc_bak 2> /dev/null
mkdir -p ~/.vim && ln -fs ~/om/pref/_.vim_vimrc ~/.vim/vimrc
 
# git hook
echo '''#!/bin/sh

bash ${HOME}/om/scripts/clean_git_repo_user-multiarch.sh "${HOME}/om"''' > ${HOME}/om/.git/hooks/pre-commit
chmod +x ${HOME}/om/.git/hooks/pre-commit

om_theend
