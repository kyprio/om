#!/bin/bash

OPTIONS=$1
LOGPATH=$2
DATE=$3

http_check() {
# renvoit le nombre de ligne pour chaque code de retour trouvé
POS=$(head -1 $LOGPATH | sed 's/ /\n/g' | nl |grep -E "20/*|30/*|40/*|50/*" |  egrep -x '.{0,10}' | awk '{print $1}')
cut -d' ' -f $POS $LOGPATH | sort -n | uniq -c 
}


octet_check() {

POS=$(head -1 $LOGPATH | sed 's/ /\n/g' | nl |grep -E "20/*|30/*|40/*|50/*" |  egrep -x '.{0,10}' | awk '{print $1}')
POS=$((POS+1))
OCTET_RESULT=$(echo "scale=4 ; $(cut -d' ' -f $POS $LOGPATH |awk '{ sum+=$1} END {print sum}') / 1024 / 1024 "  | bc) 
echo $OCTET_RESULT "Go"
}


main() {
 case $OPTIONS in
 http_check)
     http_check
     ;;
 ip_check)
     ip_check
     ;;
 octet_check)
     octet_check
     ;;

 *)
     echo "option non existante !"
     echo "options disponibles : http_check; ip_check; octet_check"
     ;;
 esac
}

main "@"

