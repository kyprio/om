liste=$(pct list | grep -v VMID | awk '{print $1}')
case "${1}" in
  'start')
    for i in ${liste[@]}; do
      echo $i
      pct start $i || break
    done
    ;;
  'stop')
    for i in ${liste[@]}; do
      echo $i
      pct stop $i
    done
    ;;
  'restart')
    for i in ${liste[@]}; do
      echo $i
      pct stop $i
      pct start $i || break
    done
    ;;
  *)
    echo 'argument invalide'
    ;;
esac
