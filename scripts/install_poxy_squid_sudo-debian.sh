echo IN PROGRESS && exit 7
#!/bin/bash
source "$HOME/om/scripts/base_script.sh" 2> /dev/null

# DEBUT TEST SUDO
clear
om_echotitle "INSTALL SQUID"
[ $UID -eq 0 ] && echo -e $red"NE PAS EXECUTER AVEC ROOT\n" && exit 1
echo -e $yellow"mot de passe du super-user $USER :"$white
read -s mdp
sudo -k
echo $mdp | sudo -S echo -e $whitebig"mot de passe correct"$white 2> /dev/null
if [ ! $? -eq 0 ]; then
	echo -e $red"MOT DE PASSE INCORECT OU $USER N'EST PAS UN SUPER-USER"
	exit 2
fi
# FIN TEST SUDO

# PRECONFIGURATION

# PRECONFIGURATION saisie_variable
echo -e "adresse ip réseau/masque(entier) :"$whitebig
read ipmasq
echo -e "FQDN proxy :"$whitebig
read fqdn

#acl $fqdn src $ipmasq >
#acl SSL_ports port 443		# https

# installation
sudo apt-get install -y squid

# backup des fichier de configuration

list_conf=("/etc/squid/squid.conf")
for file in ${list_conf[@]}; do
	if [ -f "$file\_bak" ]; then
		om_echoinfo "le backup $file\_bak existe déjà"
	else
		sudo mv $file $file\_bak && echo -e $yellow"\nbackup de "$whitebig"$file"$yellow" vers "$whitebig"$file\_bak\n"$white
	fi
done

# liste des fichier conf

om_echoinfo "Voici la liste des fichiers de configurations utilisés :"
for file in ${list_conf[@]}; do echo $file
done; echo -e $white

# liste des fichier log

liste_log=("/var/log/syslog")
om_echoinfo "Voici la liste des fichiers de log à vérifier :"
for file in ${liste_log[@]}; do echo $file
done; echo -e $white
 
om_theend
