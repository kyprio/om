echo IN PROGRESS && exit 7
#!/bin/bash

source "$HOME/om/scripts/base_script.sh" 2> /dev/null
# DEBUT TEST SUDO
clear
om_echotitle "MOUNT SSHFS"
[ $UID -eq 0 ] && echo -e $red"NE PAS EXECUTER AVEC ROOT\n" && exit 1
echo -e $yellow"mot de passe du super-user $USER :"$white
read -s mdp
sudo -k
echo $mdp | sudo -S echo -e $whitebig"mot de passe correct"$white 2> /dev/null
if [ ! $? -eq 0 ]; then
	echo -e $red"MOT DE PASSE INCORECT OU $USER N'EST PAS UN SUPER-USER"
	exit 2
fi
# FIN TEST SUDO

# vérification des outils néccessaires

om_echoinfo "\nCheck de la configuration minimale...\n"
if [ -n "`sudo dpkg -l | grep sshfs`" ]; then
	echo -e $whitebig"sshfs bien installé"$white
else
	echo -e $whitebig"sshfs non installé sur le client"$white
	requis=false
fi
[ "$requis" == "false" ] && exit 1

# PRECONFIGURATION et liste

IFS=$'\n\t'

liste_ip=(172.25.2.20 172.25.2.22 192.168.193.19 158.255.108.172)
liste_user=(admin ${USER})
liste_port=(22 2222 2219)
liste_source=(/ /home/${USER}/ /share/)
liste_ptmontage=(/media/distant/ ~/distant/)

# choix de la configuration

#choix_ip
om_echoinfo "choix d'une IP ou "A" autre"
for (( i=0; i<=$((${#liste_ip[@]} - 1)); i++ )); do echo "$i = ${liste_ip[$i]}";done
read -sn 1 choix_ip
case $choix_ip in
	A)
		echo -ne $yellow"IP = "$whitebig
		read ip
		;;
	"")
		ip=${liste_ip[0]}
		;;
	*)
		ip=${liste_ip[$choix_ip]}
		;;
esac

#choix_user
echo -e $yellow"choix de l'utilisateur ou "A" autre: "$whitebig
for (( i=0; i<=$((${#liste_user[@]} - 1)); i++ )); do echo "$i = ${liste_user[$i]}"; done
read -sn 1 choix_user
case $choix_user in
	A)
		echo -ne $yellow"utilisateur = "$whitebig
		read user
		;;
	"")
		user=${liste_user[0]}
		source=${liste_source[0]}
		;;
	*)
		user=${liste_user[$choix_user]}
		source=${liste_source[$choix_user]}
		;;
esac

#choix_port
echo -e $yellow$"choix du port ou "A" autre"$whitebig
for (( i=0; i<=$((${#liste_port[@]} - 1)); i++ )); do echo "$i = ${liste_port[$i]}"; done
read -sn 1 choix_port
case $choix_port in
	A)
		echo -ne $yellow"port = "$whitebig
		read port
		;;
	"")
		port=${liste_port[$choix_port]}
		;;
	*)
		port=${liste_port[$choix_port]}
		;;
esac

#choix_source
echo -e $yellow"choix d'un dossier source ou "A" autre"$whitebig
liste_source=( ${liste_source[@]} $(sudo showmount -e --no-headers $ip) )
for (( i=0; i<=$((${#liste_source[@]} - 1)); i++ )); do echo "$i = ${liste_source[$i]}";done
read -sn 1 choix_source
case $choix_source in
	A)
		echo -ne $yellow"chemin absolu du dossier source = "$whitebig
		read source
		;;
	"")
		source=${liste_source[0]}
		;;
	*)
		source=${liste_source[$choix_source]}
		;;
esac

#choix_ptmontage
echo -e $yellow"choix d'un point de montage ou "A" autre"$whitebig
for (( i=0; i<=$((${#liste_ptmontage[@]} - 1)); i++ )); do echo "$i = ${liste_ptmontage[$i]}";done
read -sn 1 choix_ptmontage
case $choix_ptmontage in
	A)
		echo -ne $yellow"chemin absolu du point de montage = "$whitebig
		read ptmontage
		;;
	"")
		ptmontage=${liste_ptmontage[0]}
		;;
	*)
		ptmontage=${liste_ptmontage[$choix_source]}
		;;
esac

# création du point de montage pour l'utilisateur en cours et montage du dossier distant

sudo mkdir -p $ptmontage
sudo chown $USER:$USER $ptmontage
sshfs -p $port $user@$ip:$source $ptmontage
nautilus $ptmontage

# information

echo -e $yellow"executer ce fichier pout démonter et supprimer le point de montage :"

nomProg=`echo "umount_"$ip | tr '.' '-' | tr '/' '_'`
prog="$HOME/$nomProg"".sh"
echo -e $whitebig"$prog"$white
echo '#!/bin/bash' > $prog
echo -e 'echo -e $yellow"\\n## '`echo $nomProg | tr 'a-z' 'A-Z' | tr '_' ' '`' ##\\n"$white'"\n\n" >> $prog
echo "fusermount -u $ptmontage" >> $prog
echo "sudo rm -r $ptmontage" >> $prog
echo "sudo rm $prog" >> $prog
chmod +x $prog

 
om_theend
