#!/bin/bash


# PRECONFIGURATION
##################

# EXECUTION
###########

echo "nom de l'utilisateur qui remplacera pi : "
read username
username=${username:-sysadmin}

usermod pi -l ${username} -d /home/${username} -m
groupmod pi -n ${username}
echo ${username}:@sys@ | chpasswd
sudo passwd -l root

# désactiver le ssh pour root
sed -i 's/PermitRootLogin yes/#PermitRootLogin prohibit-password/' /etc/ssh/sshd_config
systemctl restart ssh


# POSTCONFIGURATION
###################

echo "L'utilisateur pi a été remplacé entièrement par ${username} de mot de passe @sys@"
 
