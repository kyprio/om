echo IN PROGRESS && exit 7
#!/bin/bash
# option debug
set -euo pipefail
IFS=$'\n\t'
source "$HOME/om/scripts/base_script.sh" 2> /dev/null

# DEBUT TEST SUDO
clear
om_echotitle "INSTALL PROXY HTTP"
[ $UID -eq 0 ] && whiptail --title "TEST SUDO" --msgbox "NE PAS EXECUTER AVEC ROOT" 20 70 && exit 1
mdp=$(whiptail --title "TEST SUDO" --passwordbox "Mot de passe du super-user $USER :" 20 70 3>&1 1>&2 2>&3)
sudo -k
( echo $mdp; echo $mdp echo $mdp ) | sudo -S echo -e $whitebig"mot de passe correct"$white 2> /dev/null
if [ ! $? -eq 0 ]; then
	echo -e $red"MOT DE PASSE INCORECT OU $USER N'EST PAS UN SUPER-USER"
	exit 2
fi
# FIN TEST SUDO


# PRECONFIGURATION
##################


# récupération des info réseaux
# en partant du principe qu'il n'y a qu'une IP en static
iface=`grep "static" /etc/network/interfaces | cut -d" " -f2`
subnet=`echo $iface| sipcalc - | grep "Network address" | awk -F "- " '{print $2}'`
netmask=`echo $iface | sipcalc - | grep "mask" | grep -vP "bits|hex" | awk -F "- " '{print $2}'`
netmaskbits=`echo $iface | sipcalc - | grep "mask (bits)" | awk -F "- " '{print $2}'`
old_begin=`echo $iface | sipcalc - | grep "Usable range" | awk -F "- " '{print $2}'`
old_end=`echo $iface | sipcalc - | grep "Usable range" | awk -F "- " '{print $3}'`
gw=`grep -A 3 "static" /etc/network/interfaces | grep "gateway" | cut -d" " -f2`
old_dn=`hostname -d` ; [ -z "$old_dn" ] && old_dn="labo.lan"
old_dns1="8.8.8.8"
old_dns2="8.8.4.4"

om_echoerror "OPTION DEBUG set -euo pipefail ACTIVEE"


# INSTALLATION
##############


sudo apt-get install -y squid3


# POSTCONFIGURATION
###################

cd /etc/squid3
sudo mv squid.conf squid.conf.bak 
sudo cat squid.conf.bak | egrep -v -e '^[[:blank:]]*#|^$' > squid.conf 

sudo -i sed "/acl CONNECT method CONNECT/aacl LocalNet src $subnet\/$netmaskbits" squid.conf
sudo -i sed "/http_access allow localhost/ahttp_access allow LocalNet" squid.conf

sudo service squid3 restart

om_echoinfo "Squid3 utilise par defaut le port 3128 en HTTP"

om_echoinfo "FIN INSTALL PROXY HTTP"
 
om_theend
