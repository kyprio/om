#!/bin/bash
set -euo pipefail
source "$HOME/om/scripts/base_script.sh" || ( echo "base_script.sh manquant" && exit 1 )

if [ -z "${*}" ]; then
  echo "prenom_nom en argument"
  exit
fi

clientdir='/etc/openvpn/client'
keydir='/etc/openvpn/easy-rsa/keys'
easyrsadir='/etc/openvpn/easy-rsa'
ipwan="$(curl v4.ifconfig.co 2> /dev/null)"
def_vpnport=1194
om_readtext vpnport "port VPN de la box internet (coté WAN)"
[[ -z "$vpnport" ]] && vpnport=$def_vpnport

om_echoinfo "Création des certificats client"
om_echoinfo "il est conseillé d'ajuster les valeurs par défauts"
om_echoinfo "répondez y aux questions"


#dépendance
apt install unzip zip -y

for userl in $(echo "${*}"); do
  userldir="${clientdir}/${userl}_vpn"
  cd ${easyrsadir}
  source vars
  
  # création des clés client
  bash build-key "${userl}"

  mkdir -p ${userldir}
  
  cd ${keydir}

  # création du  p12 et copie du secret ta.key
  openssl pkcs12 -export -in "${userl}.crt" -inkey "${userl}.key" -certfile ca.crt -out "${userldir}/${userl}.p12"
  cp ta.key "${userldir}"

  cd ${userldir}

  # création de la config openvpn client
  echo """
client
dev tun
proto udp
remote $ipwan $vpnport
resolv-retry infinite
nobind
persist-key
persist-tun
pkcs12 ${userl}.p12
remote-cert-tls server
tls-auth ta.key 1
cipher AES-256-CBC
verb 3
""" > ${userl}.conf
  
  cd ${clientdir}

  # création de l'archive complete du dossier client à envoyer au client
  zip -r ~/${userl}_vpn.zip ${userl}_vpn

done

om_echoinfo "Les archives à envoyer aux clients sont dans $HOME"
