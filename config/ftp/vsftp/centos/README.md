# README

sous CENTOS

* VSFTPD configuré en mode SSL
* les utilisateurs sont chroot dans leur home
* le chroot autorise que les utilisateurs écrivent dans leur dossier (sinon il faut retire w du home)

## SOURCES
- https://www.digitalocean.com/community/tutorials/how-to-configure-vsftpd-to-use-ssl-tls-on-a-centos-vps
- https://www.liquidweb.com/kb/error-500-oops-vsftpd-refusing-to-run-with-writable-root-inside-chroot-solved/

