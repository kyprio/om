# README

* VSFTPD configuré en mode SSL
* les utilisateurs sont chroot dans leur home
* le chroot autorise que les utilisateurs écrivent dans leur dossier (sinon il faut retire w du home)

## SOURCES

- https://www.liquidweb.com/kb/error-500-oops-vsftpd-refusing-to-run-with-writable-root-inside-chroot-solved/
