# CONFIG om/config

Ensemble de fichier de configuration des différents paquets installables sur les système d'exploitations libres.
Il convient de ne pas modifier l'arbisresnce ni les noms des fichiers sans s'être ssure d'avoir modifier les scripts faisant appels au fichiers qui en fisait apelle

## Usage

Le but est de pouvoir s'inspirer ou copier/coller le conteu de ses fichiers.

## Contributing

1. Fork it!
2. Create your feature branch: `git checkout -b my-new-feature`
3. Commit your changes: `git commit -am 'Add some feature'`
4. Push to the branch: `git push origin my-new-feature`
5. Submit a pull request :D

## Credits

Kyprio CenKeneno

## License

GPLv3
