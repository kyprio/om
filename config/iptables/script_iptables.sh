#!/bin/bash

# suppression des regles existantes
iptables -t filter -F INPUT -j LOG --log-prefix "iptables INPUT dropped" --log-level debug
iptables -t filter -F OUTPUT

# ferme les portes
iptables -t filter -P INPUT DROP 
iptables -t filter -P OUTPUT DROP

# ouverture de la boucle locale
iptables -t filter -A INPUT -i lo -j ACCEPT
iptables -t filter -A INPUT -o lo -j ACCEPT

# ouverture du DNS TCP UDP
iptables -t filter -A INPUT -i eth0 -d 8.8.8.8 -p tcp --dport 53 -j ACCEPT
iptables -t filter -A INPUT -i eth0 -d 8.8.8.8 -p udp --dport 53 -j ACCEPT
iptables -t filter -A OUTPUT -o eth0 -s 8.8.8.8 -p tcp --dport 53 -j ACCEPT
iptables -t filter -A OUTPUT -o eth0 -s 8.8.8.8 -p udp --dport 53 -j ACCEPT

# ouverture du port 80
iptables -t filter -A INPUT -i eth0 -p tcp --dport 80 -j ACCEPT
iptables -t filter -A OUTPUT -o eth0 -p tcp --dport 80 -j ACCEPT

# ouverture du port 123 TCP UDP
iptables -t filter -A INPUT -i eth0 -p tcp --dport 123 -j ACCEPT
iptables -t filter -A INPUT -i eth0  -p udp --dport 123 -j ACCEPT
iptables -t filter -A OUTPUT -o eth0 -p tcp --dport 123 -j ACCEPT
iptables -t filter -A OUTPUT -o eth0 -p udp --dport 123 -j ACCEPT

# autorisation aux reponses des requetes etablies
iptables -t filter -A INPUT -i eth0 --state ESTABLISHED -j ACCEPT
iptables -t filter -A OUTPUT -o eth0 --state ESTABLISHED -j ACCEPT 

# NAT / Masquerade intérieur vers extérieur
iptable -t nat -A POSTROUTING -s 172.16.1.0/26 -o eth0 -j MASQUERADE

# PAT / NAT de port vers ip intérieur 3921 extérieur -> 3389 intérieur
iptables -t nat -A PREROUTING -i eth0 -p tcp -m tcp --dport 3921 -j DNAT --to-destination 172.16.1.21:3389

