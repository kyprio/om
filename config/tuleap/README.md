# README

Configuration de Tuleap pour être accessible derrière un parefeu avec :
- port coté WAN 1980 et 1943
- port coté Tuleap 80 et 443
- connecté à un LDAP
- tourne sur un CentOS7, cf installation depuis site officielle très claire

Je déconseille cette application qui est impossible à configurer proprement derrière un reverse proxy HTTPS/HTTP, et qui bug (erreur lors de suppression de UserStory dans le backlog)
