export lastexport="/srv/ldap/ldifs/exports/$(ls -t /srv/ldap/ldifs/exports/ | head -1)"

alias cdexports='cd /srv/ldap/ldifs/exports' # aller vers export
alias cdmod='cd /srv/ldap/ldifs/mod' # aller vers mod
alias cdadd='cd /srv/ldap/ldifs/add' # aller vers add


# fonction pour rechercher dans les export ldap

function getlistuid {
cat ${lastexport} | sort | sed -n /uidNumber/p | uniq
}

function getgidlist { # grep le gid d'une liste $1
grep -A 5 "dn: cn=$1,ou=groups" ${lastexport} | egrep "groups|gidNumber" --color=no
}

function getdn { # grep l'uid d'un utilisateur $1
grep "^dn: " ${lastexport} | sort | egrep $1
}

function getgidgroup { # grep les gid associé à un organisation $1
grep -A 5 "dc=$1" ${lastexport} | grep gidNumber | sort
}

function getlastgidlet { # affiche le dernier gidNumber LeT 10xx
egrep "gidNumber: 10[0-9][0-9]" ${lastexport} | sort | tail -1
}

function getlastgidofgroup { # affiche le dernier gidNumber LeT 10xx
grep -A 5 "dc=$1" ${lastexport} | grep gidNumber | sort | tail -1
}

function getinfo { # affiche la fiche de l'utilisateur $1 
cat $lastexport  | sed -n "/# $1/,/^$/p"
}
