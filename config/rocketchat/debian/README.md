# README

Cf documentation officielle
https://rocket.chat/docs/installation/manual-installation/debian/
La dernière version plantait durant le setup-wizard (configuration web) avec un 'too-many-requests'
J'ai alors modifier le lien de téléchargement de l'archive rocketchat en spécifiant la dernière version stable testé indiqué sur la page (0.70.0 en Février 2019) 

```
# INSTALLATION DE DEPENDANCE ET MONGODB
apt update
apt dist-upgrade -y
apt install -y vim tcpdump lynx curl
apt install dirmngr && apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv 2930ADAE8CAF5059EE73BB4B58712A2291FA4AD5
echo "deb http://repo.mongodb.org/apt/debian stretch/mongodb-org/3.6 main" | tee /etc/apt/sources.list.d/mongodb-org-3.6.list
apt update && apt install -y curl && curl -sL https://deb.nodesource.com/setup_8.x | bash
apt install -y build-essential mongodb-org nodejs graphicsmagick
npm install -g inherits n && n 8.11.3

# INSTALLATION DE ROCKETCHAT
# ATTENTION A LA VERSION RECUPERE
curl -L https://releases.rocket.chat/0.70.0/download -o /tmp/rocket.chat.tgz
tar -xzf /tmp/rocket.chat.tgz -C /tmp
cd /tmp/bundle/programs/server && npm install
mv /tmp/bundle /opt/Rocket.Chat

# CONFIGURATION DU SERVICE
useradd -M rocketchat && usermod -L rocketchat
chown -R rocketchat:rocketchat /opt/Rocket.Chat
echo -e "[Unit]\nDescription=The Rocket.Chat server\nAfter=network.target remote-fs.target nss-lookup.target nginx.target mongod.target\n[Service]\nExecStart=/usr/local/bin/node /opt/Rocket.Chat/main.js\nStandardOutput=syslog\nStandardError=syslog\nSyslogIdentifier=rocketchat\nUser=rocketchat\nEnvironment=MONGO_URL=mongodb://localhost:27017/rocketchat ROOT_URL=https://rocket.librescargot.fr/ PORT=3000\n[Install]\nWantedBy=multi-user.target" | tee /lib/systemd/system/rocketchat.service

vi /lib/systemd/system/rocketchat.service
# revoir les valeur ROOT_URL url définitive derrière le reverse proxy : https://rocket.truc.fr
# l'application ne pause pas de problème derrière un proxy
# et laisser le PORT=3000 et le MONGO_URL

systemctl enable mongod && systemctl start mongod
systemctl enable rocketchat && systemctl start rocketchat
```
