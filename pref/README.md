# OM om

Boite à outil (tool box) autour des systèmes d'exploitations et logiciels libre. Elle regroupe un ensemble de fichiers permettant de répondre efficacement à un besoin en ligne de commande sans passer par internet pour tout administrateur système et réseaux.

lang et wiki : apporte de l'information
application, config et scripts : accélère le déploiement de service
pref : personnalise l'environnement pour le rendre plus efficace

La force du projet réside dans son arborescence à la fois rigide et ouverte. Celle-ci permets d'emmagaziner de nouveau fichier tout en admettant des connexions fixe entre eux.
## Usage

Cet boite a outil est faite pour la ligne de commande. Il convient dans un premier temps de la déployer sur son système (dossier pref) avant de pouvoir être utiliser complètement.

## Contributing

1. Fork it!
2. Create your feature branch: `git checkout -b my-new-feature`
3. Commit your changes: `git commit -am 'Add some feature'`
4. Push to the branch: `git push origin my-new-feature`
5. Submit a pull request :D

## Credits

Kyprio CenKeneno

## License

GPLv3
