# source de bashrc inputrc et bash-aliases
[ -f ~/om/pref/_.bash-aliases ] && source ~/om/pref/_.bash-aliases
[ -f ~/wo/pref/_.bash-aliases ] && source ~/wo/pref/_.bash-aliases

# bash_completion
[ -f /usr/share/bash-completion/bash_completion ] && source /usr/share/bash-completion/bash_completion

# color prompt
parse_git_branch() {
     git branch 2> /dev/null | sed -e '/^[^*]/d' -e 's/* \(.*\)/(\1) /'
     }
#PS1='\[\033[01;32m\]\u@\h\[\033[00m\]:\[\033[01;34m\]\w\[\033[00m\]\$ '
PS1='\[\033[01;32m\]\u\[\033[01;33m\]@\[\033[01;34m\]\h\[\033[01;37m\] \t \[\033[01;34m\]$(parse_git_branch)\[\033[01;33m\]\w\[\033[01;37m\]\n\$ '

# history
HISTCONTROL=ignoreboth
shopt -s histappend
HISTSIZE=2000
HISTFILESIZE=5000

# display
shopt -s checkwinsize

# allow pattern **
shopt -s globstar

# editor
export FCEDIT=/usr/bin/vim
export EDITOR=/usr/bin/vim

# exeute personnal script after log on
[ -f ~/.wo_logon.sh ] && source ~/.wo_logon.sh

# virtualvenv python
export WORKON_HOME=~/.virtualenvs

# FUNCIONS
##########

# color for echo -e
Y="\\033[1;33m" R="\\033[1;31m" W="\\033[0;37m" B="\\033[1;37m" G="\\033[1;32m"

# echotalk
# talk througth /dev/pts/x to logged in users
function echotalk() {
  dest=$1
  tty=$2
  [[ "${dest}" != "/dev/"* ]] && dest="/dev/pts/"${dest}
  shift 2
  msg=$(echo -ne  "${R}${tty}>${Y}")${@}
  IFS=$'\n'
  echo -e $msg $dest
  for e in $(echo  ${msg} | fold -w1); do
    sleep 0.$((RANDOM %4))$((RAMDOM %9))
    printf $e > $dest
  done
}

# display when  user logged in
#sleep 1
#for i in $(who | awk '{print "/dev/"$2}' ); do
#  if [[ "$i" != "$(tty)" ]]; then
#    echotalk $i $(tty) "I'm connected" &
#  fi
#done
