# OM om

Boite à outil (tool box) autour des systèmes d'exploitations et logiciels libre GNU/Linux (pour le moment). Elle regroupe un ensemble de fichiers permettant de répondre efficacement à un besoin en ligne de commande sans passer par internet pour tout administrateur système et réseaux. A savoir : 
- lang et wiki : apporte de l'information
- config et scripts : accélère le déploiement de service
- pref : personnalise l'environnement pour le rendre plus efficace

L'arborescence est pensée pour permettre l'accés rapide à des éléments utilisés de façon courante (**wiki** et **scripts**) tout en offrant la possibilité de stocké de l'information plus détaillé ailleurs pour éviter l'encombrement (**lang**, **config** et **appli**). Il est synthétique et réponds à des besoins concrets d'administration de serveur ou de développement.

La première raison pour laquelle j'ouvre mon projet qui a débuté en 2009 est parce qu'il m'a beaucoup aidé et je suis convaincu qu'il pourra en aider d'autre comme moi.
La seconde raison est que ce projet demande de l'investissement pour évoluer au rythme de l'informatique et que, comme tout le monde, je manque de connaissance et de temps. J'aimerai donc profiter de votre contribution pour que ce projet évolue. Ce projet est libre et le restera pour la simple raison que je n'ai rien inventé pour l'écrire. J'ai simplement rassembler, filtrer et synthétiser les informations trouvées sur mon chemin.

## History

C'est à parti de 2009 que j'ai commencé à écrire un fichier regroupant l'ensemble des commandes Bash que je trouvais au fil des tutos sur linux. Je savais utiliser vim et sa fonction de recherche / et c'est grâce à cela que j'ai initié sans trop m'en rendre compte, les fichiers qui le consistue. Une simple recherche sur un nom de commande ou sur un mot clés qui présent das la définition d'une commande me permettait de retrouver la commande que j'avais apprise plusieurs jours vois plusieurs mois auparavant. Ce projet mise sur la synthèse et la hiérarchisation des informations. Ainsi, je n'avais plus besoin de perdre du temps sur internet pour rechercher une commande ou un syntax pour les scripts.
Plus tard, c'est le dossier scripts qui a pris de l'ampleur lorsque j'avais décidé d'automatiser toutes les tâches que je faisait. La encore, cela à porté ses fruits, notamment dans des phases de test de serveur à installer et configurer le plus proprement possible.
Ensuite, j'ai intégré ma manière de synthétiser mes connaissances en langages informatiques, à savoir quelque fichier pour les bases, des fichiers traitant de point plus avancés, des modeles basique pour les syntaxes ou les fonctions courantes pour être réutilisable rapidement, puis des exemples plus concret parce que de toute façon, on a beau dire (et pourtant j'ai essayé), rien n'également un exemple concret.

## Usage

Cet boite a outil est faite pour la ligne de commande. Elle se place dans le home de l'utilisateur et il est conseillé (non indispensable) de profiter des fichiers de préférences dans om/pref pour bénéficier d'un environnement plus agréable et productif (principalement des parametage de bash et des alias ainsi que celui de vim ). Cet étape de personalisation est automatisé dans un script placé dans om/scripts/.
Pour une utilisation optimale, il est néccessaire d'avoir préalablement lancé le script de configuration de sons système d'exploitation système qui se trouve dans om/scripts/. Ceci afin de s'assurer de la présence d'outils basique comme sipcalc.


## Contributing

1. Fork it!
```
git clone ssh://git@framagit.org:kyprio/om.git
```
2. Create your feature branch: `git checkout -b my-new-feature`
3. Commit your changes: `git commit -am 'Add some feature'`
4. Push to the branch: `git push origin my-new-feature`
5. Submit a pull request :D

## TO DO LIST

1. Actuellement l'ensemble du projet était pensé pour Debian/Ubuntu. Je souhaite le porter sur les autres systèmes tel que RedHat, Fedora, CentOS et si possible FreeBSD
    - om/wiki/commands : ajouter les commandes équivalentes sur les autres systèmes et préciser en fin de ligne entre parenthèse si une commande est propre à une ou deux distributions uniquement. ex "centos7 uniquement"
    - réécrire les scripts uniquements compatible debian pour leur trouver un équivalent. Il faudra alors modifier le suffixe pour indiquer l'OS auquel il est destiné. ex : sudo_install_mariadb_centos7.sh sudo_install_mariadb_deb.sh sudo_install_mariadb_rhel7.sh
2. Traduire le projet en anglais. J'ai déjà pris soin de modifier l'arborescence pour qu'elle soit dans un anglais technique correct. Il faudrait tout traduire en anglais en gardant pour mot d'ordre "La simplicité" pour qu'un français puisse comprendre ce qui est expliqué.
    - om/wiki on rajoutera eventuellement un fichier traduction qui contiendra un lexique de mot technique anglais et leur traduction en francais
3. Remplir la catégorie lang avec des fichiers théoriques concis et des fichiers d'exemple décrivant un point particulier par fichier.
4. Compléter le fichier historique du projet om/wiki/commands tout en respectant un maximum la hiérarchie des parties et sous parties du fichier. Le but est de décrire un commande puis de décrire ligne par ligne un paramètre pour arrivé à la fin du listing à une ou plusieurs lignes synthétisant l'ensemble des paramètres vue et qui réponds à un cas très courant. ex: tcpdump.
5. om/wiki trascrire en markdown les fichiers wiki et leur ajouter l'extension .md pour que vim les reconnaissent

## Credits

Kyprio CenKeneno

## License

GPLv3
