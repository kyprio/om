#  COMMANDES

##    SHELL BASH ET SCRIPTING

###      SYNTAXE DU SHELL

####        LES ARGUMENTS

`commande --parametre=14` = commande -p 14  
`commande -a -U -r` = `commande -aUr` = `commande --all --unique --restore` = commande -aU --restore  
`commande -p 14 -a` = commande -a -p 14  

####        LES ENCHAINEMENTS DE COMMANDES

Une commande renvoi toujours a l\'insue de l\'utilisateur un code d\'erreur au shell sans rien n\'afficher. Si 0 alors pas d\'erreur.

`commande1 && commande2` = execute commande1 puis, si pas d\'erreur, execute commande2  
`commande1 || commande2` = execute commande 2, si erreur sur commande1  
`commande1 ; commande2` = execute commande1 puis execute commande2 sans tenir compte des erreurs eventuelles  
`sleep 5` = attends 5 secondes (5m pour 5min, h heure, d jour) (utile entre deux enchainements)  

####        GUILLEMETS

#####          quote ' '

`'texte'`  
`'$var'`  

> laisse intacte le texte  
> le shell lancant la commande n'interprete pas le contenu  
> mais si :  
> export var  
> sed 's/motif/echo $var/' fichier  
> le programme pourra l'interprété s'il le peut  
> mais cela n'empeche pas l'outils de l\'interpréter s'il le peut

#####          double quote " "

`"$var"`  
`"~/Documents/"`  
`"*"`  

> le shell interprete le contenu  
> puis l'envoit à la commande

#####          backquote ``

`command`  

> le shell interprete la commande contenu entre backquote  
> et renvoit son résultat à la commande

####         REDIRECTION DE FLUX D\'ENTREE SORTIE

> liste des fd (=file descriptor)  
> 0 - stdin = standard input = saisie clavier ou entree pipe de la commande precedente  
> 1 - stdout = standard output = sortie normale de la commande  
> 2 - stderr = standard error = erreurs  
> 3 - fd de transition = permets generalement de switcher le stdout et stderr avec 3>&1 1>&2 2>&3

`commande > fichier` = redirige le stdout de la commande vers fichier (risque d\'ecraser fichier si deja existant)  
`commande > /dev/null` = " " vers le trou noir du systeme  
`commande >> fichier` = " " a la fin de fichier  
`commande 2> fichier` =  redirige les erreurs possibles vers fichier  
`commande &> fichier` =  redirige le stdout et le stderr  
`commande 2>&1 /dev/null` = redirige tout le flux vers le trou noir du systeme  
`commande > fichier 2> fichiererreur` = redirige le resultat standard de la commande vers fichier et les erreurs possibles vers fichiererreur  
`commande >> fichier 2>&1 fichier erreur` = redirige et fusionne le resultat standard de la commande et les erreurs a la fin >> de fichier  

`commande < fichier` = execute commande et utilise le contenu de fichier comme entree standard plutot qu\'une entree clavier attendu   
`commande << FIN` = `permets de : ecrire au clavier, d\'arreter la saisie clavier en ecrivant FIN, de traiter la saisie avec commande (sort -n >> FIN` = trie les lignes de nombres saisies au clavier et dont le deliminateur est FIN)  
`commande <<< chaine` = traite directement les valeurs que l\'on veut traiter par commande sans passer par son interface console puis revient au prompt  

`commande < fichier1 >fichier2` = traite fichier1 en stdin de commande puis retourne le resultat stdout vers fichier2 (il est impossible de lire et d\'ecrire en meme temps sur fichier1 par ce procede de redirection de flux)  
`commande1 | commande2` = (pipe/tube non nommé) execute commande1 et utilise son resultat pour commande2  
`commande1 | commande2 > fichier` = " " et ecrit le resultat dans fichier  
`command | xarg command2` = transcrit la stdout d\'un outils en argument pour un programme n\'utilisant pas le sdtin  
`ls | xarg rm` = `rm $(ls)` = recupere la sortie de ls pour l'utilise en argument dans rm  
`ls | xarg -p rm` = " " mode interactif  

`$(command)` = renvoi le retour de la commande comme un string  
`<(command)` = renvoi le retour de la commande dans le stio (utile pour parcourir les resultats dans une boucle while read  )  

####        OUTILS DE SAISIE DE VARIABLE

`read variable` = recupere la saisie clavier et place la valeur dans variable  
`read variable1 variable2` = " variable1 et 2 separee par un espace  
`read -p 'prompt' variable` = " en affichant precedemment un prompt  
`read -n 1 variable` = " en limitant la saisie a un caractere  
`read -t 5 variable` = " en limitant le temps a 5 seconde  
`read -s variable` = " en masquant les caracteres saisies  
`read -rsp $'Press any key to continue\n' -n1 key` = fait une pause dans un script en attente d\'un touche de clavier  

####        COMMANDES DU SHELL

`set un deux trois` = affecte les mots au variable de parametres de bash $1 $2 $3  
`set -euo pipefail` = active dans bash la fonction STOP lorsque -e il y a une erreur, -u une variable non definie, -o pipefail une erreur dans un pipe (pratique uniquement pour debuguer)  
`set +e` = desactive la fonction STOP en cas d'erreur  

`exec commande` = lance commande puis ferme le shell à l'arret de la commande  

`seq 5` = `{ 1..5 }` = renvoi les chiffres de 1 à 5  
`seq 2 10` = `{ 2..10 }` = " " de 2 à 10  
`seq 2 10 2` = `{ 2..10..2 }` = " " " " avec un pas de 2  
`seq 20 | xargs commande` = execute et repete 20 fois commande  
`seq 20 | xargs -I{} echo "numero:{}"` = `seq 20 | xargs -Ix echo "numero:x"` = repete la commande echo et incremente la variable de xargs  

`eval ${commande}` = force l\'interpretation des variables avant execution de la ligne  

`exit` = ferme le shell actuel (ou celui du script)  
`exit 1` = " " en renvoyant un code de retour  

`logout` = ferme la session shell en TTY  

####        BOITE DE DIALOGUE DANS INTERFACE TEXTUELLE

#####          whiptail

`whiptail --msgbox "Message d'information" 20 70` = renvoi une boite de dialogue avec un message à valider de hauteur 20 lignes et largeur 70 colonnes  
`whiptail --title "TITRE" --msgbox "Information" 20 70` = " " avec un titre  
`whiptail --title "TITRE" --yesno "Question ?" 20 70` = " " avec reponse à deux choix possible où yes renvoit 0 no renvoit 1  
`whiptail --title "TITRE" --yesno "Question ?" --yes-button "Choix1" --no-button "Choix2" 20 70` = " " avec reponse à deux choix possible où --yes-button renvoit 0 et --no-button renvoit 1  
`whiptail --title "Saisie" --inputbox "Saisir une valeur" 20 70 "valeur pre-saisie"` = " " avec une valeur à saisir. La valeur est renvoye dans le sterr 2>  
`whiptail --title "MDP" --passwordbox "mdp" 20 70` = " " demande de mot de passe  

`whiptail --title "radiolist" --radiolist "Choix unique" 20 70 3 "tag1" "Description 1" ON "tag2" "Description 2" OFF "tag3" "Description 3" OFF` = " " une liste radio avec "tag11" de pre-selectionne. 3 indique la hauteur de la liste, le tag choisi sera renvoye telle quelle au stderr  
`whiptail --title "checklist" --checklist "Choix multiple" 20 70 3 "tag1" "Description 1" ON "tag2" "Description 2" OFF "tag3" "Description 3" OFF` = " " une liste à cocher " " les tags choisis seront renvoye dans le stderr  
`whiptail --gauge "Veuillez patienter pendant l'installation" 6 60 0` = " "  
`variable=$(whiptail --title "Saisie" --inputbox "Saisir une valeur" 20 70 "valeur pre-saisie" 3>&1 1>&2 2>&3)` =  recupere la saisie dans une variable  
`password=$(whiptail --title "MDP" --passwordbox "mdp" 20 70 3>&1 1>&2 2>&3)` = recupere silencieusement le mot de passe dans une variable  
`variable=(whiptail --title "checklist" --checklist "Choix multiple" 20 70 3 tag1 "Description 1" ON tag2 "Description 2" OFF tag3 "Description 3" OFF 3>&1 1>&2 2>&3)` = recupere les tags sous forme de variable string  
`variable=( $(whiptail --title "checklist" --checklist "Choix multiple" 20 70 3 tag1 "Description 1" ON tag2 "Description 2" OFF tag3 "Description 3" OFF 3>&1 1>&2 2>&3) )` = recupere les tags sous forme de liste (avec IFS=' ')  

####        BOITE DE DIALOGUE DANS INTERFACE GRAPHIQUE

#####          zenity

> zenity affiche une boite de dialogue et renvoit le resultat dans la sortie standard du terminal

`zenity --error --title='TITRE' --text='Texte'` = affiche une fenetre d\'erreur avec un bouton OK  
`zenity --notification --window-icon=update.png --text='Texte'` = affiche une fenetre de notification avec icon personnalise  
`zenity --info --title='TITRE' --text='Texte'` = " " info " "  
`zenity --file-selection --title'TITRE'` = affiche une fenetre de selection de fichier et renvoit le chemin absolu du fichier  
`zenity --question --title 'TITRE'  --text 'Question ?'` = affiche de fenetre de choix OK Cancel et renvoit 0 ou 1   
`zenity --list --column='Items' --text='Texte' 'Item1' 'Item2' 'Item3'` = affiche une liste a choix unique contenant les elements  
`zenity --list --checklist --column='check' --column='Items' --text='Texte' TRUE 'Item1' TRUE 'Item2' FALSE'Item3'` = affiche une liste a choix multiples contenant des elements pre-coches  
`find /var/log/ -name '*.log' | zenity --list --title 'TITRE' --text 'Texte' --column 'colonne'` = affiche une liste contenant en element le stdin et permettant la selection d\'un element  
`sleep 4 | zenity --progress --pulsate` = affiche un barre de progression aller-retour pour attendre la fin de la commande precedante  

###      LES VARIABLES

####        CREATION ET MANIPULATION DES VARIABLES

`var=30` = affecte une valeur à var  
`var="valeur ${HOME}"` = " " en interprétant le contenu  
`var='valeur ${HOME}'` = " " sans interpréer le contenu  
`$var` = `${var}` = copi du contenu de variables  

`echo $var` = affiche le contenu de la variable var  

`var=$(commande)` = creation de la variable var contenant le retour d\'une commande  
`var=${var1}${var2}texte` = concatene le contenu de $var1, $var2 et texte  

`${#var}` = renvoi la longueur de la variable var  

`set` = renvoi l'ensemble des variables definies dans le shell  
`unset var` = supprime la variable var  

`export var` = `declare -x var` = exportation de la variable var vers les shells fils (la valeur de var peut varié, celle-ci sera toujours exportée)  
`export` = renvoi l'ensemble des variables exportees  

`${var:-val}` = renvoit val si null ou unset  
`${var:=val}` =  renvoit et affecte val si null ou unset  
`${var-val}` =  renvoit val si unset  
`${var=val}` =  renvoit et affecte val si unset  
`${var:+val}` = affecte val si not null et set sinon affecte null  
`${var+val}` = " " si unset " "   
`${var:?val}` =  exit erreur si null ou unset  
`${var?val}` =  " " si unset  

####        VARIABLES DE TYPE CHAINE DE CARACTERE

`var="a b"` = creation de la variable var de type chaine de caractere contenant "a b"  
`var="a` = $b" =" " en interpretant les caracteres speciaux (appelle de la variable b)  
`var='a $b'` = creation de la variable var en echapant le contenu entre guillement simple  
`var=${var//./ }` = redefinit la variable var en remplaçant les . par des espace  
`var=(${var//./ })` = " " et la converti en variable en tableau (utile pour les boucles)  
`${var/text/subs}` = mofie le retour de la variable concernant le premier motif rencontre  
`${var,,}` = lowercase the content of $var  
`${var^^}` = uppercase the content of $var  
`${var#prefixe}` = retire prefixe au debut du contenu de la variable  
`${var%suffixe}` = retire suffixe à la fin " "  

####        VARIABLE DE TYPE NUMERIQUE

`var=1` = creation de la variable var de type numerique contenant 1  
`var=$((5+6))` = creation de la variable var contenant le resultat entier d\'un calcul simple (+ - * / % **)  

####        VARIABLE DE TYPE TABLEAUX LISTE

> utile pour les boucles

`tableau=('valeur0' 'valeur1')` = creation d'un tableau contenant 2 elements  
`tableau[2]='valeur'` = creation/modification manuelle du 3eme element  
`tableau=( $(command) )` = creation d\'un tableau à partir du retour d\'une commande comme find  
`${tableau[0]}` = element situe a l\'indice 0  
`${tab[11]}` = contenu du douzieme enregistrement du tableau "tab"  
`${tab[@]}` = `${tab[*]}` = ensemble des enregistrements du tableau separes par un espace  
`${#tab[@]}` = taille de la liste
`tab2=(${tab1[@]})` = affecte tab1 a tab2  
`${#tableau[2]}` = longueur de l\'element a l\'indice 2  
`${#tableau[@]}` = nombre total d\'element  
`${tableau[@]:2:3}` = elements a partir de l'indice 2 jusqu\'a 3  
`${tableau[2]:0:4}` = quatres premieres lettres en partant de 0 de l'element 2  
`echo ${tableau[@]/A/B}` = tout le tableau en remplacant l'element A par B  
`unset ${tableau[2]}` = supprime l'element 2  

####        VARIABLES SPECIALES

`$0` = nom du script lance  
`$1 ${1}` = parametres positionnels 1  
`$#` = nombre de parametres positionnels  
`$*` = `$@` = ensemble des parametres positionnels  
`"$*"` = " " entre guillement groupe  
`"$@"` = " " entre guillement pour chaque element  

`$$` = PID du shell courant  
`$!` = PID du dernier travail lance en arriere plan  
`$?` = code retour de la dernière commande  

####        VARIABLES D\ENVIRONNEMENT

`env` = liste des variables d\'environnement  
`stty -a` = liste des signaux et caractères spéciaux avec leur comportement sur le shell  

`stty intr ^K` = modification temporaire de l\'interruption de CTRL C en CTRL K  

`$HOME` = chemin du repertoire personnel de l'utilisateur  
`$SHELL` = chemin absolu du shell utilise  
`$OLDPWD` = chemin du repertoire precedent  
`$PATH` = liste des chemins de recherche des commandes executables  
`$PPID` = PID du processus pere du shell  
`$PS1` = invite principale du shell  
`$PS2` = invite secondaire du shell  
`$PS3` = invite de la structure shell "select"  
`$PS4` = invite de l'option shell de debogage "xtrace"  
`$PWD` = chemin du repertoire courant  
`$RANDOM` = nombre entier aleatoire compris entre 0 et 32767  
`$REPLY` = variable par defaut de la commande "read" et de la structure shell "select"  
`$SECONDS` = nombre de secondes ecoulees depuis le lancement du shell  
`$BASH_SOURCE` = dossier d\'execution du script en cours  

###      LES STRUCTURES

####        LES STRUCTURES CONDITIONNELLES

####        LES BOUCLES


##    REGEX ET METACARACTERES

> caracteres ou commande interpretables par le shell ou la commande  
> il existe plusieurs standards

###      METACARACTERES DE SHELL

> utilisable avec ls

`*` = 0 ou n caractères  
`?` = 1 caractère  
`[aeiouy]` = 1 des caractères de la liste  
`[a-z]` = " " liste de a à z minuscule  
`[!a-z]` = " " inverse  

###      CARACTERES SPECIAUX

> utilisable avec echo -e

`\n` = saut de ligne  
`\b` = retour arriere (sur le caractere precedent)  
`\t` = tabulation horizontale  
`\v` = tabulation verticale  
`\a` = alerte (sonore)  
`\c` = suppression du saut de ligne final  
`\f` = saut de page  
`\123` = caractere ASCII de valeur 123  
`\x123` = caractere ASCII hexadecimal de valeur 123  

###      EXPRESSION REGULIERE REGEX

####        REGEX BRE - BASIC REGULAR EXPRESSION
 
`.` = 1 caractères  
`.*` = 0 ou n caractères  

`^` = au debut de la ligne  
`$` = a la fin de la ligne  

`[aeiouy]` = 1 des caractères de la liste  
`[a-z]` = " " de l'intervalles  
`[a-zA-Z]` = " " des intervalles  
`[^a-z]` = " " inverse   
`[a-zA-Z0-9]` = alphanumerique  

####        REGEX ERE - EXTENDED REGULAR EXPRESSION

####        REGEX ETENDUE

`a?` = a apparait 0 a 1 fois  
`a+` = " " 1 ou plusieurs fois  
`a*` = " " 0 à plusieurs fois  
`a{5}` = a apparait 3 fois obligatoirement  
`a{5,10}` = " " 5 a 10 fois  
`a{5,}` = " " 5 fois a l'infini  

`(motif1|motif2)` = motif1 OU motif2  
`motif1|2` = " "  

`(color)*` = sous section apparait 0 à plusieurs fois  

`b` = la limite d'un mot (suivant que le metacaracteres est place au debut ou a la fin du mot)  
`\` = echappe un metacaractere (ensemble des caracteres speciaux des expressions regulieres) (ou apporte un sens predefinit si suivie d'une lettre ;(PCRE/Perl)  
`'#'` = definit le debut puis la fin d'un regex ( #regex# )(PCRE/Perl) (sans les apostrophes)  
`-` = definit un intervalle entre deux valeurs coherentes et comphrehensible (0-9 a-z A-Z)  
`[0-9]` = plage contenant un chiffre de 0 a 9  
`[\^$\|(){}?+*.]` = plage contenant des caracteres speciaux (les crochets echappent tous les caracteres contenus sauf ^ si en debut # - et [] qui font sens independement des crochets)  
`[^0-9]` = plage ne contenant pas un chiffre de 0 a 9  
`(ab)` = motif ab et capture d'un motif (peut contenir d'autre regex)(chaque coupe de parenthese cree une variable ; $1 $2 $3...)  
`$1` = variable issue du premier couple de parenthese qui a capture un motif  
`$0` = variable issue de la regex entiere  
`(?:ab)` = motif ab sans capture du motif (la variable n'est pas creee)  
`!` = definit des commandes ou la negation (a clarifier)  

`(ab)*` = ab est facultif  
`[0-9]+` = des chiffres apparaîssent 1 a plusieurs fois  

####        REGEX PCRE PERL

> standard PCRE/Perl et POSIX  
> MySQL

`\t` = indique une tabulation  
`\n` = indique une nouvelle ligne  
`\r` = indique un retour de charriot  
`\s` = indique un espace  
`\S` = indique ce qui n'est pas un espace blanc  
`\w` = `[0-9a-zA-Z_]` = " ou contenant un underscore  
`\W` = `[^0-9a-zA-Z_]` = plage ne contenant ni chiffre ni alphabet ni underscore  
`\d` = `[0-9]` = plage contenant un chiffre de 0 a 9  
`\D` = `[^0-9]` = plage ne contenant pas de chiffre  

#####          option regex

> se place apres le regex tel que #regex#option

`s` = toute les lignes  
`i` = ignore la casse  

####        EXEMPLES DE REGEX PRATIQUES

> retirer les ' '

`'#^0[1-68]([-. ]?[0-9]{2}){4}$#'` = regex de numero de telephone incluant les separateurs [-. ]  
`'#^[a-z0-9._-]+@[a-z0-9._-]{2,}\.[a-z]{2,4}$#'` = regex email  
`'#(((https?|ftp)://(w{3}\.)?)(?<!www)(\w+-?)*\.([a-z]{2,4}))#'` = regex URL  
`'^([0-9]{1,3}.){3}.([0-9]{1,3})$'` = regex IP simplifiee de 0.0.0.0 à 999.999.999.999  
`\(\d\+\.\)\{3\}\d\{1,3\}` = " " pour vi  
`'^(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?).(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?).(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?).(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$'` = regex IP de 0.0.0.0 à 255.255.255.255  

###      OUTILS DE CONFORT

####        SU ET SUDO

`su -c "ls /root"` = execute en tant que root une commande  

`sudo commande` = execute commande en tant que Super Utilisateur (sudo=Subsitute User DO)  
`sudo su` = execute su (switch user) pour passer en root grâce a sudo  
`echo motdepasse | sudo -S commande` =  executer commande en sudo en lisant l\'entree standard sudo -S  
`sudo -u user1 commande` = execute commande en tant que user1  
`sudo -sEH -u www-data` = donne un environnement, un home et un shell et se loggue en www-data  

`visudo` = ouvre le fichier d\'option de la commande sudo pour ajouter des utilisateurs ou des commandes sudo sans demande de mot de passe  

####        DOCUMENTAIRE ET AIDE

#####          MANPAGE

`man commande` = renvoi la description, synopsis et option dune commande
`man man` = apprendre a lire un man  
`man 7 man` = accède au manpage de man correspondant à la catégorie 7  

> les manpage sont catégorisé par numéro  
> la catégorie est déterminé automatiquement lors de l'appel  
> mais parfois un même nom de manpage a plusieurs entrées différentes  
> exemple IP(7) et IP(8)
>  
> **1** commande utilisateur `man 1 echo`  
> **2** appel système `man 2 syslog`  
> **3** bibliothèque de langage C `man 3 scanf`  
/bin/bash: q: command not found
> **5** format de fichiers de configuration `man 5 passwd`  
> **6** jeux  
> **7** divers `man 7 man`  
> **8** commande administration système root `man 8 ip`

> **manpages-fr** est le paquet contenant les pages man en fr 

`mandb` = met a jour la liste des manuels pour les commandes de recherche d\'information de programmes  

#####          AUTRE SOURCE D'INFORMATION

`info` = documentation complete a parcourir sur toutes les commandes disponibles  
`info commande` = partie de la documentation contenant la commande.  
`info coreutils` = regroupe par theme les commandes frequentes;  

`whatis commande` = renvoi juste la description du man.  
`commande -h` = `commande --help` = aide simplifiee sur la commande  

####        HISTORIQUE DES COMMANDES

`history` = historique des commandes ecrites  
`history 10` = " " 10 dernières commandes  

####        PERSONNALISATION DU SHELL

`chsh` = permet de changer le shell par defaut (/bin/bash /bin/sh)  

####        MATHEMATIQUE DANS LE SHELL

`let 1+2` = calcul 1+2  
`a=$(($x+$y))` = calcule simple pour des variables contenant des entiers  

`bc` = ouvre la calculette  
`echo "calcul" | bc` = calcule en donnant la valeur entiere  
`echo "calcul" | bc -l` = calcule en donnant la valeur decimale la plus precise  
`echo "scale=2;calcul" | bc` = " " au centieme pres  
`echo "obase=2;64" | bc` = renvoi le binaire de 64  

`base2=({0..1}{0..1}{0..1}{0..1}{0..1}{0..1}{0..1}{0..1})` = creation d'un fonction tableau a utiliser pour convertir un nombre en binaire  
`echo ${base2[255]}` = converti en binaire 255 a partir de la fonction cree base2()  

`sipcalc` = calcul d\'adresse IP et masque  

####        ALEATOIRE ET MELANGE

`$(($RANDOM %10))` = renvoi un valeur de 0 à 9  
`liste_alea=($(shuf -i 0-9 -n 10))` = affecte à la variable une liste de 10 nombres melanges de 0 à 9  


##    NAVIGATION

###      PARCOURS DES DOSSIERS

#####          cd

`cd  /` = se deplace vers la racine  
`cd ..` = se deplace vers le dossier parent  
`cd ~` = se deplace vers le repertoire personnel  
`cd -` = se deplace vers le repertoire precedant  

#####          ls

`ls` = liste les elements presents (bleu fonce pour dossier, bleu clair opur lien)  
`ls -A` = liste presque tout sauf  .. et .  
`ls -F` = liste les elements en indiquant leur type (/ dossier, @ lien)  
`ls -l` = `ll` = detaille (droits, nombre de liens, proprietaire, groupe, tailles en octets, date de modification, nom de l\'element) ( - file, d directory, p pipe nommé, c characters, b binary, s socket )  
`ls -n` = " " en indiquant les UID GID au lieu des nom d\'utilisateurs et groupes  
`ls -h` = renvoi la taille des elements lisiblement (G M K) : ls -lh  
`ls -t` = trie par dates ; les dernières en premier  
`ls -r` = inverse l\'affichage de la liste : ls -rt  
`ls -1` = liste les elements sur une seule colonne  
`ls -i` = renvoi les inodes des fichiers pour detecter des liens physiques  
`ls -ld` = renvoi les informations de droits par default du dossier actuel  
`ls fichier` = detaile des fichiers nommes au moins fichier  
`ls -I fichier` = liste les elements du dossier sauf fichier  
`ls *.jpg` = liste les .jpg  
`ls *.jpg *.png *.gif` = liste les images  
`ls *` = liste tous les fichiers visibles et dossier incluant leur arborescence  
`ls -Al | grep ^d | cut -d " " -f 9` = listes les dossiers uniquement  


#####          du 

`du` = indique la taille des dossiers du dossier courant et la taille totale en octets   
`du -a` = " " dossiers et fichiers " "  
`du -h` = " " avec une taille lisible en G M K  
`du -s` = indique uniquement la taille totale du dossier courant  

#####          chemin

`basename ~/dossier/fichier.txt` = renvoi le nom du fichier sans son chemin  
`basename ~/fichier.txt .txt` = `basename -s .txt ~/fichier.txt` = " " sans le suffixe .txt  
`basename -s .txt ~/dossier/*` = " " pour tous les fichiers  

`dirname /chemin/fichier` = renvoi le chemin du dossier en excluant le fichier et le / final  

`realpath fichier` = renvoi le chemin absolu du fichier  

`namei /chemin/dossier` = decompose le chemin ligne par ligne en affichant les dossiers parcourus  

`pwd` = `pwd -L` = Print Working Directory : indique lemplacement actuel (affiche le lien symbolique s\'il y a lieu)  
`pwd -P` = " " reel  
`Print Working Directory` = indique lemplacement actuel  
`DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"` = stocker dans DIR l\'emplacement du script peut importe d\'ou il est lance  

`vi dossier/` = ouvre comme un fichier texte le dossier et permets d\'interargir avec les fichiers contenus avec les raccourcis claviers indiques  

#####          création d\'un pipe nommé

`mknod pipe0 p` = créer un fichier pipe0 de type pipe nommé  
`cat pipe0` = lecture du fichier pipe en attente de flux  
`ls -l > pipe0` = écriture d\'information dans le fichier pour affichage avec la commande précédente  

###      RECHERCHE

`find fichier` = recherche dans le dossier en cours les documents contenants fichier  
`find /chemin fichier` = recherche dans /chemin les documents contenants fichier  
`find -name fichier` = recherche uniquement les fichiers nommes fichier  
`find -name fichier*` = recherche les documents dont leur nom commence par fichier  
`find -name *.ext` = " " termine par .ext  
`find -size +10M` = cherche un fichier de taille superieur a 10Mo (-/+/= G/M/K) (par defaut en octet)  
`find -atime +5` = cherche un fichier modifie il y a 5 jours  
`find -type d` = cherche un dossier (f/d)  
`find fichier -printf "%p - %u \n"` = cherche fichier et renvoi les resultats sous forme : file - owner (retour a la ligne)  
`find fichier -delete` = cherche fichier et le supprime  
`find -name "*.jpg" -exec mv {} /home/kyprio/images \;` = cherche les images *.jpg puis execute une instruction (deplace chaque fichier trouve) et cloture l\'instruction avec \;  
`find -name fichier -exec grep mot-cles {} \;` = cherche fichier et execute une instruction ( renvoi la ligne contenant mot-cles pour chaque fichier trouve)  

`liste_file=( $(find fichier) )` = recupere la sortie de find et liste les fichiers  

`which ls` = indique l\'emplacement du fichier de la commande ls  


##    GESTION DES FICHIERS

###      TYPE DE FICHIER

> **-** regular file  
> **d** directory file  
> **b** block devive file (partition de périphérique) envoit des blocks de données
> **c** character special file (tty) envoit des caractère par caractère en ASCII
> **p** named pipe file (tube nommé utilisant un rapport writer-reader)  
> **s** symbolic link file (fichier situé dans le repertoire et non la table des inodes et pointant vers un autre nom de fichier)  
> **s** socket file (type socket entre couche 4 et 5)  

###      FICHIER EN LOCAL

####        COPIER

`cp fichier copiefichier` = copi fichier en copiefichier  
`cp fichier dossier/` = copi fichier vers dossier/  
`cp -t dossier/ fichier` = " "  
`cp -i fichier copiefichier` = copi fichier avec demande de confirmation si fichier existant  
`cp -R dossier copiedossier` = copi dossier et son arborescence en copiedossier  
`cp *.jpg dossier` = copi dans dossier toutes les images .jpg  
`cp unf* dossier` = " " les fichiers commencants par unf  
`cp -p fichier copiefichier` = copi fichier en copiefichier en preservant ses attributs  
`cp -L fichier copiefichier` = copi en suivant les liens symboliques  

`pv fichier > destination` = copie fichier vers destination avec une barre de progression  

####        DEPLACER ET RENOMMER DES FICHIERS

`mv fichier dossier/` = deplace fichier vers dossier  
`mv fichier1 fichier2 dossier/` = " " fichier1 ... vers dossier  
`mv dossier1 ..` = deplace dossier vers le dossier parent  
`mv fichier1 fichier2` = renomme fichier1 en fichier2  
`mv /etc/chemin/fichier{,_bak}` = backup un fichier sans avoir a réécrire le chemin 2 fois.

`rename 's/.txt//' *.txt` = supprime l\'extension .txt des fichiers .txt et les renomme  
`rename -f 's/.txt//' *.txt` = " " en ignorant les fichiers existants  
`rename 's/^[0-9]+//' *` = supprime les chiffres au debut des noms de fichiers  
`rename 's/^/debut/' *` = rajoute en prefixe 'debut' a tous les fichiers  
`rename 'y/A-Z/a-z/' *` = remplace les majuscules par des minuscules  
`rename 's/[0-9]//g' *` = supprime les chiffres des noms des fichiers  

####        SUPPRIMER

`rm fichier` = supprime fichier  
`rm -i fichier` = " " avec confirmation a chaque fois  
`rm -f fichier` = " " sans confirmation et malgre les problemes potentiels  
`rm -v fichier` = " " mode verbose  
`rm -r dossier` = supprime le repertoire  
`rmdir dossier` = supprime le repertoire vide  
`rm -r *` = supprime le contenu du dossier actuel  
`rm ./"-tiret1"` = `rm -- -tiret1` = supprime le fichier -tiret1  
`rm [1-9]` = supprime les fichiers nommes 1 2 ...9  

`ln fichier_source lien_fichier` = creer un lien de fichier utilisant le meme inode (contenu)  
`ln -s fichier_source lien_fichier` = " " lien symbolique (vers fichier vers son inode)   
`ln -sf fichier_source lien_fichier` = " " ecraser si deja existant  

####        SYNCHRONISER EN LOCAL

`rsync -az fichiersource --port=22 user@192.168.1.2:/chemin/complet` = synchronise via SSH un fichier vers une destination distance en mode archive et compressé  

`rsync source destination` = synchronise en sauvegardant incrementiellement de source vers destination  
`rsync -a source destination` = " " en mode archive ( recursif, conservation des droits, concervation des liens symboliques )  
`rsync -az source destination` = " " " " en compressant les donnees avant le transfert  
`rsync -r source destination` = " " recursif  
`rsync -v source destination` = " " option verbose  
`rsync -rL source destination` = " " en suivant les liens symboliques  
`rsync  --exclude="dossier1/*" source destination` = " " en excluant les fichiers contenus dans source/dossier1/  
`rsync --delete -r source destination` = " " supprime les fichiers n'existant pas dans source  
`rsync --delete -r -b --suffix=.bak source destination` = " " et fait un backup des fichiers supprimes leur ajoutant une extention .bak ( si non specifie : suffix=~)  
`rsync -e "ssh -p 22" source destination` = " " en specifiant le port  

`comm fichier1 fichier2` = compare en trois colonnes fichier1 et fichier2  
`md5sum image_cd.iso` = calcul l\'empreinte md5 de image_cd.iso  
`md5sum -c MD5SUMS` = calcul et compare la validite des fichiers dont le nom est liste dans la liste MD5SUMS.  

####        CREER MODIFIER

`touch  fichier` = met a jour la date de modification / cree fichier  
`touch "fichier avec espace"` = " " avec espace  

`mkdir dossier1` = cree le repertoire dossier1  
`mkdir /etc/skel/{dossier1,dossier2}` = créé une liste de répertoires dans une arborescence  
`mkdir -p ~/dossier/dossier1` = " " sans afficher d\'erreurs et cree l\'arborescence si besoin  

`mktemp` = cree un fichier temporaire pour l\'utilisateur en cours et renvoi son chemin absolu  

`mkfifo fichier1` = créé un tube nommé  

####        DROIT D\'ACCES BASIQUE AUX FICHIERS

> RWX  
> 4 read  
> 2 write  
> 1 execute  
> Droit supplémentaire (par defaut = 0 )  
> 4 setuid  
> 2 setgid  
> 1 stickybit  
> exemple : 751 = 0751 = rwxr-x--x  
> exemple : 5751 = rwsr-x--t

`chmod +r fichier` = ajoute le droit d\'execution a tout le monde  
`chmod u+r fichier` = ajoute le droit de lecture au proprietaire du fichier  
`chmod g-w fichier` = enleve le droit d\'execution au groupe du fichier  
`chmod o=x` = on attribut uniquement le droit d\'execution aux autres pour fichier  
`chmod o=- fichier` = affecte l\'impossibilite d\'acces aux autres pour fichier  
`chmod u=rwx,g=rwx,o=r fichier` = affecte respectivement les droits rwx rwx et r du fichier au proprietaire, au groupe et aux autres  
chmod 775 fichier= donne les droits rwx au proprietaire du fichier, rwx au groupe et rx aux autres :read4+write2+execute1=7
`chmod  -R 700 /home/kyprio` = affecte les droits 700 a l\'arborescence kyprio/  

`chown kyprio fichier` = affecte fichier a l\'utilisateur kyprio  
`chown 1000 fichier` = affecte fichier a l\'utilisateur ayant pour UID 1000  
chown kyprio:groupe fichier= affecte fichier au proprietaire kyprio et au groupe groupe
`chown -R kyprio:kyprio dossier` = affecte dossier et son arborescence complete a kyprio et groupe kyprio  

`chmod 5750 fichier` = `affectation des droits complets incluant le 5` = 4 pour setuid et 001 pour stickybit  
`chmod u+s` = ajout du setuid pour le droit d'executer avec les droits du propriétaire du dossier  
`chmod g+s` = ajout du setguid " " groupe  
`chmod o+t` = ajout d'un stickybit  

`chgrp groupe fichier` = affecte fichier a groupe  

`umask` = renvoi la valeur du masque du dossier courant qui permet d\'en deduire les droits par defauts lors de la creation d\'un fichier  
`umask 0222` = `umask 222` = retire le droit d\'ecriture User Group et Other (commande recursive)  

####        POSIX ACL

> Access Control Lists  
> le systeme de fichier ext4 contient deja l'option ACL  
> elle doit etre active pour les autres systemes de fichier  
> ACL permet de pouvoir cumuler des autorisations sur un fichier a plusieurs utilisateurs et groupes differents

`ls -l fichier` = si les droits rwx sont suivi d\'un "+" c\'est que les ACL ont ete specifies  
`getfacl fichier` = renvoi l\'ensemble des permissions sur fichier  
`getfacl -omit-header` = " " en supprimant les 3 premieres lignes  
`getfacl -d` = liste les acl par defaut uniquement,  ne s\'appliquent qu\'aux dossiers  
`getfacl -a` = liste les acl d\'acces fichier uniquement  

`setfacl -m u::rw fichier` = `chmod u:rw fichier` = affecte les droits rw au proprietaire de fichier (idem avec g::rw o::rw)  
`setfacl -m u:kyprio:rw fichier` = ajoute ou affecte les droits rw a kyprio sur fichier  
`setfacl -Rm u:kyprio:rw dossier` = " " de maniere recursive  
`setfacl -m u:kyprio:rw,u:xuan:rwx fichier` = " " rw pour kyprio et rwx pour xuan  
`setfacl -m d:u:root:rwx dossier` = definit les droits par defaut pour dossier pour tout nouveau fichier  

`setfacl -x fichier` = supprime toutes les persmissions sur fichier  
`setfacl -b fichier` = supprime les ACL sur fichier  

####        NFS V4 ACL

> paquets nfs4-acl-tools  
> L'ACE (entree) a plus haute est prioritaire sur les lignes qui suivent  
> il convient donc de mettre les regles englobantes en dernières et de faire les exceptions en premieres  
> une ACE est de la forme type:flags:principle:permissions  
> Liste des permission  
> 	R = alias pour read de POSIX ACL (plus pratique pour les droits simplifies)  
> 	W = alias pour write " "  
> 	X = alias pour execute " "  
> 	r = read-data (files) / list-directory (directories)  
> 	w = write-data (files) / create-file (directories)  
> 	x = execute (files) / change-directory (directories)  
> 	a = append-data (files) / create-subdirectory (directories)  
> 	t = read-attributes - read the attributes of the file/directory.  
> 	T = write-attributes - write the attributes of the file/directory.  
> 	n = read-named-attributes - read the named attributes of the file/directory.  
> 	N = write-named-attributes - write the named attributes of the file/directory.  
> 	c = read-ACL - read the file/directory NFSv4 ACL.  
> 	C = write-ACL - write the file/directory NFSv4 ACL.  
> 	o = write-owner - change ownership of the file/directory.  
> 	y = synchronize - allow clients to use synchronous I/O with the server.  
> 	d = delete - delete the file/directory. Some servers will allow a delete to occur if either this permission is set in the file/directory or if the delete-child permission is set in its parent direcory.  
> 	D = delete-child - remove a file or subdirectory from within the given directory (directories only)  
> Liste des type  
> 	A = Allow  
> 	D = Deny  
> Liste des flags  
> 	g = group  
> 	d = directory-inherit  
> 	f = file-inherit  
> 	n = no-propagate-inherit  
> 	i = inherit-only  
> Liste des principles  
> 	nom_utilisateur@domain.lan  
> 	nom_groupe  
> 	OWNER@ = proprietaire du fichier  
> 	GROUP@ = groupe du fichier  
> 	EVERYONE@ = tous

`nfs4_getfacl fichier` = renvoi les NFSv4 ACL  

`nfs4_setfacl -a A::alice@nfsdomain.org:rxtncy fichier` = ajout une ACE autorisant read et execute a alixe@nfsdomaine.org sur fichier  
`nfs4_setfacl -a A::alice@nfsdomain.org:RX foo` = " " en utilisant les alias POSIX R et X (Read, Write,eXecute)  

###      ARCHIVE ET FICHIERS COMPRESSES

> par convention on rassemble les fichiers dans un dossier  
> qui est archiver en .tar pour pouvoir le compresser

####        ARCHIVER UN DOSSIER SANS COMPRESSION

`tar -c  dossier` = concatene les fichiers contenus dans dossier en une sortie unique  
`tar -vc dossier` = " " verbose  
`tar -c fichier1 fichier2 fichier3` = concatene les fichiers " " (non recommande)  
`tar -cf archive.tar dossier` = `tar -c dossier > archive.tar` = cree un fichier concatene archive.tar issue de dossier  

`tar -tf archive.tar` = `tar -t < archive` = liste les fichiers concatenes dans archive  

`tar -rf archivei.tar fichier` = ajoute fichier a l\'archive  

####        EXTRAIRE UN ARCHIVE NON COMPRESSE

`tar -xf archive` = extrait les fichiers de l\'archive dans le dossier courant  
`tar -xfC destination archive` = " " vers destination  

####        COMPRESSER UN ARCHIVE

`zip archive.zip fichier` = compresse fichier en une archive.zip  
`zip -r archive.zip dossier` = compresse le contenu de dossier en une archive.zip  

`gzip archive.tar` = compresse archive.tar et ajoute l\'extension .gz (compression normale et repandue)  
`bzip2 archive.tar` = compresse archive.tar et ajoute l\'extension .bz2 (compression elevee)  

####        DECOMPRESSER UN ARCHIVE

`unzip archive.zip` = decompresse archive.zip  
`unzip archive.zip -d repertoire` = " " dans repertoire  

`gunzip archive.tar.gz` = decompresse archive.tar.gz et retire l\'extension .gz  

`bunzip2 archive.tar.bz2` = decompresse archice.tar.bz2 et retire l\'extension .bz2  

`unrar e archive.rar` = decompresse l\'archive.rar (e non -e)  

####        ARCHIVER ET COMPRESSER DIRECTEMENT UN DOSSIER

`tar -zcf archive.tar.gz dossier` = archive et compresse le dossier avec gzip  
`tar -jcf archive.tar.bz2 dossier` = " " bzip2  

####        EXTRAIRE ET DECOMPRESSER DIRECTEMENT UN DOSSIER

`tar -zxf archive.tar.gz` = decompresse et extrait les fichiers de archive.tar.gz avec gunzip dans le dossier courant  
`tar -zxf archive.tar.gz -C dossier` = " " vers dossier  
`tar -jxf archive.tar.bz2` = " " avec bunzip2 " "  

####        AFFICHER UN TEXTE BRUT COMPRESSER

`zcat fichier.gz` = renvoi fichier en le decompressant a la volee (zmore zless fonctionnent aussi)  

###      LISTER LES FICHIERS D\'UN ARCHIVE OU D\'UN DOSSIER COMPRESSION

`unzip -l archive.zip` = liste le contenu de archive.zip  
`tar -ztf archive.tar.gz` = liste le contenu de l\'archive avec gzip  
`tar -jtf archive.tar.bz2` = " " bzip2  

####        CRYPTAGE DE FICHIER

`gpg -c fichier` = crypte fichier vers fichier.gpg apres avoir demandé de saisir un mot de passe  
`gpg fichier.gpg` = décrypte fichier  

###      FICHIERS DISTANTS

####        TELECHARGEMENT

`wget adresse_fichier` = telecharge fichier depuis un protocole HTTP ou FTP publique  
`[CTRL]+[C]` = stop le telechargement sans supprimer le fichier deja sur le disque  
`wget -c http....adresse_fichier` = continue le telechargement d\'un fichier en parti telecharge  
`wget --background http.....fichier` = telecharge en arriere plan (wget-log en sortie par defaut)  

####        COPIER A DISTANCE

`scp fichier login@ip:~/destination` = copie en mode securise fichier vers le serveur  
`scp -r dossier login@ip:~/destination` = " " avec l'option recursive  
`scp login@ip:fichier destination` = " depuis le serveur vers le client  
`scp -P port login@ip:fichier destination` = " en utilisant un port specifique pour ssh ( attention majuscule a P )  

####        SYNCHRONISER A DISTANCE

`rsync source login@ip:destination/` = synchronise source vers destination en mode securise SSH vers un serveur  
`rsync login@ip:source/ destination` = " "  
`rsync -va source login@ip:destination/` = " " avec les options habituelles ( verbose archive recursif )  
`rsync -va source login@ip:destination/ --port=22` = " " en specifiant le port SSH a utiliser  
`rsync --delete -r source login@ip:destination/` = " " supprime les fichiers n'existant pas dans source  
`rsync --delete -r -b --suffix=.bak source login@ip:destination/` = " " et fait un backup des fichiers supprimes leur ajoutant une extention .bak ( si non specifie : suffix=~)  

###      CLIENT FTP/FTPS

####        COMMANDES DE CONNEXION

#####          commandes de connexion ftp simple

`ftp` = lance le client FTP sans se connecter  
`open 192.168.1.10` = se connecte a une adresse IP ou a un nom de domaine  

`ftp 192.168.1.10` = ouvre FTP client et se connecte sur 192.168.1.10 sur le port par defaut  
`ftp 192.168.1.10 2121` = " " 192.168.1.10 sur le port 2121  

`close` = ferme la connexion sans quitter le client FTP  
`quit` = `bye` = `exit` = ferme la connexion et le client FTP  

#####          commndes de connexion ftps

`lftp -u user 192.168.0.10` = connexion au serveur avec prompt mot de passe  
`set set:verify-certificate no` = valide temporairement les certificats non signé par CA
`set ftp:passive-mode on` = active le passive mode  

####        COMMANDES DE NAVIGATION

`ls` = liste des fichiers dans le repertoire courant  
`cd dossier/` =  aller vers dossier/  
`cdup` = remonter dans l\'arborescence d\'un niveau  
`pwd` = afficher le chemin du repertoire courant  
`lpwd` = `!pwd` = connaitre son emplacement sur le client  
`lcd /home/` = aller vers le dossier /home/ du pc client  

####        COMMANDE D'UTILISATION

`get fichier` = download/telecharge fichier  
`mget fichier1 fichier2` = upload de plusieurs fichier (comptatible * et ?)  
`put fichier` = upload fichier  
`mput fichier1 fichier2` = upload fichier1 fichier2  
`delete fichier` = supprime fichier du serveur  
`!commande` = execute commande sur client (cd, cp, mv ...)  
`man ftp` = liste les commandes ftp disponibles  

####        COMMANDE DE CONFIGURATION DE MODE

`binary` = parametre le transfert en BINAIRE  
`ascii` = parametre le transfert en ASCII  

###      LES IMAGES

`imagemagick` = ensemble d\'outils pour convertir et modifier des images  

`identify image.jpg` = apporte des informations principales sur image.jpg  
`identify -verbose image.jpg` = apporte toutes les informations sur image.jpg  
`identify -verbose image.jpg | grep Quality` = " " concernant la qualite de l\'image  

`mogrify -resize 800x600 *.jpg` = redimensionne et ecrase les .jpg du dossier courant  
mogrify -resize 800x600 -path repertoire/ *.jpg redimensionne et enregistre les .jpg dans le dossier repertoire/.

`convert -thumbnail \'50x50\' photo destination` = faire une image miniature de 50x50 de photo_original et l\'enregistre en photo_miniature  
`convert -thumbnail \'50x50>\' photo destination` = " " seulement si l\'image est plus grande que 50x50  
`convert -thumbnail 20% photo destination` = " " 20% de la taille originale  

`convert -crop 50%x100% +repage image.svg image_%d.svg` = divise en 2 verticalement une image  

###      LES TEXTES

####        EDITEURS DE TEXTE

`nano` = editeur simple par defaut sans interface graphique  
`gedit` = editeur simple par defaut avec interface graphique  
`vi` = editeur puissant sans interface graphique (par defaut) installe partiellement  
`view` = vi en mode read-only  
`vim` = version amelioree de VI (a installer)  
`mc` = midnight commander est un editeur avec une interface graphique pour console comme nano (a installer)  

####        EDITEUR VI

`vi fichier` = edite fichier  
`vi + fichier` = edite fichier a partir de la dernière ligne  
`vi +10 fichier` = " " a la ligne 10  

####        MANIPULATION DE FICHIER TEXTE

`pdftotext fichier.pdf fichier.txt` = converti fichier.pdf en fichier texte (paquet poppler-utils)  
`convert -density 200 fichier.pdf image.jpg` = converti chaque page d'un fichier pdf en plusieurs images jpg  
`iconv --from-code=ISO_8859-1 --to-code=UTF8 fichier.php > fichier.php_utf8` = converti un code source php iso_8859-1 latin1 en utf8  

####        AFFICHAGE DIRECTE DE TEXTE

`echo texte` = renvoi simplement texte  
echo "\
texte sur
plusieurs ligne\
`"` = renvoi du texte sur plusieurs ligne  
`echo -e texte\ntexte` = renvoi texte en interpretant les backslash (\b espace arriere, \t tabulation, \v tabulation verticale, \n saut de ligne, \a bip)  
`echo -n texte` = renvoi texte et ne fais pas de retour a la ligne (le prompt ou la commande suivante suivra)  
`echo {a..z}` = renvoi les lettres de a a z separes d\'un espace  
`echo -e {1..100}"\n"` = renvoi les nombres de 1 a 100 separes d\'un saut de ligne  
`echo !!` = renvoi la dernière commande passee  

`echo -e \\033[1;39m JAUNE` = change la couleur du texte de toutes futures sorties du bash en jaune   
`color="\\033[1;33m" ; normal="\\033[1;30m" ; echo -e $color JAUNE $normal` = " " en utilisant une variable et en remettant la couleur initiale a la fin  

`\\033[1;33m` = jaune  
`\\033[1;30m` = blanc gris  
`\\033[1;31m` = rouge  

`printf '%20s'` = renvoi 20x espaces  
`printf '%0.s#' {1..5}` = renvoi #########          avec 0 entre les #   
`printf '%0.s#' $(seq 1 $i)` = renvoi $i nombre de #####          avec 0 entre les #   

####        AFFICHAGE D\'UN FICHIER

`cat fichier` = renvoi tout fichier  
`tac fichier` = " " à l'envers ligne par ligne  
`cat -n fichier` = renvoi tout un fichier en numerotant les lignes  
`cat << EOF > fichier` = récupère l'entrée standard pour écrire dans fichier et utilise le délimiteur textuel EOF pour s'arrêter  

`fmt fichier` = affiche un fichier sans couper les mots  
`fmt -w 100 fichier` = " " 100 caracteres maximum par ligne  

`nl fichier` = affiche les lignes contenant du texte et les numerote  
`nl -b a fichier` = " " toutes les lignes (y compris vide)  
`nl -b t fichier` = " " mais ajoute un saut de ligne pour les lignes vides  
`nl -b pMOT fichier` = affiche lignes contenants du texte, saute une ligne pour le vides et numerote uniquement celle contenant le motif MOT  
`nl -n ln fichier` = " " numero à gauche  
`nl -n rn fichier` = " " numero à gauche apres tab (default)  
`nl -n rz fichier` = " " numero XXXXXX à gauche  

`view fichier` = consulte fichier avec vi en mode read-only  

`tee ??` = duplique un flux de donnees  

`less` = renvoi progressivement tout fichier  
`less fichier` = renvoi progressivement un fichier  

`head fichier` = renvoi les permieres lignes de fichier (10 par defaut)  
`head -n 3 fichier` = renvoi les 3 premieres lignes  

`tail fichier` = renvoi les dernières lignes de fichier (10 par defaut)  
`tail -n 3 fichier` = renvoi les 3 dernières lignes  
`tail -f fichier` = afficle les dernières lignes de fichier et continu de le suivre toute les secondes  
`tail -f fichier1 fichier2` = " " 2 fichiers en meme temps  
tail -f -s 4 fichier= " " les 4 secondes

`sort fichier` = renvoi fichier en ordonnant les lignes alphabetiquement  
`sort -n fichier` = " " numeriquement ( par defaut sort reconnais les nombres comme des lettres )  
`sort -r fichier` = " " dans l\'ordre inverse  
`sort -R fichier` = desordonne les lignes de fichier et les affichent  
`sort -o destination source` = enregristre le tri de source dans destination  
`sort -t':' -k 7d -k 3n /etc/passwd` = tri les fichiers par colonne 7 alphabetiquement et par colonne 3 numeriquement(d:dictionnaire,n=numeric,n:chronologique)(r:reverse)  

`wc fichier` = compte les lignes, mots, octets de fichier (-l=ligne, -w=word, m=caractere, c=octet)  
`wc -l fichier` = compte les lignes de fichier  
`wc fichier1 fichier2` = " " fichier1 et fichier2 puis renvoi le total  

`uniq fichier` = renvoi fichier en supprimant les lignes repetees a la suite  
`uniq fichier nouveaufichier` = reecrit fichier sans lignes doublons vers nouveaufichier  
`uniq -c fichier` = renvoi fichier en supprimant les lignes en doublons mais en indiquant combien de fois chaque ligne est repetee  
`uniq -d fichier` = renvoi uniquement et une fois les lignes qui sont doublees  

`join -1 1 -2 1 fichier1 fichier2` = concatene les de 2 fichier qui partage un champ commun (ici 1 pour le fichier 1 et 2)  
`join -t':' -1 1 -2 1 fichier1 fichier2` = " " avec comme separateur de colonne:  

`paste fichier1 fichier2` = concatene à l'horizontal plusieurs fichiers (ligne1fichier1+ligner1fichier2)  

`split -l 2 fichier` = divise fichier en plusieurs fichier de 2 lignes max et genere directement les fichiers  
`split -l 2 fichier sortie_` = " " determine le nom des fichiers de sortie  
`split -c 10M fichier sortie_10M` = " " avec une taille max  

####        FILTRE

####        EXPAND UNEXPAND

`expand fichier` = remplace les espaces par des tabulations (8 espaces par defaut)  
`expand -t 2 fichier` = remplace 2 espaces par une tabulation  

`unexpand fichier` = remplace les tabulations de debut de ligne par de espaces  
`unexpand -a fichier` = " " toutes les tabulations  

####        GREP

> grep est un filtre par ligne

`grep motif fichier` = renvoi les lignes contenants "motif" dans fichier  
`grep -i motif fichier` =  renvoi les lignes contenants "motif" (ignore la casse)  
`grep "motif avec espace" fichier` = renvoi les lignes contenants "motif avec espace" dans fichier   
`grep -r motif dossier` = recherche et renvoi les lignes contenants motif dans les fichiers de l'arborescence  
`grep -w motif fichier` = renvoi les lignes contenant le mot  "motif" precisement (n'etant pas colle a un chiffre, une lettre ou un underscore_)  
`grep -x motif fichier` = renvoi les lignes ne contenant que "motif"  

`grep motif --include=*.php --include=*.html` = recherche le motif uniquement dans les .php et .html  

`grep -n motif fichier` = renvoi les lignes contenant motif en indiquant le numero de la ligne  

`grep -v intrus fichier` = renvoi toutes les lignes ne contenant pas "intrus"  
`grep -I motif fichier` = " en excluant de la recherche les fichiers binaires (a utiliser avec -r)  

`grep -c motif fichier` = renvoi uniquement le nombre de ligne ou l'occurence motif est trouvee  

`grep -r motif dossier` = renvoi les lignes contenant motif dans tous les fichiers de dossier  

`grep -A 3 motif fichier` = renvoi la ligne contenant motif et les 3 lignes suivantes  

#####          grep et regex


`grep` = `grep -G` = grep avec les regex BRE  
`egrep` = `grep -E` =  grep avec les regex BRE et ERE  
`fgrep` = `grep -F` = grep sans regex  
`grep -P` = regex de PCRE/Perl (prise en chage incomplete)  

`'. * ^ $ [liste] [^instrus] \b'` =  liste des regex POSIX reconnu par grep par defaut (sans les apostrophes)  

`grep ".ebut" fichier` = lettre quelconque suivie de "ebut"  
`grep "^debut" fichier` = "debut" ecrit en debut de ligne  
`grep "\bmot" fichier` = tout motif ayant pour limite de debut "mot"  
`grep "mot\b" fichier` = tout motif ayant pour limite de fin "mot"  
`grep "fin$" fichier` = "fin" ecrit en fin de ligne  
`grep "[Aa]lias" fichier` = "alias" ou "Alias"  
`grep "[0-9]" fichier` = chiffre  
`grep "[a-zA-Z]" fichier` = lettre simple minuscule ou majuscule  

`grep -P ".{5}" fichier` = renvoi 5 caracteres quelconques  
`grep -P ".{5,10}" fichier` = renvoi 5 a 10 caracteres quelconques  
`grep -P "\n" fichier` = renvoi les sauts de ligne (fonctionne aussi avec \s \S \w \W \d \D)  

`egrep "^debut.fin$" fichier` = renvoi les lignes contenant 2 motifs ^debut et fin$  

####        CUT

> cut est un filtre de chaine de caracteres

`cut -c 5 fichier` = renvoi le 5eme caractere de fichier  
`cut -c 5-10 fichier` = renvoi  du 5eme au 10eme caracteres de fichier  
`cut -c 5,10 fichier` = renvoi le 5eme et 10eme caracteres de fichier  
`cut -c -5 fichier` = renvoi les 5 permiers caracteres de fichier  
`cut -c 5- fichier` = " 5 derniers "   

`cut -f 1 fichier` = renvoi le 1er champ de chaque ligne de fichier separe avec une tabulation (separateur par defaut)  
`cut -d , -f 5- fichier` = renvoi tous les champs a partir du 5eme separe par le separateur ","  
`cut -d , -f 5,10 fichier` = renvoi le 5eme champ et le 10eme separe par le separateur ","  
`cut -d , -f 5,10-15 fichier` = renvoi le 5eme champ et du 10eme au 15eme "  

####        TR

> tr est un filtre de transcription
> Il ne reconnait pas les expressions rationnelles  
> il reconnait cependant les metacaracteres d'intervalle de type a-z 0-9, les backslah et les code ASCII  
> tr s'utilise avec les entrees sorties de commandes | < > >> <<  
> attention a la syntaxe : tr [filtre] < fichier >fichier 

`tr 'a-z' 'A-Z'` = remplace les caracteres minuscule en majuscul  
`tr -c '0' '1'` = remplace tous les caracteres complementaires a 0 par 1 (donc tous sauf 0 sera remplace par un 1)  

`tr -d 'a-z'` = supprime toutes les minuscules  
`tr -d "\n"` = supprime les sauts de lignes (fonctionne aussi avec \t)  
`tr -s ' '` = supprime toutes repetitions d'espaces  
`tr -s "\n"` = supprime toutes repetitions de saut de ligne  

####        SED

> sed est un filtre fonctionnant ligne par ligne  
> les delimiteurs / par defaut pour separer les expressions peuvent etre remplace par un autre delimiteur comme ####        pour ne pas se confondre avec le chemin d'acces a un fichier- Pour fonctionner, sed lit la premiere ligne de flux d'entree, la stocke dans l'espace de travail en enlevant le caractere fin de ligne \n, traite la ligne si besoin, l'renvoi en remettant un caractere de fin de ligne \n, puis vide son espace de travail pour passer a la suivante.
> \n n'est donc pas reconnu.

#####          commande basique sed

`sed '' fichier` = renvoi toutes les lignes de fichier  

`sed 's/motif/remplacement/' fichier` = modifie l\'occurence 1 de chaque ligne  
`sed 's/motif/remplacement/g' fichier` = modifie de façon global les lignes  

`sed -i '/instruction_sed/' fichier` = en  

`sed 5q fichier` = affiche jusqu\'a la 5eme ligne  
`sed '/instruction_sed/ ; 5q'` = ferme le filtre a la 5eme ligne  

#####          gestion des options et drapeaux

`sed -n 's/motif/remplacement/p' fichier` = mode silence (-n) et affiche les lignes modifiees (/p)  

`sed '/instruction_sed/w out_file' fichier` = renvoit le reultat et enregistre uniquement les lignes modifiees dans out_file  

`sed 's/motif/commande bash $var/e' fichier` = remplace par la sortie de la commande bash (si export var)  

`sed -n 1,3p fichier` = renvoi les lignes 1 a 3  
`sed -n $p fichier` = renvoi la dernière ligne  

#####          substitution basique sed

`sed 's/substitution/' fichier` = subtitution  

#####          suppression de lignes

`sed 5d fichier` = supprime la ligne 5  
`sed 5!d fichier` = supprime toutes lignes sauf la 5  

`sed /^$/d fichier` = supprime les lignes vides de fichier (saut de lignes)

#####          insertion de lignes

`sed "5itexte" fichier` = renvoi fichier et insere la ligne texte avant la ligne 5  
`sed "5i$variable" fichier` = renvoi fichier en inserant la ligne, valeur de variable, avant la ligne 5  
`sed "5i$variable" fichier` = renvoi fichier en inserant la ligne "$variable" avant la ligne 5 sans le contenu de variable  
`sed "5i$variable" fichier` = renvoi fichier en inserant la ligne, valeur de variable, avant la ligne 5  

#####          ajout de ligne

`sed "5atexte" fichier` = ajoute la ligne texte apres la ligne 5  
`sed "^atexte" fichier` = " " premiere ligne du fichier  
`sed "$atexte" fichier` = " " dernière ligne du fichier  

#####          remplacement de ligne

`sed "5ctexte" fichier` = renvoi fichier et remplace la ligne 5 par la ligne texte  

#####          utilisation de plusieurs filtres sed

`sed -e '/instruction_sed/' -e '/instruction2_sed/' fichier` = enchaine plusieurs commande sed  
`sed '/instruction_sed/ ; /instruction2_sed/' fichier` = " "  

`sed -f script fichier` = utilise le fichier script comme script de filtrage sur fichier  

#####          filtre

`sed -n '/motif/p' fichier` = renvoi les lignes contenant motif  
`sed -n '\#motif#p' fichier` = renvoi les lignes contenant motif en utilisant le deliminateur # (pratique si le motif contient /)  
`sed -n '/debut/,/fin/ p' fichier` = renvoi les lignes entre le motif debut et fin  
`sed '5,10d' fichier` = renvoi fichier et supprime les lignes 5 a 10  
`sed ',10d' fichier` = renvoi fichier sans les lignes 1 a 10  
`sed '/motif1/d ; /motif2/d' fichier` = renvoi fichier sans les lignes contenant motif1 et sans les lignes contenant motif2  

`sed -ne 's/.*\(http[^"]*\).*/\1/p' fichier` = renvoi les urls present dans fichier  

`sed -r 's/[0-9]{2}/00/g' fichier` = option pour utiliser la syntaxe avec des expressions rationnelles etendue, remplace deux chiffres consecutif par 00  
`sed -ri -e 's/[0-9]{2}/00/g' fichier` = " " en activant l'option script et regex (l'ordre des parametre a une importance surtout pour i)  
`sed -i -n 1p fichier` = modifie directement fichier en appliquant le filtrage  

#####          substitution du motif par un autre

`sed 's/la/LA/' fichier` = remplace le premier "la" par "LA"  
`sed 's/la/LA/g' fichier` = remplace tous les "la"   
`sed 's/la/LA/2' fichier` = remplace le 2eme "la"  

`sed 's/motif1/sub1/ ; s/motif2/$sub2/' fichier` = chaine plusieurs subsitutions  
`sed "s/motif1/$var1/ ; s/motif2/$var2/" fichier` = " " en utilisant des variables dont le contenu est sans caracteres speciaux ou deja echapes  

`sed '/^#/ s/la/LA/g' fichier` = remplace les la des lignes commentees   
`sed '0,/motif/ s//LA/' fichier` = remplace le premier motif (0,/motif/) rencontre par un LA  

#####          remplacement caractere par caractere

`sed 'y/aäâáa/aaaaa/g' fichier` = remplace chacune des lettres respectivement par l'autre  
`sed 'y/a-z/A-Z/g' fichier` = remplace les minuscules sans accents par des majuscules sans accents  

`sed '/[ ]/ s/la/LA/g' fichier` = remplace tous les la des lignes contenant un espace  
`sed '/[lL]a/ s/a/A/g' fichier` = remplace les a par A si presence d'un la ou d'un La  

`sed '4 s/la/LA/g' fichier` = remplace les la de la ligne 4   
`sed '1,4 s/la/LA/g' fichier` = " entre la ligne 1 et 4 incluse   
`sed '/debut/,/fin/ s/la/LA/g' fichier` = remplace les la par LA entre debut et fin  
`sed '1,/fin/ s/la/LA/g' fichier` = " ligne 1 a fin   
`sed '1,$ s/la/LA/g' fichier` = " ligne 1 a fin du fichier   

`sed '1,3 s/.*/REMPLACE/' fichier` = remplace tous le texte de la ligne 1 a 3 incluses par REMPLACE  
`sed '1,3 {1b;3b; s/.*/REMPLACE/; }' fichier` = " excluses  
`sed '1,3 {1b; s/.*/REMPLACE/; }' fichier` = " ligne 1 excluses  

#####          selection de plusieurs motifs
`sed 's/\(motif1\)texteinutile\(motif2\)/\2inverse\1/' fichier` = selectionne et inverse deux motifs \1 et \2  

#####          translitteration

`sed 'y/liste1/liste2/' fichier` = renvoi fichier en remplaçant chaque caractere de liste1 par ceux correspondant de liste2  

#####          multiligne

`sed -e 'N ; s/\n//' fichier` = charge la premiere et deuxieme ligne dans l'espace de travail en effaçant le dernier caractere de fin de ligne \n puis effectue le traitement pour supprimer le caractere de fin de ligne (qui est present a la fin de la premiere ligne) puis passe a la troisieme et quatrieme ligne  
`sed -e '/motif/ {N;D}` = chaine toutes lignes a la suite contenant motif  

####        GAWK AWK

> (g)awk est la version de GNU de awk (a installer)

`gawk '{print}' fichier` = renvoi fichier  
`gawk '{print NR}' fichier` = renvoi uniquement les numeros des lignes de fichier  
`gawk '{print "Ligne "NR" : "$0}' fichier` = renvoi $0 ($0 correspond aux lignes entieres du fichier) precede du numero de chaque ligne mis en page  

`gawk -F "," '{print $2}' fichier` = renvoi le champ 2 des lignes separe par ","  
`gawk -F "," '{print NF}' fichier` = renvoi le nombre de champ ($NF est la valeur du dernier champ)  

`gawk '/regex/ {print $0}' fichier` = renvoi les lignes de fichier correspondant a la regex uniquement  

`gawk -F ":" '{print $1 ; print $2}' fichier` = enchaine plusieurs instructions (pour print un saut de ligne s'effectue a chaque execution)  
`gawk -F'[][]' '{print $2}' fichier` = affiche les éléments entre crochet  

`awk '$1>10 {print $0}' fichier` =  affiche la ligne uniquement si le premier champ est superieur à 10  

`ps -f |  awk -v pid=$$ '{if ($3 == pid) print $0;}'` = créé une variable et test si présent à colonne 3


#####          utilisation avance : les conditions :

> // == / != / >= / <= / || / && / !

`gawk -F ":" '{ if ($1==test) print $1}' fichier` = si le champ 1 correspont a test alors renvoi celui-ci  
`gawk -F ":" '{ if (substr($1, 0, 1)==a print $1}' fichier` = si le permier 1 caractere 0 du champ 1 $1 vaut a alors renvoi $1  

#####          fichier script awk

`awk -F : -f script_awk /etc/passwd` = lance awk en utilisant un script awk  

> BEGIN { SOMME = 0 ; print "Debut\n" }  
>   
> {  
> SOMME += $3  
> print $1 " " $3 " " $4  
> }  
>   
> END {  
> print "\nFin du traitement"  
> print "SOMME = " SOMME  
> }


##    MATERIEL

###      INFORMATION MATERIEL

`lsusb` = liste les peripheriques usb  
`lspci` = " " pci  
`dmidecode` = liste complete d\'information complete du materiel  
`dmidecode -t memory` = liste /  

`upower -i /org/freedesktop/UPower/devices/battery_BAT1` = information sur la batterie et son niveau de charge  
`upower -e` = liste des fichiers consultatables  
`upower -i $(upower -e | grep BAT ) | egrep "percentage|state"` = affiche le pourcentage du niveau de batterie et son etat de charge  

`cat /proc/device-tree/model` = (raspbian) renvoit le modèle du RPi

###      PERIPHERIQUE DE STOCKAGE

####        INFORMATION PARTITION

`cat /proc/partitions` = renvoi les partitions montes sur le systeme  
`fdisk -l` = liste les partitions des disques montes et leur informations  
`blkid` = lit les informations d\'identification de la table de partition des disques (Block ID)  
`lsblk` = liste les partitions, la taille et les points de montage des peripheriques de stockage (ls block)  
`df -h` = renvoi des informations detailles sur l\'espace disque  

####        MONTAGE DES PARTITIONS ET PERIPHERIQUES DE STOCKAGE

`mount /dev/sda1 /point-de-montage` = monte /dev/sda1 dans le dossier LABEL existant avec les options auto  
`umount /point-de-montage` = `umount /dev/sda1` = demonte /dev/sda1 avec root uniquement  
`fusermount -u /point-de-montage` = demontage d\'un systeme de fichier disponible a l\'utilisateur en cours  
`mount -a` = monte toutes les partitions inscrites dans /etc/fstab  
`mount -o remount /point-de-montage` = remonte la partition en prenant en compte les modifications du fichier /etc/fstab  
`mount -o ro,noload -t ext4 /dev/sdb1 /point-de-montage` = remonte une partition ext4 d'un disque defectueux en lecture seule (read-only)  

`mount -t cifs //192.168.1.5 /point-de-montage -o username=kyprio` = monte un dossier partage du type protocole samba/smb (cifs) en utilisant en login kyprio  
`mount -t cifs //192.168.1.5 /point-de-montage -o username=kyprio,password=motdepasse ` = " " avec un mot de passe pre-remplie  

`sshfs -p 22 user@192.168.1.5:/home/user/ /point-de-montage` = montage d'un point de montage d'un dossier disantt  

#####          montage d\'une partition dans une image .img d\'une distribution linux

`fdisk -l debian.img` = puis relever le nombre de bytes par secteurs et le numero du secteur de debut pour la partition a monter  
`let "nombre=122880*512"` = stockage du numero de la partition et du nombre de bytes par secteurs dans une variable  
`losetup /dev/loop0 debian.img -o $nombre` = stockage des parametres de la partition dans le fichier /dev/loop0  
`/dev/loop0 /mnt/debian` = monte la partition dans le dossier pre-existant /mnt/debian  
`umount /mnt/debian` = demonte la partition  
`losetup -d /dev/loop0` = supprime les donnees dans /dev/loop0  

####        CONSEIL PARTITIONNEMENT

#####          partitionnement pour un serveur

> swap = 0 a 6x taille de la RAM  
> / = 1,44M (si fluxbox utilise) a 8Go  
> /var/log = tres solicite sur un serveur  
> /home  
> // partitionnement pour un client  
> swap = 0 a 3x taille de la RAM  
> / = tout

`mkfs -t ext4 /dev/sda1` = creation du systeme de fichier ext4 sur la partition /dev/sda1 prealablement cree  

####        OUTILS PARTITIONNEMENT STANDARD

`fdisk /dev/sda` = entre dans la configuration du disque sda1  
`	m` = liste des commandes  
`	n` = cree une nouvelle partition, le numero de partition 1 correspondra à /dev/sda1  
`	d` = supprime les partitions  
`	w` = quitte et enregistre les modifications  
`	q` = quitte sans enregistrer  
`mkfs -t ext4 /dev/sda1` = formate en ext4 la partition sda1  
`fsck /dev/sda1 ??` =  verifie la partition /dev/sda1  

`mkfs.xfs /dev/sda1` = formate en xfs  

####        OUTILS PARTITIONNEMENT LVM

> a partir d'un disque vierge ou prealablement vide avec fdisk d
> d'abord on ajoute des disques ou des partitions vierges au Physical Volume ( il ne sera plus expoitable par son nom direct )  
> ensuite on cree/ajoute un Volume Group PV  
> enfin on cree/affecte de l'espace dans de PV a une Logical Volume qui sera lu comme une partition vierge.

`pvdisplay` = renvoi les Physical Volume  
`vgdisplay` = " " les Volume Group  
`lvdisplay` = " " Logical Volume  

`lvdisplay | grep -A 1 Path` = liste les path complets de chaque lv  


`pvcreate /dev/sdb /dev/sdc` = ajoute sdb et sdc au Physical Volume  
`vgcreate MyVG /dev/sdb /dev/sdc` = cree le Volume Group MyVG avec sdb et sdc dedans  
`lvcreate -n MyLV1 -L 130G MyVG` = cree la partition Logical Volume nomme MyLV1 dans le VG MyVG et faisant 130Gb  
`lvcreate -n MyLV1 -l 100%FREE MyVG` = cree la partition Logical Volume nomme MyLV1 dans le VG MyVG et occupant tout l'espace disponible  

`lvremove /dev/proxmox/snap_vm-104-disk-1_comment` = supprime une lv ou un snapshot   


`vgextend MyVG /dev/sdd` = etends MyVG en integrant sdd prealablement ajoute au PV  
`lvextend -L +120G /dev/mapper/MyVG-MyLV1` = ajoute 120G a la partition MyLV1  
`resize2fs /dev/mapper/MyVG-MyLV1` = etends a chaud la partition formatee MyLV1 par rapport a sa nouvelle taille (ext2/ext3/ext4)  
`xfs_growfs /dev/mapper/MyVG-MyLV1` = " " (xfs)  

`vgreduce MyVG -a` = retire les PV inutilises  
`vgremove MyVG /dev/sdb` = retire /dev/sdb du VG  

`pvremove /dev/sda` = retire sda des Physical Volume  
`pvresize /dev/sda1` = etends a chaud le PV apres avoir ete redimenssionne par fdisk  

###      PERIPHERIQUE EXTERNE

#####          connexion en TTL port serie RS 232

`dmesg | grep ttyUSB` = determine le numero du port ttyUSB a utiliser  
`screen /dev/ttyUSB0 115200` =  se connecte en TTL avec une vitesse de transfert de 115200 bps sur un Raspberry Pi  
`fils Vert Blanc Noir - Rouge` = connexion en TTL sur GPIO du Raspberry Pi en fournissant l'alimentation  
`fils Vert Blanc Noir - -` = " " sans fournir l'alimentation  

###      RASPBERRY PI

#####          GPIO B/B+/Zero/2B/3B

> en utilisant le cable IDE seul les 2 colonnes s'inversent

`01 / 02` = 3V3 Power / 5V Power  
`03 / 04` = GPIO2 SDA1 I2C / 5V Power  
`05 / 06` = GPIO3 SCL1 I2C / Ground  
`07 / 08` = GPIO4 / GPIO14 UART0_TXD  
`09 / 10` = Ground / GPIO15 UART0_RXD  
`11 / 12` = GPIO17 / GPIO18 PCM_CLK  
`13 / 14` = GPIO27 / Ground  
`15 / 16` = GPIO22 / GPIO23  
`17 / 18` = 3V3 Power / GPIO24  
`19 / 20` = GPIO10 SPI0_MOSI / Ground  
`21 / 22` = GPIO9 SPI0_MISO / GPIO25  
`23 / 24` = GPIO11 SPI0_SCLK / GPIO8 SPI0_CE0_N  
`25 / 26` = Ground / GPIO7 SPI0_CE1_N  
`27 / 28` = ID_SD I2C ID EEPROM / ID_SC I2C ID EEPROM  
`29 / 30` = GPIO5 / Ground  
`31 / 32` = GPIO6 / GPIO12  
`33 / 34` = GPIO13 / Ground  
`35 / 36` = GPIO19 / GPIO16  
`37 / 38` = GPIO26 / GPIO20  
`39 / 40` = Ground / GPIO21  

####        RESISTANCE

`10 000Ω 5%` = marron noir orange or  
`221Ω +2%` = rouge rouge marron noir marron  
`330Ω +5%` = orange orange marron or  

####        BRANCHEMENT

#####          simple sans gpio

3V3+ -> Resistance 221Ω -> LED -> 3V3-/Ground

#####          simple avec gpio en root

7 GPIO4 -> Resistance -> LED -> Ground
echo 4 > /sys/class/gpio/export 
`echo out > /sys/class/gpio/gpio4/direction ` = initialise le GPIO en SORTIE (in pour IN)  
`echo 1 > /sys/class/gpio/gpio4/value` = allume la LED  
`echo 0 > /sys/class/gpio/gpio4/value` = eteint la LED  

#####          etat bouton

7 GPIO4 -> Resistance -> bouton -> Ground
echo 4 > /sys/class/gpio/export 
`echo in > /sys/class/gpio/gpio4/direction ` = initialise le GPIO en SORTIE (in pour IN)  
`cat /sys/class/gpio/gpio4/value` = renvoi l'etat du bouton (1=circuit ouvert, 0=circuit ferme)  

####        INFO LED

`led blanche` = 23mA tension 3V 60mW  
`led rouge` = 20 mA 1,6V 30mW  
`led bleue` = 20mA 3.6V 70mW  
`led verte` = 20mA 2.1V 40mW  
`led jaune` = 20mA 2.1V 40mW  


##    ADMINISTRATION SYSTEME

###      INFORMATION SYSTEME

####        INFORMATION SYSTEME

`uname -a` = information sur l\'ordinateur, la version de la distribution linux et l\'architecture du systeme  
`hostname` = renvoi le nom de l\'ordinateur  
`hostname machine` = affecte temporairement le nom machine a l\'ordinateur  
`hostnamectl set-hostname machine` = affecte définitivement " " après reboot  
`lsb_release -a` = liste toutes les informations du systeme d'exploitation  
`lsb_release -d` = liste uniquement le nom de l'OS et sa version  
`lsb_release -ds` = " " en supprimant le nom de la categorie (pratique pour les script)  

`timedatectl` = affiche les informations sur la timezone  
`timedatectl list-timezones` = liste de timezone reconnue pris en charge  

####        INFORMATION SUR L\'ACTIVITE

`uptime` = temps depuis dernier allumage, charge CPU et RAM actuelle  
`uptime -p` = temps depuis dernier allumage uniquement human readable  
`uptime -s` = date et heure d\'allumage  

`w` = renvoi l\'activite du systeme sur lequel on se trouve, en indiquant l\'heure, la duree de fonctionnement de l\'ordinateur, le nombre d\'utilisateurs sur un shell, la charge moyenne sur 1min, 5min, 15min (max=2 pour un dual core, 4 quad core), la liste des connectes utilisateurs en indiquant pour chacun son login, la console utilisee, son adresse, l\'heure de connexion, le temps d\'inactivite depuis la dernière commande  
`tload` = renvoi un graphique mis a jour chaque minute sur l\'utilisation du processeur  
`who` = renvoi la liste des connectes a l\'ordinateur   

`ps` = liste les processus en cours d\'utilisation uniquement au moment ou la commande est lancee ( par defaut : de l\'utilisateur en cours avec une mise en page oriente utilisateur )  
`ps -o pid,cmd` = " " uniquement les colonnes pid et cmd  
`ps --no-headers` = " " sans les headers  
`ps -f` = " " avec la colonne du nom de l\'utilisateur  
`ps -e` = " " de tous utilisateurs  
`ps -j` = " " avec une mise en page oriente processus  
`ps -H` = " " hierarchiquement ( processus lanceurs d\'autres processus )  
`ps -u root` = liste les processus lances par root  

`ps -ef` = liste tous les processus de façon complète  
`ps ef` = liste tous les procéssus de façon généalogique et compact  

`top` = liste dynamiquement les processus lances et les tries en fonction de l\'utilisation du processeur  
`lsof` = liste les fichiers ouverts  
`lsof -i` = " " fichiers IP uniquement  
`lsof -P` =   

####        JOURNAUX LOG ET TEMP

`dmesg` = renvoi les messages du noyau notament lie au boot  

`tail -f /var/log/mail.info` = renvoi en temps reel le fichier mail.info  

`mktemp` = cree un fichier temporaire accessible par l\'utilisateur en cours et renvoi son chemin  

`last reboot` = liste des reboots  

`journalctl -xe` = check des log systèmes
`journtalctl -xef` = " " en mode live (comme tailf)

####        LANCER UN PROCESSUS

`commande` = lance une commande dans la console et la garde en premier plan  
`commande &` = lance commande en arriere plan en restant attache a la console. [1] 1er processus en arriere plan, 12345=PID.  
`commande > fichier 2>&1 &` = " " redirige le resultat et les erreurs dans fichier (neccessaire sur certaines commandes qui ne fonctionnent en arriere-plan mais qui doivent afficher leurs resultats dans la console)  
`nohup commande` = lance commande et la detache de la console (ceci redirige le resultat et les erreurs a la fin d\'un fichier nohup.out)  

`CTRL z` = met en pause une commande trop longue puis renvoi son PID et son numero de processus mis en pause  
`bg` = reprends la dernière commande mise en pause et la fait tourner en arrieve-plan  
`fg` = " " et la fait tourner en premier plan  
`fg %2` = reprends la commande [2] mise en pause pour la passer en premier plan  
`jobs` = liste les processus en arriere-plan ou mis en pause  

`wait` = attend la fin des processus en arriere plan  
`wait 344` = " " du pid 344  

`time commande` = execute commande et renvoit le temps reel utilisateur et systeme  

####        COMMUNICATION AVEC PROCESSUS

`kill PID` = tue le processus a partir de son numero PID  
`kill PID1 PID2` = " " pour plusieurs processus  
`kill -9 PID` = force la fermeture d\'un processus  
`killproc processus` = tue processus  
`killall processus` = tue l\'ensemble des processus issues de processus  

####        LE TEMPS

`date` = renvoi la date par defaut (jours, date, heure, decalage horaire)  
`date "+%T %D"` = " " le temps et la date completes separees d\'un espace (%H heure %M minute %S seconde %D date complete %d jour %m mois %Y annee)  
`date "+%Y%m%d_%H-%M"` = " " en format pratique pour l'incrementation de fichier  
`date MMDDhhmmYYYY` = ajuste la date et l\'heure du systeme (au format Mois Jours Heures minutes Annee)  

####        PLANIFIER UN PROCESSUS

`at hh:mm` = planifie une action a hh:mm (il faut ensuite entrer les commandes et conclure avec [CTRL]+[D]  
`at hh:mm tomorrow ...` = " " demain midi  
`at hh:mm MM/dd/YY` = " " date precise  
`at now +mm minutes` = " " dans mm minutes (hours days weeks months years)  

`atq` = liste les jobs plannifies et leur numeros  
`atrm 12` = supprime la tâche 12  

`crontab -l` = renvoi le fichier crontab de plannification de tâches reguliere  
`crontab -r` = supprime sans confirmation le crontab de l\'utilisateur en cours  
`crontab -e` = modifie avec l\'editeur par defaut le crontab (format : m minute  h heure  dom jour du mois  mon mois  dow jour de la semaine  commande>> fichier_sortie 2>&1)(valeurs possibles:* pour non specifie, [1-9][0-9] sachant que dimanche vaut 0 et samedi 6, 1,3,5 pour 1et3et5, 1-10 de 1a10, */5 tous les 5, )  
`(crontab -l 2>/dev/null; echo "*/5 * * * * /path/to/job -with args") | crontab -` = ajoute un cron de façon non interractive

###      COMMANDES SYSTEME

####        EXTINCTION SYSTEME

`halt` = `shutdown` = `init 0` = arrete l\'ordinateur  
`reboot` = `shutdown -r` = `init 6` = redemare  
`shutdown -c` = annule un shutdown  
`shutdown +5` = eteint dans 5min  
`shutdown 20:00` = eteint a 8pm  
`shutdown -k +10 "L\'ordinateur va bientot fermer vos sessions mais ne s\'arretera pas completement"` = ferme les sessions avec affichage de message dans 10min  

####        NOTIFICATION AVEC NOTIFY-SEND

> les icones sont disponibles dans  
> /usr/share/icons/ et /usr/share/notify-osd/

`notify-send -u critical "NOTIFICATION"` = renvoi une notification sur le bureau de l'utilisateur  
`notify-send -u critical "TITRE" "contenu\ncontenu" -i emblem-default` = " " avec titre et texte ainsi qu'une icones OK  
`notify-send -u critical "TITRE" "contenu\ncontenu" -i software-update-urgent` = " " avec titre et texte ainsi qu'une icones de crash  

####        CONFIGURATION DES PREFERENCES SYSTEMES

`locale-gen fr_FR.UTF-8 en_US.UTF-8` = configure les locales de maniere permanente  
`locale-gen --purge fr_FR.UTF-8 en_US.UTF-8` = installe uniquement les locales indiquee de maniere permanente  
`dpkg-reconfigure locales` = reconfigure les locales (inscrire au moins la langue du systeme en_US.UTF-8)  

> timezone sur centos/redhat
> ln -sf /usr/share/zoneinfo/Europe/Paris /etc/localtime

###      GESTION DES PROGRAMMMES

####        DEBIAN UBUNTU APT

#####          gestion des dépots

apt update = apt-get update = mets la liste des paquest à jour et compare avec les paquets installés

> ajout d'un dépot tiers 
> /etc/apt/sources.list/new_repo.list  
> `deb http://repo.mongodb.org/apt/debian stretch/mongodb-org/4.0 main`  
> puis ajouter la clés public si besoin

`wget -O - https://riot.im/packages/debian/repo-key.asc | apt-key add -` = telecharge à la volée et ajoute la clés au dépot autorisé

#####          installation deinstallation

`apt install paquet` = installe paquet  
`DEBIAN_FRONTEND=noninteractive apt install paquet` = " " en utilisant temporairement le mode non interractif  
`apt install -y grospaquet` = installe paquet et accepte l\'installation des dependances  
`apt install -q paquet` = installe en mode silencieux ce qui limite les indications de progression (-q et -qq)  

`apt remove paquet` = deinstalle paquet  
apt remove --purge deinstalle paquet ses fichiers de configurations et ses modules
`apt autoremove` = desinstalle les dependances orphelines  
`apt autoremove paquet` = desinstalle nompaquet et ses dependances  

`apt remove paquet --run-dry` = simule la desinstallation de paquet  

`apt purge $(dpkg -l | awk '{print $2}' | grep -E "linux-(image|headers)-$uname -r || cut -d- -f1).* | grep -v $(uname -r | sed -r -e 's:-[a-z]+*'))` = purge anciens noyaux  

`apt upgrade` = apres un update, met a jour les paquets du systeme  
`apt upgrade -y` = " " en confirmant automatique les choix proposes  
`apt dist-upgrade` = met a jour les paquets installes et le noyau linux  

`do-release-upgrade --check-dist-upgrade-only` = verifie la presence d\'une mise a niveau  
`do-release-upgrade --sandbox` = permets de tester une mise a niveau sans risque  
`do-release-upgrade` = recherche et met a niveau le systeme en suivant la politique par default de mise a niveau de root  

#####          recherche de paquet

`apt search paquet` = cherche paquet dans la liste des depots  
`apt show paquet` = description d\'un paquet avec sa version  

`dpkg -l paquet` = indique l\'etat du paquet sur le systeme (ii=installe) (debian)  
`dpkg -S /bin/ls` = 

#####          configuration paquet

`dpkg --configure -a` = configure les dependances lors d\'un probleme mineur  
`apt install -f` = a faire apres la commande precedente  
`dpkg-reconfigure -phigh -a` = suite a un probleme lors d\'une mise a niveau  
`dpkg-scanpackages ./ /dev/null | gzip -9c Packages.gz` = liste et analyse les .dev du  dossier en cours pour creer le fichier Packages.gz pour les depots (lent et non verbeuse) (paquet dpkg-dev)(pratique pour creer un depot à partir des DVD)  

####        RED HAT ENTERPRISE LINUX FEDORA CENTOS RPM YUM DNF

####           souscription et licence

> http://www.redhat.com/wapps/store/catalog.html  
> login/mdp requis  
> souscription d'évaluation 30 disponible

`subscription-manager register` = se loggue avec l'utilisateur et mdp puis active la licence  
`subscription-manager list --available` = liste des abonnements possibles pour le compte  
`subscription-manager attach --poolxxxxxxxx` = attache la machine d'id poolxxxxxx à la souscription du compte  

#####          installation deinstallation manuellement

`rpm -i paquet.rpm` = installe manuellement  
`rpm -iv paquet.rpm` = " " verbose (v,vv)  
`rpm -ih paquet.rpm` = " " avec barre de progession  
`rpm -i --test paquet.rpm` = " " en mode simulation  

#####          installation desinstallation via les depots

`dnf install paquet` = `yum install paquet` = installe paquet  

#####          mise à jour

`rpm -U paquet.rpm` = met à jour manuellement le paquet  

`dnf makecache` = `yum makecache` = met à jour la liste des paquets

`dnf update` = `yum update` = met à jour la base et met à niveau les paquets du systèmes  
`dnf updatednf` = `yum update yum` = " " (au premier lancement du gestionnaire)  

#####          information paquet

`dnf info paquet` = `yum info paquet` = information et version de paquet  
`dnf list available` = `yum list available` = liste les paquets des dépots disponibles non installés  
`dnf list all` = `yum list all` = liste tous les paquets des dépots  
`dnf list installed` = `yum list installed` = liste les paquets installés  

`rpm -qf /path_file` = affiche a quel paquet appartient le fichier  

`rpm -q paquet` = affiche les informations sur le paquet non installé  
`rpm -qi paquet` = affiche les information sur le paquet installé  
`rpm -qpi paquet` = " " paquet.rpm " "  

`rpm -qid paquet` = " " et les fichiers de documentations  
`rpm -qic paquet` = " " et les fichiers de configuration  

`rpm -qil paquet` = affiche les informations sur le paquet et liste les fichiers  
`rpm -qils paquet` = " " et affiche leur état (normal,not installed,replaced)  

#####          vérification du paquet

> propre au paquet rpm

`rpm -V paquet` = vérifie le contenu du paquet avec les metadonnees de la base  
`rpm -Vp paquet.rpm` = vérifie le contenu du paquet.rpm " "  

####        LIBRAIRIE

`ldconfig` = recherche les dernières versions de librairies installes sur le systeme et recree les liens vers les noms par defaut  

####        INTERFACE GRAPHIQUE GUI

`apt remove --auto-remove --purge libx11-*` = desinstalle tous les paquets en lien  avec le serveur interface graphique X  

`dconf write /org/compiz/profiles/unity/plugins/core/hsize 2` = pour Unity, fixe à 2 le nombre de colonnes du bureau workspace  
`dconf write /org/compiz/profiles/unity/plugins/core/vsize 2` = " " lignes " "  
`dconf read /org/compiz/profiles/unity/plugins/core/hsize` = pour Unity, renvoi le nombre actuelle de colonnes du bureau workspace  
`dconf read /org/compiz/profiles/unity/plugins/core/vsize` = " " lignes " "  

####        COMPILATION

> build-essential = dependances gcc pour le C g++ pour le C++, make pour compiler  
> gcj gfortran = dependances gcj pour le JAVA gfortran pour le Fortran

`tar -zxvf source.tar.gz` = decompresse et extrait les sources du programmme  
`cd programme` = va dans le dossier principal du dossier extrait  
`./configure` = effectue des tests pour voir la compatibilite avec le systeme en faisant une checklist des dependances requises et installees  
`make` = `si pas d\'erreur precedemment, compile en un fichier executablhwudo make install` = execute l\'installation  
`make uninstall` = desinstalle le programme (a executer depuis le dossier source)  


#####          c

> les librairies sont placées dans les dossiers /lib ou /usr/lib  
> ou définies dans /etc/ld.so.conf

`gcc main.c fic1.c -o programme` = compile les codes sources et créé un executable nommé programme  
`gcc -Wall -06 -g main.c -o main` = commande de compilation complète (recommandé)  
`gcc -O6 main.c -o main` = optimise au maximum puis compile le code  

`gcc main.c fic1.c -o programme -Wall` = en affichant toutes les erreurs et avertissements (recommandé)  

`gcc -c ma_lib.c -o ma_lib.o` = pré-compile une bibliothèque statique vers un .o  
`ar -q ma_lib.a ma_lib.o` = compile la bibliothèque statique partagée en .a à partir de la bibliothèque statique .o  
`gcc main.o autre.o ma_lib.a -o main` = compile le programme utilisant la bibliothèque statique partagée  

`gcc -c fPIC ma_lib.c -o ma_lib.o` = pré-compile une futur librairie dynamique.  
`gcc -shared -fPIC ma_lib.o -o ma_lib.so` = compile une bibliothèque dynamique partagée " "  
`gcc -fPIC main.o autre.o lib.o ma_lib.so -o nom_programme` = compile  " " utilisant des bibliothèqe dynamiques partagées  


#####          c++

`g++ fichierprincipal fichiercomplementaire -o programm` =  " " pour du code en C++  

#####          java

`javac projet.java` = compile le projet  
`javac -deprecation projet.java` = " " en indiquant si les fonctions obsoletes  
`javac -deprecation -Xlint projet.java` = " " avec les alertes non bloquantes  
`javac -classpath "/chemin/lib.jar:/chemin/lib2.jar" projet.java` = compile un projet néssecitant une librairie jar  
`java projet` = execute le binaire projet.class  
`java -jar projet.jar` = execute un archive jar  

#####          c sharp

> package : mono-devel (cf mono-projet )  
> mono-devel = compilateur minimum  
> mono-complete = librairie supplémentaire ("assembly not found")  
> mono-dbg = debug lors de compilation

`mcs projet.cs` = compile le projet  
`mcs *.cs -out:Main.exe` = " " et force le nom de sortie  
`mcs Main.cs /r:Mono.Security.dll` = " " en ajoutant une dll  
`mcs Main.cs -pkg:gtk-sharp-3.0` = " " en utilisant un package  

`mono Main.exe` = execute le projet  

#####          creation d\'un archive .jar

`echo -e "Main-Class: projet" > manifest` = cree un fichier manifest contenant la liste des fichiers du projet java  
`jar cvmf0 manifest projet.jar projet.class` = cree l\'archive projet.jar a partir du manifest et du projet.class  

###      GESTION DES UTILISATEURS

####        FICHIER DE CONFIGURATION ET LOG ASSOCIES


#####          FICHIER DE CONFIGURATION

* /etc/default/useradd = paramètre par defaut à affecter lors de la création d'un utitlisateur
* /etc/skel = template de home
* /etc/login.defs = variable d'environnement et paramètres avancés lié aux utilisateurs
* /etc/security/pwquality.conf = politique des mots de passe

####        INFORMATION UTILISATEUR ET GROUPE

`id` = liste les informations relatives a l\'utilisateur courant  
`id kyprio` = " " kyprio  
`whoami` = renvoi l\'identifiant " "  

####        UTILISATEUR

`/etc/passwd` = liste des utilisateurs du systeme tous confondus avec toutes leurs informations  
`getent passwd | grep 1000` = " " de l\'utilisateur principal uniquement  
`getent passwd | grep -P 1[0-9]{3} | cut -d: -f1 ` = liste les logins des veritables utilisateurs de l'UID 1000 a 1999  
`/etc/shadow` = " " avec l\'empreinte des mots de passe  
`getent shadow` = " "  

`adduser nom` = ajoute un utilisateur en mode interactif sur debian et non-interactif sur redhat  
`useradd nom` = " " en mot non-interactif par defaut  
`useradd -c "commentaire" nom` = " " " " en ajoutant un commentaire  
`useradd -m nom ` = " " et cree son home  
`useradd -p empreinte_mot_de_passe nom` = " " et active le login par mot de passe  
`useradd -s /bin/bash nom` = " " et redefinit le shell pour nom  
`useradd -m -p mot_de_passe_chiffre nom --shell /bin/bash` = " " mot non-interactif  
`mkpasswd mot_de_passe_clair` = renvoi une empreinte du mot de passe  

`deluser nom` = `userdel nom` = supprime l\'acces a nom  
`deluser --remove-home nom` = `userdel -r nom` = " " avec ses fichiers et dossiers personnels  

`usermod pi -l kyprio` = renomme l\'utilisateur pi  par kyprio mais ne change pas son home ni son groupe  
`usermod pi -l kyprio -d /home/kyprio -m` = renomme l\'utilisateur ainsi que son home sans mais pas son groupe  

`groupmod pi -n kyprio` = renomme le groupe pi en kyprio  
`groupmod pi -g 1010` = modifie le gid du groupe  

#####          renommage utilisateur

> cas pour raspbian  
> mettre la console au demarrage au lieu de l'interface graphique  
> activer le root et se logguer avec puis

```
usermod ancien -l nouveau -d /home/nouveau -m
groupmod ancien -n nouveau
passwd nouveau
sed -i 's/ ancien / nouveau /g' /usr/bin/raspi-confi
```

> puis desactiver le root et se delogguer

```
passwd -l root
```

####        GROUPE

`/etc/group` = liste des groupes  

`addgroup nom` = ajoute le groupe nom  
`delgroup groupe1` = supprime groupe1  

`useradd -G groupe1,groupe2,groupe3 kyprio` = redefinit la liste des groupes secondaires de kyprio en creant les groupes inexistants  

`adduser kyprio groupe` = ajoute kyprio à groupe  
`usermod -aG groupe kyprio` = ajoute le groupe secondaire groupe a kyprio  
`usermod -G kyprio,groupe kyprio` = redefinit la liste complète des groupes secondaires de kyprio
`usermod -l cyprien kyprio` = renomme l\'utilisateur kyprio par cyprien mais ne change pas son home  
`usermod -g groupe  kyprio` = definit le groupe primaire  auquel appartient Kyprio (par defaut : kyprio)  

`groups` = liste les groupes de l'utilisateur courant  
`groups kyprio` = liste les groupes de kyprio en commençant par le groupe primaire  

####        ACCES ET MOTS DE PASSE

> bibliothèque **libpwquality** pour le travail sur les mot de passes

`passwd` = modifie et active le mdp de root  
`passwd kyprio` = " " kyprio  
`passwd -g groupe1` = " "groupe1  
`passwd -r -g groupe1` = desactive le mot de passe  
`passwd -s user` = status du mot de passe (dernière modif, temps min avant modif, temps max avant modif )

`chpasswd` = change les mots de passes de façon des listes saisies sous la forme user_name:mot_de_passe (semi-interractif)  
`echo root:mdp | sudo chpasswd` = affecte à root le mot de passe mdp en mot (non-interractif)  

`chage kyprio` = change la duree d\'expiration du mot de passe de kyprio  

`mkpasswd -m sha-512 mot_de_passe` = renvoi un hash sha-512 du mot de passe (paquet whois)  

`pwmake 96` = genere un mot de passe en suivant les valeurs pars defaut de pwmake.conf vec une entropie de 96 bits  
`pwscore user` = test la qualité d'un mot de passe d'un utilisateur  

####        SERVEUR NFS

#####          client

`showmount -e 192.168.1.1` = affiche les montages NFS disponible

####        SERVEUR OPENLDAP SLAPD

#####          commande de base de slapd (Debian)

`slapcat` = affiche le contenu complet de la base principal ldap  
`slapcat -b 'cn=config'` = " " de la base config  
`slapadd -l fichier.ldif` = ajoute un entrée dans la base par default ldap  
`slapadd -b 'cn=config' -l schema.ldif` = ajoute un schema dans la base config ldap  

#####          ldap-utils

> paquet ldap-utils

`ldapsearch -x -b dc=labo,dc=lan` = test l'annuaire en verifiant la base  
`ldapsearch -x dc=labo,dc=lan 'uid=kyprio'` = recherche et renvoi le nombre de resultat correspondant a la recherche uid=kyprio dans labo.lan  
`ldapsearch -x -b dc=labo,dc=lan 'uid=kyprio'` = " " en affichant le details de chaque resultat  
`ldapsearch -x -b dc=labo,dc=lan 'uid=kyprio' cn  gidNumber` = "" "" en affichant uniq le cn et gidNumber du resultat  
`ldapsearch -x -b -LLL dc=labo,dc=lan 'uid=kyprio' cn gidNumber` = "" "" "" en omettant les informations lies a ldap et la requete lancee  

`ldapsearch -Q -LLL -Y EXTERNAL -H ldapi:/// -b cn=config dn` = commande de debogage pour afficher le DIT de slapd-config  
`ldapsearch -x -LLL -H ldap:/// -b dc=labo,dn=lan dn` = recherche les entrees"dn" du domaine labo.lan  
`ldapsearch -x -LLL -b dc=labo,dc=lan 'uid=kyprio' cn gidNumber` = recherche l'uid kyprio dans le DIT labo.lan  

`ldapadd -x -D cn=admin,dc=labo,dc=lan -W -f add_users.ldif` = ajoute au DIT de admin.labo.lan les utilisateurs et groupes present dans le fichier add_users.ldif  

`ldapmodify -a -x -D "cn=admin,dc=librescargot,dc=fr" -W -H ldap:// -f mod_librescargot.ldif` = modify le DIT avec fichier_modif.ldif  

`slappasswd -h {SHA} -s abcd123` = renvoi l'empreinte SHA du mot de passe  
`slappasswd -h {MD5} -s abcd123` = renvoi l'empreinte md5 du mot de passe  

`ldapwhoami -h localhost -D "cn=edu,ou=users,dc=edu,dc=librescargot,dc=fr" -W` = teste l'authentification du user edu  

`ldapsearch -x -LLL -b dc=edu,dc=librescargot,dc=fr uidNumber=* | grep uidNumber: | cut -d: -f2 | sort | tail -1` = last uidNumber  

#####          ldapscripts

> paquet ldapscripts  
> automatise certaine tâches ldap et non ldap  
> cf /etc/ldapscripts/ldapscripts.conf
> il permet notamenet de créer le home lors de la création d'un user  

`lsldap` = affiche les attributs et valeurs de la base ldap  
`lsldap -u` = liste les utilisateurs ldap  
`lsldap -g` = " " groupes  

`ldapadduser user1 users` = ajoute un user au groupe users dans la base ldap 
`ldapdeleteuser user1` = supprime user1 de la base ldap  
`ldapaddgroup eleves` = ajoute un groupe dans la base ldap  
`ldapaddgroup eleves 500 = "" avec le gidNumber 500 

`ldapmodifyuser user1` = modifie de façon interractive au format ldif les attributs du user  

####        SERVEUR FREEIPA IDM

`ipa user-add` = ajout un user (interafactif)  
ipa user-add jsmith --first=John --last=Smith --manager=bjensen --email=johnls@example.com --homedir=/home/work/johns --password

`ipa group-add groupe` = cree group  
`ipa group-add-member groupe --users={user1,user2}` = ajoute des utilisateurs à groupe  
`ipa group-add-member groupe --groups={group1,group2}` = ajoute des groupes à groupe  

###      CONSOLE

####        TERMINAL

`tty` = informe sur le device utilisé (chaque temrinal est relié à un terminal différent)  

####        SSH

`ssh 192.168.0.10` = se connecte en ssh a un serveur ssh en utilisant le nom de l'utilisateur en cours  
`ssh root@192.168.0.10` = " " en utilisant le nom d'utilisateur root  
`ssh 192.168.0.10 -p 22` = " " en specifiant le port a utiliser  
`ssh -t 192.68.0.10 "commande a executer"` = lance une commande via ssh sur le serveur et redirige le flux standard du tty vers le client ( utile pour les scripts )  
`exit` = quitte la session ssh en cours  

`apt install openssh-server` = install ssh pour un nouveau serveur  

#####          configuration pour une utilisation reguliere sans mot de passe utilisateur

`ssh-keygen -t rsa` = cree la paire de cles rsa dont la *.pub a envoyer au serveur.  
`ssh-copy-id -i ~/.ssh/id_rsa.pub login@ip` = copie le contenu de id_rsa.pub cote client vers ~/.ssh.authorized_keys  
scp ~/.id_rsa.pub -P 22 login@ip:~x
`ssh-add` = ( si passphrase ) agent ssh permettant de retenir la passphrase durant une session  

`ssh login@ip` = se connecte a un serveur sur le compte d'un utilisateur specifique  
`ssh login@ip -p 12345` = " avec le port 12345 ( cf CONFIGURATION )  
`ssh -6 login@ipv6` = " avec une ipv6  

`scp fichier login@ip:~/destination` = copi en mode securise fichier vers le serveur  
`scp login@ip:fichier destination` = " depuis le serveur vers le client  
`scp -P port login@ip:fichier destination` = " en utilisant un port specifique pour ssh ( attention majuscule a P )  

`sftp login@ip` = se connecte a un ftp en mode securise  
`sftp login@ip -P 22 ` = " " avec un port spécifique  
`man sftp` = liste les commandes sftp disponibles ( cf commandes ftp )  

`rsync` = cf commandes, permets de synchroniser deux sources avec le protocole SSH  

#####          tunnel

`ssh -g 444:ip_dest user@ip_passerelle:443` = créé un tunnel ssh accédant au port 443 de ip_dest en passant par ip_passerelle et qui est accessible avec le port 444 de localhost  
`ssh -D 4444 -f -C -q -N user@sshserver.fr -p 22` = créé un proxy sur le localhost:4444 du client pour accéder au réseau via sshserver.fr (configurer le navigateur en conséquence)  
`ssh -L 8888:192.168.1.50:8080 kyprio@server -p 2222` = créé un tunnel via kyprio@server:2222 vers le port 192.168.1.50:8080 et accessible en localhost:8888 sur le client  
`cat archive.tar.gz | ssh kyprio@server "tar -zxf - -C /data/archive` = extrait via tunnel ssh un archive vers une machine distante  

####        CLUSH

> paquet clustershell  
> ssh vers un groupe de clients
> cf  om/config/clush  

####        SCREEN

`screen -S session1` = ouverture d'une nouvelle session de terminal virtuel  
`screen -x session1` = se connecte sur session1 en duplex  
`CTRL a?` = aide de screen  
`exit` = `CRTL ad` = quitte et ferme la session en cours s\'il ne reste qu\'un terminal  
`CTRL aDD` = quitte et ferme completement la session en cours  
`screen -ls` = liste les sessions screen ouvertes par l'utilisateur  
`CTRL aD` = se detache de la session en cours  
`screen -d session1` = force le detachement de session1 afin de reprendre la main depuis une autre console  
`screen -r session1` = se rattache a session1  
`screen -wipe` = ferme les sessions mortes/inaccessibles  

`CTRL ac` = cree un terminal (supplementaire) dans la console actuelle  

`CTRL an` = accede au terminal suivant par ordre de creation  
`CTRL ap` = accede au terminal precedent par ordre de creation  
`CTRL aa` = renvoi au terminal utilise juste avant  
`CTRL a0` = accede au premier terminal ouvert  
`CTRL aw` = liste les terminaux par leur nom  
`CTRL aA` = renomme le terminal actuel  
`CTRL a'` = ouvre un prompt pour renseigner le nom ou numero du terminal ou l\'on veut se rendre  
`CTRL a"` = liste les terminaux pour selectionner celui a atteindre  

`CTRL a|` = separe verticalement la console en deux sans creer de terminal  
`CTRL aS` = separe horizontalement la console en deux " "  
`CTRL a TAB` = accede a la console suivante  
`CTRL aQ` = supprime tous les consoles sauf la courante  
`CTRL aK` = supprime la console courante  
`CTRL aF` = pedimensionne la session screen au dimension de notre fenetre  
`CTRL aH` = loggue tout ce qui passe sur la session dans un fichier ~/screenlog.numero_de_la_session  

> copie dans screen d\'une console a une autre

`CTRL a ESC` = change de mode et passe en mode visuel dans la console en cours  
`ENTER` = commence/termine une selection  
`CTRL a TAB` = change de console (si besoin)  
`CTRL a[` = colle la selection precedente  

###      VIRTUALISATION ET ISOLATION

####        DOCKER

#####          scénarios

> essentiel :   
> avec root  
> récupérer une image, instancier un conteneur, démarer/arrêter un conteneur

systemctl start docker
`docker pull image_centos` = recupere une image docker sur le depot officiel  
`docker run -d  --name centos01 -it image-centos /bin/bash` = instancie en mode un conteneur a partir de l\'image avec les options demon, interractif, creation de terminal et en nommant le conteneur centos01  
`docker attach centos01` = se connecter au tty du conteneur  
`CTRL a CTRL q` = se détache sans fermer le conteneur (sinon faire : exit)  
`docker exec name_cont ifconfig eth0` = exectue une commande du conteneur déjà démarré  
`docker stop name_cont` = arrête le conteneur (ne supprime ni le conteneur ni ses données)  
`docker rm centos01` = supprime définitivement le conteneur  
`docker image rm name_image_os` = supprime l\'image  

> Créer une image à partir d'un conteneur existant (simple)

`docker commit name_actual_cont localhost:5000/name_new_image` = créée une image sur le dépot local à partir d\'un conteneur actuel  

> Créer une image via un dockerfile (complexe)

> création préalable d'un dockerfile placé dans le répertoire courant  
> Exemple d'un dockerfile (cf om/config/docker/dockerfile/) pour créer :   
> - une image basée sur debian  
> - contenant des paquets supplémentaires  
> - lancant une commande lors de l\'instanciation  
> FROM centos  
> MAINTAINER kyprio <kyprio@kyprio-libre.fr>  
> RUN yum install -y httpd net-tools  
> VOLUME /app/ ####        partage le volume /app- EXPOSE 80 ####        ouvre le port 80- add http://example.com/fichier  
> CMD ["-D", "FOREGROUND"]  
> ENTRYPOINT ["/usr/sbin/httpd"]  
> l\'image peut être générée sur le dépot localhost accessible au port 5000

`docker build --tag=localhost:5000/httpd_perso .` = génère l\image nommée httpd_perso à partir du dockerfile du dossier courant  

> Création de conteneur dans un réseau bridge isolé des autres réseaux

`docker network create RED` = créé un réseau (par défaut : bridge) sur l'hôte  
`docker run -it --name centos01 --network RED centos /bin/bash` = instancie un conteneur utilisant le réseau RED  
`docker run -it --name centos02 --network RED centos /bin/bash` = " " utilisant le réseau RED et qui pourra donc communiquer avec centos01  
`docker run -it --name centos03 centos /bin/bash` =  " " utilisant le réseau bridge par défaut et qui ne pourra pas communiquer avec centos01 et centos02  

#####          administration générale de docker : docker start/stop/pause/kill/rm

`systemctl start docker` = lance le service docker  

`docker start name_cont` = démarre un conteneur déjà instancié (docker run ...)  
`docker stop name_cont` = arrête le conteneur (mais ne le supprime pas)  
`docker kill name_cont` = force l\'arrêt du conteneur  
`docker pause name_cont` = suspend le conteneur  
`docker unpause name_cont` = relance le conteneur en pause  

`docker rm name_cont` = supprime un conteneur arrêté  
`docker rm --force name_cont` = supprime un conteneur  

#####          monitoring docker : docker ps

`docker ps` = liste les conteneurs actifs  
`docker ps -a` = liste les contenaires tous les dockers (démarrés, en pause et arrêtés)  

#####          relévé d\'informations : docker inspect

> docker inspect renvoit des informations en json  

`docker inspect name_cont` = liste les informations du conteneur ( création, nom, état, groupes de controles, drivers, montages, réseaux, mappages réseaux, ...)  
`docker inspect name_image` = liste les informations de l\'image  
`docker inspect --format='{{ range .NetworkSettings.Networks}}{{.MacAddress}}{{end}}' name_cont` = affiche l\'adresse mac du conteneur  
`docker inspect --format='Le conteneur se nomme : {{.Name}}' name_cont` = affiche du texte et des valeurs du conteneur  

#####          Instanciation des conteneurs : docker run

> un conteneur est instancié à partir d\'une image

`docker run -d -it --name centos centos /bin/bash` = lancement d\'un conteneur de type OS  

`docker run centos` = instancie un conteneur depuis l\'image centos puis l\'arrête (faute d'autre actions à effectuer)  
`docker run centos echo "texte"` = " " et lui envoit une commande " "  
`docker run centos /bin/bash -c "command1"` = " " en lui lancant une commande  
`docker run -it centos /bin/bash` = " " en allouant un pseudo tty pour s'y connecter (-t) et active le mode interactif (-i)  
`docker run --name centos01 centos` = " " en spécifiant un nom plutôt que de générer un nom aléatoire  
`docker run -d centos` = " " en mode fond de tâche (démon)  

`docker run -d -p 8080:80 nginx` = instancie nginx et mappe le port 8080 de l\'hôte vers le port 80 du conteneur  

`docker run -it --network host centos` = utilise le driver host (cf network)  

`docker run -v dossierhote:dossierconteneur nginx` = associe un dossier de l\hôte au conteneur  

######          interraction avec les conteneurs : docker exec/attach

`docker exec name_cont ifconfig eth0` = execute une commande sur le conteneur déjà démarré  
`docker exec name_cont /bin/bash -c 'command'` = " "  avec l\'interpréteur bash  

`docker attach name_cont` = se rattache au terminal du conteneur (si l'option -t a été spécifiée à l\'instanciation)  

CTRL p CTRL q : sort du tty du conteneur

#####          réseaux : docker network

`docker network ls` = liste les informations réseaux des conteneurs  
`docker network inspect bridge` = liste les informations réseaux du réseau par défaut nommé bridge  
`docker network inspect RED` = " " du réseau nommé RED  

`docker network create RED` = créé un réseau (bridge) sur l'hôte pour les conteneurs  
`docker network create --driver bridge RED` =  " "  
`docker network rm RED` = supprimer le réseaux docker nommé RED  

`docker exec web cat /etc/hosts` = liste des hosts reliés au conteneur web  
`docker exec web env` = liste les variables d\'environnements des liaisons existantes du conteneur web  

`docker network inspect bridge` = liste les informations du réseaux bridge de docker et les conteneurs associés au réseau bridge  
`docker network inspect RED` = liste les informations du réseau personnalisé nommé RED  

> par defaut le bridge est utilisé sur l'interface réseau de l'hôte docker0  
> les drivers disponibles : bridge, host, null  
> host = toutes les interfaces réseaux de l'hôte  
> bridge = bridge  
> null = aucun

`iptables -t nat -L` = liste les nat (dont les bridge des conteneurs)  

#####          dépot docker hub et dépot local

> un dépot est nommé registry

`docker login -u username` = se loggue au depot docker hub (pré-requis : avoir un compte)  

`docker tag debian kyprio/projet:debian` = créé un point de départ d'une image existante vers une nouvelle image perso  

`docker push username/projet:name_image` = envoi name_image vers le dépot perso sur docker hub  
`docker push 192.168.50.10:5000/imagename` = " " vers le dépot local  

`docker commit name_cont localhost:5000/name_cont` = cree une nouvelle image en local à partir du conteneur actuel  

#####          gestion des images : docker pull/images/search/rmi

`docker pull debian` = récupère une image debian du dépot officiel  

`docker images` = liste les images existantes sur le système  
`docker search nginx` = recherche des images disponibles sur docker hub  

`docker rmi name_image` = supprime une image docker  
`docker rmi --force name_image` = " " force  


#####          creation d'image avec dockerfile

> creation prealable d'un fichier dockerfile placé aléatoirement dans docker/

`docker build --tag=localhost:5000/httpd_perso docker/` = lance la construction d'une image nommé httpd_perso à partir du dockerfile dans docker/  

#####          backup

`docker save kyprio/projet:nginx -o kyprio_projet_nginx.tar` = archive une image docker  
docker load -i kyprio_projet_nginx.tar

#####        docker-compose

`docker-compose up -d` = instancie et démare une application multi-docker  
`docker-composer stop` = stop l'application multi-docker  

####        LXC LXD

> LXC et LXD (surcouche) sont orientés machines virtuelles légères longue durée  
> à l'inverse de docker qui est orienté application éphémère  
> répertoire :
> /var/lib/lxc/nom_container

`lxc-create -t download -n u1` = créé un contenaire de façon interractive (ex: ubuntu xenial adm64) nommé u1 en téléchargeant un template  
`lxc-create -t download -n u1 -- -d ubuntu -r xenial  -a amd64` = " " non interractive  
`lxc-destroy u1` = supprimer un container arrêté  
`lxc-destroy u1 -s` = " " avec les snapshots  

`lxc-start u1` = demare le container  
`lxc-stop u1` = stop le container  

`lxc-attach -n u1` = ouvre la console root du container  

`lxc-snapshot -n u1` = 

####        VAGRANT

`vagrant up` = lance la VM, la telecharge dans ~/.vagrant la vm si non existante  
`vagrant ssh` = lance ssh sur la vm du ./vagrantfile  

####        QEMU

`qemu-img convert vm.qcow2 vm.raw` = converti un disque de vm qcow2 vers raw  

####        OVIRT

`hosted-engine --vm-status` = statut de la vm de management webgui d\'ovirt  

####        KVM VIRSH

`virsh list --all` = liste les machines virtuelles  

`virsh dumpxml nom_vm > nom_vm.xml` = exporte la config de la vm  

####        OPENSTACK

`openstack` = ouvre la CLI openstack (facultatif)  

#####          gestion des plateformes

`spoon list` = list
`spoon plateform1` = entre dans plateform1 et les vm associés  
`nova list` = liste les vm de la plateforme en cours  

`openstack catalog list` = liste les services/fonctionnalités openstack accessibles et leurs endpoints (nova,cinder,...)


#####          gestion des images et des instances

`openstack image list` = liste les images d'OS
`openstack image delete image1` = supprime image1
`openstack image show image1` = affiche les details sur image1  
`openstack image set image1` = modifie à jours une image

`openstack server list` = `nova list` = liste des instances et leur statut (openstack server list est mieux formaté)  
`openstack server show instance1` = affiche les details sur instance1

`openstack server start instance1' = allume l'instance  
`openstack server stop instance1' = eteint proprement l'instance  

`openstack flavor list` = liste les modeles d'instances  


#####          gestion des utilisateurs

`openstack user list` = liste les utilisateurs 

####        PROXMOX

`vzlist` = `lxc-ls` = proxmox3/proxmox4 liste les conteneurs  
`qm list` = `qemu list` = proxmox3/proxmox4 liste les machines virtuelles  

`vzctl enter 120` = proxmox3 se connecte en console dans un conteneur d\'ID 120  

###      LES SERVEURS

####        GESTION DES SERVICES LINUX

`service nom_service start` = demarre nom_service (start/stop/restart/reload/force-reload)  
`service --status-all` = indique l\'etat de tous les services de la machine  

`systemctl status nom_service` = statut du service  
`systemctl start nom_service` = demare le service (start/stop/restart/reload/force-reload)  
`systemctl enable nom_service` = active au demarage le services (enable/disable)  
`systemctl is-active nom_service` = verifie si le service est actif par le renvoit de active ou inactive  
`systemctl list-units --type=service` = liste les services actifs  

> creation nouveau service  
> modele : om/config/systemd/_etc_systemd_system_nom-service.service

`chmod a+x /etc/systemd/system/nom-service.service` = rendre executable le nouveau service  
`systemctl start nom-service.service` = test et demare le nouveau service  
`systemctl enable nom-service.service` = active le service au demarrage  

####        OPENVPN

`openvpn --config connexion.conf` = lance le vpn à partir du fichier de configuration  

####        DNS

#####          THEORIE DU DNS

######            Type d'enregistrement DNS

`A` =  (Adresse d'hôte) mappage entre un nom d'hôte et une adresse IPv4 (32 bits)  
`AAAA` = mappage entre un nom d'hôte et une adresse IPv6 (128 bits)  
`CNAME` = (Canonical NAME) mappage entre un nom d'hôte et un autre nom d'hôte. Permet de créer des alias pour un nom d'hôte donné à une même machine  
`HINFO` = (Host INFO) spécifie le type CPU/OS associé à un nom d'hôte 
`DNS` =  Liste Ressources 
`MX` = (Mail eXchanger) identifie les serveurs de messageries  
`NS` =  (Name Server) identifie les serveurs DNS de la zone DNS, dans le cadre de la délégation DNS  
`PTR` = (PoinTeR) mappage entre une adresse IP et un nom d'hôte (zones de recherches inversées)  
`SOA` = (Start Of Authority) contient le nom d'hôte et l'adresse IP du serveur DNS qui héberge actuellement la zone DNS principale. Un seul enregistr. SOA par zone DNS, le 1er enregistrement crée dans une zone DNS  
`SRV` = (service) permet de mapper un nom d'hôte à un type de service donné. Permet de retrouver la liste des serveurs HTTP ou des DC. Possibilité d’une priorité différente à chaque enregistrement SRV  
`WINS` = (Windows Internet Name Service) indique au serveur DNS l'adresse IP d'un serveur WINS à en cas d'échec lors de la résolution de nom d'hôte, uniquement en zone de recherche directe  
`WINS­R` = ne peut être crée que dans une zone de recherche inversée  

#####          BIND9

`named-checkconfig` = vérifie la configuration de bind et conseille sur les erreurs courantes  
`named-check-zone nom_zone fichier.db` = vérifie la zone associé au fichier  

`dig gateway.librescargot.lan` = interroge le DNS

>    ; <<>> DiG 9.9.4-RedHat-9.9.4-51.el7_4.2 <<>> let-win-dev01.librescargot.lan  
>    ;; global options: +cmd  
>    ;; Got answer:  
>    ;; ->>HEADER<<- opcode: QUERY, status: NOERROR, id: 23607  
>    ;; flags: qr aa rd ra; QUERY: 1, **ANSWER: 1**, AUTHORITY: 1, ADDITIONAL: 2  
>      
>    ;; OPT PSEUDOSECTION:  
>    ; EDNS: version: 0, flags:; udp: 4096  
>    ;; QUESTION SECTION:  
>    **;let-win-dev01.librescargot.lan.  IN  A**  
>      
>    ;; ANSWER SECTION:  
>    **let-win-dev01.librescargot.lan. 10800 IN A  172.16.1.9**  
>      
>    ;; AUTHORITY SECTION:  
>    **librescargot.lan. 10800 IN  NS  ns.librescargot.lan.**  
>      
>    ;; ADDITIONAL SECTION:  
>    ns.librescargot.lan.  10800 IN  A 172.16.1.10  
>      
>    ;; Query time: 0 msec  
>    ;; SERVER: **172.16.1.10#53**(172.16.1.10)  
>    ;; WHEN: Mon Feb 26 22:56:53 UTC 2018  
>    ;; MSG SIZE  rcvd: 108  

####        HTTP

#####          requete HTTP

> GET /page1 /HTTP/1.1  
> Host: www.serverweb.lan

#####          action http standard rest

`GET` = demande une page  
`HEAD` = test une url sans récupération complete  
`POST` = envoi d\'information  
`PUT` = modifie tout un element  
`OPTIONS` = demande des vérifications du serveur web et les options autorisées  
`PATCH` = modifie uniquement un element   

#####          reponse http

> HTTP/1.1 200 MESSAGEDE RETOUR  
> Server: nginx  
> Date: Tue, 24 Jan 2017 13:23:42 GMT  
> Content-Type: text/html; charset=UTF-8  
> Transfer-Encoding: chunked  
> Connection: keep-alive  
> X-Powered-By: PHP/5.6.27

#####          code de retour http

`100` = Continue Attente de la suite de la requête.  
`101` = Switching Protocols Acceptation du changement de protocole.  
`102` = Processing WebDAV : Traitement en cours (évite que le client dépasse le temps d’attente limite).  
`200` = OK Requête traitée avec succès.  
`201` = Created Requête traitée avec succès et création d’un document.  
`202` = Accepted Requête traitée, mais sans garantie de résultat.  
`203` = Non-Authoritative Information Information retournée, mais générée par une source non certifiée.  
`204` = No Content Requête traitée avec succès mais pas d’information à renvoyer.  
`205` = Reset Content Requête traitée avec succès, la page courante peut être effacée.  
`206` = Partial Content Une partie seulement de la ressource a été transmise.  
`207` = Multi-Status WebDAV : Réponse multiple.  
`210` = Content Different WebDAV : La copie de la ressource côté client diffère de celle du serveur (contenu ou propriétés).  
`226` = IM Used RFC 32293 : Le serveur a accompli la requête pour la ressource, et la réponse est une représentation du résultat d'une ou plusieurs manipulations d'instances appliquées à l'instance actuelle.  
`300` = Multiple Choices L’URI demandée se rapporte à plusieurs ressources.  
`301` = Moved Permanently Document déplacé de façon permanente.  
`302` = Moved Temporarily Document déplacé de façon temporaire.  
`303` = See Other La réponse à cette requête est ailleurs.  
`304` = Not Modified Document non modifié depuis la dernière requête.  
`305` = Use Proxy La requête doit être ré-adressée au proxy.  
`306` = (aucun) Code utilisé par une ancienne version de la RFC 26164, à présent réservé.  
`307` = Temporary Redirect La requête doit être redirigée temporairement vers l’URI spécifiée.  
`308` = Permanent Redirect La requête doit être redirigée définitivement vers l’URI spécifiée.  
`310` = Too many Redirects La requête doit être redirigée de trop nombreuses fois, ou est victime d’une boucle de redirection.  
`400` = Bad Request La syntaxe de la requête est erronée.  
`401` = Unauthorized Une authentification est nécessaire pour accéder à la ressource.  
`402` = Payment Required Paiement requis pour accéder à la ressource.  
`403` = Forbidden Le serveur a compris la requête, mais refuse de l'exécuter. Contrairement à l'erreur 401, s'authentifier ne fera aucune différence. Sur les serveurs où l'authentification est requise, cela signifie généralement que l'authentification a été acceptée mais que les droits d'accès ne permettent pas au client d'accéder à la ressource.  
`404` = Not Found Ressource non trouvée.  
`405` = Method Not Allowed Méthode de requête non autorisée.  
`406` = Not Acceptable La ressource demandée n'est pas disponible dans un format qui respecterait les en-têtes "Accept" de la requête.  
`407` = Proxy Authentication Required Accès à la ressource autorisé par identification avec le proxy.  
`408` = Request Time-out Temps d’attente d’une requête du client écoulé.  
`409` = Conflict La requête ne peut être traitée en l’état actuel.  
`410` = Gone La ressource n'est plus disponible et aucune adresse de redirection n’est connue.  
`411` = Length Required La longueur de la requête n’a pas été précisée.  
`412` = Precondition Failed Préconditions envoyées par la requête non vérifiées.  
`413` = Request Entity Too Large Traitement abandonné dû à une requête trop importante.  
`414` = Request-URI Too Long URI trop longue.  
`415` = Unsupported Media Type Format de requête non supporté pour une méthode et une ressource données.  
`416` = Requested range unsatisfiable Champs d’en-tête de requête « range » incorrect.  
`417` = Expectation failed Comportement attendu et défini dans l’en-tête de la requête insatisfaisante.  
`418` = I’m a teapot « Je suis une théière ». Ce code est défini dans la RFC 23245 datée du premier avril 1998, Hyper Text Coffee Pot Control Protocol.  
`421` = Bad mapping / Misdirected Request La requête a été envoyée à un serveur qui n'est pas capable de produire une réponse (par exemple, car une connexion a été réutilisée).  
`422` = Unprocessable entity WebDAV : L’entité fournie avec la requête est incompréhensible ou incomplète.  
`423` = Locked WebDAV : L’opération ne peut avoir lieu car la ressource est verrouillée.  
`424` = Method failure WebDAV : Une méthode de la transaction a échoué.  
`425` = Unordered Collection WebDAV RFC 36486. Ce code est défini dans le brouillon WebDAV Advanced Collections Protocol, mais est absent de Web Distributed Authoring and Versioning (WebDAV) Ordered Collections Protocol.  
`426` = Upgrade Required RFC 28177 Le client devrait changer de protocole, par exemple au profit de TLS/1.0.  
`428` = Precondition Required RFC 65858 La requête doit être conditionnelle.  
`429` = Too Many Requests RFC 65859 Le client a émis trop de requêtes dans un délai donné.  
`431` = Request Header Fields Too Large RFC 658510 Les entêtes HTTP émises dépassent la taille maximale admise par le serveur.  
`449` = Retry With Code défini par Microsoft. La requête devrait être renvoyée après avoir effectué une action.  
`450` = Blocked by Windows Parental Controls Code défini par Microsoft. Cette erreur est produite lorsque les outils de contrôle parental de Windows sont activés et bloquent l’accès à la page.  
`451` = Unavailable For Legal Reasons Ce code d'erreur indique que la ressource demandée est inaccessible pour des raisons d'ordre légal11,12.  
`456` = Unrecoverable Error WebDAV : Erreur irrécupérable.  
`520` = Unknown Error L'erreur 520 est utilisé en tant que réponse générique lorsque le serveur d'origine retourne un résultat imprévu.  
`521` = Web Server Is Down Le serveur a refusé la connexion depuis Cloudflare.  
`522` = Connection Timed Out Cloudflare n'a pas pu négocier un TCP handshake avec le serveur d'origine.  
`523` = Origin Is Unreachable Cloudflare n'a pas réussi à joindre le serveur d'origine. Cela peut se produire en cas d'échec de résolution de nom de serveur DNS.  
`444` = No Response Indique que le serveur n'a retourné aucune information vers le client et a fermé la connexion.  
`495` = SSL Certificate Error Une extension de l'erreur 400 Bad Request, utilisée lorsque le client a fourni un certificat invalide.  
`496` = SSL Certificate Required Une extension de l'erreur 400 Bad Request, utilisée lorsqu'un certificat client requis n'est pas fourni.  
`497` = HTTP Request Sent to HTTPS Port Une extension de l'erreur 400 Bad Request, utilisée lorsque le client envoie une requête HTTP vers le port 443 normalement destiné aux requêtes HTTPS.  
`499` = Client Closed Request Le client a fermé la connexion avant de recevoir la réponse. Cette erreur se produit quand le traitement est trop long côté serveur13.  
`500` = Internal Server Error Erreur interne du serveur.  
`501` = Not Implemented Fonctionnalité réclamée non supportée par le serveur.  
`502` = Bad Gateway ou Proxy Error Mauvaise réponse envoyée à un serveur intermédiaire par un autre serveur.  
`503` = Service Unavailable Service temporairement indisponible ou en maintenance.  
`504` = Gateway Time-out Temps d’attente d’une réponse d’un serveur à un serveur intermédiaire écoulé.  
`505` = HTTP Version not supported Version HTTP non gérée par le serveur.  
`506` = Variant Also Negotiates RFC 229514 : Erreur de négociation. Transparent content negociation.  
`507` = Insufficient storage WebDAV : Espace insuffisant pour modifier les propriétés ou construire la collection.  
`508` = Loop detected WebDAV : Boucle dans une mise en relation de ressources (RFC 584215).  
`509` = Bandwidth Limit Exceeded Utilisé par de nombreux serveurs pour indiquer un dépassement de quota.  
`510` = Not extended RFC 277416 : la requête ne respecte pas la politique d'accès aux ressources HTTP étendues.  
`511` = Network authentication required RFC 658517 : Le client doit s'authentifier pour accéder au réseau. Utilisé par les portails captifs pour rediriger les clients vers la page d'authentification.  
`524` = A Timeout Occurred Cloudflare a établi une connexion TCP avec le serveur d'origine mais n'a pas reçu de réponse HTTP avant l'expiration du délai de connexion.  
`525` = SSL Handshake Failed Cloudflare n'a pas pu négocier un SSL/TLS handshake avec le serveur d'origine.  
`526` = Invalid SSL Certificate Cloudflare n'a pas pu valider le certificat SSL présenté par le serveur d'origine.  
`527` = Railgun Error L'erreur 527 indique que la requête a dépassé le délai de connexion ou a échoué après que la connexion WAN ait été établie.  

####        LIGHTTPD

####        HAPROXY

`haproxy -f haproxy.cfg` = check la configuration haproxy

####        APACHE HTTPD

`apachectl -V` = `httpd -V` = liste des modules, options, et informations du serveur  
`apachectl start|stop|restart` = gestion du serveur  
`apachectl status|fullstatus` = status du serveur  
`apachectl graceful-stop` = ferme le serveur en attendant la fin des connexions clients  
`apachectl configttest` = verifie la syntaxe de la configuration  
`apache2 -l` = liste les modules compilés dans le core apache2  

`a2enmod module` = active un module  
`a2dismod module` = désactive " "  
`a2enconf conf` = active une configuration  
`a2ensite vhost` = active un site vhost  


`htpasswd -s htpasswd user1` = ajoute une entrée au fichier htpasswd user + mot de passe en SHA1  
`htpasswd -sc htpasswd user1` = " " recréé le fichier htpasswd  

####        NGINX

####        PYTHON

#####          gestion des dépendances

`pip install --upgrade pip` = `pip3 install --upgrade pip3` = met à jour le gestionnaire de dépendance python2/python3  

`sudo -H pip3 install nommodule` = install un module sur le système sans tenir compte du home de l'utilisateur courant   

#####          Création environnement virtuel complexe


> Création environnement virtuel Python3
>    python3 -m venv .venv
>    source .venv/bin/activate
> sortir environnement virtuel
>    deactivate

#####          Création environnement virtuel complexe

> Création environnement virtuel d'une version précis python avec dépendances
> contenu de requirement.txt
>     asn1crypto==0.22.0 \
>       --hash=sha256:d232509fefcfcdb9a331f37e9c9dc20441019ad927c7d2176cf18ed5da0ba097 \
>       --hash=sha256:cbbadd640d3165ab24b06ef25d1dca09a3441611ac15f6a6b452474fdf0aed1a
>    cffi==1.10.0 \
>      --hash=sha256:446699c10f3c390633d0722bc19edbc7ac4b94761918a4a4f7908a24e86ebbd0 \
>      --hash=sha256:562326fc7f55a59ef3fef5e82908fe938cdc4bbda32d734c424c7cd9ed73e93a \
>      --hash=sha256:7f732ad4a30db0b39400c3f7011249f7d0701007d511bf09604729aea222871f 
>
>    apt install python-pip python-virtualenv
>    VENV_PATH="/home/user/.venv" 
>    VENV_BIN="$VENV_PATH/bin" 
>    virtualenv --no-site-packages --python python2 "$VENV_PATH" 
>    $VENV_BIN/pip  install --upgrade pip
>    source $VENV_BIN/activate
>    $VENV_BIN/pip install --disable-pip-version-check --no-cache-dir --require-hashes -r requirement.txt 

#####          Simple Serveur web python

`python -m SimpleHTTPServer 3000` = lance un mini serveur web desservant les pages du dossier courant  

####        PHP

`php -i` = info sur php
`php --version` = version php
`scl enable rh-php70 bash` = activer l'environnement php70 (version SCL rhel) dans bash

#####          PHP-FPM

> php-fpm propose un serveur thread-safe à part entiere  
> en ecoute sur un socket ou en TCP/IP  
> seule alternative possible pour combiner apache mpm_worker/event qui sont thread-safe avec PHP

`systemctl start|stop|restart php7.0-fpm` = gestion du serveur php  

#####          FRAMEWORK SYMFONY

`cd my_projetc_symphony/` = se place à la racine du projet symphony  

######            Symphony 3

> Symphony 3  
> php --version >= 5.5.9

`php bin/symphony_requirements` = liste les dépendances php pour symphony  

######            Symphony 4

> Symphony 4  
> php --version >= 7.1.3

`composer require symphony/requirements-checker` = créé un fichier dans public/ listant les dépendances requises 
`composer remove symphony/requirements-checker` = supprime le fichier listant les dépendances

####        MYSQL

`apt install mysql-server mysql-client` = installe l\'environnement complet de mysql  

`mysql -u root -p` = se connecte à mysql/mariadb en root sans preciser le mot de passe  
`mysql -u usernombase -p NomBase` = " " et rentre dans la base de donnee  
`mysql -u root -p --default-character-set=utf8` = " " en definissant l\'encodage UTF8  
`mysql -h localhost -u root -p` = " " sur la boucle locale  

`mysql -u root -p -e "COMMANDE SQL"` = execute une commande SQL depuis bash  

mysql --user="$user" --password="$password" --database="$database" --execute="DROP DATABASE $user; CREATE DATABASE $databasename character set UTF8mb4 collate utf8mb4_bin;"

`mysqldamin -u root -h localhost password nouveau_mdp -p ancien_mdp` = change le mot de passe du root de mysql  
mysql -u utilisateur -p --default-character-set=utf8

`mysqldump -u user -p --opt nombdd > nombdd_backup.sql` = sauvegarde de la structure et du contenu la base de donnee (mais pas la bdd elle-meme qu'il faut creer à nouveau)  
`mysql nombdd < nombdd_backup.sql` = restauration de la structure et du contenu de la bdd  

`mysql -u user -ppassword -e "SQL QUERY"` = execute une requete SQL  

####        SQLITE


> SQLITE3 est un SGBDR sans moteur travaillant sur des fichiers.  
> les fichiers (db.sqlite3) peuvent ainsi être supprimé à la main  

`sqlite3 path/db.sqlite3` = ouvre sqlite et accède à la base fichier  

`.schema` = affiche les codes de création de la structure SQL (equivalent show tables)  
`.exit` = exit de sqlite  
`.help` = affiche l'aide  

> le reste est du requetage SQL

####        POSTGRESQL

> user linux a utiliser : postgres
>    su - postgres  

`psql` = ouvre le sgbdr  

#####          administration général de PostgreSQL

`createuser user` = créé un utilisateur sans mot de passe  
`createuser -P user` = " " avec mot de passe en stdin  
`dropuser user` = supprime user (il faut avoir préalablement réaffecter ses relations avec les db de postgresql)

`createdb -O user dbuser` = créé une base affectée à user  
`dropdb dbuser` = supprime la base

`psql -U user dbuser` = connexion à la base dbuser avec l'utilisater user et demande de mdp si besoin  

`ALTER USER user WITH PASSWORD 'mdp';` = modification du mot de passe de user sur psql  

#####          installation

> depuis dépot rpm rehl 7: (certaines extention requiere epel)  
>    wget https://download.postgresql.org/pub/repos/yum/10/redhat/rhel-7-x86_64/pgdg-redhat10-10-2.noarch.rpm  
>    rpm -i pgdg-redhat10-10-2.noarch.rpm  
>    yum install -y postgresql10-server postgresql10  

#####          commande interne pgsql

`\?` = help  
`\h` = aide mémoire  
`\h INSERT` = " " pour INSERT  

`\q` = quit  

`\l` = list les base de donnée  
`\d` = affiche la base courante  
`\dt` = affiche uniquement les tables de la base  
`\d table1` = affiche le shema de table1  
`\c bdd1` = se connecter à la basse bdd1  

`\cd dossier` = cd sur l\'hote  

####        MONGODB

`mongo --username alice --password abc123 --host mongodb0.tutorials.com --port 28015` = connexion a une base distance avec authentification  

####        GIT

> serveur avec user git

`sudo su -m -c "mkdir -p depot && cd depot && git init --bare" git` = initialise un nouveau depot git  

#####          client

`git config --global user.name 'monNom'` = information figurant dans les push  
`git config --global user.email 'monadresse@gmail.com'` = " "  
`git config --global color.ui 'auto'` = préférence de couleur  

`git init` = initialise un dossier en depot git  
`git add *` = ajoute ou indexe des fichiers du repertoire au suivi de version (a faire pour les nouveaux fichiers ou les fichiers modifies)  
`git add *.c` = " " uniquement les sources en C  
`git add README` = ajoute le fichier README au suivi de version  

`git commit` = ouvre un editeur de texte pour y saisir un commentaire des dernières modifications  
`git commit -v` = " " en indiquant affichant un git diff  
`git commit -m 'version initiale du projet' fichier` = valide les modifications et ajoute un commentaire  
`git commit -a -m 'modification globale'` = indexe toutes les modifications des fichiers deja suivi et les valides (evite un git add pour de simple modification de fichier deja suivi)  
`git commit --amend` = ajoute a la validation precedente l\'indexation d\'un fichier (apres un git add fichier_oublie_a_dernière_validation)  

`git push origin master` = pousse les modifications de la branche master vers le depot origin distant  

`git fetch` = recupere les index du depot distant uniquement sur la branche en cours sansfaire de merge  

#####          gestion des version

`git checkout fichier` = revient a la version initiale du fichier ou la dernière version indexee du fichier  
`git checkout origin/master fichier` = revient a la version du depot git de fichier  
`git reset  --hard origin/master` = revient a la version totale du depot git  
`git reset HEAD fichier` = desindexe un fichier pour pouvoir ensuite revenir a la version avant commit

`git clone depot1 depot2` = clone le dépot1 local vers depot2  
`git clone git://github.com/user/projet.git` = recupere le projet, initialise le dossier git et affecte les droits pour etre modifiable  
`git clone git://github.com/user/projet.git projet_renomme` = " " en renommant le dossier du projet  

`git status` = informe de l\'etat du suvis de version du repertoire (fichiers non suivis, fichiers suivi modifies mais non indexes)  
`git status fichier` = " " sur un fichier  
`git diff` = a definir  
`git diff --staged` = a definir  
`git log` = renvoi les logs des versions du depot  
`git log -p 2` = renvoi le detail des deux dernières modifications  

`git rm fichier.sh` = supprime un fichier et desactive son suivi  
`git mv fichier nouveau_fichier` = renomme un fichier en continuant le suivi  

`git remote` = renvoi les informations du depot distant  
`git remote -v` = " " avec son adresse  
`git remote show origin` = renvoi les informations de la branche origin du depot distant  
`git remote add domain ssh://git@domain.dom:2222/srv/git/example` = ajoute un depot git distant en specifiant le port et en lui affectant le nom domain  
`git remote remove origin` = supprime le depot origin  
`git remote rename domain origin` = renomme le depot domain en origin  

#####          gestion des branches

`git branch nom_branche` = cree une nouvelle branche  
`git branch nom_branche` = cree une nouvelle branche avec track des modifs  
`git checkout -b nom_branche` = creation et placement sur la nouvelle branche  

`git checkout nom_branche` = se place vers la branche (master est la branche principal)  
`git checkout nom_branche --track` = " " avec option de tracage local (recommandé et a faire sur chaque depot)  

`git branch -a` = liste toutes les branches  
`git branch -d nom_branche` = supprimer la branche  

`git push -u git://git.example.dom/depot nom_branche` = pousse la branche vers le dépot distant  

`git merge nom_branche` = integre les modifications de nom_branche sur la branche en cours  

#####          gestion des tags

`git tag -l` = liste les tag  

`git tag v0.1-beta` = cree un tag leger  
`git tag -a v.01-beta -m "information"` = cree un tag annote (recommandee)  

`git push origin nom_tag` = pousse le tag vers le dépot distant  

#####          soucis frequent commit d'avance sur remote

`git fetch`  
`git rebase -p`  
`git push`  

#####          conflit et merge

> **cas** : Automatic merge failed  
> modification de fichier1 sur hostA  
> modification de fichier1 sur hostB  
> git push depuis hostA -> OK  
> git push depuis hostB -> KO :  
>    (...)  
>    ! [rejected]        master -> master (fetch first)  
>    (...)  
>    hint: Updates were rejected because the remote contains work that you do  
>    hint: not have locally. This is usually caused by another repository pushing  
>    (...)  
> git pull sur hostB -> KO :  
>    (...)  
> Automatic merge failed; fix conflicts and then commit the result.  
>    $ cat fichier1  
>    <<<<<<< HEAD  
>    ici modification sur hostB   
>    =======  
>    ici modification sur hostA dans le dépot  
>    >>>>>>> xxxxxxxxxxxxxyyyyyyyyyyyyyyyyyyyyy  
> **solution** :   
> debut correction manuelle sur fichier1  
> fin correction
>    git add fichier1   
>    git commit -m "correction fichier1 merge"   
>    git push

> **cas**  : commit your changes or stash them  
> modification de fichier1 sur hostA  
> modification des mêmes lignes de fichier sur hostB  
>    git push depuis hostA -> OK  
>    git pull depuis hostB -> KO :  
>    error: Your local changes to the following files would be overwritten by merge:  
>    fichier1  
>    Please, commit your changes or stash them before you can merge.  
> **solution**  
>    git stash  
>    git pull  
>    git stash apply  
> debut correction manuelle sur fichier1  
>    <<<<<<< Updated upstream  
>    modification distance  
>    =======  
>    modification locale stashée  
>    >>>>>>> Stashed changes  
> fin correction  
>    git add fichier1  
>    git commit -m "correction fichier1 merge"  
>    git push

#####          git-crypt

> git-crypt permet de chiffré des fichier d'un dépot  
> en utilisant une clés git-crypt-key externe au dépot  
> il utilise le fichier .gitattributes pour lister les fichiers chiffrés  

>    mkdir depot  
>    cd depot  
>    git init 
>    git-crypt keygen ~/.keys/git-crypt_key  
>    git-crypt init ~/.keys/git-crypt_key  
>    vi .gitattributes
>      
>    * filter=git-crypt diff=git-crypt  
>    .gitattributes !filter !diff  

`git-crypt init ~/.keys/git-crypt_key` = dévérouille un dépot git chiffré  

####        BAREOS

`bconsole` = lance la console bareos  
`status dir` = liste les jobs en cours et un petit historique des dernier jobs avec leurs états  

####        MAVEN

> pom.xml = configuration du projet maven en cours  
>   
> ajouter :   
>  <properties>  
>    <maven.compiler.source>1.8</maven.compiler.source>  
>    <maven.compiler.target>1.8</maven.compiler.target>  
> </properties>

`mvn archetype:generate -DgroupId=com.mycompany.app -DartifactId=my-app -DarchetypeArtifactId=maven-archetype-quickstart -DinteractiveMode=false` = créé l'arborescence d'un projet minimal prêt à compiler dans un dossier nommé "my-app"  
`mvn package` = compile en .jar le projet  
`java -cp target/my-app-1.0-SNAPSHOT.jar com.mycompany.app.App` = execute le projet.jar  

####        ANSIBLE

> cette section explique uniquement les ad-hoc commandes et les commandes ansible  qui peuvent être en s'appuyant sur les fichiers de son arborescence
> cf wiki/ansible/...

`ansible --help` = info sur ansible  
`ansible-doc -l` = liste des modules disponibles  
`ansible-doc postgresql-db` = information sur le module postgresql-db  
`ansible-doc -s postgresql-db` = courte information avec example " "  

#####          gestion de playbook

`ansible-lint playbook.yml` = corrige la syntaxe de playbook (dependance ansible-lint)

`ansible-playbook playbook_web.yml` = joue le playbook_web  
`ansible-playbook -C playbook_web.yml` = " " en mode simulation  

#####          gestion des rôles

> https://galaxy.ansible.com/

`ansible-galaxy init apache`= initialise un nouveau rôle et structure le dossier automatiquement

`ansible-galaxy install user.role1` = clone le role1 depuis le depot ansible-galaxy


#####          ad-hoc command

> ensemble d'action pouvant être lancé directement depuis ansible sans passer par les playbooks/roles.

`ansible all -m ping` = ping tous les hosts du fichier inventory par defaut avec le user par default
`ansible all -m ping -i inventory ` = " " depuis le fichier inventory

`ansible web1 -m setup` = liste des variables nommées facts de web1

`ansible web1 -m setup -a "filter=ansible_distribution_major_version"` = renvoit la version principal des hosts  
`ansible all -m debug -a "var=nom_variable"` = retourne la valeur de nom_variable parmis tous les hosts  

`ansible all -m copy -a "dest=/tmp/ansible content=Hello"` = execute le module copy contenant les arguments de source et de contenu pour écrire un fichier vers tous les hosts  
`ansible  all -a "hostname -I"` = execute une commande envoyé en argument sur tous les hosts et renvoit le retour  
`ansible web -m yum -a 'name=httpd state=present'` = installe un paquet à distance  
`ansible web -m yum -a 'name=httpd state=latest'` = met à jour un paquet à distance  
`Iansible web -a "/usr/bin/foo" -u username --become --ask-become-pass` = execute une commande en changeant d\'utilisateur  

#####          modules ansible

`ansible web -m dnf -a 'name=paquet state=present'` = gestion de paquet (state=present|absent|latest)  
`ansible web -m yum -a 'name=paquet state=present'` = gestion de paquet (state=present|absent|latest)  
`ansible web -m apt -a 'name=paquet state=present'` = gestion de paquet (state=present|absent|latest)  

`ansible web -m file -a 'dest=/srv/foo/b.txt mode=600 owner=www-data group=www-data'` = transfert de fichier  
`ansible web -m template -a 'dest=/etc/httpd/conf/vhost.d/vhost.conf src=vhost.con.j2'` = transfert un template e le complétant  
`ansible web -m file -a "dest=/path/to/create mode=755 owner=root group=root state=directory"` = cree un dossier et son arborescence  
`ansible web -m file -a 'dest=/path/to/delete state=absent'` = supprime un fichier/dossier  

`ansible web -m user -a 'name=kyprio password=<crypted password here>'` = creation utilisateur  
`ansible web -m user -a 'name=kyprio state=absent'` = suppression utilisateur  

`ansible clients -m timezone -a 'name=Europe/Paris'` = modifie la timezone  

> user:  
>     name: shado  
>     password: "la_faucheuse"  
>     comment: "Shadowallker"  
>     shell: /bin/bash  
>     uid: 1040  
>     group: admin, shado, sudo  
>     generate_ssh_key: yes  
>     ssh_key_bits: 2048  
>    ssh_key_file: .ssh/id_rsa

`ansible webservers -m service -a 'name=httpd state=restarted'` = gestion de service (state=started|stopped|restarted|reloaded)  
`ansible webservers -m service -a 'name=httpd enabled=yes'` = active le service au démarrage  

`ansible all -B 360 -P 0 -a "yum update -y"` = lance une action longue en arrière plan avec un timeout de 1h  


####        MESSAGERIE ZARAFA

`zarafa-admin -l` = liste des groupes et des utilisateurs  
`zarafa-admin -l --detail username1` = renvoi le detail du compte username  
`zarafa-admin --sync` = synchronise le zarafa au ldap  
`zarafa-admin --type group --details groupe` = renvoi le detail du groupe  

####        MESSAGERIE MTA EXIM

`exim -bt groupe@mail.com` = simule l\'envoi d\'un mail à une liste  

##    RESEAUX

###      CLASSE RESEAU ET CALCUL RESEAU

> **classe A** 1.0.0.0 - 126.255.255.255 /8 (255.0.0.0) 00000001.x.x.x 011111111.x.x.x
> **classe B** 128.0.0.0 - 191.255.255.255 /16 (255.255.0.0) 10000000.x.x.x 10111111.x.x.x
> **classe C** 192.0.0.0 - 223.255.255.255 /24 (255.255.255.0) 11000000.x.x.x 11011111.x.x.x
> **classe D** 224.0.0.0 - 239.255.255.255 /32 (255.255.255.255) 11100000.x.x.x 11101111.x.x.x

> **plage réservée de type classe A pour réseaux privés** 10.0.0.0/8 : 10.0.0.0 - 10.255.255.255  
> **plage réservée de type classe A pour boucle locale** 127.0.0.0
> **plage réservée de type classe B pour réseaux privés** 172.16.0.0/12 : 172.16.0.0 - 172.31.255.255   
> **plage réservée de type classe C pour réseaux privés** 192.168.0.0/16 : 192.168.0.0 - 192.168.255.255

> /nbits                /nbits -8           ...               ...
> 2^(32-masque) adrs    x256                ...               ...
> reseau - broadcast    ...                 ...               ...
> /32 255.255.255.255   /24 255.255.255.0   /16 255.255.0.0   /8 255.0.0.0  
> 2^0 = 1 adresses      1x256               1x256x256  
> 192.168.1.1 - .1      192.168.0.0 - .255
> /31 255.255.255.254   /23 255.255.254.0   /15 255.254.0.0   /7 254.0.0.0  
> 2^1 = 2 adresses      2x256               2x256x256  
> 192.168.1.0 - .1      192.168.0.0 - .1.255
> /30 255.255.255.252   /22 255.255.252.0   /14 255.252.0.0   /6 252.0.0.0
> 2^2 = 4 adresses  
> 192.168.1.0 - .3      192.168.0.0 - .3.255
> /29 255.255.255.248   /21 255.255.248.0   /13 255.248.0.0   /5 248.0.0.0  
> 2^3 = 8 adresses
> 192.168.1.0 - .7      ...
> /28 255.255.255.240   /20 255.255.240.0   /12 255.240.0.0   /4 240.0.0.0  
> 2^4 = 16 adresses  
> 192.168.1.0 - .15  
> /27 255.255.255.224   /19 255.255.224.0   /11 255.224.0.0   /3 224.0.0.0  
> 2^5 = 32 adresses  
>  192.168.1.0 - .31  
> /26 255.255.255.192   /18 255.255.192.0   /10 255.192.0.0   /2 192.0.0.0  
> 2^6 = 64 adresses  
>  192.168.1.0 - .63  
> /25 255.255.255.128   /17 255.255.128.0   /9  255.128.0.0   /1 128.0.0.0
> 2^7 = 128 adresses  
> 192.168.1.0 - .127   

###      MODELE OSI  MODELE TCP/IP

| OSI                    | TCP/IP            | définition |
|------------------------|-------------------|------------|
| **APPLICATION**        | **APPLICATION**   | Interface utilisateur, logiciel, FTP, RSH, Multimedia, SNMP, telnet, rlogin |
| **PRESENTATION**       | --                | compression, cryptage,  RTP |
| **SESSION**            | --                | DNS, transaction, RPC, RTCP |
| **TRANSPORT**          | **TRANSPORT**     | "Segments", TCP/UDP |
| **RESEAU**             | **RESEAU**        | "Datagrammes", IP, Routeurs, RARP, ARP, IP, ICMP, IGMP |
| **LIAISON DE DONNEES** | **ACCES RESEAUX** | "Trames", MAC, switch, connexion |
| **PHYSIQUE**           | --                | "Bits", cables, ethernet |

###      LISTE DES PORTS PAR SERVICE OU PROTOCOLE

> DEBUT

`1 TCP` = TCPMUX  
`2 TCP` = compressnet  
`3 TCP` = compressnet  
`5 TCP` = rje  
`7 TCP` = echo/ICMP(ping)  
`9 TCP` = Discard Protocol (en)  
`11 TCP` = SYSTAT (en)  
`13 TCP` = Daytime Protocol (en) RFC 867  
`17 TCP` = qotd  
`18 TCP` = msp  
`19 TCP` = CHARGEN  
`20 TCP` = ftp-data  
`21 TCP` = ftp  
`22 TCP` = SSH  
`23 TCP` = telnet  
`24 TCP` = any private mail system  
`25 TCP` = smtp  
`27 TCP` = nsw-fe  
`29 TCP` = msg-icp  
`31 TCP` = msg-auth  
`33 TCP` = dsp  
`35 TCP` = any private printer server  
`37 TCP` = Time Protocol RFC 868  
`38 TCP` = rap  
`39 TCP` = rlp  
`41 TCP` = graphics  
`42 TCP` = nameserver  
`43 TCP` = nicname  
`44 TCP` = mpm-flags  
`45 TCP` = mpm  
`46 TCP` = mpm  
`47 TCP` = ni-ftp  
`48 TCP` = auditd  
`49 TCP` = login  
`50 TCP` = re-mail-ck  
`51 TCP` = la-maint  
`52 TCP` = xns-time  
`53 UDP/TCP` = domain name server DNS  
`54 UDP` = xns-ch  
`55 TCP` = isi-gl  
`56 TCP` = xns-auth  
`57 TCP` = any private terminal access  
`58 TCP` = xns-mail  
`59 TCP` = any private file service  
`61 TCP` = ni-mail  
`62 TCP` = acas  
`64 TCP` = covia  
`65 TCP` = tacacs-ds  
`67 UDP` = bootp/dhcpd server  
`68 UDP` = bootp/dhcp client  
`69 UDP` = tftp  
`70 TCP` = gopher  
`71 TCP` = netrjs-1 Remote Job Service  
`72 TCP` = netrjs-2 Remote Job Service  
`73 TCP` = netrjs-3 Remote Job Service  
`74 TCP` = netrjs-4 Remote Job Service  
`75 TCP` = any private dial out service  
`76 TCP` = deos  
`77 TCP` = any private RJE service  
`78 TCP` = vetTCP  
`79 TCP` = finger  
`80 TCP` = www-http  
`81 TCP` = host2-ns  
`82 TCP` = xfer  
`83 TCP` = mit-ml-dev  
`84 TCP` = ctf  
`85 TCP` = mit-ml-dev  
`86 TCP` = mfcobol  
`87 TCP` = any private terminal link  
`88 TCP` = kerberos  
`89 TCP` = su-mit-tg  
`90 TCP` = dnsix  
`91 TCP` = mit-dov  
`92 TCP` = npp  
`93 TCP` = dcp  
`94 TCP` = objcall  
`95 TCP` = supdup  
`96 TCP` = dixie  
`97 TCP` = swift-rvf  
`98 TCP` = tacnews  
`99 TCP` = metagram  
`100 TCP` = newacct  
`101 TCP` = hostname  
`102 TCP` = iso-tsap  
`103 TCP` = gppitnp  
`104 TCP` = acr-nema  
`105 TCP` = csnet-ns  
`106 TCP` = 3com-tsmux  
`107 TCP` = rtelnet (en)  
`108 TCP` = snagas  
`109 TCP` = pop2  
`110 TCP` = pop3  
`111 UDP/TCP` = sunrpc  
`112 TCP` = mcidas  
`113 TCP` = auth  
`114 TCP` = audionews  
`115 TCP` = sftp  
`116 TCP` = ansanotify  
`117 TCP` = uucp-path  
`118 TCP` = sqlserv  
`119 TCP` = nntp  
`120 TCP` = cfdptkt  
`121 TCP` = erpc  
`122 TCP` = smakynet  
`123 UDP/TCP` = NTP NTPv4  
`124 TCP` = ansatrader  
`125 TCP` = locus-map  
`126 TCP` = unitary  
`127 TCP` = locus-con  
`128 TCP` = gss-xlicen  
`129 TCP` = pwdgen  
`130 TCP` = cisco-fna  
`131 TCP` = cisco-tna  
`132 TCP` = cisco-sys  
`133 TCP` = statsrv  
`135 TCP` = loc-srv  
`136 TCP` = profile  
`137 TCP` = netbios-ns  
`138 TCP` = netbios-dgm  
`139 TCP` = netbios-ssn  
`140 TCP` = emfis-data  
`141 TCP` = emfis-cntl  
`142 TCP` = bl-idm  
`143 TCP` = imap2, imap4  
`144 TCP` = news  
`145 TCP` = uaac  
`146 TCP` = iso-tp0  
`147 TCP` = iso-ip  
`148 TCP` = cronus  
`149 TCP` = aed-512  
`150 TCP` = sql-net  
`151 TCP` = hems  
`152 TCP` = bftp  
`153 TCP` = sgmp  
`154 TCP` = netsc-prod  
`155 TCP` = netsc-dev  
`156 TCP` = sqlsrv  
`157 TCP` = knet-cmp  
`158 TCP` = pcmail-srv  
`159 TCP` = nss-routing  
`160 TCP` = sgmp-traps  
`161 UDP` = SNMP  
`162 UDP` = SNMP trap  
`163 TCP` = cmip-man  
`164 TCP` = cmip-agent  
`165 TCP` = xns-courier  
`166 TCP` = s-net  
`167 TCP` = namp  
`168 TCP` = rsvd  
`169 TCP` = send  
`170 TCP` = print-srv  
`171 TCP` = multiplex  
`172 TCP` = cl/1  
`173 TCP` = xyplex-mux  
`174 TCP` = mailq  
`175 TCP` = vmnet  
`176 TCP` = genrad-mux  
`177 TCP` = xdmcp  
`178 TCP` = nextstep  
`179 TCP` = bgp  
`180 TCP` = ris  
`181 TCP` = unify  
`182 TCP` = audit  
`183 TCP` = ocbinder  
`184 TCP` = ocserver  
`185 TCP` = remote-kis  
`186 TCP` = kis  
`187 TCP` = aci  
`188 TCP` = mumps  
`189 TCP` = qft  
`190 TCP` = gacp  
`191 TCP` = prospero  
`192 TCP` = osu-nms  
`193 TCP` = srmp  
`194 TCP` = Internet relay chat (IRC)  
`195 TCP` = dn6-nlm-aud  
`196 TCP` = dn6-nlm-red  
`197 TCP` = dls  
`198 TCP` = dls-mon  
`199 TCP` = smux  
`200 TCP` = src  
`201 TCP` = at-rtmp  
`202 TCP` = at-nbp  
`203 TCP` = at-3  
`204 TCP` = at-echo  
`205 TCP` = at-5  
`206 TCP` = at-zis  
`207 TCP` = at-7  
`208 TCP` = at-8  
`209 TCP` = tam  
`210 TCP` = z39.50  
`211 TCP` = 914c/g  
`212 TCP` = anet  
`213 TCP` = ipx  
`214 TCP` = vmpwscs  
`215 TCP` = softpc  
`216 TCP` = atls  
`217 TCP` = dbase  
`218 TCP` = mpp  
`219 TCP` = uarps  
`220 TCP` = imap3  
`221 TCP` = fln-spx  
`222 TCP` = rsh-spx  
`223 TCP` = cdc  
`243 TCP` = sur-meas  
`245 TCP` = link  
`246 TCP` = dsp3270  
`264 UDP` = BGMP  
`344 TCP` = pdap  
`345 TCP` = pawserv  
`346 TCP` = zserv  
`347 TCP` = fatserv  
`348 TCP` = csi-sgwp  
`371 TCP` = clearcase  
`372 TCP` = ulistserv  
`373 TCP` = legent-1  
`374 TCP` = legent-2  
`375 TCP` = hassle  
`376 TCP` = nip  
`377 TCP` = tnETOS  
`378 TCP` = dsETOS  
`379 TCP` = is99c  
`380 TCP` = is99s  
`381 TCP` = hp-collector  
`382 TCP` = hp-managed-node  
`383 TCP` = hp-alarm-mgr  
`384 TCP` = arns  
`385 TCP` = ibm-app  
`386 TCP` = asa  
`387 TCP` = aurp  
`388 TCP` = unidata-ldm  
`389 TCP` = Lightweight Directory Access Protocol (LDAP)  
`390 TCP` = uis  
`391 TCP` = synotics-relay  
`392 TCP` = synotics-broker  
`393 TCP` = dis  
`394 TCP` = embl-ndt  
`395 TCP` = NETscout Control Protocol  
`396 TCP` = netware-ip  
`397 TCP` = mptn  
`398 TCP` = kryptolan  
`400 TCP` = work-sol  
`401 TCP` = ups  
`402 TCP` = genie  
`403 TCP` = decap  
`404 TCP` = nced  
`407 TCP` = Timbuktu (software) (en)  
`408 TCP` = prm-sm  
`409 TCP` = prm-nm  
`410 TCP` = decladebug  
`411 TCP` = rmt  
`412 TCP` = synoptics-trap  
`413 TCP` = smsp  
`414 TCP` = infoseek  
`415 TCP` = bnet  
`416 TCP` = silverplatter  
`417 TCP` = onmux  
`418 TCP` = hyper-g  
`419 TCP` = ariel1  
`420 TCP` = smpte  
`421 TCP` = ariel2  
`422 TCP` = ariel3  
`423 TCP` = opc-job-start  
`424 TCP` = opc-job-track  
`425 TCP` = icad-el  
`426 TCP` = smartsdp  
`427 TCP` = svrloc  
`428 TCP` = ocs_cmu  
`429 TCP` = ocs_amu  
`430 TCP` = utmpsd  
`431 TCP` = utmpcd  
`432 TCP` = iasd  
`433 TCP` = nnsp  
`434 TCP` = mobileip-agent  
`435 TCP` = mobileip-mn  
`436 TCP` = dna-cml  
`437 TCP` = comscm  
`438 TCP` = dsfgw  
`439 TCP` = dasp  
`440 TCP` = sgcp  
`441 TCP` = decvms-sysmgt  
`442 TCP` = cvc_hostd  
`443 TCP` = https  
`444 TCP` = snpp  
`445 TCP` = microsoft-ds SMB, anciennement appele CIFS (Common Internet File System)  
`446 TCP` = ddm-rdb  
`447 TCP` = ddm-dfm  
`448 TCP` = ddm-byte  
`449 TCP` = as-servermap  
`450 TCP` = tserver  
`465 TCP` = smtp securise (ssl) (non officiel)  
`497 TCP` = retrospect  
`500 TCP` = ISAKMP (Internet Security Association and Key Management Protocol), un des composants d'IPsec  
`502 TCP` = Modbus sur TCP.  
`514 UDP` = Syslog RFC 3164 NB : ce service n'est pas liste habituellement dans le fichier etc\services  
`515 TCP` = printer  
`517 TCP` = talk  
`518 TCP` = ntalk  
`520 UDP` = Routing (RIP)  
`521 UDP` = Routing (RIP)  
`525 TCP` = timed  
`526 TCP` = tempo  
`546 UDP` = DHCP  
`548 TCP` = AppleShare IP Server  
`554 TCP` = RTSP (Real Time Streaming Protocol) RFC 2326  
`563 TCP` = nntp securise (ssl) RFC 4642  
`587 TCP` = smtp office365 / Message Submission for Mail RFC 5068 RFC 6409  
`631 TCP` = Internet Printing Protocol  
`636 TCP` = LDAPS  
`662 TCP/UDP` = rpc.statd (nfs)  
`706 TCP` = Secure internet live conferencing  
`706 UDP` = Secure internet live conferencing  
`873 TCP` = rsync  
`892 TCP/UDP` = rpc.mountd (nfs)  
`902 TCP/UDP` = VMware Vsphere client et Vcenter heartbeat  
`993 TCP` = imap securise (ssl)  
`995 TCP` = pop3 securise (ssl)  
`1080 TCP` = SOCKS  
`1194 UDP/TCP` = OpenVPN  
`1337 TCP/UDP` = Port utilise par les developpeurs pour tester des applications (Leet speak)  
`1352 TCP` = Lotus Notes  
`1414 TCP` = IBM MQSeries  
`1433 TCP` = Microsoft SQL Server  
`1434 TCP` = Microsoft SQL Monitor  
`1521 TCP` = Serveur Oracle  
`1524 TCP` = Ingreslock, voir Ingres (base de donnees)  
`1720 TCP` = H.323  
`1723 TCP` = PPTP VPN  
`1863 TCP` = MSN (tchat)  
`2049 TCP` = NFS v4  
`2106 TCP/UDP` = L2J LoginServer  
`2164 TCP/UDP` = Dynamic DNS  
`2427 UDP` = MGCP  
`3000 TCP` = First Class Server  
`3128 TCP` = Squid Proxy HTTP / Spice Proxy
`3129 TCP` = Squid Proxy HTTPS  
`3051 TCP` = AMS (Agency Management System)  
`3074 TCP/UDP` = nintendo server  
`3306 TCP` = MySQL Server/MariaDB
`3389 TCP` = Microsoft Terminal Server (RDP)  
`3632 TCP` = distcc (compilation partagee)  
`3725 TCP/UDP` = Netia NA-ER Port  
`3979 TCP/UDP` = Serveur Transport Tycoon deluxe en open source (openttd)  
`4011 UDP` = PXE  
`4126 TCP/UDP` = ddrepl data domain replication service
`5060 TCP/UDP` = serveur SIP (Session Initiation Protocol)  
`5062 TCP/UDP` = Localisation access  
`5222 TCP` = serveur Jabber  
`5223 TCP` = serveur Jabber securise (ssl)  
`5269 TCP` = serveur a serveur (server to server) Jabber  
`5280 TCP` = serveur BOSH  
`5353 TCP/UDP` = Multicast DNS  
`5432 TCP` = serveur PostgreSQL  
`5498 TCP` = Hotline Tracker  
`5500 TCP` = Hotline Server  
`5501 TCP` = Hotline Server  
`5900 TCP` = VNC Server  
`5984 TCP` = Couchdb Server  
`6000 TCP/UDP` = X11  
`6112 TCP` = Warcraft III  
`6522 TCP` = Gobby Server (Sobby)  
`6667 TCP` = Serveur IRC  
`6697 TCP` = Serveur IRC securise (ssl)  
`7000 TCP` = Serveur IRC securise (ssl) alternatif  
`7648 TCP` = CU-SeeMe (en)  
`7725 TCP/UDP` = Port par defaut de Faronics Deep Freeze (console/serveur)  
`7777 TCP/UDP` = Serveur Terraria / L2J Gameserver  
`8000 TCP` = Hotline  
`8006 TCP` = Dell AppAssure (en) Replication/Management Interface  
`8008 TCP` = Serveur CalDAV  
`8009 TCP` = Port AJP utilise par Tomcat  
`8080 TCP` = http alternatif (webcache)  
`8098 TCP` = Administration Microsoft Windows Server 2003  
`8140 TCP` = Serveur Puppet (port par defaut pour le master)  
`8443 TCP` = Serveur CalDAV securise (ssl)  
`8787 TCP` = Serveur RStudio  
`8888 TCP` = VolumeOnLan Client/serveur  
`9009 TCP` = Pichat  
`9091` = interface web transmission-daemon  
`9102 TCP/UDP` = bacula-fd file daemon  
`9443 TCP` = vmware webgui  
`10443 TCP/UDP` = Fortinet SSL VPN
`11371 TCP/UDP` = OpenPGP  
`20048 TCP/UDP` = rpc.mountd (nfs)  
`25565 TCP/UDP` = Serveur Minecraft  
`27000-27050 TCP/UDP` = Serveur Steam  
`27017 TCP` = mongodb  
`47808-47823 UDP` = BACnet (BAC0 a BACF)  

> FIN

###      CONNEXIONS RESEAUX

####        INFORMATION DES CONNEXIONS

`ifconfig` = `ip addr show up` = `liste les informations des interfaces reseaux (ethX= port ethernet, lo` = boucle locale, wlan0=cartes wifi)  
`iwconfig` = liste les interface reseaux sans fils  
`ifconfig | grep HW` = " " en n\'affichant que les adresses MAC  

`ifconfig eth0 | awk '/inet addr/{print substr($2,6)}'` = renvoi et isole l'adresse IP  
`ip addr show dev eth0 | awk '/inet /{print substr($2,0)}'` = renvoi et isole l'adresse IP et le masque sous reseau  
`ip addr show dev eth0 | awk '/inet /{print substr($4,0)}'` = `ifconfig eth0 | awk '/inet addr/{print substr($3,7)}'` = renvoi et isole le broadcast  
`ifconfig eth0 | awk '/inet addr/{print substr($4,6)}'` = renvoi et isole le masque  

`www.whatismyip.com` = renseigne des informations sur notre reseau  
`dig +short myip.opendns.com @resolver1.opendns.com` = renvoi l\'adresse IP publique  

`getent hosts` = liste les IP et Noms de domaine du fichier /etc/hosts  

####        NMCLI NMTUI NETWORKMANAGER

> networkmanager permet de configurer les interfaces reseaux en console  
> nmcli est la ligne de commande  
> nmtui est l\'interface textuelle de networkmanager

`nmcli help` = aide de networkmanager  

`nmcli con status id "Wired connection 1"` = status de la connexion  



`nmtui` = ouverture de l\'interface textuelle de networkmanager en console  
`nmcli con show` = renvoi les interfaces reseaux actives  
`nmcli con up "eth0"` = active l\'interface reseau  
`nmcli con mod "eth0" connection.zone zone1` = assigne eth0 a la zone1  

####        CONNEXION ETHERNET

`systemctl restart network.service` = `service network restart` = redémarre le service réseau  

`ip addr add 192.168.1.5/24 dev eth0` = `ifconfig eth0 192.168.1.5/24` = fixe temporairement l\'adresse IP a eth0  
`ip addr add 10.0.0.1/8 eth0 label eth0:1` = `ifconfig eth0:1 10.0.0.1/8` = " " sur une nouvelle interface virtuelle  
`ip addr del 192.168.1.5/24 dev eth0` = supprime temporairement l\'adresse IP de eth0  
`ip link set eth0 address 00:11:22:33:44:55` = `ifconfig eth0 hw 00:11:22:33:44:55` = change virtuellement l\'adresse mac de eth0  

`ip link set eth0 up` = `ifconfig eth0 up` = active eth0 (up down)  
`ip link set eth0 down` = `ifconfig eth0 down` = deactive eth0 et relâche l\'adresse IP attribuee  
`dhclient eth0` = envoi un ping a 0.0.0.0 pour demander au serveur DHCP une adresse IP valide  

`ip addr flush dev eth0` = relache l\'adresse affecte a eth0relache   

#####          windows

`ipconfig /release` = vide la configuration ip des interfaces  
`ipconfig /renew` = reconfigure la configuration ip des interfaces  

####        ROUTAGE

`ip route show` = `route -n` = liste les informations de routage  

`ss` = liste des sockets  
`ss -l` = liste des sockets en ecoute  
`ss -ln` = " " en remplacant le nom des protocoles par leur numero de   
`route -n | grep 'UG[ \t]' | awk '{print $2}'` = renvoi et isole la gateway par defaut  
`ip route get 192.168.88.77` = indique le routage prevu pour les paquets à destination de l'IP  

`route del default` = supprimer la route vers la gateway par defaut  
`ip route del 10.10.20.0/24` = supprime une route en particulier  
`ip route add 192.168.3.0/24 dev eth3` = supprime une route  

`ip route add default via 192.168.50.100` = `route add default gw 192.168.50.100` = ajoute une route par default  
`ip route add 10.10.20.0/24 via 192.168.50.100 dev eth0` = ajoute une route en utilisant eth0 et en passant par 192.168.50.100 (temporaire)  

` cat /proc/sys/net/ipv4/ip_forward` = renvoi l\'etat (1 ou 0) du routage des paquets etrangers  
`echo "net.ipv4.ip_forward=1" > /etc/sysctl.conf && sysctl- p && sysctl net.ipv4.ip_forward` = active le routage des paquets etrangers et verifie la valeur du noyau  

`tracepath 8.8.8.8` = liste les routeurs parcourus pour atteindre la cible  

####        INFORMATION DNS

`nslookup yahoo.fr` = indique l\'ensemble des serveurs repondant à yahoo.fr  
`dig yahoo.fr` = " " avec plus de details  

####        CONNEXION SMTP MAIL

#####          test de port 

telnet smtp.orange.fr 465

#####          test complet d\'envoi reception de mail vers l\'utilisateur interne root@mail.com

telnet localhost 25
HELO truc.com
MAIL FROM: <bidon@truc.com>
RCPT TO: <root@mail.com>
DATA
Subject: Test d'envoi de mail
Contenu de test du mail
.
QUIT

#####          test complet d\'envoi de mail vers une adresse mail externe reelle destinataire@gmail.com

telnet localhost 25
HELO truc.com
MAIL FROM: <bidon@truc.com>
RCPT TO: <destinataire@gmail.com>
DATA
Subject: Test d'envoi de mail
Contenu de test du mail
.
QUIT

####        MAIL

`echo "Test mail from postfix" | mail -s "Test Postfix" adresse@mail.com` = teste la configuration d'envoi de mail depuis le serveur smpt  

###      SECURITE

####     HASHAGE SUM

`echo -n motdepasse | openssl dgst -sha256` = renvoit le hash sha256
`echo -n motdepasse | openssl dgst -sha1` = renvoit le hash sha1
`echo -n motdepasse | openssl dgst -md5` = renvoit le hash md5

####        LE PAREFEU IPTABLES

> Test des informations du paquets en suivant l'ordre des règles iptables  
> la dernière règle doit être la plus large possible (DENY all) sauf si une POLICY est déjà appliquée  
> comphéhension générale :  
>
> ####        Chain
> 
> chaine = point de passage des paquets
> 
>               |--------->FORWARD---------->|             
> PREROUTING--->|                            |--->POSTROUTING  
>               |----->INPUT   OUTPUT ------>|  
>                        ↑       ↓           
> ####        Tables
> 
> table = catégorisation des règles 
> 
> filter (default) : INPUT FORWARD OUTPUT  
> nat (DNAT) : PREROUTING OUTPUT = routage de paquets  
> nat (SNAT) : INPUT POSTROUTING = " "  
> raw : PREROUTING OUTPUT = suivie des connexion (conntrack)  
> mangle : PREROUTING INTPUT FORWARD OUTPUT POSTROUTING  
> security : INPUT POSTROUTING security des echanges (SElinux)  
>   
> ####        priorité de lecture des tables sur chaque chaines :- PREROUTING raw / nat / mangle  
> 
> FORWARD filter / mangle  
> INPUT filter / security / nat  
> OUTPUT filter raw /  nat / mangle  
> POSTROUTING nat / mangle / security   

> ####        table raw : statefull- NEW : premier paquet d'une transmision détecté (TCP/UDP)  
> 
> ESTABLISED : paquet avec inversion des sources/destionations contenant ou non un SYN/ACK (tcp vs udp)  
> RELATED : paquet similaire à un autre paquet enregistré mais non lié à une connexion (ex: FTP DATA)  
> INVALID : paquet non routable ou non associé à une connexion comme ils prétendent l'être  
> UNTRACKED : bypass  
> SNAT : paquet qui devra subir une modification de son adresse source (précédemment modifié par un NAT)  
> DNAT : " " adresse destination ( " " )

#####          configuration permantente avec iptables-persistent

`iptables-save > /etc/iptables/rules.v4` = enregistrement des règles ipv4 vers la configuration permanente iptables  
`ip6tables-save > /etc/iptables/rules.v6` = " " ipv6 " "  
`systemctl enable iptables-persistent`  

#####          import export des règles

`iptables-restore < /etc/iptables/rules.v4`  

#####          suppression et vidage des règles en cours

`iptables -F` = `iptables --flush` = `iptables -F -t filter` = vide le contenu de la table par defaut filter  
`iptables -F -t nat`  
`iptables -F -t mangle`  
`iptables -F`  
`iptables -X`  

`iptables -D chaine1 regle/1...` = `iptables --delete chaine1 regle/1...` = supprime une règle ou un numero de règle d'une chaine spécifique  

#####          iptables commandes simples : ajout simple

`iptables -A regle...` = `iptables --append regles...` = ajoute une règle   
`iptables -A INPUT -i eth0 -j ACCEPT` = AJOUTE le filtre :ENTREE du serveur arrivant par l'interface eth0   
`iptables --append INPUT --interface eth0 --jump ACCEPT` = " "  

#####          iptables commandes simples : liste

`iptables -L chaine1` = liste les règles de la chaine1  
`iptables-save` = `iptables -S` = affiche l'ensemble des règles de la table par défaut (filter)  

#####          iptable -t nat et FORWARD de port

`iptables -A PREROUTING -t nat -i eth0 -p tcp --dport 80 -j DNAT --to-destination 192.168.1.10:8080` = nat des paquets du port 80 vers une autre ip en port 8080. Le paquet part ensuite en chaine FORWARD  
`iptables -A FORWARD -p tcp -d 192.168.1.10 --dport 8080 -j ACCEPT` = laisse passer le nouveau paquet natté vers la chaine suivante (POSTROUTING)  

#####          iptable -t nat MASQUERADE

`iptables -t nat -A POSTROUTING -s '10.10.10.0/24' -o eth0 -j MASQUERADE` = créé le nat pour le reseau 10.10.10.0 sur la carte wan eth0  

#####          cas pratique : limité les connexions à un protocole à 5/2min

`iptables -N SSH_CHECK`  
`iptables -I INPUT -p tcp --dport 22 -m state --state NEW -j SSHCHECK `  
`iptables -A SSHCHECK -m recent --set --name SSH`  
`iptables -A SSH_CHECK -m recent --update --seconds 120 --hitcount 5 --name SSH -j DROP`  

#####          Diagnostique iptables

`iptables -L -v` = affiche le total de paquet traités pour chaque chaine (utile pour voir si une chaine est utilisé ou non)

####        PFSENSE OPNSENSE

> backup config globale : /conf/backup

`pfctl -d` = désactive completement la protection parefeu (webgui accessible partout)  
`pfctl -e` = active la protection du parefeu  

####        UFW OUTIL PAREFEU

> ufw est un service et outil de simplification pour iptables de parefeu client  
> il ne permet pas de configurer iptable en passerelle  
> ufw lit les regles de haut en bas  
> la premiere regle prendra le dessus sur la deuxieme

`ufw enable` = active ufw  
`ufw disable` = desactive ufw  

`ufw status` = indique l\'etat d\'ufw et ses regles actives  
`ufw status verbose` = " " mode verbeux  
`ufw status numbered` = " " en numerotant les regles  
`ufw delete 1` =  supprimer la regle numerotee 1  
`ufw insert 1 deny regle_complexe` = insert une regle (cf plus bas) de refus en premiere position  

`ufw app list` = liste les applications reconnus pour faciliterl\'insertion de regles  

`ufw default allow` = autorise le trafic entrant suivant les regles par defaut  
`ufw default deny` =  refuse " "  
`ufw default allow incomming` = autorise le trafic entrant uniquement suivant les regles par defaut  
`ufw default allow outgoing` = " " sortant " "  

`ufw allow 22` = autorise le trafic entrant/sortant du port 22  
`ufw allow 22/tcp` = " " en tcp  
`ufw allow SSH` = autorise le trafic entrant du protocol SSH  
`ufw allow out regle` = autorise une regle en sortie  

`ufw allow from 192.168.1.0/24 to any port 111` = autorise le trafic entrant de la plage 192.168.1.0/24 vers toutes les ip sur le port 111  
`ufw deny from 192.168.1.4` = refuse le trafic venant d\'une ip  

`ufw allow logging on` = active les log (on/off)  

####        FIREWALLD OUTILS PAREFEU

> firewald est un service et outil de configuration dynamique pour iptables  
> il permet de configurer iptable en passerelle (si on active le routage)  
> RedHat contribue au projet mais existe aussi sur Debian  
> certaines options de commande existent seulement a partir de RHEL 7.3  
> il permet de gerer les zones d\'interface reseau aussi par networkmanager  
> la zone public regroupe toutes les interfaces et est definit par defaut  
> liste des services pris en charges : /usr/lib/firewalld/services  
> liste des services utilisateurs : /etc/firewalld/services/  
> debug de firewalld sur RHEL : FIREWALLD_ARGS='--debug' dans /etc/sysconfig/firewalld

`firewall-cmd --reload` = recharge les regles permanentes  

`firewall-cmd --list-all` = renvoi les regles de toutes les zones actives de la configuration en cours  
`firewall-cmd --permanent --list-all` = " " de la configuration permanente  
`firewall-cmd --state` = etat du service  
`firewall-cmd --get-default-zone` = renvoi la zone par defaut  
`firewall-cmd --get-active-zones` = renvoi les zones actives  
`firewall-cmd --get-zones` = renvoi les zones disponibles  
`firewall-cmd --get-zone-of-interface=eth0` = renvoi la zone assignee a eth0  
`firewall-cmd --permanent --zone=trusted --list-sources` = liste les sources assignee a une zone trusted  
`firewall-cmd --info-zone=public` = detaille la zone public  
`firewall-cmd --list-services` = liste les services de la zone par defaut  
`firewall-cmd --info-service=ftp` = detaille le service ftp  
`firewall-cmd --zone=internal --list-ports` = liste des ports autorises de la zone  
`firewall-cmd --zone=external --query-mascarade` = verifie si la mascarade est active sur la zone  
`firewall-cmd --direct --get-all-rules` = liste les regles direct iptables envoyees via firewalld  

`firewall-cmd --add-zone=nom-zone` = ajoute une nouvelle zone  
`firewall-cmd --set-default-zone=internal` = definit une zone par defaut  
`firewall-cmd --permanent --zone=internal --change-interface=eth0` = assigne une interface a une zone  
`firewall-cmd --permanent --zone=trusted --add-source=192.168.2.0/24` = assigne une plage ip a une zone  
`firewall-cmd --permanent --zone=trusted --remove-source=192.168.2.0/24` = supprime la plage ip de la zone  
`firewall-cmd --permanent --zone=internal --add-source=1.2.3.4/32` = assigne une ip a une zone  
`firewall-cmd --permanent --zone=trusted --add-source=ipset:liste_ip` = assigne un objet de liste ip a une zone  

#####          creation d\'un objet liste ip

`firewall-cmd --permanent --new-ipset=liste_ip --type=hash:ip` = cree un objet de liste ip  
`firewall-cmd reload`  
`firewall-cmd --ipset=liste_ip --add-entry=192.168.1.11` = ajoute une ip a la liste  

`firewall-cmd --permanent --new-zone=zone1` = cree une nouvelle zone  

`firewall-cmd --permanent --zone=internal --add-service=ssh` = ajoute le service ssh a une zone  
`firewall-cmd --zone=internal --add-service={http,https,dns}` = " " plusieurs services  
`firewall-cmd --permanent --zone=internal --remove-service=ssh` = supprime le service ssh a la zone  
`firewall-cmd --zone=internal --add-port=443/tcp` = ajoute le port 443 en tcp a une zone  

#####          masquerading nat

> set up two zones: internal external  
> configure masquerading on the external zone

`firewall-cmd --permament --set-default-zone=internal` = definit la zone internal par defaut  
`firewall-cmd --permament --zone=external --add-masquerade` = active la mascarade sur external  
`firewall-cmd reload`  
`firewall-cmd --zone=external --add-forward-port=port=3753:proto=tcp:toport=22` = redirige le port 3753 vers le 22 pour que le port 22 d'une machine en zone external soit accessible depuis le port 3753 du parefeu  
`firewall-cmd --permanent --zone=external --add-forward-port=port=3753:proto=tcp:toport=22:toaddr=10.0.0.1` = " " vers une ip  

#####          creation d\'un service pour firewalld

> modele : om/config/firewalld/_etc_firewalld_services_nom-service.xml

`restorecon /etc/firewalld/services/nom-service.xml` =  assign les attributs de securte SElinux au fichier  
`chmod 640 /etc/firewalld/services/nom-service.xml` = affecte les bon droits au service pour firewalld  
`firewall-cmd --permanent --zone=internal --add-service=nom-service` = ajoute le nouveau service a une zone  

#####          ajout de module

> exemple : ajout du suivie des connexions ftp

`echo ip_nat_ftp > /etc/modules-load.d/firewall_ftp.conf`  
`echo ip_conntrack_ftp >> /etc/modules-load.d/firewall_ftp.conf`  

`firewall-cmd --direct --add-rule ipv4 filter INPUT 0 -p tcp --dport 9000 -j ACCEPT` = ajout d\'une regle iptable via firewalld (ouvre le port 9000)  

####        CERTIFICAT DE SECURITE SSL TLS


#####          test certificat

`openssl x509 -enddate -noout -in certicat.pem` = renvoi la date d\'expiration du certificat  
`openssl x509 -noout -in certificat.pem -checkend 7862400` = renvoi 1 si le certificat expire avant 3 mois (7862400secondes)  

#####          creation certificat

`openssl genrsa -out key.pem 2048` = création d'une paire privé publique de type RSA
`opensal rsa -in key.pem -text -noout` = lit le contenu dune cles
`openssl rsa -in key.pem -aes256 -out key.pem` = chiffre la clés rsa
`openssl rsa -in key.pem -pubout -ou keypub.pem` = lit et extrait la clés chiffrée
`openssl req -new -x509 -keyout cert.pem -out cert.pem -days 3650 -nodes -subj "/C=PAYS/ST=REGION/L=VILLE/O=SOCIETE/OU=SOCIETE/CN=NOM"` = cree automatiquement sans prompt un certificat.pem 2048 bit  
`openssl dhparam -out $HOME/dh2048.pem 2048` = cree une clef de cryptage de Diffie-Hellman de 2048 bits 512,1024,2048,4096)  
`openssl req -config openssl_users.cnf -newkey rsa:2048 -keyout key/user-prenom_nom.pem -out req/prenom_nom.req` = creation d\'une cles prive  
`openssl ca -config openssl_users.cnf -out cert/date_prenom_nom.crt -in req/user-prenom_nom.req` = creation d'un certificat  
`openssl pkcs12 -export -in cert/date_prenom_nom.crt -inkey key/user-prenom_nom.pem -certfile CA/ca.crt -out p12/prenom_nom.p12` = renouvellement du certificat et creation de l\'archive cryptee p12  

#####          chiffreer dechiffre message

`openssl rsautl -encrypt -in message.txt -in-key keypub.pem -out message_chiffre.txt`

#####          renouvellement certificat

`openssl ca -config openssl.cnf -revoke cert/anciennedate_prenom_nom.crt` = revoque l\'ancien certificat  
`openssl ca -config openssl_users.cnf -out cert/date_prenom_nom.crt -in req/user-prenom_nom.req` = creation d'un nouveau certificat  
`openssl pkcs12 -export -in cert/date_prenom_nom.crt -inkey key/user-prenom_nom.pem -certfile CA/ca.crt -out p12/prenom_nom.p12` = renouvellement de ma cles et creation de l\'archive cryptee p12  

####        PENTEST ET DDOS

`hping3 -c 10000 -d 120 -S -w 64 -p 80 --flood --rand-source IP_cible` = ddos par envoit de 10000 paquets à IP_cible avec des ip sources aleatoire sur le port 80 (ceci peut faire tomber le reseaux wifi ou bloquer le routeur) (paquet hping3 requis)  

###      ECOUTE DU RESEAUX

####        TCPDUMP

`tcpdump -n not "icmp" and not "icmp6" and not "arp" and not "stp"` = ecoute standard sans pollution visuelle d'un procole autre que ICMP, ARP STP  

`tcpdump` = ecoute le reseaux depuis toutes les interfaces connectees  
`tcpdump -q` = "" en mode moins quiet (moins verbeux)  
`tcpdump -v` = "" en mode verbeux  
`tcpdump -vv` = "" en modee tres verbeux  
`tcpdump -vvv` = "" en mode verbeux maximal  
`tcpdump -n` = " " sans resoudre les DNS  
`tcpdump -c 20` = " " sur 20 paquets  
`tcpdump -D` = liste les interfaces disponibles en ecoute pour tcpdump  
`tcpdump -i eth0` = ecoute le reseaux depuis eth0  

`tcpdump port http` = ecoute du traffic TCP 80  
`tcpdump -n net 192.168.1.0/24` = ecoute la plage 192.168.1.0/24  
`tcpdump -n dst net 192.168.1.0/24` = " " en destination  
`tcpdump src 192.168.1.100 and dst 192.168.1.2 and port ftp` = ecoute le reseaux suivant en filtrat la source la destination et le port utilise  
`tcpdump host www.google.fr` = filtrer selon qu'une des deux machines destinataire oue experitrive s'appelle    
`tcpdump not "arp"` = ecoute le reseau en excluant le protocole arp  
`tcpdump port not "22"` = ecoute le reseau en excluant le port ssh  
`tcpdump "icmp" or "arp"` = ecoute le reseau en affichant uniquement les protocoles icmp ping et arp  
`tcpdump "dst host 192.168.1.1 and (dst port 80 or dst port 443)"` = ` feltre cll-cmd --state` = etat du service  
omplexe avec parenthese
`tcpdump tcp dst portrange 1-1023` = ecoute tous les flux ayant pour port de destination 1 à 1023  

`tcpdump -F fichier_conf` = analyse le traffic selon la configuration dans fichier_conf   
`tcpdump -w fichier_dest` = enregistre sous fichiier_test  
`tcpdump -r fichier_source` = lit depuis fichier_source  

####        ARP ET MAC

`arp -a` = renvoi la table de concordance des IP et MAC  
`ip neigh add 192.168.0.1 lladdr 00:11:22:33:44:55 nud permanent dev eth0` = `arp -i eth0 -s 192.168.0.1 00:11:22:33:44:55` = ajoute une entree a la table ARP  
`ip link set dev eth0 arp off` = `ifconfig -arp eth0` = switch l\'etat de la resolution ARP d\'une interface  

####        NETSTAT SS

`ip -s link` = `netstat -i` = renvoi les informations d\'activites des interfaces reseaux (colonne RX-ERR)  
`netstat -u` = `tcpdump udp` = renvoi les connexions UDP (peu utile)  
`netstat -t` = " " tcp  
`netstat -a` = renvoi les connexions y compris les connexions en attente en incluant l\'information de l\'adresse locale et l\'adresse distante connectee avec l\'adresse locale  
`netstat -e` = " " avec le nom de l'utilisateur  
`netstat -n` = renvoi le numero des ports (a utiliser avec -aut)  
`netstat -p` = " " avec le pid  

`netstat -l` = renvoi les connexions avec le filtre sur l\'etat en ecoute pour voir les ports ecoutant le reseau comme SSH(a utiliser avec l\'option -t)  
`netstat -ltn` = " " en changeant pas leurs ports correspondants  
`netstat -s` = statistiques reseaux de la machine local  

`netstat -laputen` = renvoi l'etat des connexions en detail avec le numero de port et son service associe  

`ss` = liste des sockets  
`ss -l` = liste des sockets en ecoute  
`ss -ln` = " " avec les numeros de port  
`ss -t` = " " uniquement tcp  
`ss -u` = " " uniquement udp  

`ss -lntu` = liste des sockets en ecoute avec leur numero de port en TCP/UDP uniquement  

####        NC NETCAT

`nc host.example 80` = tente une connection en TCP au port 80 HTTP  
`nc -u host.example.com 53` = tente une connection en UDP au port 53 DNS  
`nc -zv host.example.com 80 20 22` = reporte (-z) l'etat des ports 80 20 22 de l\'hote distant  
`nc -zvu 80.15.92.128 21194` = "" port 21194 en UDP  

####        NMAP

> en sudo

`nmap host.example.com` = teste tous les ports ouverts en TCP  
`nmap -A host.example.com` = "" en recuperant plus de details sur les protocoles reconnus  
`nmap -O host.example.com` = "" et detecte l\'OS  
`nmap -P host.example.com` = "" en estimant que le serveur dejà distant est connecte  
`nmap -n host.example.com` = "" en passant la recherche reverse DNS  
`nmap -p 1194 remote_host` = "" sur le port 1194 uniquement  
`nmap -sS 192.168.1.1` = syn scan (demande de connexion)  
`nmap -sU 192.168.1.1` = scan des ports UDP  
`nmap -sS -sU -O -oN nmap.log 192.168.0.1-254` = information complete sur le reseau stocke dans un fichier log  

####        SNMP

`snmpwalk 192.168.1.1` = intercepte les donnees snmpd de la machine cible  
`snmpwalk -v 2c -c secret 192.168.1.1` = " " en version 2 snmpd avec mdp

####        DIAGNOSTIQUE WEB

`curl -v http://localhost:8080/` = affichage brut des donnees HTTP renvoyes par le serveur  
`curl -u user:password http://localhost:8080` = test un accès protégé par htpasswd  

####        RECHERCHE DNS/IP

`host IP` = liste des machines enregistrees dans le DNSdu domaine  
`host nom.hote.fr` = affichier l\'IP correspondant a ce nom d\'hote " "  
`whois siteduzero.com` = informations sur le nom de domaine siteduzero.com (proprietaire, adresse ...)  
`traceroute6 siteduzero.com` = renvoi la liste des passerelles traverses pour atteindre la cible  
`curl ipinfo.io/8.8.8.8` = affiche la location de l'IP  
`curl ipinfo.io/8.8.8.8/city` = affiche la ville de l'IP


