#  SYSTEME INFORMATIQUE


## SYSTEME INFORMATIQUE
ensemble de materiels, logiciel et systeme permettant de stocker, traiter et transferer des donnees

### stockage :
support magnetique : HDD
support laser : CD DVD
support electrique : SSD

### transfert
vitesse lumiere : C = 300000Km/s
NVP Nominal Velocity Propagation

Support electrectrique : signaux sinusoidaux (sinus), signaux numerique (carre) = 70% à 90% de C
Support optique : NVP = 95% de C

### bit
Binary Digit : support magnetique, laser, electrique, optique pouvant prendre 2 états qu'on code 0 ou 1
// codage
transformer des 0 et des 1 vers le support de stockage
// décodage
inverse
//coder sur 8bit = 2^8 = nombre allant de 0 à 256 

## SECURITE

### pourquoi
Eviter : 
- intrusion (acceder aux donnees...) ulir
- piratage (...sans laisser de trace)
- vandalisme(modifier les donnees)
- saturation volontaire(envoi de requetes trop important par une bombe logique(=programme evenementiel qui se declanche à une heure precise))

### comment
- en filtrant (parefeu, antivirus,...)
- en cryptant les donnees

### regle
la securite informatique est 80% du bon sens (eviter ingenerie social et employer des gens bien former et bien payer les salaries) et 20% de technique

### vulnerabilite
absence de politique de securite ->
ensemble d\'actions 
- physique (accès au machine)
- logique (logiciels)
- organisationnel (sensibilisation)

### attentes
//disponibilite
probalite calculant une ratioentre le temps d\'utilisation et le temps total d\'un système

// confidentialité
Traduction du fait que seuls ceux qui sont autorises accedent au donnees
moyen d\'assurer la confidentialite :
- droit d\'acces (login)
- droit d\'utilisation

//integrite
technique permettant de retrouver ses donnees telles qu'elles au moment de leur sauvegarde ou emissions
Emetteur : calcul de CRC, FCS, bit de parite
Recepteur : calculde CRC, FCS, ou bit de parite comparaison avec celui de l\'emetteur

//preuve (authentification, non repudiation, tracabilite)*
authentification	exemple kerberos : client serveur envoi de la passphrase du serveur vers client qui reponds par la reponse (assurer que la personne 
non repudiation : impossibilite de nier avoir recu un message
tracabilite : utilisation de journaux log

## HACKER

### les différent type

Acces sans autorisation à un système. Plusieurs types de hackers :
- black hats : but personnel ou financier au détriment du proprietaire du systeme vise.
- whte hats : but est d'aider a la sécurisation des systèmes ou de montrer ses compétences à l'entreprise viser en vue d'un recrutement
- grey hats : ne revelent pas leur retrouvaille au entreprise
- script kiddies : testeur de script mis à disposition sur internet
- hacker universitaire : opensource du hack. Echange leur procédé de hack

!!! veille technologique : securite matériel/equipement, système,logiciel

### Outils
- collecte d'information
- architecture réseau :
 + SNA de IBM
 + X25 liaison spécialisé
 + TCP/IP
 + SPX/IPX de Novell
- Système d'exploitation permet de mieux cibler l'attaque
- les ports logiques ouvet
- recherche sur internet

### SNMP MIB
Simple Network Management Protocol /  Management Information Base

#### client SNMP
agent snmp créé un MIB
l'envoit périodiquement au serveur de monitoring

On peut intercepter les données SNMP avec snmpwalk

#### serveur de monitoring
MIB manager stocker l'ensemble des MIB des clients
Un logiciel de moniroting (EON) envoit des mails pour monitorer

## MATERIEL RESEAU

### Bandes passantes
Ensemble des fréquences autorisées sur un media

ex: oreille humaine 20Hz à 20000Hz

### WIFI EHTERNET
WIFI 11 canaux
RJ45 27 canaux
Mode connecté : Réservation d'une crcuit (suite de canaux) entre les deux communiquants

#### Reseaux sans fils

Les catégories de réseaux sans fils 
- WPAN : Wireless Personnal Area Network
  -  Bluetooth :	- 802.15.1
- 1 Mbit/s
- quelques mètres
  -  HomeRF : - 100m
- 10 MBits/s
  - ZigBee :	- 802.15.4
- quelques mètres 
- consommation minimale
  - InfraRouge

- WLAN Wireless Local Area Network
  - WiFi : 
    - 802.11a
    54Mbits/s
    bande passante : 5Ghz
    - 802.11b
    11Mbits/s 
    bande passante : 2.402Ghz à 2.487Ghz	
    utilisation des canaux 1,6,11
    - 802.11g
    54Mbits/s 
    bande passante : 2.402Ghz à 2.487Ghz	
    utilisation des canaux 13 premiers canaux
    - 802.c (borne wifi centrale entre deux borne wifi de deux réseaux)
    permet de faire un pont (création d'un pont de niveau liaison) pour faire du filtrage d'adresse MAC d'un coté de l'autre du pont
  - HiperLan2.0
  Hight Performance Lan 2.0 (européen)
  - DECT (Digital Enhanced Cordless Telecommunication)
     - nome de téléphone militaire
     - Dans sa version mitilaire : supporte la poussiere eau choc
- WWAN Wireless Wide Area Network
 - GSM
 - GPRS 
 - UMTS
 - WIMAX

#### roaming

Passer d'un point d'accès à un autre sans me déconnecté/connecté

#### WIFI mode connection physique

BSS Basic Service Set: 1 point d'accés et les équipements clients (BSSIS)

### PAREFEU

#### Fontionnement
filtre en entrée ou en sortie des trames

type de filtrage :
- adresse IP
- ports
- procotoles
- OS
- geographique ou en entreprises
- application

#### Table de filtrage

|N°Regle	|IP source	|IP destination	|Port source	|Port Dest	|Protocole	|Action		|
|---------------|---------------|---------------|---------------|---------------|---------------|---------------|
|1		|		|		|		|		|		|		|
|2		|		|		|		|		|		|		|
|3		|		|		|		|		|		|		|
|*		|*		|*		|*		|*		|*		|*		|

## IPV4 IPV6 MAC

### MAC

48bits : 
- 2bits = Unicast 0 / Multicast 1
- 22bits = numero du constructeur
- 24bits = numéro dans la série


### IPV4

#### Information

IPv4 32bits (4miliards)

### IPv6

#### Besoin
- manque d'adresses IPv4
- Identification mais non localisation
	problème IPv4 = elle localise et identifie
	en IPv6 : identifie uniquement
#### Information

IPv6 128bits
Exemple : FF00:234B:7BCD:AFE7:DDDD:0000:0000:B2C3 = FF00:234B:7BCD:AFE7:DDDD:::B2C3

Les types d'adresses IPv6
- Unicast (est associé à une seule interface)
  - unicast global = universelle donc routable
  - unicast local = non routable
- Multicast (diffusion sur plusieurs noeud/réseau)
- anycast ( ensemble d'interface dans plusieurs noeud)
- --Il n'y a plus de type Broadcast (diffusion sur un noeud/reseau)--

#### prefixe

2000:BBBB:CCCC:DDDD:EEEE:FFFF:EEEE:FFFF = unicast global
0400:BBBB:CCCC:DDDD:EEEE:FFFF:EEEE:FFFF = unicast local
FC00:BBBB:CCCC:DDDD:EEEE:FFFF:EEEE:FFFF = IPX
FF00:BBBB:CCCC:DDDD:EEEE:FFFF:EEEE:FFFF = Multicast
0000::DDDD:EEEE:FFFF:EEEE:FFFF = loop back

Structure d'une adresse IPv6 Unicast global

|préfixe 48bits	|sousreseaux 16bits	|InterfacesID 64bits	|
|---------------|-----------------------|-----------------------|
|type de cast	|			|les 24premiers bits de l'adresse MAC + 16bits + 24 autres bits MAC |

#### ENTETE DE TRAME IPv6

|version 4|classe 8|identification de flux (numéro de circuit pour identifier la trame parmis d'autre et permet de réduire le temps de latence sur les routeurs) 20|longueurs des données 16|entete suivante 8|Nombre de sauts 8|adresse source 128|adresse destination 128|extention (routage ext. 43, auth ext.51) |

### COUCHE OSI

1. couche physique :
  - converti les bits en signaux (analogique, numérique, hertzien, wifi, Fibre Optique)
2. couche liaison :
  - gestions des adresses MAC (destinataire et reception)
  - controle des erreurs de la couche physique (CRC)(calcul du CRC de la trame placé à la fin de la trame en départ. Puis recalcul de la trame à l'arriver et comparaison avec le CRC de départ)
