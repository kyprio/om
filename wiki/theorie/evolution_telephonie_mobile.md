# EVOLUTION DE LA TELEPHONIE MOBILE

Année 70 :
- 1G au USA(FDMA dans la bande passante de 400Mhz)
Année 90 :
- 2G : GSM
- 2,5G : GPRS
Année 2000 :
- 2,75 : EDGE
Le EDGE est du GPRS l'utilisation la QAM (modulation d'amplitude, de frequence de phase)
   + Amplitude : utilisation de plusieurs amplitude
y=A.sin(w.T+ phi)
avec A= Amplitude (max(y) - min(y))
avec phi=phase
avec w=pulsation
avec T=période

Si on utilise 2^n amplitude en une période, on transmet n.bits

   + Frequence : variation du temps des périodes
f=1/T

2^n frequence permettent de multiplier par n le débit initial

   + Phase : angle de l'onde
Modulation de l'angle tangent à la courbe

ex 
Pour modulation sur 2 phases :
avec 0 = +1 -1
avec 1 = -1 +1
1010 = +1-1 -1+1 +1-1 -1+1

On peut utiliser 2^n phase qui permette de coder n.bits sur chaque période

EDGE est une combinaison des 3 modulation (QAM 16 32 64 : 16*32*64 = 32768)

Année 2010 : 
- 3G UMTS : Le réseau UMTS est basé sur les réseaux GSM et GPRS
l'UMTS permet la communication de la voix, du data, et du multimédia

Ajout des Node B entre le terminal et les RNC/MSC(gsm)/SGSN(gprs)
Node B : gère le codage du canal, l'entrelassement et l'adaptation du débit
RNC : Radio Network Controler (controleur de Nodes B, gestion de la congestion et controle d'admission et d'allocation de codes pour les nouveaux utilisateurs)
