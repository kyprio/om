# TOIP / VOIP

## Téléphonie Analogique

PABX Private Automatic Branch eXchange = Commutateur de circuit :
- Serveur de messagerie vocale
- Serveur de taxation (gestion du temps de communication par utilisateur)
- RTC Réseau Téléphonique Commuté
- Serveur de programmation du PABX
- connexion vers téléphoe analogique en RJ11

utilité / avantage :
- association d'une ligne  un client
- activation d'une numéro de tél à un client
- ajout/modif/suppression d'un client

inconvévient : 
- pas de controle d'accès
- Usurpartionde boites vocales
- transfert de profil
- ecoute
- etc...

## VOIX SUR IP

Téléphone -> PABX -> Passerelle -> WAN ...

Passerelle : routeur avec des fonctions de traduction (protocole, application, etc...) utilisant la couche 4, 5, 6, 7
Elle traduit le signal analogique en paquets et inversement

## Téléphonie sur IP

Téléphone numérique -> switch IP standard -> routeur -> WAN ...

Le téléphone numérique contient possède un composant logiciel/matériel qui joue le rôle de passerelle voix sur IP

utilité / avantage : 
- protocole : on passe du mode circuit au mode paquet
- auth
- gestion des priorités
- par l'intermédiaire du protocole 802.1P
- le réseaux passe du TDM (time division Multiplexage) au LAN/WAN
- on passe du PABX propriétaire au serveur informatique standardisé
- cryptage de conversation

H323 : ensemble de protocole permettant de mettre en oeuvre la TOIP 
SIP : Session Initiation Protocol (Concurrent du H323) (

Protocole utilisés :
- codec audio
- video
- protocole de signalisation
- protocole pour le FAX
- protocol multimedia

