#  CACHIER DES CHARGES TYPE

##    1 Présentation générale du problème
###      1.1 Objet
###      1.2 Contexte du projet
####        1.2.1 Situer le projet dans un programme plus vaste (études en cours, ...)
####        1.2.2 Affecter les responsabilités
###      1.3 Expression du besoin
###      1.4 Environnement de la solution (produit et service)
###      1.5 Conditions de fonctionnement

##    2 Expression fonctionnelle des besoins
###      2.1 Fonctions de service principales
###      2.2 Fonctions complémentaires
###      2.3 contraintes

##    3 Critères d’appréciation

##    4 Cadre de réponse
