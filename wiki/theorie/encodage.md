# ENCODAGE ET INTERCLASSEMENT

## LES DIFFERENTS TYPES D'ENCODAGE ET JEU DE CARACTERES

| nom table	| nb de bits	| nb caractères	| informations					|
|---------------|---------------|---------------|-----------------------------------------------|
| ASCII		| 7		| 128		| 26 lettre min MAJ, 0 à 9, 33 controle, 95 autres |
| ISO-8859-1	| 8		| 256		| latin1 langue europe occidentale 		|
| ISO-8859-7	| 8		| 256		| lettres grecques				|
| ISO-8859-11	| 8		| 256		| thai						|
| ISO-8859-15	| 8		| 256 		| révision moderne du ISO-8859-1 latin1 (dont €)|
| UTF-8		| 16		| 65536		| codé sur 1 ou 2 octet	selon caractère		|

## RAPPEL

1 bit = 2^1
