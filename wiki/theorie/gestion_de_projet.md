---
title: Gestion de projet
taxonomy:
    category: docs
---

#  GESTION DE PROJET

##    DEFINIR LES RISQUES ET LE PERIMETRE

Concernant le futur **système information** :
du point de vue du Maitre d'Ouvrage (client) et du Maitre d'Oeuvre (prestataire)

- Risques de conception
- Risques de réalisation
- Risques de Management
- Risques de mise en place et lancement

###      Exemple de risque pour un Maitre d'Ouvrage


**Les risques de conceptions**  
*Base de données clients* :  
- les informations clients contenues dans les contrats signés sont-elles complètes et conformes au schéma proposé par la société informatique (exemple : utilise-t-on les mêmes abréviations : M., Mme, Melle, av, bd,…) ?  On peut citer d’autres exemples
- les problèmes deviennent cruciaux lorsque plusieurs bases de données clients existent dans la société et qu’elles doivent être rapprochées. 
*Modifications des éléments de facturation en cours de projet* Il y a toujours des modifications en cours de projet, leur importance et leur fréquence pourront mettre en cause la qualité du produit final et retarder le lancement du projet. Nous pouvons imaginer quelques modifications réclamées par la direction commerciale
- l’instauration tardive d’un tarif dégressif en fonction du nombre de prestations et de     nouveaux films requis par l’abonné.
- le prélèvement automatique des clients ou le prépaiement par carte bancaire.
- une modification réglementaire vient d’être publiée par le CSA
Il peut s’agir ici de nouvelles fonctionnalités absolument indispensables, mais elles correspondent à une modification du cahier des charges. La société informatique demandera un délai supplémentaire et un avenant au contrat, pour prendre en compte ces évolutions.

**Les risques de réalisation**  
La connexion de deux ordinateurs d’origines différentes et de finalité spécifique est susceptible d'occasionner des difficultés comme une incompatibilité des systèmes d’exploitation du matériel du filtrage et du système de facturation. 
Période de tests plus longue que prévue

**Risques de Management**  
Rotation du chef de projet de la société informatique
Sous-estimation de l’assistance à consacrer au sous traitant
Suivi de l’avancement

**Risques de mise en place et de lancement**  
Arrivée du matériel de filtrage et installation :
- la livraison est incomplète
- la commande était incomplète : il manque des références
- l’interface ne fonctionne pas après le contrôle sur place
- l’enchaînement des tâches entre les modules ne fonctionne pas à l’optimum.



###      OMD

- Objectif
- Moyens
- Delais

## SPECIFICITE D'UN PROJET INFORMATIQUE

- immaterielle
- nouveauté du secteur
- évolution des techniques
- reproductibilité de l'ouvrage (très facile grace au cloud)
- informatique stratégique couteuse (sécurisation du service)
- difficulté tout au long du processus
- 60% d'échec pour erreur de managements

