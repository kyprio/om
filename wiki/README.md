# WIKI om/wiki

Ensemble de fichier de type dico ou wiki écrit en markdown traitant de système d'information.

## Usage

L'utilisation du markdown permet d'avoir des fichiers organiser clairement et lisible depuis un editeur de texte basique ou un lecture de markdown.
Il est conseillé d'ouvrir ces fichiers avec vim/emacs et d'activer la coloration syntaxique.

## Contributing

1. Fork it!
2. Create your feature branch: `git checkout -b my-new-feature`
3. Commit your changes: `git commit -am 'Add some feature'`
4. Push to the branch: `git push origin my-new-feature`
5. Submit a pull request :D

## Credits

Kyprio CenKeneno

## License

GPLv3
