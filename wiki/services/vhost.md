

 par {{lastupdated_by}}

#  Vhost

* Problématiques traitées :
- création des dossier vhosts de test
- utilisateur et permission des dossiers vhosts

* 00-default.conf :
- dessert  */data/apache/adminutils* 
- utilise l'utilisateur unique adminustils sans login.

##    Permissions et Utilisateurs

###      Cas de Apache + PHP-FPM

*1x vhost 1x user 1x groupe-run et apache appartient au groupe-run*

* ajout *user* et *user-run*
( Exceptionnelement pour le 00-default : 1 seul user adminutils:adminustils /sbin/nologin)
```
adduser user                      # user propriétaire
adduser -s /sbin/nologin user-run # groupe-run propriétaire
```

Permissions min : rw- r-- --- user:user-run (fichier)
Permissions max : rwx rws --- user:user-run (dossier et fichier cache)

* Droit max 
```
chmod o-rwx -R test_librescargot_fr
```

* Changement de propriétaire (exemple avec let)
```
cd /data/apache
chown let:let-run test_librescargot_fr -R
```

* Affectation des permissions + cas par cas
```
chmod g+s test_librescargot_fr -R
```

* Ajout user apache au groupe user-run
```
usermod -aG user-run apache
```

Puis mettre en place le script et cron : "**Réaffectation de droit**"

###      Cas de apache + mod-PHP

*1x vhost appartient à user:apache/www-data*

Droit minimum : rw- rws ---
Droit maximum : rwx rws ---

* Droit max 
```
chmod o-rwx -R test_librescargot_fr
```

* Changement de propriétaire (exemple avec let)
```
cd /data/apache
chown let:apache test_librescargot_fr -R
chmod g+w test_librescargot_fr -R
chmod g+s test_librescargot_fr -R
```

##    Scripts cron

L'ensemble des scripts lié au Vhost sont disponible dans le git 
```
git clone ssh://git.librescargot.dom/data/git/utils/vhost
```

###      Mise en place d'un cron

###      Réaffectation de droit

script pour réaffecter les droits R-- et RW- au vhost (à adapter)

```
touch /home/user/make_vhost_secure.sh
```

pour le rendre executable en tant que root et non modifiable :
```
chown root:user make_vhost_secure.sh
chmod 4750 make_vhost_secure.sh
```

####        Script pour application Symphony

```
#!/bin/bash
#
# PREPARATION DU SCRIPT
# chown root:user make_karumbureau_secure.sh
# chmod 4750 make_karumbureau_secure.sh
# 

# VARIABLES
listrw=(\
'/data/apache/karumbureau_recettes_librescargot_vpc/var/cache' \
'/data/apache/karumbureau_recettes_librescargot_vpc/var/logs' \
)
vhost='/data/apache/karumbureau_recettes_librescargot_vpc'

# EXECUTION
chown karum:karum-run $vhost -R
chmod u+rw $vhost -R
chmod g-w $vhost -R
chmod o-rwx $vhost -R
find $vhost -type f -exec chmod ugo-x {} \;
find $vhost -type d -exec chmod g+xs {} \;

for (( i=0; i<=$((${#listrw[@]} - 1)); i++ )); do
  echo "rw : ${listrw[$i]}"
  mkdir -p "${listrw[$i]}"
  chmod g+w -R "${listrw[$i]}"
done
```

Pour que l'utilisateur du vhost puisse l'executer
```
visudo -f /etc/sudoers.d/user

Cmnd_Alias        CMDS = /home/user/make_vhost_secure.sh
user ALL=NOPASSWD: CMDS
```

Pour exécuter le script depuis le user (à éxécuter après toute modification)

```
sudo /home/user/make_vhost_secure.sh
```

####        Script pour application Drupal 7

```
#!/bin/bash

# Help menu
print_help() {
cat <<-HELP
This script is used to fix permissions of a Drupal installation
you need to provide the following arguments:

  1) Path to your Drupal installation.
  2) Username of the user that you want to give files/directories ownership.
  3) HTTPD group name (defaults to www-data for Apache).

Usage: (sudo) bash ${0##*/} --drupal_path=PATH --drupal_user=USER --httpd_group=GROUP
Example: (sudo) bash ${0##*/} --drupal_path=/usr/local/apache2/htdocs --drupal_user=john --httpd_group=www-data
HELP
exit 0
}

if [ $(id -u) != 0 ]; then
  printf "**************************************\n"
  printf "* Error: You must run this with sudo or root*\n"
  printf "**************************************\n"
  print_help
  exit 1
fi

drupal_path=${1%/}
drupal_user=${2}
httpd_group="${3:-www-data}"

# Parse Command Line Arguments
while [ "$#" -gt 0 ]; do
  case "$1" in
    --drupal_path=*)
        drupal_path="${1#*=}"
        ;;
    --drupal_user=*)
        drupal_user="${1#*=}"
        ;;
    --httpd_group=*)
        httpd_group="${1#*=}"
        ;;
    --help) print_help;;
    *)
      printf "***********************************************************\n"
      printf "* Error: Invalid argument, run --help for valid arguments. *\n"
      printf "***********************************************************\n"
      exit 1
  esac
  shift
done

if [ -z "${drupal_path}" ] || [ ! -d "${drupal_path}/sites" ] || [ ! -f "${drupal_path}/core/modules/system/system.module" ] && [ ! -f "${drupal_path}/modules/system/system.module" ]; then
  printf "*********************************************\n"
  printf "* Error: Please provide a valid Drupal path. *\n"
  printf "*********************************************\n"
  print_help
  exit 1
fi

if [ -z "${drupal_user}" ] || [[ $(id -un "${drupal_user}" 2> /dev/null) != "${drupal_user}" ]]; then
  printf "*************************************\n"
  printf "* Error: Please provide a valid user. *\n"
  printf "*************************************\n"
  print_help
  exit 1
fi

cd $drupal_path
printf "Changing ownership of all contents of "${drupal_path}":\n user => "${drupal_user}" \t group => "${httpd_group}"\n"
chown -R ${drupal_user}:${httpd_group} .

printf "Changing permissions of all directories inside "${drupal_path}" to "rwxr-x---"...\n"
find . -type d -exec chmod u=rwx,g=rx,o= '{}' \;

printf "Changing permissions of all files inside "${drupal_path}" to "rw-r-----"...\n"
find . -type f -exec chmod u=rw,g=r,o= '{}' \;

printf "Changing permissions of "files" directories in "${drupal_path}/sites" to "rwxrwx---"...\n"
cd sites
find . -type d -name files -exec chmod ug=rwx,o= '{}' \;

printf "Changing permissions of all files inside all "files" directories in "${drupal_path}/sites" to "rw-rw----"...\n"
printf "Changing permissions of all directories inside all "files" directories in "${drupal_path}/sites" to "rwxrwx---"...\n"
for x in ./*/files; do
  find ${x} -type d -exec chmod ug=rwx,o= '{}' \;
  find ${x} -type f -exec chmod ug=rw,o= '{}' \;
done
echo "Done setting proper permissions on files and directories"
```

Pour que l'utilisateur du vhost puisse l'executer
```
visudo -f /etc/sudoers.d/user

Cmnd_Alias        CMDS = /home/user/make_vhost_secure.sh --drupal_path=/data/apache/www_smartcanal_com --drupal_user=smart --httpd_group=smart-run

user ALL=NOPASSWD: CMDS
```

Pour exécuter le script depuis le user (à éxécuter après toute modification)
```
sudo make_vhost_secure.sh --drupal_path=/data/apache/www_smartcanal_com --drupal_user=smart --httpd_group=smart-run
```

##    Création du dossier vhost de test Symfony3

%{color:orange}Il s'agit uniquement de tester le minimum vital avant la mise en place d'une recette
```

Dossier de destination : */data/apache/test_librescargot_fr* url test.librescargot.fr

Téléchargement du site de démo pour Symfony3.3
```
cd /data/apache
wget https://github.com/symfony/demo/archive/v1.0.5.tar.gz
mv  demo-1.0.5 test_librescargot_fr
cd test_librescargot_fr
```

création des utilisateurs et Affectation des permissions
cf Permissions et Utilisateurs

Installation des dépendances avec composer (composer requis cf wiki Symfony)
```
scl enable rh-php70 bash # activation du php70 version SCL
php bin/symfony_requirements # SI OK alors passer à la suite
composer install  --no-interaction
```

Lancement d'un test avec mini serveur web php
```
php bin/console server:run
# CRTL+C pour fermer
```

test en localhost depuis un autre terminal
```
lynx http://127.0.0.1:8000
# :q pour quitter
```
