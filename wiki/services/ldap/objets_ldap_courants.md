# OBJETS LDAP COURANTS


## STRUCTURE DU DIRECTORY INFORMATION TREE

### DomainComponent

```
dn: dc=edu,dc=librescargot,dc=fr
objectClass: top
objectClass: dcObject
objectClass: organization
o: Edu Librescargot
dc: edu
```

### OrganizationalUnit

#### groups

```
dn: ou=groups,dc=edu,dc=librescargot,dc=fr
objectClass: organizationalUnit
ou: groups
```

#### users

```
dn: ou=users,dc=edu,dc=librescargot,dc=fr
objectClass: organizationalUnit
ou: users
```

#### locations

```
dn: ou=locations,dc=edu,dc=librescargot,dc=fr
objectClass: organizationalUnit
ou: locations
```

## OBJETS DE L'OU USERS

objet permettant de définir un utilisateur posix avec quelques informations pratiques de classement administratif.

```
dn: uid=login,ou=users,dc=edu,dc=librescargot,dc=fr
objectClass: inetOrgPerson # class STRUCTURAL
objectClass: posixAccount # class AUXILIARY
objectClass: shadowAccount # class AUXILIARY
commonName: Prenom Nom
surname: nom
givenName: prenom
uid: login
uidNumber: XXXX
gidNumber: XXX # cf gidNumber objet GROUP
homeDirectory: /srv/share/homes/login
userPassword: XXXXXXXX
loginShell : /bin/bash
mail: email@mail.com
```
