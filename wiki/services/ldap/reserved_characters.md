# RESERVED CHARACTERS

```
  space or # character at the beginning of a string
  space character at the end of a string
, comma 
+ plus sign
" double quote
\ backslash
< left angle bracket
> right angle bracket
; semicolon
LF line feed
CR carriage return
= equals sign
```
