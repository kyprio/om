# SOURCES 

- https://linuxfr.org/users/peb--2/journaux/ldap-un-peu-de-technique#toc_0 explication technique de ldap
- https://ldapwiki.com/wiki/GroupOfUniqueNames%20vs%20groupOfNames Pourquoi utiliser GroupOfUniqueNames
- https://ldapwiki.com/wiki/Groups%20Are%20Bad Pourquoi ne pas utiliser de Groups
- http://jean-luc.massat.perso.luminy.univ-amu.fr/ens/jee/ldap.html utilisation basique (trop) de ldap mais explication interressant sur les opérateurs de comparaison de champs
- http://www.ldapman.org/articles/tree_design.html exemple cohérent de structure de DIT ldap
- http://ldapbook.labs.libre-entreprise.org/book/html/ch08s02.html explication schema
