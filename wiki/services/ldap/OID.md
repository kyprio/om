#  OID

##    OID des syntaxes

Boolean                         1.3.6.1.4.1.1466.115.121.1.7 (TRUE or FALSE)
DN                              1.3.6.1.4.1.1466.115.121.1.12
Directory String(str UTF-8)     1.3.6.1.4.1.1466.115.121.1.15 (ne pas mettre d'accent sinon la valeur est encodé)
IA5String (str ASCII)           1.3.6.1.4.1.1466.115.121.1.26
Integer                         1.3.6.1.4.1.1466.115.121.1.27

###      Liste complète

cf RFC2256

ACI Item                        1.3.6.1.4.1.1466.115.121.1.1
Access Point                    1.3.6.1.4.1.1466.115.121.1.2
Attribute Type Description      1.3.6.1.4.1.1466.115.121.1.3
Audio                           1.3.6.1.4.1.1466.115.121.1.4
Binary                          1.3.6.1.4.1.1466.115.121.1.5
Bit String                      1.3.6.1.4.1.1466.115.121.1.6
Boolean                         1.3.6.1.4.1.1466.115.121.1.7
Certificate                     1.3.6.1.4.1.1466.115.121.1.8
Certificate List                1.3.6.1.4.1.1466.115.121.1.9
Certificate Pair                1.3.6.1.4.1.1466.115.121.1.10
Country String                  1.3.6.1.4.1.1466.115.121.1.11
DN                              1.3.6.1.4.1.1466.115.121.1.12
Data Quality Syntax             1.3.6.1.4.1.1466.115.121.1.13
Delivery Method                 1.3.6.1.4.1.1466.115.121.1.14
Directory String                1.3.6.1.4.1.1466.115.121.1.15
DIT Content Rule Description    1.3.6.1.4.1.1466.115.121.1.16
DIT Structure Rule Description  1.3.6.1.4.1.1466.115.121.1.17
DL Submit Permission            1.3.6.1.4.1.1466.115.121.1.18
DSA Quality Syntax              1.3.6.1.4.1.1466.115.121.1.19
DSE Type                        1.3.6.1.4.1.1466.115.121.1.20
Enhanced Guide                  1.3.6.1.4.1.1466.115.121.1.21
Facsimile Telephone Number      1.3.6.1.4.1.1466.115.121.1.22
Fax                             1.3.6.1.4.1.1466.115.121.1.23
Generalized Time                1.3.6.1.4.1.1466.115.121.1.24
Guide                           1.3.6.1.4.1.1466.115.121.1.25
IA5 String                      1.3.6.1.4.1.1466.115.121.1.26
INTEGER                         1.3.6.1.4.1.1466.115.121.1.27
JPEG                            1.3.6.1.4.1.1466.115.121.1.28
LDAP Syntax Description         1.3.6.1.4.1.1466.115.121.1.54
LDAP Schema Definition          1.3.6.1.4.1.1466.115.121.1.56
LDAP Schema Description         1.3.6.1.4.1.1466.115.121.1.57
Master And Shadow Access Points 1.3.6.1.4.1.1466.115.121.1.29
Matching Rule Description       1.3.6.1.4.1.1466.115.121.1.30
Matching Rule Use Description   1.3.6.1.4.1.1466.115.121.1.31
Mail Preference                 1.3.6.1.4.1.1466.115.121.1.32
MHS OR Address                  1.3.6.1.4.1.1466.115.121.1.33
Modify Rights                   1.3.6.1.4.1.1466.115.121.1.55
Name And Optional UID           1.3.6.1.4.1.1466.115.121.1.34
Name Form Description           1.3.6.1.4.1.1466.115.121.1.35
Numeric String                  1.3.6.1.4.1.1466.115.121.1.36
Object Class Description        1.3.6.1.4.1.1466.115.121.1.37
Octet String                    1.3.6.1.4.1.1466.115.121.1.40
OID                             1.3.6.1.4.1.1466.115.121.1.38
Other Mailbox                   1.3.6.1.4.1.1466.115.121.1.39
Postal Address                  1.3.6.1.4.1.1466.115.121.1.41
Protocol Information            1.3.6.1.4.1.1466.115.121.1.42
Presentation Address            1.3.6.1.4.1.1466.115.121.1.43
Printable String                1.3.6.1.4.1.1466.115.121.1.44
Substring Assertion             1.3.6.1.4.1.1466.115.121.1.58
Subtree Specification           1.3.6.1.4.1.1466.115.121.1.45
Supplier Information            1.3.6.1.4.1.1466.115.121.1.46
Supplier Or Consumer            1.3.6.1.4.1.1466.115.121.1.47
Supplier And Consumer           1.3.6.1.4.1.1466.115.121.1.48
Supported Algorithm             1.3.6.1.4.1.1466.115.121.1.49
Telephone Number                1.3.6.1.4.1.1466.115.121.1.50
Teletex Terminal Identifier     1.3.6.1.4.1.1466.115.121.1.51
Telex Number                    1.3.6.1.4.1.1466.115.121.1.52
UTC Time                        1.3.6.1.4.1.1466.115.121.1.53

##    OID des opérateurs de comparaison

objectIdentifierMatch [ OID1 == OID2 ]    2.5.13.0
distinguishedNameMatch [ DN1 == DN2 ]     2.5.13.1
caseIgnoreMatch [ string == string ]      2.5.13.2
caseIgnoreOrderingMatch [ string1 < string2 ]                 2.5.13.3
caseIgnoreSubstringsMatch  [ *string1* ~= string2 ]           2.5.13.4
numericStringMatch cf caseIgnoreMatch en ignorant les espaces 2.5.13.8
numericStringSubstringsMatch cf caseIgnoreSubstringMatch en ignorant les espaces 2.5.13.10
uniqueMemberMatch [DN1 == DN2 ] or [ uid1 == uid2 ]           2.5.13.23

###      Liste complètes

cf RFC2252

## Génération d'un OID

### OID officiel 1.3.6.1.4.xxx

les OID sont gratuits et distribués par le IANA pour chaque organisation désirant créer ses propres schemas. Il faut remplir un formulaire et demandé son enregistrement (à vie).

### OID test 2.25.xxx

Il est possible sinon de généré soi-même un OID dans une branche d'OID LDAP (2.25....) spécialement conçu pour le test :
https://www.uuidgenerator.net/
puis convertion du UUID en OID
https://www.igorkromin.net/?uuid=1

exemple :  2.25.142747436635781531863528556598281238984
