#  SCHEMA ET ATTRIBUTS LDAP

##    DEFINITION

- Un **attribut** est une variable typé qui contient une valeur permettant définir une propriété de l'objet.
- Les **objectClasses** correspondent à des classes d'objet (voire une table SQL). Ils correspondent à une liste d'attributs. Ils contiennent des attributs à définir obligatoirement et des attributs dont la valeur peut être null.
- Un **schema** contient la définition d'un ensemble d'attributs et la définition de plusieurs objectClasses utilisant une sélection des attributs du schema.

Ex : le schema core.schema définit :
- les attributs : sn, gn, member, ...
- les objectClasses person(sn,cn,userPassword,telephoneNumber,seeAlso,description), device(cn,serialNumber,seeAlso,owner), ...

3 types de objectClass : 
- une classe **STRUCTURAL** (person, organizationalUnit,...) permet d'instancié/structuré les objets. Il ne peut y en avoir qu'une seul par objet. Il peut y avoir des héritages d'une classe STRUCTURAL à une autre.
- une classe **ABSTRACT** (top) permet d'être héritée. Toutes les classes LDAP hérite un moment donné de top. Elle contient comme unique attribut objectClass.
- une classe **AUXILIARY** (posixAccount) permet de complété la structure d'un objet. L'objet doit déjà être issue d'une classe STRUCTURAL.

2 types d'attributs : (peu utile)
- par defaut un attribut appartient à un objet d'une classe et sa valeur n'est pas partagée avec un autre objet
- un attribut peut être **COLLECTIVE** (si le schema collective est implémanté) et peut alors être accessible depuis plusieurs objets. (fonctionnalité méconnue)

Un objet s'instancie donc à partir d'une objectClass STRUCTURAL (qui hérite elle-même d'une classe STRUCTURAL ou ABSTRACT ) et peut se complété des attributs définit dans des classes AUXILIARY.

##    Liste des objectClasses courants et attributs par schema

/etc/ldap/schema/...ldif

###      schema core

Schema traitant :
- les domaines
- les organisations
- les personnes
- les groupes

####        dcObject

héritage : top
type : AUXILIARY (ex: utiliser avec organization)

attribut obligatoire :
- **dc/domainComponent'**

attribut facultatif :
aucun

####        organization

héritage : top
type : STRUCTURAL

attribut obligatoire :
- **o/organizationName**

attribut facultatif (et inutile) :
- userPassword
- searchGuide
- seeAlso
- businessCategory
- x121Address
- registeredAddress
- destinationIndicator
- preferredDeliveryMethod
- telexNumber
- teletexTerminalIdentifier
- telephoneNumber
- internationaliSDNNumber
- facsimileTelephoneNumber
- street/streeAddress (nom de la rue)
- postOfficeBox (numéro de boite au lettre)
- postalCode (code postal)
- postalAddress (adresse complete)
- physicalDeliveryOfficeName
- st/stateOrProvinceName
- l/localityName (ville)
- description

####        organizationalUnit

héritage : top
type : STRUCTURAL 

attribut obligatoire :
- **ou/organizationalUnitName**

attribut facultatif (et inutile) :
cf objectclass organization

####        person

héritage : top
type : STRUCTURAL 

attribut obligatoire :
- **sn/surname** (nom)
- **cn/commonName** (nom complet d'usage ou login)

attribut facultatif :
- userPassword
- telephoneNumber
- seeAlso (DN de référence)
- description

####        organizationalPerson

héritage : person
type : STRUCTURAL

attribut obligatoire : (dont héritage)
- **sn/surname** (nom)
- **cn/commonName** (nom complet d'usage ou login)

attribut facultatif : (dont héritage)
- userPassword
- telephoneNumber
- seeAlso (DN de référence)
- description
- title
- x121Address
- registeredAddress
- destinationIndicator
- preferredDeliveryMethod
- telexNumber
- teletexTerminalIdentifier
- telephoneNumber
- internationaliSDNNumber
- facsimileTelephoneNumber
- street/streeAddress (nom de la rue)
- postOfficeBox (numéro de boite au lettre)
- postalCode (code postal)
- postalAddress (adresse complete)
- physicalDeliveryOfficeName
- ou/organizationalUnitName
- st/stateOrProvinceName
- l/localityName (ville)

###      schema nis

Schema traitant :
- network Information Service
- compte utilisateur sur GNU/linux

####        posixAccount

héritage : top
type : AUXILIARY

attribut obligatoire :
- **cn/commonName** (nom complet d'usage ou login)
- **uid/userid** (login)
- **uidNumber**
- **gidNumber**
- **homeDirectory**

attribut facultatif :
- userPassword
- loginShell
- gecos (common name, General Electric Comprehensive Operating System)
- description

####        shadowAccount

héritage : top
type : AUXILIARY

attribut obligatoire :
- **uid/userid** (login)

attribut facultatif :
- userPassword
- shadowLastChange
- shadowMin
- shadowMax
- shadowWarning
- shadowInactive
- shadowExpire
- shadowFlag
- description

####        posixGroup

héritage : top
type : STRUCTURAL

attribut obligatoire :
- **cn/commonName** (nom complet d'usage ou login)
- **gidNumber**

attribut facultatif :
- userPassword
- memberUid
- description

###      schema inetorgperson

Extension des objetsclass de Core de x500 prenant en compte des attributs supplémentaires plus dans l'air du temps.

####        inetOrgPerson

héritage : organizationalPerson
type : STRUCTURAL

attribut obligatoire : (dont héritage)
- **sn/surname** (nom)
- **cn/commonName** (nom complet d'usage ou login)

attribut facultatif : (dont héritage)
- userPassword
- telephoneNumber
- seeAlso (DN de référence)
- description
- title (métier)
- x121Address
- registeredAddress
- destinationIndicator
- preferredDeliveryMethod
- telexNumber
- teletexTerminalIdentifier
- telephoneNumber
- internationaliSDNNumber
- facsimileTelephoneNumber
- street/streeAddress (nom de la rue)
- postOfficeBox (numéro de boite au lettre)
- postalCode (code postal)
- postalAddress (adresse complete)
- physicalDeliveryOfficeName
- ou/organizationalUnitName
- st/stateOrProvinceName
- l/localityName (ville)
- audio
- businessCategory
- carLicense
- departmentNumber
- displayName (nom à afficher)
- employeeNumber
- employeeType
- givenName (prenom)
- homePhone
- homePostalAddress
- initials
- jpegPhoto
- labeledURI
- mail
- manager (DN du manager)
- mobile
- o/organization
- pager
- photo
- roomNumber
- secretary (DN du secretaire)
- uid/userid (login)
- userCertificate
- x500uniqueIdentifier
- preferredLanguage
- userSMIMECertificate
- userPKCS12
