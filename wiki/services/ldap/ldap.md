# LDAP

## Commandes

cf commands.md

## Information utile

le dn (Distinguished Name) est l'identifiant long de tout objet LDAP. Il est constitué de son apartenance au niveau structurel (et non de son appartenance aux groupes)

## SCHEMA

### Definition

Structure/table contenant des champ obligatoires et des champs facultatifs. Un schéma correspond à un champ d'application spécifique. Un objet hérite d'un ou plusieurs schémas et doit donc contenir une valeur pour au moins chaque champs obligatoire.
Il convient de faire hériter notre objet des schemas qui vont être utile à définir l'objet?

### iNetOrgPerson

#### rôle de iNetOrgPerson

Permet de définir un individu dans une organisation.

#### Champs courants de iNetOrgPerson

aucun champ obligatoire

- displayName
- givenName : Prénom
- mail
- uid : login

### posixAccount

#### rôle de posixAccount

Permet de définir un compte sur un système GNU/Linux

#### Champs courants de posixAccount

- cn : CommonName (obligatoire)
- uid : login (obligatoire)
- uidNumber : ID unique de 0 à 65535 (obligatoire)
- gidNumber  : ID du groupe (obligatoire)
- homeDirectory : chemin absolu du home (obligatoire)
- userPassword : mot de passe hashé ou non (obligatoire)

### posixGroup

#### rôle de posixGroup

Permet de gérer des groupes de comptes sous GNU/Linux.

#### Champ courants de posixGroup

- gidNumer : ID du groupe (obligatoire)
- memberuid : uidNumber du compte membre (au moins 1 obligatoire sinon plusieurs possibles)

## SOURCE

- http://www.ietf.org/rfc/rfc2798.txt : Definition of the inetOrgPerson LDAP Object Class
- http://www.zytrax.com/books/ldap/ape/#posixaccount : liste des schema, définitions, listes des champs obligatoires et facultatifs
- https://www.thegeekstuff.com/2015/02/openldap-add-users-groups : utilisation de posixgGroup
