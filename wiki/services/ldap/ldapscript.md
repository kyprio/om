# LDAPSCRIPT

```
apt install ldapscripts
```

/etc/ldapscripts/ldapscripts.conf
```
- #SERVER="ldap://localhost"
+ SERVER="ldap://localhost"

- #SUFFIX="dc=example,dc=com" # Global suffix
+ SUFFIX="dc=edu,dc=librescargot,dc=fr" # Global suffix

- #GSUFFIX="ou=Groups"        # Groups ou (just under $SUFFIX)
+ GSUFFIX="ou=groups"        # Groups ou (just under $SUFFIX)

- #USUFFIX="ou=Users"         # Users ou (just under $SUFFIX)
+ USUFFIX="ou=users"         # Users ou (just under $SUFFIX)

- BINDDN="cn=Manager,dc=example,dc=com"
+ BINDDN="cn=admin,dc=edu,dc=librescargot,dc=fr"

BINDPWDFILE="/etc/ldapscripts/ldapscripts.passwd"

- GIDSTART="10000" # Group ID
+ GIDSTART="10000" # Group ID

UIDSTART="10000" # User ID

- CREATEHOMES="no"      # Create home directories and set rights ?
+ CREATEHOMES="yes"      # Create home directories and set rights ?

- #HOMEPERMS="755"       # Default permissions for home directories
+ HOMEPERMS="750"       # Default permissions for home directories

PASSWORDGEN="pwgen"

- RECORDPASSWORDS="no"
+ RECORDPASSWORDS=yes"

- PASSWORDFILE="/var/log/ldapscripts_passwd.log"
+ PASSWORDFILE="/etc/ldapscripts/ldapscripts_passwd.log"
```

mdp dans /etc/ldapscripts/ldapscripts.passwd (sans caractère spécial de retour à ligne)

```
echo -n 'xxxxxx' > /etc/ldapscripts/ldapscripts.passwd
```

securisation du dossier
```
chmod o-rwx /etc/ldapscripts/
```
test
```
lsldap
```
