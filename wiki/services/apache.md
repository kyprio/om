

 par {{lastupdated_by}}

##    Installation HTTPD

###      Installation de HTTPD (version officielle sur rhel7)

%{color:orange}Conseil pour soucis de cohérence : installer cette version uniquement si la version PHP est celle des dépots officielles et non SCL, sinon installer HTTPD24-HTTPD (version SCL sur rhel7).
```



Ajout apache/www-data au groupe du user du dossier (cf vhost)
```
usermod -G apache user
```

```
yum install httpd
```

###      Installation de HTTPD24-HTTPD (version SCL sur rhel7)

%{color:orange}Conseil pour soucis de cohérence : utiliser cette version pour les PHP venant de SCL (notament PHP-FPM)
```

Activation des Software Collections de Centos
`yum install centos-release-scl```

Installation
```
yum install httpd24-httpd httpd24-mod_ssl
systemctl enable httpd24-httpd
```

Ajout d'un lien symbolique vers /etc/httpd24
```
ln -s /opt/rh/httpd24/root/etc/httpd/ /etc/httpd24
```

Désactivation du vhosts Welcome.conf
```
mv /opt/rh/httpd24/root/etc/httpd/conf.d/welcome.conf /opt/rh/httpd24/root/etc/httpd/conf.d/welcome.conf_bak
```

##    Administration et Configuration HTTPD (toutes versions)

(!) %{color:red}(avril 2018) L'utilisation du répertoire vhosts.d au détriment de conf.d pour y placer les vhosts n'est plus maintenue. On conserve désormais la configuration par défaut des vhosts dans conf.d (le rajout de l'IncludeOptional vhosts.d/*.conf n'est plus utile dans httpd.conf)
```

###      Service HTTPD

Démarrage/arrêt du service
```
# pour HTTPD version officielle sur rhel7
systemctl start|stop|restart|status httpd.service
# pour HTTPD24-HTTPD version SCL sur rhel7
systemctl start|stop|restart|status httpd24-httpd.service
```

###      DirectoryIndex pour PHP et PHP-FPM

Dans le cas de l'utilisation de PHP/PHP-FPM

httpd.conf :
`<code class="diff">
-    DirectoryIndex index.html
+    DirectoryIndex index.html index.php
</code>```

###      SeLinux

[[company-1:Sécurité-SeLinux#Apache-Data-Directory|Apache Data Directory]]

###      Pare-feu

```
firewall-cmd --add-service http --permanent
firewall-cmd --add-service https --permanent
firewall-cmd --reload
```

###      Configuration HTTPD pour PHP-FPM

* Changement de moteur mpm prefork vers event

%{color:orange}*prefork vs event/worker  = process fork vs thread-safe*
```

*/opt/rh/httpd24/root/etc/httpd/conf.modules.d/00-mpm.conf*
```
<code class="diff">
- LoadModule mpm_prefork_module modules/mod_mpm_prefork.so
+ LoadModule mpm_event_module modules/mod_mpm_event.so
</code>
```

###      Création des vhosts et du 00-default.conf

Dans /etc/httpd/conf.d/

Le 00-default.conf pointe vers **/data/apache/adminustils**

####         Vhost simple pour avec mod_php

*00-default.conf* :
```
<VirtualHost *:80>

  # Identity
  ServerName eiweb00.librescargot.fr
  ServerAlias www.eiweb00.librescargot.fr
  DocumentRoot /data/apache/adminutils

  # Log
  ErrorLog "/var/log/httpd/error_adminutils.log"
  CustomLog "/var/log/httpd/access_adminutils.log" combined

    # Public document root
  <Directory "/data/apache/adminutils">
      Require all granted
  </Directory>

</VirtualHost>

```

####         Vhost simple pour avec php-fpm

* *00-default.conf*
```
<VirtualHost *:80>

  # Identity
  ServerName eiweb00.librescargot.fr
  ServerAlias www.eiweb00.librescargot.fr
  DocumentRoot /data/apache/adminutils

  # Log
  ErrorLog "/var/log/httpd/error_adminutils.log"
  CustomLog "/var/log/httpd/access_adminutils.log" combined

  # php individuel
  <FilesMatch ".+\.php$">
      SetHandler "proxy:unix:/var/run/php7.0-fpm-adminutils.sock|fcgi://localhost"
  </FilesMatch>

  # Public document root
  <Directory "/data/apache/adminutils">
      Require all granted
  </Directory>

</VirtualHost>
```

###      Analyse et gestion des logs HTTPD

*[[LOG_HTTPD]]*

###      SSL

* Si installation de certificat Letsencrypt : 
[[company-1:Sécurité-Letsencrypt|Let's encrypt]]

%{color:orange}Exemple complet de vhost https en fin de section SSL
```

* Redirection du 80 HTTP vers 443 HTTPS  *vhosts.d/00-default.conf*

`<code class="diff">
<VirtualHost *:80>
(...)
+        # Redirection 80 HTTP vers 443 HTTPS
+        RewriteEngine   On
+        RewriteCond     %{SERVER_PORT} ^80$
+        RewriteRule     ^/(.*) https://%{SERVER_NAME}/$1 [L,R=301]
</VirtualHost>
</code>```
 
* Activation du 443 HTTPS et transfert de la config du 80 vers 443 *vhosts.d/00-default.conf*

`<code class="diff">
<VirtualHost *:80>
        ServerName      web04.librescargot.fr

        CustomLog       logs/access_adminutils_librescargot_fr_ssl_log    combined
        ErrorLog        logs/error_adminutils_librescargot_fr_ssl_log

-        DocumentRoot    /data/apache/adminutils

-        <Directory /data/apache/adminutils>
-                AllowOverride All
-                Options Indexes FollowSymLinks
-                 Order allow,deny
-                 Allow from all
-        </Directory>

        # Redirection 80 HTTP vers 443 HTTPS
        RewriteEngine   On
        RewriteCond     %{SERVER_PORT} ^80$
        RewriteRule     ^/(.*) https://%{SERVER_NAME}/$1 [L,R=301]
</VirtualHost>
+ <VirtualHost *:443>
+        ServerName      web04.librescargot.fr

+        CustomLog       logs/access_adminutils_librescargot_fr_ssl_log    combined
+        ErrorLog        logs/error_adminutils_librescargot_fr_ssl_log

+        DocumentRoot    /data/apache/adminutils

+        <Directory /data/apache/adminutils>
+                AllowOverride All
+                Options Indexes FollowSymLinks
+                 Order allow,deny
+                 Allow from all
+        </Directory>

</VirtualHost>
</code>```

* Si PHP-FPM 

Transfert de l'appel php-fpm vers vhost 443
`<code class="diff">
<VirtualHost *:80>
(...)
-  # php individuel
-  <FilesMatch ".+\.php$">
-      SetHandler "proxy:unix:/var/run/php7.0-fpm-adminutils.sock|fcgi://localhost"
-  </FilesMatch>
(...)
</VirtualHost>
<VirtualHost *:443>
(...)
+ # php individuel
+  <FilesMatch ".+\.php$">
+      SetHandler "proxy:unix:/var/run/php7.0-fpm-adminutils.sock|fcgi://localhost"
+  </FilesMatch>
</VirtualHost>
</code>```

conf.d/ssl.conf

`<code class="diff">
#
# When we also provide SSL we have to listen to the 
# the HTTPS port in addition.
#
Listen 443 https
SSLPassPhraseDialog builtin
SSLSessionCache         shmcb:/opt/rh/httpd24/root/var/run/httpd/sslcache(512000)
SSLSessionCacheTimeout  300
SSLRandomSeed startup file:/dev/urandom  256
SSLRandomSeed connect builtin
SSLCryptoDevice builtin

##
## SSL Virtual Host Context
##

- <VirtualHost _default_:443>
- ErrorLog logs/ssl_error_log
- TransferLog logs/ssl_access_log
- LogLevel warn
- SSLEngine on
- SSLProtocol all -SSLv2
- SSLCipherSuite HIGH:MEDIUM:!aNULL:!MD5
- SSLCertificateFile /etc/pki/tls/certs/localhost.crt
- SSLCertificateKeyFile /etc/pki/tls/private/localhost.key
- <Files ~ "\.(cgi|shtml|phtml|php3?)$">
-     SSLOptions +StdEnvVars
- </Files>
- <Directory "/var/www/cgi-bin">
-     SSLOptions +StdEnvVars
- </Directory>
- BrowserMatch "MSIE [2-5]" \
-          nokeepalive ssl-unclean-shutdown \
-          downgrade-1.0 force-response-1.0
- CustomLog logs/ssl_request_log \
-           "%t %h %{SSL_PROTOCOL}x %{SSL_CIPHER}x \"%r\" %b"
- </VirtualHost>                        
</code>```

* vhosts.d/XX_ssl.inc

`<code class="diff">
##
## Split form ssl.conf
##

+ LogLevel warn
+ SSLEngine on
+ SSLProtocol all -SSLv2 -SSLv3 -TLSv1 -TLSv1.1
+ SSLCipherSuite ECDHE-ECDSA-AES256-GCM-SHA384:ECDHE-RSA-AES256-GCM-SHA384:ECDHE-ECDSA-CHACHA20-POLY1305:ECDHE-RSA-CHACHA20-POLY1305:ECDHE-ECDSA-AES128-GCM-SHA256:ECDHE-RSA-AES128-GCM-SHA256:ECDHE-ECDSA-AES256-SHA384:ECDHE-RSA-AES256-SHA384:ECDHE-ECDSA-AES128-SHA256:ECDHE-RSA-AES128-SHA256
+ SSLCertificateFile /etc/pki/letsencrypt/httpd/httpd-cert.pem
+ SSLCertificateKeyFile /etc/pki/letsencrypt/httpd/httpd-key.pem
+ SSLCertificateChainFile /etc/pki/letsencrypt/httpd/httpd-fullchain.pem
+ <Files ~ "\.(cgi|shtml|phtml|php3?)$">
+     SSLOptions +StdEnvVars
+ </Files>
+ <Directory "/var/www/cgi-bin">
+     SSLOptions +StdEnvVars
+ </Directory>
+ BrowserMatch "MSIE [2-5]" \
+          nokeepalive ssl-unclean-shutdown \
+          downgrade-1.0 force-response-1.0
</code>```

###      Restriction d’accès

####        Par certificat

#####          Dossiers 

* Dossier contenant les CA des utilisateurs 

```
mkdir <httpd_config_dir>/ssl.crt
```

#####          Certificats

Dans le dossier %{color:DarkMagenta}<httpd_config_dir>/ssl.crt% ajouter toutes les CA au format PEM dans le fichier %{color:DarkMagenta}<httpd_config_dir>/ssl.crt/ca-bundle-<vhost_name>.pem
```

Exemple pour les vmdevletXX :

* /opt/rh/httpd24/root/etc/httpd/ssl.crt/ca-bundle-wXX_stletXX_librescargot_dom.pem
```
-----BEGIN CERTIFICATE-----
XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
-----END CERTIFICATE-----
```

#####          Configuration

######            Vhost

* vhosts.d/20_stlet1_dom.conf
`<code class="diff">
<VirtualHost *:443>
...
+        Include         vhosts.d/XX_ssl.inc
+        Include         vhosts.d/XX_ssl_auth-wXX_stletXX_librescargot_dom.inc

+        Include         vhosts.d/YY_location_sfnp-web-app_dev-login.inc
</VirtualHost>
```

######            Extension auth ssl

* vhosts.d/XX_ssl_auth-wXX_stletXX_librescargot_dom.inc
```
SSLCACertificateFile ssl.crt/ca-bundle-wXX_stletXX_librescargot_dom.pem
SSLCARevocationFile ssl.crl/ca-bundle-wXX_stletXX_librescargot_dom.pem
SSLCARevocationCheck chain no_crl_for_cert_ok
SSLVerifyClient none
SSLVerifyDepth 4
```

######            Location

* vhosts.d/YY_location_sfnp-web-app_dev-login.inc
```
<Location "/sf_SFNP/web/app_dev.php/login">
    SSLOptions +StdEnvVars +ExportCertData
    SSLVerifyClient optional
    SSLVerifyDepth 4
    SSLRequireSSL

    RewriteEngine On
    RewriteCond %{SSL:SSL_CLIENT_VERIFY} !^SUCCESS
    RewriteRule .* /sf_SFNP/web/app_dev.php/login-2fa [R,L]
</Location>
```


####        Htpasswd et accès restreint par authentification apache

```
yum install httpd-tools
mkdir /etc/httpd/htpasswd
htpasswd -sc /etc/httpd/htpasswd/adminutils.htpasswd let
htpasswd -s /etc/httpd/htpasswd/adminutils.htpasswd letdev
```

* vhosts.d/00-default.conf

`<code class="diff">
<VirtualHost *:80>
        ServerName      web04.librescargot.fr

        CustomLog       logs/access_web04_librescargot_fr_log combined
        ErrorLog        logs/error_web04_librescargot_fr_log

        DocumentRoot /var/www/html

-        Include         vhosts.d/XX_certbot.inc

+        RewriteEngine   On
+        RewriteCond     %{SERVER_PORT} ^80$
+        RewriteRule     ^/(.*) https://%{SERVER_NAME}/$1 [L,R=301]
</VirtualHost>

<VirtualHost *:443>
        ServerName      web03.librescargot.fr

        CustomLog       logs/access_adminutils_librescargot_fr_ssl_log    combined
        ErrorLog        logs/error_adminutils_librescargot_fr_ssl_log

        DocumentRoot    /data/apache/adminutils

        <Directory /data/apache/adminutils>
+                AuthType Basic
+                AuthName "Restricted Area"
+                AuthBasicProvider file

+                AuthUserFile /opt/rh/httpd24/root/etc/httpd/htpasswd/adminutils.htpasswd

+                Require valid-user
                AllowOverride All
                Options Indexes FollowSymLinks
                Order allow,deny
                Allow from all
        </Directory>

        Include         vhosts.d/XX_ssl.inc
        Include         vhosts.d/XX_certbot.inc
        #Include         vhosts.d/XX_phpMyAdmin.inc
</VirtualHost>
</code>```

###      Modules Macro pour le templating de vhosts

Vérification de l'activation du module macro (par defaut activé) : 
*/etc/httpd/conf.modules.d/00-base.conf*
```
LoadModule macro_module modules/mod_macro.so
```

*Exemple d'utilisation du module mod_macro* (template de vhost) pour générer dynamiquement des configurations de vhosts.

Définition des variables et des templates :
*/opt/rh/httpd24/root/etc/httpd/vhosts.d/200-vhost_macro.conf*
```
# VHostSimple
# sans htpasswd avec php7.0-fpm
<Macro VHostSimple $host $dir>
  <VirtualHost *:80>

    # Identity
    ServerName "$host"
    ServerAlias "www.$host"
    DocumentRoot "/data/apache/$dir"
    
    # Log
    ErrorLog "/var/log/httpd24/error_$dir.log"
    CustomLog "/var/log/httpd24/access_$dir.log" combined
    
    # php individuel
    <FilesMatch ".+\.php$">
        SetHandler "proxy:unix:/var/run/php7.0-fpm-$dir.sock|fcgi://localhost"
    </FilesMatch>

    # Public document root
    <Directory "/data/apache/$dir">
        Require all granted
    </Directory>

  </VirtualHost>
</Macro>

# VHostPrivate
# avec htpasswd partout avec php7.0-fpm
<Macro VHostPrivate $host $dir $htpasswd>
  <VirtualHost *:80>
    
    # Identity
    ServerName "$host"
    DocumentRoot "/data/apache/$dir"
    
    # Log
    ErrorLog "/var/log/httpd24/error_$dir.log"
    CustomLog "/var/log/httpd24/access_$dir.log" combined

    # private limited access
    <Directory "/data/apache/$dir">
      AuthType Basic
      AuthName "Restricted Content"
      AuthUserFile "/opt/rh/httpd24/root/etc/httpd/htpasswd/$htpasswd"
      Require valid-user
    </Directory>
    
    # php individuel
    <FilesMatch ".+\.php$">
        SetHandler "proxy:unix:/var/run/php7.0-fpm-$dir.sock|fcgi://localhost"
    </FilesMatch>

  </VirtualHost>
</Macro>

#VHostComplete
# avec htpasswd sur un dossier avec php7.0-fpm
<Macro VHostComplete $host $aliases $dir $private $htpasswd>
  <VirtualHost *:80>
    
    # Identity
    ServerName "$host"
    ServerAlias "$aliases"
    DocumentRoot "/data/apache/$dir"

    # Log
    ErrorLog "/var/log/httpd24/error_$dir.log"
    CustomLog "/var/log/httpd24/access_$dir.log" combined

    # Public document root
    <Directory "/data/apache/$dir">
        Require all granted
    </Directory>

    # private limited access
    <Directory "/data/apache/$private">
      AuthType Basic
      AuthName "Restricted Content"
      AuthUserFile "/opt/rh/httpd24/root/etc/httpd/htpasswd/$htpasswd"
      Require valid-user
    </Directory>
    
    # php individuel
    <FilesMatch ".+\.php$">
        SetHandler "proxy:unix:/var/run/php7.0-fpm-$dir.sock|fcgi://localhost"
    </FilesMatch>

  </VirtualHost>
</Macro>
```

Désactivation des variables des macros : 
*/opt/rh/httpd24/root/etc/httpd/vhosts.d/299-vhostundefMacro.conf*
```
# permet de désactiver les variables du dernier macro
UndefMacro VHostSimple
UndefMacro VHostPrivate
UndefMacro VHostComplete
```

Les vhosts utilisant les macro doivent avoir un prefixe ici entre 200 et 299 :
*201-test1_librescargot_fr.conf*
```
Use VHostSimple test1.librescargot.fr test1_librescargot_fr
```

###      Conteneur et bloc conditionnel inversé

Exemple : le bloc s'applique à toutes les urls sauf ./well-known

```
        <Location ~ "^((?!/.well-known).)*$">
                AuthType Basic
                AuthName "Restricted Area"
                AuthBasicProvider file
                AuthUserFile /etc/httpd/conf/htpasswd/vpcsmart02.htpasswd
                Require valid-user
        </Location>
```
