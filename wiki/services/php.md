#  PHP

Installation et configuration de l'interpréteur PHP.
- mode normal (utilisé par apache mod-php)
- mode standalone (FPM)

La configuration d'HTTPD est différente entre php et php-fpm

##    PHP (mode normal)

###      PHP5.4/PHP5.5/ (version officielle sur rhel7)

PHP5.4 est la version par defaut depuis les dépots officiels rhel.
PHP5.5 est aussi proposé dans les dépots officiels.

```
# php 5.4
yum install php
# PHP 5.5
yum install php55
```

installation des modules php courant (exemple pour php 5.4)
cf modules

###      PHP5.6/PHP7.0/PHP7.1 (version SCL sur rhel7)

Activation des Software Collections de Centos
`yum install centos-release-scl```

Installation
```
# PHP5.6
yum install rh-php56 rh-php56-php
# PHP7.0
yum install rh-php70 rh-php70-php
# PHP7.1
yum install rh-php71 rh-php71-php
```

Activer l'environnement de la version php5.6/7.0 à bash (temporaire)
```
# pour switcher à PHP5.6
scl enable rh-php56 bash
# pour switcher à PHP7.0
scl enable rh-php70 bash
# pour switcher à PHP7.1
scl enable rh-php71 bash
```

##    PHP-FPM (mode standalone)

L'intérêt de PHP-FPM est qu'il permet d'installer plusieurs versions autonome de PHP avec une sécurisation des vhosts par pool.
Par conséquent l'installation n'a pas grand d'intérêt à se faire depuis les dépots officiels qui ne gère qu'une seule version de PHP.

###      PHP-FPM plusieurs versions (version SCL sur rhel7)

%{color:orange}Penser à adapter la configuration des vhosts httpd avec les éléments indiqués dans les pools
```

####        Installation des versions PHP-FPM

Activation des Software Collections de Centos
`yum install centos-release-scl```

Installation
```
# PHP5.6-FPM
yum install rh-php56 rh-php56-php-fpm
systemctl enable rh-php56-php-fpm
# PHP7.0-FPM
yum install rh-php70 rh-php70-php-fpm
systemctl enable rh-php70-php-fpm
```

Activer l'environnement de la version php5.6/7.0 à bash (temporaire)
```
# pour switcher à PHP5.6
scl enable rh-php56 bash
# pour switcher à PHP7.0
scl enable rh-php70 bash
```

Création des liens symboliques (pour plus de confort)
```
ln -s /etc/opt/rh/rh-php70 /etc/php70 # PHP 70
ln -s /etc/opt/rh/rh-php56 /etc/php56 # PHP 56
```

###      Gestion du service

```
# pour PHP5.6-FPM
systemctl start|restart|stop|status rh-php56-php-fpm
# pour PHP7.0-FPM
systemctl start|restart|stop|status rh-php70-php-fpm
```

Vérification des pools
```
systemctl status rh-php70-php-fpm.service
...
   CGroup: /system.slice/rh-php70-php-fpm.service
           ├─25710 php-fpm: master process (/etc/opt/rh/rh-php70/php-f...
           └─25711 php-fpm: pool karumbureau_recetteslet_librescargot_fr...
...
```

###      Configuration des pools PHP-FPM

####        Création du couple utilisateur user/user-run de chaque pool

cf [[Vhost]]

####        Backup du pool www.conf d'origine


```
mv /etc/opt/rh/rh-php70/php-fpm.d/www.conf /etc/opt/rh/rh-php70/php-fpm.d/www.conf_bak
```

####        Création d'un pool

*Un vhost = un pool/process php = un utilisateur linux lambda* (user-run)

* configuration dans */etc/opt/rh/rh-php70/*
* emplacement des pools : /etc/opt/rh/rh-php70/php-fpm.d/*.conf
* Le vhost par defaut (00-default.conf d'Apache) utilise *php7.0-fpm-adminutils.sock*.

* Exemple pour pool d'administration du serveur */etc/opt/rh/rh-php70/php-fpm.d/adminutils.conf* (vhost par defaut):
( Exceptionnelement pour le 00-default : 1 seul user adminutils:adminustils /sbin/nologin)
```
[adminutils]

listen = /var/run/php7.0-fpm-adminutils_librescargot_fr.sock
listen.allowed_clients = 127.0.0.1

user = adminutils
group = adminutils

listen.owner = apache
listen.group = apache

pm = dynamic
pm.max_children = 5
pm.start_servers = 1
pm.min_spare_servers = 1
pm.max_spare_servers = 3

php_admin_value[error_log] = /var/opt/rh/rh-php70/log/php-fpm/adminutils_librescargot_fr-error.log
php_admin_flag[log_errors] = on
php_value[session.save_handler] = files
php_value[session.save_path] = /var/opt/rh/rh-php70/lib/php/session_adminutils
```

* exemple pour pool standard */etc/opt/rh/rh-php70/php-fpm.d/test1_librescargot_fr.conf*
```
[test1]

; user a utiliser pour le moteur php
user = test1-run
group = test1-run
; user qui contacte le socket
listen.owner = apache
listen.group = apache
; socket
listen = /var/run/php7.0-fpm-test1_librescargot_fr.sock
listen.allowed_clients = 127.0.0.1
; chroot de php
php_admin_value[open_basedir] = /data/apache/test1_librescargot_fr:/tmp

; moteur a utiliser (dynamic/ondemand/static)
pm = dynamic
pm.max_children = 5
pm.start_servers = 1
pm.min_spare_servers = 1
pm.max_spare_servers = 3

;pm = ondemand
;pm.max_children = 5
;pm.process_idle_timeout = 10s;

;pm = static
;pm.max_children = 5
```

####        Option supplémentaire dans les pools

L'utilisation des *php_value* dans les .htaccess  dans les vhosts n'est plus compatible.

Il faut les ajouter  à la fin de la conf du pool.

#####          Cache php-soap


```
; php-soap
php_value[soap.wsdl_cache_dir]  = /var/opt/rh/rh-php70/lib/php/wsdlcache/nom_du_vhost
```

```
mkdir -p /var/opt/rh/rh-php70/lib/php/wsdlcache/nom_du_vhost
chown user-run: /var/opt/rh/rh-php70/lib/php/wsdlcache/nom_du_vhost
```

#####          Debug et log

```
; debug
php_admin_value[error_log] = /var/opt/rh/rh-php70/log/php-fpm/adminutils_librescargot_fr-error.log
php_admin_flag[log_errors] = on
```

```
mkdir /var/opt/rh/rh-php70/log/php-fpm/nom_du_vhost
chown user-run: /var/opt/rh/rh-php70/log/php-fpm/nom_du_vhost
```

#####          Dossier de session

Les applications web peuvent utiliser des informations de sessions.

(!) %{color:red}Cette valeur peut être inscrite dans le pool mais peut être écrasée par une valeur de l'application.
```

* PHP-55/56/70 (depuis dépot SCL) : */var/opt/rh/rh-php70/lib/php/session/nom_du_vhost*
* PHP (depuis dépot officiel ) : */var/lib/php/session/nom_du_vhost*

```
php_value[session.save_path]    = /var/opt/rh/rh-php70/lib/php/session/nom_du_vhost
```

```
mkdir /var/opt/rh/rh-php70/lib/php/session/nom_du_vhost
chown user-run: /var/opt/rh/rh-php70/lib/php/session/nom_du_vhost
```

##    MODULES PHP

###      Modules courants PHP

```
yum install php-bcmath php-pdo php-mbstring  php-xml php-pear php-mysql php-json
```

installation des modules php courant (exemple php70)
```
yum install rh-php70-php-bcmath rh-php70-php-pdo rh-php70-php-mbstring rh-php70-php-xml rh-php70-php-pear rh-php70-php-opcache rh-php70-php-json
```

Quelques modules complémentaires (exemphe php71)
```
rh-php71-php-pgsql
```


##    Diagnostiques de PHP

Vérifier la version de PHP accessible par Bash
```
php -i
```

Activer l'environnement de la version php5.6/7.0 SCL à bash (temporaire)
```
# pour switcher à PHP5.6
scl enable rh-php56 bash
# pour switcher à PHP7.0
scl enable rh-php70 bash
```

##    Configuration de PHP

PHP-FPM et PHP normal

###      php.ini

/etc/php.ini
ou
/etc/opt/rh/rh-php70/php.ini
ou
/etc/opt/rh/rh-php71/php.ini
ou
/etc/opt/rh/rh-php56/php.ini

```
<code class="diff">
- ;date.timezone = 
+ date.timezone = Europe/Paris
</code>
</pre >

Interprétation des balises courtes
```
short_open_tag = On
```
