



#  MySQL

##    Installation

```
 # yum install mariadb-server
```
%{color:DarkMagenta} # systemctl enable mariadb.service
```

###      Selinux

[[company-1:Sécurité-SeLinux#MySQL-Data-Directory|MySQL Data Directory]]

###      Droits

(!) %{color:red}Juin 2018 : cette valeur est laissé par defaut /var/lib/mysql
```

```
 # chown mysql: /srv/mysql
```

###      Configuration

####        Moteur

```
* /etc/my.cnf

`<code class="diff">
-datadir=/var/lib/mysql
+datadir=/srv/mysql
+innodb_file_per_table
</code>
```

####        Comptes Root

```
 # systemctl start mariadb
```
%{color:DarkMagenta} # mysql_secure_installation
```

```
NOTE: RUNNING ALL PARTS OF THIS SCRIPT IS RECOMMENDED FOR ALL MariaDB
      SERVERS IN PRODUCTION USE!  PLEASE READ EACH STEP CAREFULLY!
In order to log into MariaDB to secure it, we'll need the current
password for the root user.  If you've just installed MariaDB, and
you haven't set the root password yet, the password will be blank,
so you should just press enter here.
Enter current password for root (enter for none): %{color:blue}ENTER
```
OK, successfully used password, moving on...
Setting the root password ensures that nobody can log into the MariaDB
root user without the proper authorisation.
Set root password? <notextile>[</notextile>%{color:blue}Y%/n] 
New password: 
Re-enter new password: 
Password updated successfully!
Reloading privilege tables..
 ... Success!
By default, a MariaDB installation has an anonymous user, allowing anyone
to log into MariaDB without having to have a user account created for
them.  This is intended only for testing, and to make the installation
go a bit smoother.  You should remove them before moving into a
production environment.
Remove anonymous users? <notextile>[</notextile>%{color:blue}Y%/n] 
 ... Success!
Normally, root should only be allowed to connect from 'localhost'.  This
ensures that someone cannot guess at the root password from the network.
Disallow root login remotely? <notextile>[</notextile>%{color:blue}Y%/n] 
 ... Success!
By default, MariaDB comes with a database named 'test' that anyone can
access.  This is also intended only for testing, and should be removed
before moving into a production environment.
Remove test database and access to it? <notextile>[</notextile>%{color:blue}Y%/n] 
 - Dropping test database...
 ... Success!
 - Removing privileges on test database...
 ... Success!
Reloading the privilege tables will ensure that all changes made so far
will take effect immediately.
Reload privilege tables now? <notextile>[</notextile>%{color:blue}Y%/n] 
 ... Success!
Cleaning up...
All done!  If you've completed all of the above steps, your MariaDB
installation should now be secure.
Thanks for using MariaDB!

##    Création d'un utilisateur

```
CREATE USER 'user'@'localhost' IDENTIFIED BY 'MotDePasse';
```

###      Attribution des droits de minimum pour la connexion

```
GRANT USAGE ON * . * TO 'user'@'localhost' WITH MAX_QUERIES_PER_HOUR 0 MAX_CONNECTIONS_PER_HOUR 0 MAX_UPDATES_PER_HOUR 0 MAX_USER_CONNECTIONS 0 ;
```

###      Attribution des droits de pour un utilisateur de sauvegarde

```
GRANT SELECT ,
SHOW DATABASES ,
LOCK TABLES ,
EVENT,
SHOW VIEW,
TRIGGER ON * . * TO 'svguser'@'localhost' WITH MAX_QUERIES_PER_HOUR 0 MAX_CONNECTIONS_PER_HOUR 0 MAX_UPDATES_PER_HOUR 0 MAX_USER_CONNECTIONS 0 ;
```

##    Création d'une base en utf8

```
# CREATE DATABASE IF NOT EXISTS database ;
CREATE DATABASE IF NOT EXISTS databasename character set UTF8mb4 collate utf8mb4_bin
```

###      Attribution des droits

```
GRANT ALL PRIVILEGES ON database . * TO 'user'@'localhost';
```

##    Nettoyage complet d'une base

(!) %{color:red}Attention supprime tout le contenu% (!)

* mysql_proc_cleanup_database.sql
```
DROP PROCEDURE IF EXISTS `drop_all_tables`;

DELIMITER $$
CREATE PROCEDURE `drop_all_tables`()
BEGIN
    DECLARE _done INT DEFAULT FALSE;
    DECLARE _tableName VARCHAR(255);
    DECLARE _cursor CURSOR FOR
        SELECT table_name 
        FROM information_schema.TABLES
        WHERE table_schema = SCHEMA();
    DECLARE CONTINUE HANDLER FOR NOT FOUND SET _done = TRUE;

    SET FOREIGN_KEY_CHECKS = 0;

    OPEN _cursor;

    REPEAT FETCH _cursor INTO _tableName;

    IF NOT _done THEN
        SET @stmt_sql = CONCAT('DROP TABLE ', _tableName);
        PREPARE stmt1 FROM @stmt_sql;
        EXECUTE stmt1;
        DEALLOCATE PREPARE stmt1;
    END IF;

    UNTIL _done END REPEAT;

    CLOSE _cursor;
    SET FOREIGN_KEY_CHECKS = 1;
END$$

DELIMITER ;

call drop_all_tables(); 

DROP PROCEDURE IF EXISTS `drop_all_tables`;
```

```
$ mysql -u mon_utilisateur -p mabase < mysql_proc_cleanup_database.sql
```
