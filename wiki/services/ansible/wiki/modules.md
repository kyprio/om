# MODULES COMMUN

- https://docs.ansible.com/ansible/latest/modules/modules_by_category.html

##    COMMANDS MODULES : COMMAND SHELL

- https://docs.ansible.com/ansible/latest/modules/list_of_commands_modules.html

###      COMMAND (recommandé)

> Execute des commandes simples sans interpréteur shell


test la commande getenforce et récupère le code de retour dans la variable sestatus
```
- name: test SeLinux
  command: getenforce
  register: sestatus
  changed_when: false # ne renvoit pas de changement (recommandé pour les commandes de debug)
```
###      SHELL

> utilise /bin/sh
> Execute des commandes complexes néccessitant un interpréteur shell (pipe,renvoi,...)

###      SCRIPT

> Transfert et execute un script vers le serveur distant (bash/powershell/...)

###      RAW

> Execute des commandes uniquement via SSH sans passé par python.
> utile pour installer les dépendances python utilisant ansible

##    PACKAGING MODULES

- https://docs.ansible.com/ansible/latest/modules/list_of_packaging_modules.html

###      YUM APT APK

> vérifie la présence d'un paquet

installe un paquet
```
- name: install httpd
  yum:
    name: httpd
    state: present
```

installe/mets à jour un paquet
```
- name: install httpd
  yum:
    name: httpd
    state: latest
```

##    FILE MODULES

###      COPY

> copi un fichier

copi un fichier du dossier par default files vers le serveur distant
```
- name: copy le fichier
  copy:
    src: vhost1.conf
    dest: /etc/httpd/conf.d/vhost1.conf
```

###      FILE

> s'assure de l'existence d'un fichier


créé un dossier
```
- name: ajoute un repertoire
  file:
    path: /nom/dossier
    state: directory
```

créé un lieen symbolique
```
- name: lien symbolique
  file:
    src: /dossier/source
    dest: /dossier/lien
    state: link
```

###      GET_URL

> récupère un fichier depuis un url 

###      GIT

> clone un depot

##    PING

##    DEBUG

###      SERVICE

> action à mener sur l'état d'un service (restarted/reloaded/started)
> généralement utilisé en handlers

```
- name: reload httpd
  service:
    name: httpd
    state: reloaded
```

##    SUNCHRONIZE

###      TEMPLATE

> génère un fichier depuis un template jinja2 et le transfert

genère un fichier depuis le dossier template du role
```
- name: config httpd.conf
  template:
    src: httpd.j2
    dest: /etc/httpd/conf/httpd.conf
```

genère un fichier depuis le dossier template par défaut du role en incluant les permissions
```
- name: config httpd.conf
  template:
    src: httpd.j2
    dest: /etc/httpd/conf/httpd.conf
    owner: root
    group: root
    mode: 0644
```

##    URI

> verifie l'état d'un lien url

Verifie qu'une page renvoit bien un texte "Hello Moon"
```
- name: check une page web
  uri:
    url: http://site.com/fichier
    return_content: yes
  register: result
  until: '"Hello Moon" in result.content'
```

##    WAIT_FOR

##    ASSERT
