# SYNTAXE ANSIBLE

## HANDLERS

> les handlers sont des actions à mener 1fois
> généralement utilisé avec le module service pour restart un service

exemple d'appel depuis une tâche
```
- name : tache basique
  copy:
    src:vhost1.conf
    dest:/etc/httpd/conf.d/
  notify: restart apache
```

syntaxe d'un handler
```
- name : restart apache
  service:
    name: httpd
    state: restarted
```

L'ensemble des handlers d'un fichier task peuvent être lancé immediatement 
```
meta: flush_handlers
```
