#  VI UTILISATION COMPLETTE

sudo apt-get install vim
vimtutor = tutoriel basique sur vim

##  NAVIGATION

UP/DOWN/LEFT/RIGHT = navigation toleree par vim
CTRL G = affiche les informations sur la position du curseur par rapport au fichier

# navigation globale

h = BACKSPACE = va un caractere a gauche
m = SPACE = " " droite
j = ENTREE = va une ligne en bas
k = " " en haut 

# navigation horizontale

0 = ^^ = ORIGIN = va au debut de la ligne
$ = END = " " a la fin de la ligne 
5l = va au 5eme caractere suivant
5h = " " precedent
w = va au debut du mot courant et se deplace vers la droite
b = va au debut du mot suivant et se deplace vers la gauche
e = va a la fin du mot courant et se deplace vers la droite
ge = va a la fin du mot precedent et se deplace vers la gauche
5w = va au debut du 5eme mots suivant
5b = " " precedent
5e = va a la fin du 5eme mots suivant
5ge = " " precedent

# nagivation verticale

gg = go =va au debut du fichier
G = " " a la fin " "
5j = descend de 5 lignes
5k = monte " "
CTRL f = defilement vers le bas par 20 lignes
CTRL u = " " haut " "
:5 = 5G = 5gg = va a la ligne 5
L = va sur la ligne en bas de l'ecran
M = va sur la ligne au milieu de l'ecran
H = va sur la ligne en haut de l'ecran
( = se deplace au debut du paragraphe
) = " " a la fin " "
{ = va au dessus du paragraphe actuel
} = va au dessous " "

# navigation avancee

% = navigue d'une parenthese ou d'une a une autre (et plus en fonction de la reconnaissance du langage)
' = ` = passe d'une ligne memorisee a une autre

######################################################################

# ACTIONS DANS VI

######################################################################

# action sur le fichier courant

:w = enregistre le fichier courant
:w fichier = enregistre sous 'fichier'
:w! = " " en ometant la lecture seule
:w! fichier = enregistre en ecrasant 'fichier'
:q = quitte si le fichier courant est enregistre
:q! = quitte meme si modifications non enregistrees
:wq = enregistre et quitte
:w !sudo tee % = enregistre le fichier courant avec les droits root
:200w fichier2 = enregistre la ligne 200 du fichier en cours vers fichier2

# action equivalente au commande

qq = enregistre le fichier courant
ZZ = enregistre puis quitte le fichier courant

:e fichier = ouvre fichier si le fichier courant est enregistre
:e! fichier = " " sans enregistrer le fichier courant
vi fichier1 fichier2 = ouvre plusieurs fichiers a la fois
:n/:N = passe au fichier suivant/precedant si le fichier courant est enregistre
:n!/:N! = " " sans enregistrer le fichier courant

# affichage divise en viewport

:sp = divise la page vi horizontalement
:vsp = " " vertivalement
CTRL w CTRL  w = navigue de viewport en viewport
CTRL wj = passe vers celui du dessous (K dessus h gauche l droite)
CTRL wk = " " dessus
CTRL w= = egalise la grandeur des viewports
CTRL w+ = agrandit la viewport actuelle (- reduit)
CTRL wr = rotation de la position du viewport actuelle avec celui qui partage la meme fenetre (R pour une rotation inversee)
CTRL wq = ferme le viewport actuel


######################################################################

# EDITION DU FICHIER #

######################################################################

# MODE NORMAL
##################################################

#----------
#- mode par defaut
#- servant de passerelle au autres modes
#----------

ESC = mode commande / normal

# annuler répéter

u = annule la derniere action ( attention aux nombres d'annulation retenue dans l'historique de vim ; par defaut x1 )
U = annule les dernieres actions d'une ligne complete
CTRL r = retablit l'annulation de la derniere action

. = repete la derniere entree

# couper copier coller

y y = Y = la ligne actuelle
5 y = copi 5 ligne a partir de la ligne courante vers le bas
y ) = copi de la ligne courante jusqu'a la fin du paragraphe
y = copier l'element selectionne
" ayy = copi la ligne actuelle dans le tampon a
" ayy = copi en rajoutant la ligne actuelle au tampon a
p = colle apres le curseur le dernier element copie ou supprime
P = colle avant le curseur " "
"ap = colle les élément du tampon a sous la ligne actuelle

# suppression horizontale

x = SUPPR = supprime le caractere sous le curseur puis ceux de droite
X = BACKSPACE en mode insertion = supprime le caractere a gauche du curseur
5x = 5d RIGHT = supprime 5 caracteres a droite dont celui du curseur
5X = 5d LEFT = supprime 5 caractere a gauche
d$ = supprime du caractere sous le curseur jusqu'a la fin de la ligne
d0 = supprime tout ce qu'il y a avant le curseur
dw = supprime de la position du curseur jusqu'au debut du mot suivant
diw = supprime le mot complet sos le curseur
bdw = supprime le mot actuel si le curseur est au milieu de celui-ci 
de = supprime de la position du curseur jusqu'a la fin du mot

# suppresion verticale

dd = supprime la ligne
D = vide la ligne
5dd = supprime 5 lignes vers le bas
5dk = 5d UP = " " vers le haut
5D = " " uniquement a partir de la position du curseur
d( = supprime jusqu'a debut de la phrase
d) = " " fin de la phrase
d{ = " " jusqu'au debut du paragraphe
d} = " " jusqu'a la fin du paragraphe.
dGdgg = vide le fichier.

J = concatene la ligne du dessous avec la ligne courante

# MODE INSERTION
##################################################

# insertion et ajout

i = insertion sur la position du curseur
I = insertion en debut de ligne
a = ajout apres la position du curseur
A = ajout a la fin de la ligne
o = ajout d'une ligne sous la ligne courante et insertion de texte
O = ajout d'une ligne sur la ligne courante et insertion de texte

CTRL O = passe temporaire en mode action le tems d\'une action

CTRL X CTRL O = auto-complete dans le omni complete de vim (ex:fermer une balise html apres avoir ecris '</')

CTRL k = insertion d'un caractère spécial à partir de son digraph (ex : Om = Ω )

# remplacement simple et concatenation

r = remplace juste la lettre sous le curseur
R = mode Remplacement
s = supprime le caractere sous le curseur et passe en mode insertion
S = vide la ligne et passe en mode insertion
cw = c E = supprime le reste du mot et passe en mode insertion
cb = " " le debut " "
C = c$ = Remplace tout le reste de la ligne
c0 = Remplace tout le debut de la ligne

# MODE VISUEL
##################################################

v = passe en mode Visuel pour selectionner sur une ligne ou plusieurs
V = passe en mode Visuel Ligne pour selectionner des lignes.
CTRL V = passe en mode Visuel block (colonne).

~ = bascule la casse de la selection (majuscule minuscule)

# MODE COMMANDE
##################################################

: = passe au mode commande

:1,5 t 50 = copie colle les lignes 1 a 5 vers la ligne 50
:1,5 m 50 = deplace (coupe et colle ) " "
:commande_vi1 | commande_vi2 = execute la premiere commande vi, demande confirmation, puis execute la seconde.

# recherche depuis mode commande

:/motif = idem que en mode normal

# suppresion en commande

:5d = supprime la ligne 5
:1,5d = supprimer de la ligne 1 a 5

# MODE EX
##################################################

#----------
#- mode commande
#- mais reste en mode ex après avoir saisie une commande
#----------

Q = passe en mode ex

:visuel = :vi = sort du mode ex

######################################################################

# RECHERCHE #

######################################################################

# recherche simple de chaine de caractere

/chaine = cherche 'chaine' du curseur jusqu'a la fin du fichier
?chaine = cherche 'chaine' du curseur jusqu'au debut du fichier
/^chaine = cherche 'chaine' au debut de chaque ligne
n = resultat suivant de la derniere recherche ( en fonction du sens de la recherche )
N = resultat precedent " "
CTRL c = stoppe la recherche (si trop longue )
:noh = arrete le surlignement des resultats

# remplacement et substitution

:1,5 s /chaine1/chaine2 = remplace chaine1 par chaine2 entre la ligne 1 a la ligne5 a chaque premier resultat de recherche
:1,5 s /chaine1/chaine2/g = remplace chaine1 par chaine2 " "
:% s /chaine1/chaine2 = " " dans tout le fichier
:5,$ /chaine1/chaine2 = " " entre la ligne 5 a la fin 
:%s /gcbalise\//balise/g = remplace tout les /balise/ par balise. ( '\' permet d'echaper un caractere special )
:%s /chaine/\r/g = remplace chaine par un saut de ligne appele retour de charriot

######################################################################

# ACTIONS EXTERNES #

######################################################################

# lancement direct de commande

:1,5 w fichier = ecrit les lignes 1 a 5 dans fichier
:1,5 w! fichier = " " en ecrasant fichier si deja existant
:r fichier = recupere sous la ligne courante le contenu de fichier
:5r fichier = recupere et insere fichier apres la ligne 5 ( donc ligne 6 )
:0r fichier = recupere et insere fichier au debut du fichier ( ligne 1 )
:r! commande_shell = recupere le resultat de la commande shell sous la ligne courante
:r! sed -n '1,5p' fichier = recupere le contenu de fichier de ligne 1 a 5 sous la ligne courante
:r! sed -n '5,$p' fichier = " " ligne 5 a la derniere ligne "
:r! sed '/^\#/ s/chaine1/chaine2/g' fichier.txt = recupere le fichier et remplace des lignes commentees uniquement chaine1 par chaine2
:r! sed -n '/^\#/p' fichier.txt = recupere uniquement les commentaires du fichier

# lancement de commande après une selection en mode visuel

:'<,'>! sort = trie  intelligement les lignes sélectionéns avec l'outils sort

######################################################################

# OPTIONS #

######################################################################

nohl = enleve le surlignage de la recherche precedente 
set nu = numero des lignes
set showmatch = surligne le duo de la parenthese parcouru (autre signe d'englobement)
set matchtime=3 = definit a 3sec le surlignage
