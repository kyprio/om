#  FICHIERS LOG

##    CONNEXION

/var/log/auth.log = liste les connexions (debian)
/var/log/secure = liste des connexions (rhel)
/var/log/lastlog = derniere connexions reussie

##    SERVICE

/var/log/syslog = log du systeme et ses services
/var/log/ = dossier ou sont places les log des programmes
/var/log/dhcp/dhclient.leases = log des informations envoyees par le serveur dhcp aux interfaces reseaux
/var/lib/NetworkManager/ = log les leases par NetworkManager
/var/log/messages = tout les messages afficher par le systeme depuis le debut
