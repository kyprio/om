#  CISCO TCL

Tool Command Language

##    TERMINOLOGIE

la plupart des termes sont definit dans la norme 802.1Q

VID = Vlan IDentifier, VID 0 = une tagged frame contenant le VID 0 est transmise au VLAN natif sera considere comme clear frame
PVID = Port Vlan IDentifier
access port/link = port/liaison entre deux ports contenant un ou plus d'untagged Vlan
trunk port/link = port/liaison entre deux ports contenant un ensemble de VLANS qui sont tous tagged
hybrid port/link = port/liaison entre deux ports contenant a la fois des untagged Vlans et des tagged Vlans (par defaut)
tagged frame = trame ethernet ou 802.3 avec un tag 802.1Q
clear frame = trame ethernet ou 802.3 sans tag
VLAN trunking = transmission de multiple VLAN a travers une liaison. Cela fonctionne uniqueme si les VLANs ont ete configures prealablement avec les memes VID sur tous les routeurs.
native Vlan = Vlan natif/par defaut (1) qui est utilise sans tag ou qui recoit les frame appartenant a VID 0. Si un routeur tombe en panne ou qu'il ne gere pas les tagged frame, les echanges seront place dans le vlan natif

##    COMMANDES ET CONFIGURATIONS COMMUNES AUX EQUIMENTS CISCO

###      COMMANDES GENERALES

`enable` = ouvrir la console en mode privilégiée
`conf t = ouvrir la configuration
`exit` = retour arrière
`end` = retour à la console non privilégiée  
`copy run start` = `write` = enregistrement de la config  

###      DEBUGAGE BASIQUE

`show running-config` = affiche la configuration actuel  
`show starting-config` = affiche la configuration de démarage  
`show vlan` = affiche les vlan (vlan.dat, différent de la config actuel)

`show cdp neighbors` = affiche activité des ports et cartes  

`show interface nom_interface` = affiche le détail de l'interface  

`show ip route` = affiche toutes les routes connues

> info sur les informations de `show ip route`  
> s : statique
> r : RIP
> d : EIGRP
> o : OSPF
> b : BGP

`show ip protocols` = détail sur les protocoles utilisés  
> info sur les informations de `show ip protocoles`  
> protocoles activés ?  
> - version utilisée ?  
> - auto-summary activé (routage)?  
> - réseaux déclarés ?  

##    BACKUP/RESTORATION  

###      backup

`enable`
`show run`
> copier/coller vers un fichier texte
> supprimer les premières lignes jusqu'au premier "!"
> récupérer eventuellement la liste des vlan `show vlan`

###      restauration

`enable`
`conf t`
> coller le contenu

##    INITIALISATION  

> 10 sec sur le bouton reset pour un reset factory

###      initialisation système  

`enable`
`conf t`
`erase startup-config` = reset

`hostname Router1` = attribue le nom de l'équipement  

`no ip domain lookup` = désactive la résolution DNS (très pratique)

###      initilisation des terminaux

####        mot de passe session par accès direct  

`enable password motdepasse`
`enable secret motdepassesecret`

####        mot de passe session en port console  

`line console 0`
`password motdepasseclair`
`login`
`exec-timeout 0 0`
`logging synchronous`

####        mot de passe et accès pour telnet (port 23)

`line VTY 0 4` = autorisation de 5 connexions simultanées
`password motdepasse`
`login`

####        mot de passe et accès pour ssh

`ip domain-name domain.lan`
`crypto key generate RSA`
`line VTY 0 2` = autorisation de 3 connexions simultanées
`transport input ssh`
`login local`
`exit`
`username USER password motdepasse`
`service password-encryption`

####        modification de la banniere

`banner motd "Nouvelle Banniere"`

##    PORTS ET INTERFACES RESEAUX  

> nommage des ports  
> gi = Gigabitethernet  
> fa = Fastethernet  
> se = Serial  

`show ip interface brief` = affiche le résumé de toutes les interfaces  

###   TYPE ETHERNET  

####       assigner une IP à une interface ethernet

`interface Fa0/0`
`ip address 192.168.1.1 255.255.255.0`
`no shutdown`

####        TYPE SERIAL  

> utilisé pour les routeurs
> ajout module HWIC-2T  
> DCE : Data Communication Equipment (coté FAI)  
> DTE : Data Terminal Equipment (coté client)  

#####          assigner une IP à une interface serial DCE

`interface Serial 1/0`
`ip address 10.0.0.1 255.255.255.252`
`clock rate 64000`
`BANDWITH 64`
`description "Connexion vers client1"`
`no shutdown`

#####          assigner une IP à une interface serial DTE

`interface Serial1/0`
`ip address 10.0.0.2 255.255.255.252`
`BANDWIDTH 64`
`description "Connexion vers BT"`
`no shutdown`

###      IP ACCESS LIST ACL

> Permet de gérer des groupes d'IP par nom et  
> utilisable pour plusieurs fonctions de l'équipement comme le SNMP.  

`ip access-list standard nom_acl`
`permit 10.0.0.1 0.0.0.0` = autorise l'ip 10.0.0.1
`permit 192.168.1.0 0.0.0.255` = autorise le réseau 192.168.1.0/24

###      SNMP

####        SNMP V2

> fonctionne via des gestions de communauté (groupe partageant le même mdp)

`snmp-server community LIBRESCARGOT ro` = active le SNMP v1/2 sur la communauté LIBRESCARGOT en mode Read Only
`snmp-server community LIBRESCARGOT rw` = " " en mode Read Write (non recommandé)
`snmp-server community LIBRESCARGOT rw nom_acl` = " " en mode Read Write avec filtrage via une ACL prédéfinie (recommandé pour ro et rw)

`snmp-server enable traps` = active l'envoi de remontée d'info au serveur SNMP 

`snmp-server location emplacement1` = précise la localisation de l'équipe dans les info SNMP
`snmp-server chssis-id numeroID` = précise l'In de l'équipe dans les info SNMP

##    ROUTEUR

###      ACL PREFIX-LIST

> permet d'autoriser/accepter ou refuser des routes diffusées ou à diffuser

#### création d'ACL

Création d'une liste example1 contenant une règle refusant les routes par default
```
ip prefix-list example1 deny 0.0.0.0
```

#### utilisation d'ACL

cf chaque protocole de routage

###      ROUTAGE STATIQUE

(non recommandé)

`ip route 0.0.0.0 0.0.0.0 172.31.1.197` = Configuration d'une route par defaut  
`ip route 192.168.2.0 255.255.255.0 10.0.0.2` = ajout d'une route statique non par défaut
`no ip route 0.0.0.0 0.0.0.0` = Suppression d'une route  

###      ROUTAGE DYNAMIQUE  

`passive-interface default` = désactive par defaut la propagation du routage sur les interfaces (evite de propager la topologie du réseau à tout le monde)

`no passive-interface Fa0/0` = active la propagation sur une interface  
`passive-interface Fa0/0` = désactive la propagation sur une interface (coté FAI ou switch)  

####        RIP 2  

> Caractéristique :
> - protocole à vecteur de distance
> - meilleur chemin calculé par rapport aux nombres de sauts (routeur)
> Avantages :  
> - rapide sur petit réseau  
> - diffusion de sa table de routage sur ses ports  
> Désavantages  :  
>  - gère au maximum 15 sauts  

`router rip`
`version 2`
`network 192.168.1.0` =  prend en charge le réseau 192.168.1.0
`network 10.10.10.0` = " " 10.10.10.0
`no auto-summary` = ne fusionne pas des sous-réseaux en réseau (x2 /25 = /24)
`default-information originate # (pour routeur coté FAI uniquement) créé une route externe par defaut`

> désactiver l'envoi de la table de routage
> sur les ports non relié à des routeurs

#####        redistribution de protocole vers rip

```
router rip
version 2
redistribute eigrp 1 metric 1
redistribute ospf 1 metric 1
```

####        OSPF

Open Shortest Path First
Protocole Standard

> Caractéristiques :
> - protocole à état de lien
> - gestion des routes par zone
> - processus ospf associé à un PID à définir
> - calcul le chemin le plus rapide par rapport à la vitesse théorique de ses interfaces (FastEthernet,Serial, Ethernet...) avec pour base par défault max de 100Mbits/s
> - centralise la topologie de chaque area sur un DR (Designated Router) (désigné définitivement lors de l'activation du routage OSPF grâce à son indice de priorité et son IP )
> - ne prends donc pas en compte la charge temporaire du réseau (vs EIGRP)
> Avantages :  
> - calcul lui même l'ensemble de sa topologie (vs EIGRP)
> Désavantages  :  
> - plus gourmand en ressource

`ospf priority 10` = augmente l'indice de priorité du routeur (cf Designated Router) (default=1) (se fait avant l'établissement du routage ospf)

`router ospf 1` = active ospf de pid 1 (choix du pid sans incidence)
`network 192.168.1.0  0.0.0.255 area 1` = prend en charge le réseau 192.168.1.0/24 dans la zone 1
`network 10.10.10.0  0.0.0.3 area 1` = " " 10.0.0.0/30 " "

`autocost reference-bandwidth 1000` = modifie le débit théorique maximum 1000  

= routage par ospf sur un numéro de PID 100  
= EXEMPLE DE CONFIGURATION ROUTEUR  
= rip sur Fa0/0  
`conf t`
`int Fa 0/0`
`ip address 192.168.1.1 255.255.255.0`
`no shutdown`
`int Fa0/1`
`ip address 10.0.0.2 255.255.255.252`
`no shutdown`
`exit`
`router ospf 100`
`network 192.168.1.0  0.0.0.255 area 1`
`network 0.0.0.0  255.255.255.255 area 1`
`passive-interface Fa0/1`
`end`
`copy run start`

#####        redistribution de protocole vers ospf

```
router ospf 100
redistribute rip metric 200 subnets
redistribute eigrp 1 metric 100 subnets
redistribute ospf 1 subnets
```

####        EIGRP Enhanced Interior Gateway Routing  

> Caractéristiques :
> - licence propriétaire Cisco
> - processus eigrp associé à un PID à définir
> - prend en compte la charge temporaire du réseau (vs OSPF)
> - récupère la topologie via ses voisins direct (vs OSPF)
> Avantages :  
> - consommes moins que OSPF
> Désavantages  :  
> - calcul régulièrement ses meilleurs chemins et renvoit aux voisins ses calculs si différents

`router eigrp 1` = active le routage EIGRP de pid 1  
`network 192.168.1.0  0.0.0.255` = prend en charge le réseau 192.168.1.0/24 dans la zone 1
`network 10.10.10.0  0.0.0.3` = " " 10.0.0.0/30 " "
`no auto-summary` = ne fusionne pas des sous-réseaux en réseau (x2 /25 = /24)

= affiche la table de voisinage  
`show ip eigrp`
|    |     |    |
|----|-----|----|
|R1  |<--->|R2  |
|    |Hello|    | 

#####        redistribution de protocole vers eigrp

```
router eigrp 1
redistribute rip metric 10000 1 255 1 1500
redistribute ospf 1 metric 10000 100 255 1 1500
```

####        BGP

> routage de type EGP (external)
> BGP fait la différence entre eBGP et iBGP
> eBGP est le routage d'un AS vers un autre AS dont le voisin est sur un réseau relié directement
> iBGP est le routage dans un même AS dont le voisin est sur un réseau non relié directement

routage eBGP entre deux AS différent (directement connecté)
```
router bgp 300
neighbor 192.168.10.10 remote-as 200
network 192.168.0.0 mask 255.255.255.0
```

#####          redistribution de protocole vers BGP

```
routeur bgp 200
redistribute ospf 100
```

#####          redistribution de protocole vers BGP

####        TRUNK COTE ROUTEUR

#####          affectation d'un réseau à un VLAN

`int fa0/0`
`encapsulation 802.1Q 10` # dépend du routeur
`encapsulation dot1Q 10` # dépend du routeur
`ip address 192.168.10.1  255.255.255.0`
`no shutdown`

##    SWITCH

###      VLAN  

Virtual Local Area Network
Par défaut les switchs contiennent le VLAN 1 de management (VLAN NATIF) qui peut avoir une IP

> Caractéristique :
> - LAN au niveau logique identifié par un numéro
> - séparation logique des sous réseaux d'un ou plusieurs switch
> - seul le VLAN natif peut prendre une IP (par defaut VLAN 1)
> Avantages :
> - diminue du gaspillage de ports inutilisés (et donc des coûts d'achats des switchs) par une gestion global des réseaux (vs 1switch/LAN)
> - Réduire le domaine de diffusion (broadcast) en vue d'une meilleur performance & securité (vs 1 LAN pour tout)

> 5 types de VLAN :
> 1) Data
> 	- Dédié au trafic de données
> 2) Default
> 	- Vlan 1
> 	- Gère tout le trafic administratif (CDP, STP, ACP…)
> 	- Par défaut, il est access et trunk aussi
> 	- BEST PRACTICE, il faut éviter d’utiliser le VLAN 1
> 3) Native
> 	- Transporte tout le trafic qui n’est pas taggué
> 4) Management
> 	- Pour les accès a distance
> 5) Voice
> 	- Pour la voix et la vidéo
> 	- QoS minimum (Qualité of Service/Class of Service)
> 
> Port access - Donne acces au réseau dans un seul et unique VLAN
> Port trunk - Laisse transiter le trafic de tous les VLAN

`show vlan` = Affiche les VLAN  

####        création de VLAN

`vlan 10,20,30,40` = création de plusieurs VLAN

attributtion d'une IP de VLAN et d'un nom
```
interface vlan 10
name ADMIN
ip adresse 192.168.1.1 255.255.255.0
```

####        attribution d'un VLAN à un ou plusieurs ports

`int Fa0/2` = sélectionne un port  
`int range Fa0/4-10` = sélectionne une plage de port  

`switchport mode access vlan 30`


###      TRUNK

un lien trunk permet de taggué et faire transiter plusieurs VLAN d'un switch vers un autre

####       attribution d'un lien trunk à un port

`switchport mode trunk` = active le trunk (plusieurs VLAN) aux ports sélectionnés (parfois, ne suffit pas)
`switchport trunk allowed add vlan 10,20,30,40` = alloue spécifiquement des VLAN à un port

###      VTP VLAN TRUNKING PROTOCOLE 

> Le VTP server  permet d'envoyer la table des VLAN aux autres VTP clients.
> Il peut y avoir plusieurs server VTP synchronisé.
> Il se définisse au moins par un nom de domaine et le numéro de révision doit être le même.


`vtp mode transparent` = permet de réinitialisé le nméro de révision

####        VTP server

```
vtp mode server
vtp domain nomdomaine
```

####        VTP client

```
vtp mode client
vtp domain nomdomaine
```

