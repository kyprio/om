# D-LINK

##    CONFIGURATION

###      reset 

> simple reset des accès = (admin/admin)
> appuis long sur le bouton reset

util restore_factory_defaults = reset factory default
>  https:> 192.168.10.1
>  si : Secure Connection Failed (...) a weak ephemeral Diffie-Hellman
>  solution dans Firefox :
>  about:config
>  security.ssl3.dhe_rsa_aes_128_sha default boolean false
>  security.ssl3.dhe_rsa_aes_256_sha default boolean false

###      AFFICHAGE

####        lan

show net lan ipv4 setup = config complète du lan
show net lan dhcp leased_clients list = liste les IP et mac des clients DHCP
show net lan dhcp reserved_ip setup = liste les IP réservés

###      OUTILS RESEAUX

####        diagnostique

util ping 192.168.0.5 = ping l\'adresse IP "OU"
util system_check ping 192.168.0.5 = " "

util system_check display_IPV4_routing table = table de routage
