# AVAYA

##    TERMINOLOGIE

cf cisco.md

##    LISTE DES VLANS ET CONFIGURATIONS

VLAN [1] : 2
  in-band address = adresse par default du switch/vlan/de depart ??
  in-band sub-net mask = masque ??
  Default gateway
  Read-Only Community String[public] ??
  Read-Write Community String[private] ??
  in-band IPV6 Address/Prefix_length ??
  in-band IPV6 Default Gateway
  DHCP server
  IP address pool name = plage/domaine d'adress IP ??
  start IP address of the pool
  end IP address of the pool
  mask address of the pool
  DNS server (IPs)
  Router (IPs) 

###      LISTE DES COMMANDES

####        acces a l'interface ligne de commande ACLI

sudo screen /dev/ttyUSB0 9600 = (Putty "Serial" Speed = 9600 COM1 ou 2 ...) = acces en TTL aux Avayas
CTRL+Y pour demarrer la transmission

####        les 2 differents modes et le sous-mode configuration

USER EXEC > (pas de commande) = mode par defaut USER EXEC
PRIVILEGED EXEC > enable = mode admin
  GLOBAL CONFIGURATION > configure = sous-mode configuration issue de PRIVILEGED EXEC
  GLOBAL CONFIGURATION using Terminal > configure terminal = sous-mode configuration en indiquant directement que y accede depuis le port console

####        Parametre par defaut

restore factory-default -y = reset le systeme complet et efface la NVRAM avec l'option -y pour accepter tous les changements

####        aide

? = liste les commandes disponibles a partir du prompt
show vlan type proto? = liste les commandes possibles a ecrire apres les elements precedents
show vlan type proto[TAB] = autocomplete la ligne de commandes si une seule commande est possible

####        script et fonction automatique

install = creation interractive de VLANs
run ipoffice = parametrage automatique pour un reseau utilisant des telephone sur ip et des postes de travail

####        affichage 

// mode PRIVILEGED EXEC

show vlan = liste tous les vlans
show vlan id 1-5 = liste tous les vlans de 1 a 5
show vlan id 1,5 = liste les vlans 1 et 5
show vlan type port = liste les vlans bases sur les port
show vlan type protocol =  liste les vlans bases sur des protocols
show vlan type protocol-ipv6Ether2 = liste les vlans bases sur le protocol ipv6Ether2
show vlan interface vids = liste les VIDs sur chaques ports
show vlan interface info = affiche la configuration des vlans par port incluant les tagging, pvid
s( "management vlan" ?ohother2 | protocol-ipEther2 | protocol-ipv6Ether2 |
101 // protocol ipx802.2 | protocol-ipx802.3 | protocol-ipxEther2  = affiche le vlan d'administration du routeur
show vlan voice-vlan = affiche le "Voice vlan information"
show vlan summary = affiche le total des VLANS bases sur port ou protocol
show vlan configcontrol = affiche l'option activee de modification des vlans
show mac-address-table = affiche toutes les tables d'adresses mac de tous les vids
show mac-address-table vid 2 = affiche la table d'adresse mac du 2
show mac-address-table aging-time = affiche le temps d'expiration en secondes d'une adresse inutilisee
show mac-address-table dynamic = affiche toutes les lignes des tables d'adresses mac enregistrees dynamiquemement ("static"=manuellement, dynamic)

####        manipulation des vlans existants

// sous-mode GLOBAL CONFIGURATION

vlan mgmt 2 = attribut le management vlan au vid 2 (oar default 1)
default ip address = supprime/reset l'adresse IP du vlan
default vlan mgmt = reset/reconfigure le vlan mgmt par default (1)
vlan delete 2 = supprime le vlan 2
vlan configcontrol flexible = active l'option "flexible" au Configuration Control ("automatic", "autopvid", "flexible", "strict"=default)

####        information d'acces au switch

show ip = affiche les informations propres au switch
ip address switch 192.168.2.1 = affecte l'IP du switch
ip address netmask 255.255.255.0 = affecte le netmask du switch
ip default-gateway 192.168.1.2 = affecte la passerelle par defaut du switch

####        services

ip dhcp-server = active le serveur dhcp
ip dhcp-relay = active le relai dhcp

####        creation des vlans

// sous-mode GLOBAL CONFIGURATION
// CREATION DES VLANS :
// vlan create <VID_list> [name <LINE>] type {port [voice-vlan]|
// protocol decEther2 | protocol-ipEther2 | protocol-ipv6Ether2 |
// protocol ipx802.2 | protocol-ipx802.3 | protocol-ipxEther2 |
// protocol ipxSnap | protocol-Netbios | protocol-RarpEther2 |
// protocol sna802.2 | protocol-snaEther2 | protocol-vinesEther2 |
// protocol-xnsEther2 | protocol-Userdef {<4096-65534> | ether |
// llc | snap }[voice-vlan]
//
// CONFIGURATION DES VLANS :
// vlan ports [<portlist>] [tagging {enable | disable | tagAll |
// untagAll | tagPvidOnly | untagPvidOnly}] [pvid <1-4094>]
// [filter-untagged-frame {enable | disable}] [filter-
// unregistered-frames {enable | disable}] [priority <0-7>] [name
// <line>]

vlan create 2-5,10 type port = cree les vlans d'ID 2, 3, 4, 5, 10 bases sur les ports
vlan create 6 name Vlanator6 type port = cree le vlan d'ID 6 nomme Vlanator6 base sur les ports
vlan name 2 Vlanator2 = change le nom en Vlanatator2 au VID 2
auto-pvid = active le PVID automatique
vlan  ports all tagging untaggAll =  untag tous les echanges 

vlan configcontrol flexible = active la configuration vlan en flexible
vlan members 1 NONE = n'affecte aucun port au Vlan natif (1)
vlan members 2 1-8 = affecte les ports 1 a 8 au vlan 2
vlan members add 5 1 = ajoute au VID 5 le port 1 (configcontrol flexible requis)
vlan members add 5 1/2 = ajoute au VID 5 le port 2 du switch 1
vlan members remove 5 1 = retire au VID 5 le port 1 (configcontrol flexible requis)

####        adresses mac

default mac-address-table learning = reactive l'apprentissage des mac pour tous les ports (par defaut)
default mac-address-table learning 1,4 = reactive l'apprentissage des mac pour les ports 1 a 4
no mac-address-table learning 2-8 = desactive l'apprentissage des macs pour les ports 2 a 8
mac-address-table aging-time 600 = definit le temps d'expiration d'une adresses mac inutilisee dans la table d'adresse a 10min / 600sec
default mac-address-table aging-time = definit le temps d'expiration par defaut d'une adresses mac inutilisee dans la table d'adresse a 300sec
mac-address-table static 11:22:33:44:55:66 2 interface mlt 2 = inscrit l'adresse mac dans la table pour le vid 2 en trunk 2 ("mlt"=trunk, "FastEthernet"=port)
clear mac-address-table = nettoie la table d'adresses mac
clear mac-address-table interface vlan 2 = nettoie la table d'addresses mac du vlan 2
clear mac-address-table interface FastEthernet 1-8 = nettoie la table d'adresses mac des ports ethernet 1 a 8
clear mac-address-table interface mlt 9-10 = nettoie la table d'adresses mac des ports trunk 9 et 10
clear mac-address-table address 11:22:33:44:55:66 = supprime des tables l'adresse mac

####        importation d'une config sur un routeur
// branchement par RS/ethernet et ethernet pour le transfert

ip address 10.201.11.229 255.255.255.0 = definition d'une adresse ip
ping 10.201.11.229 = test depuis le PC sur la meme plage d'addresse du routeur son aaccessibilite ( ex : 10.201.11.14 )
copy tftp config address 10.201.11.14 filename FICHIER-CONFIG =  telecharge le fichier config sur le serveur tftp d'adresse X.X.X.14
// le routeur se redemarre et se parametre
//Verification :
show vlan
show ip address

// GUI web
Configuration > Edit > File System : Config / Image / Diag : 
TftpServerInetAddress : X.X.X.X du serveur TFTP
BinaryConfigFileName : FICHIER-CONFIG sur le serveur
Apply
upldConfig
Apply
// le routeur se redemarre et se parametre

####        raccourcis

en = enable
config = configure
config t = configure terminal
sh = show

####        personnalisation

show banner custom = affiche/active la banniere customisee
banner 1 " texte"
banner 2 " texte" = remplace les lignes 1 et 2 de la banniere par texte
