# FORMAT DES FICHIERS DE CONFIGURATION

structure et syntaxe des différents types de format de fichiers de configurations

## INI

```
[ NOM_SECTION ]
clés1 = valeur
clés2 = valeur
clés3 = valeur ; commentaires
;clés4 = valeur ; clés valeur commentés


[ NOM_SECTION2]
clés1 = valeur
```


## JSON

un fichier json reprends la forme d'un dictionnaire complexe python.
il n'utilise que des ' ' et non des " ".
il n'y a pas de commentaire en json

```
{
  "cles1" : "valeur",
  "dict1" : {
    "souscles1" : "valeur",
    "souscles2" : "valeur"
  },
  "liste1": [
    "element1",
    "element2",
    {
      "cles1": "valeur",
      "cles2": "valeur"
    },
    "element4"
  ]
}
```

## YML / YAML

le fichier yml est ue simplificiation du json.
il correspond aussi à un dictionnaire de clés valeur.


```
---
cles1: valeur
dict1:
  souscles1: valeur #commentaire
  souscles2: valeur
# commentaire
liste1:
- element1
- element2
- cles1: valeur
  cles2: valeur
- element4

```

## XML

le fichier XML contient (normalement) une entête indiquant la version du fichier mais aussi la source du fichier indiquant la structure des données d'extention .dtd


```
<?xml version="1.0" encoding="UTF-8" ?>
  <cles1>valeur</cles1>
  <dict1>
    <souscles1>valeur</souscles1>
    <souscles2>valeur</souscles2>
  </dict1>
  <liste1>element1</liste1>
  <liste1>element2</liste1>
  <liste1>
    <cles1>valeur</cles1>
    <cles2>valeur</cles2>
  </liste1>
  <liste1>element4</liste1>
```

exemple de fichier xml et dtd

data.xml
```
<!DOCTYPE series
[
<!ELEMENT series(nom)>
<!ELEMENT nom (personnage)*>
  <!ATTLIST nom
    titre CDATA #REQUIRED
    lang (en | fr) #REQUIRED>
<!ELEMENT personnage (#PCDATA)>
]>
```

data.dtd
``
<!DOCTYPE donnees
[
<!ELEMENT series(nom)>
<!ELEMENT nom (personnage)*>
  <!ATTLIST nom
    titre CDATA #REQUIRED
    lang (en | fr) #REQUIRED>
<!ELEMENT personnage (#PCDATA)>
]>
```
