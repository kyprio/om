using static System.Console;

class main {
  // methode d'alteration par defaut
  public static void ChangeTab(int[] tab, int y) {
    tab[0] = y; 
  }
  public static void ChangeInt(int num, int y) {
    num = y ;
  }
  // methode d'alteration par ref (surchage)
  public static void ChangeTab(ref int[] tab, int y) {
    tab[0] = y; 
  }
  public static void ChangeInt(ref int num, int y) {
    num = y ;
  }
  // methode d'alteration par out (surchage de ref impossible)
  public static void ChangeoutInt(out int num, int y) {
    num = y ;
  }
  public static void ChangeoutTab(out int i, int y) {
    i = y; 
  }

  static void Main(string[] args) {   

    // attribut
    int[] tab1 = {0, 1};
    int num1 = 0;
    
    // execution

    // avant modification
    WriteLine("Valeur intiale :");
    WriteLine(" tab1[0] : {0} " , tab1[0]);
    WriteLine(" num1 : {0} " , num1);
  
    // modification par defaut
    // -> int/string/... renvoit leur valeur
    // -> []/Array/... renvoit leur référence 
    WriteLine("Passage en params par defaut");
    ChangeTab(tab1, 10);
    ChangeInt(num1, 10);
    WriteLine(" tab1[0] : {0} -> modifié" , tab1[0]);
    WriteLine(" num1 : {0} -> non modifié" , num1);

    // modification par passage en param en reference out
    // -> int/string/[]/Array/... renvoit leur référence
    // les variables ont besoin d'être déclaré et initialisé
    WriteLine("Passage en params par ref");
    ChangeTab(ref tab1, 15);
    ChangeInt(ref num1, 15);
    WriteLine(" tab1[0] : {0} -> modifié" , tab1[0]);
    WriteLine(" num1 : {0} -> modifié" , num1);
    
    // modification par passage en param en reference out
    // -> int/string/... renvoit leur référence
    // ne fonctionne pas avec les []/Array ... car la référence est prise par défaut
    // il faut alors envoyé uniquement la variable qu'il contient
    WriteLine("Passage en params par out");
    ChangeoutTab(out tab1[0], 20);
    ChangeoutInt(out num1, 20);
    WriteLine(" tab1[0] : {0} -> modifié" , tab1[0]);
    WriteLine(" num1 : {0} -> modifié" , num1);
  }   
}
