using System;
class A
{
public void f() {Console.WriteLine("Fonction f de A");}
public void g() {Console.WriteLine("Fonction g de A");}
public delegate void T(); // définition de type délégué
}
class Program
{
static void Main()
{ A a = new A();
A.T de = new A.T(a.f);
de(); // exécution de f de A
de = new A.T(a.g);
de(); // exécution de g deA
}
}
