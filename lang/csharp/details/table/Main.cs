// importation de la classe static Console
using static System.Console;
using System;

public class main {

  public static void Main() {

    // attributs

    // tableau 1 ligne (5 colonnnes)
    int[] tab1 = new int[5] { 0,1,2,3,4 };
    WriteLine("tab1[1] : " + tab1[1]);
    WriteLine("dimension tab1 : " + tab1.Rank);
    WriteLine("");
    
    string[] list1 = new string[7] { "sun","mon","tues","wen","thur","fri","sat"};
    WriteLine("list1[1] : " + list1[1]);
    WriteLine("dimension list1 : " + list1.Rank);
    WriteLine("Position de sun dans list1 : " + Array.IndexOf(list1,"sun"));
    // modifie list1 pour la trier
    Array.Sort(list1);
    foreach(string d in list1) { WriteLine(d);}
    WriteLine("");
    
    // tableau 2 dimensions (2 lignes, 3 colonnes)
    // { {ligne 0}, {ligne 1} }
    int[,] tab2 = new int[2,3] { { 0,1,2 } , { 10,11,12 } };
    WriteLine("tab2[0,0] : " + tab2[0,0]);
    WriteLine("tab2[0,1] : " + tab2[0,1]);
    WriteLine("tab2[0,2] : " + tab2[0,2]);
    WriteLine("tab2[1,0] : " + tab2[1,0]);
    WriteLine("tab2[1,1] : " + tab2[1,1]);
    WriteLine("tab2[1,2] : " + tab2[1,2]);
    WriteLine("dimension tab2 : " + tab2.Rank);
    WriteLine("");
    
    // tableau 3 dimensions (2 lignes, 2 colonnes, 2 prodondeur)
    // { { tableau 2dimensions prodonfeur 0} , { tableau 2dimensions profondeur 1}
    int[,,] tab3 = new int[2,2,2] { { {0,1},{10,11} } , { {100, 101} , { 110, 111} } } ;
    WriteLine("tab3[0,0,0] : " + tab3[0,0,0]);
    WriteLine("tab3[0,0,1] : " + tab3[0,0,1]);
    WriteLine("tab3[0,1,0] : " + tab3[0,1,0]);
    WriteLine("tab3[0,1,1] : " + tab3[0,1,1]);
    WriteLine("tab3[1,0,0] : " + tab3[1,0,0]);
    WriteLine("tab3[1,0,1] : " + tab3[1,0,1]);
    WriteLine("tab3[1,1,0] : " + tab3[1,1,0]);
    WriteLine("tab3[1,1,1] : " + tab3[1,1,1]);
    WriteLine("dimension tab3 : " + tab3.Rank);
    WriteLine("");

  }
}
