namespace PM {
  partial class ClasseA {

    // définition de la méthode partielle (facultatif)
    partial void pMethode(string s) {
      System.Console.WriteLine("Nouvelle instance de ClasseA avec : {0}", s);
    }
    
  }
}
