# Partial

Une méthodes partielles permets de rendre facultatifs la définition d'une méthode.
Il s'agit plutôt de module complémentaire.
Même si la méthode est appelé, le compilateur va s'assurer de l'existe d'une définition de la méthode sans quoi il supprime l'instruction.

Les méthodes partielles
- doit être intégrée à une classe signée partial
- porter la même signature
- retourner void
- rester en private (valeur par défaut)
- peuvent être défini ou non
