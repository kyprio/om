namespace PM {
  partial class ClasseA {
    public string x{get;set;}

    public ClasseA(string x) {
      this.x = x;
      pMethode(x);
      pMethode1(x); // methode non définie donc instruction supprimée
    }
    // déclaration d'une méthode partielle
    partial void pMethode(string s);
    partial void pMethode1(string s);

  }
}
