# STRUCT vs CLASS

**struct** vs **class** = **type de valeur** vs **type de référence**

## classe : référence

Une instance de classe est une variable qui contient une référence vers l'objet d'origine stocké en mémoire.
Cette référence peut être copiée dans une autre variable qui pointera vers le même objet d'origine dans la mémoire.
Deux variables peuvent donc pointer vers le même objet en mémoire.

## struct : valeur

Une instance de struc est une variable contenant directement les données du struct.
N'ayant pas de référence, la copie d'une instance correspond à la copie complète des données.
Deux variables ne peuvent donc pas pointer vers le même objet en mémoire.


