namespace teststruc {

// struct permet de déclarer des attributs sans pouvoir leur donner de valeur lors de la création de l'instance
// l'initiasation se fait ultérieurement
struct Adresse {
  // attribut
  public long numero ;
}
}
