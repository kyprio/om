namespace teststruc {

// struct permet de déclarer des attributs sans pouvoir leur donner de valeur lors de la création de l'instance
// l'initiasation se fait ultérieurement
struct Client {
  // attribut
  public int code ;
  public string nom ;
  public string prénom ;
  public Adresse adresse;
}
}
