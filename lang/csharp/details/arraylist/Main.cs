using System.Collections;

class main
{
  static void Main(string[] args)
  {
    // attribut
    // nouvelle instance ArrayList contenant des string
    ArrayList liste = new ArrayList(); 
    
    // ajout d'élément string
    liste.Add("mot0");
    liste.Add("mot99");
    liste.Add("mot1");
    
    // suppression d'un élément via son indice
    liste.Remove(1);
    
    // affichage de liste tel quel
    //  [mot0, mot1]
    System.Console.WriteLine("# affichage de liste tel quel");
    System.Console.WriteLine(liste);

    
    // parcours de la liste sans index
    // mot0
    // mot1 
    System.Console.WriteLine("# parcours de la liste sans index");
    foreach(string e in liste) {
      System.Console.WriteLine(e); }

    // parcours de la liste avec index
    System.Console.WriteLine("# parcours de la liste avec index");
    int i;
    for(i=0;i<liste.Count;i++) {
      System.Console.WriteLine("Element[" +i+"] : " + liste[i]); }
  }
}
