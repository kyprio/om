using System.IO; 
class main
{
  static void Main(string[] args)
  {
    string filepath = "fichier.txt";

    // écrase ou créé un fichier
    using (StreamWriter f = File.CreateText(filepath)) {
      System.Console.WriteLine("rouge >> " + filepath);
      f.WriteLine("rouge");
      System.Console.WriteLine("bleu >> " + filepath);
      f.WriteLine("bleu");
    }
  }
}
