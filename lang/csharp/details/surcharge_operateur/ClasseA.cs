// NE FONCTIONNE PAS

namespace operateur {
class ClasseA {
  // attribut
  public string x{get;set;}
  public string y{get;set;}
  
  // constructeur
  public ClasseA(){}
  public ClasseA(string x, string y) {
    this.x = x;
    this.y = y;
  }
  
  // surcharge d'opérateur
  public static ClasseA operator +(ClasseA ca1,ClasseA ca2)
  { ClasseA ca = new ClasseA() ;
    ca.x = ca1.x + ca2.x;
    ca.y = ca1.y +ca2.y;
    return ca ;
  }
  
}
}

