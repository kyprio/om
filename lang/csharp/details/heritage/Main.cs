
class main
{
  public static void show1(Mere o1) { o1.ShowStd(); }
  public static void show2(Mere o1) { o1.ShowVirtual(); }
  public static void show3(Mere o1) { o1.ShowSealed(); }

  static void Main(string[] args)
  {
    // attribut
    Mere m1 = new Mere();
    Fille f1 = new Fille("GrandeNinie", "coquette");
    SsFille s1 = new SsFille("Mini-Ninie", "minipouce");

    // execution
    System.Console.WriteLine("  public static void show1(Mere o1) { o1.ShowStd(); }");
    show1(m1);
    show1(f1);
    show1(s1);

    System.Console.WriteLine("  public static void show2(Mere o1) { o1.ShowVirtual(); }");
    show2(m1);
    show2(f1);
    show2(s1);

    System.Console.WriteLine("  public static void show3(Mere o1) { o1.ShowSealed(); }");
    show3(m1);
    show3(f1);
    show3(s1);
  }
}
