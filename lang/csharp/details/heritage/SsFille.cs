public class SsFille:Fille {
  // attribut supplémentaire

  // constructeur avec héritage
  public SsFille(string nom, string x):base(nom, x) { }
  
  // redéfinition d'une classe existante
  public new void ShowStd(){
    System.Console.WriteLine("ShowStd SsFille ( {0} {1} )", nom, x);
  }
  // redéfinition d'une classe existante
  public override void ShowVirtual(){
    System.Console.WriteLine("ShowVirtual SsFille ( {0} {1} )", nom, x);
  }
  
  /* tentative de redéfinition d'une classe sealed au niveau au dessus :
  error CS0239: `SsFille.ShowSealed()':
  cannot override inherited member `Fille.ShowSealed()' because it is sealed

  public sealed override void ShowSealed(){
    System.Console.WriteLine("ShowSealed SsFille ( {0} {1} )", nom, x);
  }
  */
  // malgé la redéfinition par new suivante, la méthode n'est pas prise compte
  public new void ShowSealed(){
    System.Console.WriteLine("ShowSealed SsFille ( {0} {1} )", nom, x);
  }

}
