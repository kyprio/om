public class Fille:Mere {
  // attribut supplémentaire
  protected string x ;

  // constructeur avec héritage
  public Fille(string nom, string x):base(nom) {
  this.x = x;
  }
  
  // redéfinition d'une classe existante
  public new void ShowStd(){
    System.Console.WriteLine("ShowStd Fille ( {0} {1} )", nom, x);
  }
  // redéfinition d'une classe existante
  public override void ShowVirtual(){
    System.Console.WriteLine("ShowVirtual Fille ( {0} {1} )", nom, x);
  }
  
  // redéfinition d'une classe existante
  // sealed bloque la redéfinition par les sous-classes
  public sealed override void ShowSealed(){
    System.Console.WriteLine("ShowSealed Fille ( {0} {1} )", nom, x);
  }
}
