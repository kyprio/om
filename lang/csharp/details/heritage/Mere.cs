public class Mere:GrandMere {
  
  // constructeur
  // paramètres facultatifs
  public Mere (string nom = "Maman"):base() {
  this.nom = nom;
  }

  // methodes standard
  public void ShowStd(){
    System.Console.WriteLine("ShowStd Mere ( {0} )", nom);
  }
  // methodes heritable
  public virtual void ShowVirtual(){
    System.Console.WriteLine("ShowVirtual Mere ( {0} )", nom);
  }
  
  // methodes heritable
  public virtual void ShowSealed(){
    System.Console.WriteLine("ShowSealed Mere ( {0} )", nom);
  }

}
