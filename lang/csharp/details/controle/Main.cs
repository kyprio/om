class main
{
    static void Main(string[] args)
    {
      // System.Console.WriteLine("");

      // attribut
      string string1 = "texte";
      string string2 = "texte \"bonjour\"";
      string string3 = "texte";
      int num = 10 ;
      string[] list1 = new string[5] { "el0", "el1", "el2", "el3", "el4" };

      // IF ELSE
      if ( string1 == string2 ) { System.Console.WriteLine("string 1 = string2"); }
      else if ( string1 == string3 ) { System.Console.WriteLine("string 1 = string3"); }
      else { System.Console.WriteLine("string1 != string2 et string3");}
      System.Console.WriteLine("");

      // WHILE
      // test condition puis execute
      // affiche 10 9 8 7 6 5 4 3 2 1 0
      num = 10;
      System.Console.WriteLine("num AVANT while : " + num);
      while ( num >= 0 ) {
        System.Console.WriteLine("num dans while : " + num);
        num--;
      }
      System.Console.WriteLine("num APRES while : " + num);
      System.Console.WriteLine("");

      // DO WHILE
      // execute puis test condition
      // affiche -1
      System.Console.WriteLine("num AVANT do while : " + num);
      do {
        System.Console.WriteLine("num dans do while : " + num);
        num--;
      } while ( num >= 0 );
      System.Console.WriteLine("num APRES do while : " + num);
      System.Console.WriteLine("");

      // SWITCH
      string1 = "texte2";
      switch (string1) {
        case "texte":
          System.Console.WriteLine("switch string1 = texte");
          break;
        case "texte2":
          System.Console.WriteLine("switch string1 = texte2");
          break;
        default:
          System.Console.WriteLine("switch string1 = *");
          break;
      } 
      System.Console.WriteLine("");

      // FOR
      // affiche 10 9 8 7 6 5 4 3 2 1 0
      for ( int i = 10; i >= 0; i--) {
        System.Console.WriteLine("i dans for : " + i);
      }
      System.Console.WriteLine("");

      // FOREACH tableau
      foreach(string e in list1) { 
        System.Console.WriteLine("e de list1 : " + e);
      }
      System.Console.WriteLine("");

    }
}
