using System;
class main
{
    // methodes statique typées
    public static int addition(int int1, int int2)
    { return int1+int2; }
   
    // methodes statique typées dynamiquement (typage exact non connu avant l'exécution de l'application)
    public static dynamic operation(dynamic o1, dynamic o2)
    { return o1+o2;}

    // methode multiple params
    public static double moyenne(params double[] notes) {
      double sommes = 0;
      foreach ( double i in notes )
      { sommes += i; }
      return sommes/notes.Length;
    }

    static void Main(string[] args)
    {
    
    // attribut
    int int1 = 1;
    int int2 = 2;
    dynamic o1 = 3;
    dynamic o2 = 4;
    double[] notes = { 12, 13, 17, 11 };

    // execution
    Console.WriteLine("Addition int1 et int2 : {0}" , addition(int1,int2));
    Console.WriteLine("Opération de o1 et o2 : {0}" , operation(o1,o2));
    Console.WriteLine("Moyenne des notes : {0}" , moyenne(notes));
    }
}
