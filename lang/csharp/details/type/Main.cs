class main
{
    enum Day { Sun, Mon, Tue, Wed, Thu, Fri, Sat };

    static void Main(string[] args)
    {
      // System.Console.WriteLine("var : " + var);
      // chaine et manipulation de chaine
      char char1 = 'b';
      System.Console.WriteLine("char1 : " + char1);
      string string1 = "Texte \"Bonjour\"";
      System.Console.WriteLine("string1 : " + string1);
      string string2 = "Texte";
      System.Console.WriteLine("string2 : " + string2);
      System.Console.WriteLine("string2 : " + string2.Substring(1,2));
      System.Console.WriteLine("string2 : " + string2.ToUpper());
      System.Console.WriteLine("string2 : " + string2.ToLower());
      System.Console.WriteLine("string2 : " + string2.Replace("ex","ê"));

      // nombre et manipulation de nombre
      int int1 = 1;
      System.Console.WriteLine("int1 : " + int1);
      float float1 = 1.2f;
      System.Console.WriteLine("float1 : " + float1);
      double double1 = 1.2;
      System.Console.WriteLine("double1 : " + double1);
      string tvaString = "18.6";
      float tva = float.Parse(tvaString);
      System.Console.WriteLine("tva : " + tva);

      // le type "var" pour un typage détecté à l'assignation
      // le type est ensuite figé
      var var1 = 1;
      System.Console.WriteLine("var1 : " + var1);

      // constante
      const int constint1 = 100;
      System.Console.WriteLine("constint1 : " + constint1);

      // enumération

    {
        int x = (int)Day.Sun;
        int y = (int)Day.Fri;
        System.Console.WriteLine("Sun = {0}", x);
        System.Console.WriteLine("Fri = {0}", y);
    }

  }
}
