using System;
class main
{
  static void Main(string[] args)
  {
    // attribut
      // identification
    Personne pere= new Personne((string)"dupond".ToUpper(),"Paul");
    Personne enfant1 =new Personne(pere.nom,"Pascal");
    Personne enfant2=new Personne(pere.nom,"Marc");
      // age
    pere.date_naiss = new DateTime(1954,12,23);
    enfant1.date_naiss = new DateTime(1979,10,5);
    enfant2.date_naiss = new DateTime(1982,4,18);
      // indexation des enfants au pere
    pere[0] = enfant1;
    pere[1] = enfant2;

      // execution
    Console.WriteLine("M.{0} {1} né le {2} a {3} ans", pere.nom, pere.prenom, pere.date_naiss, pere.age );
    Console.WriteLine("{0} {1} qui a {2} ans", pere[0].nom, pere[0].prenom, pere[0].age);
    Console.WriteLine("{0} {1} qui a {2} ans", pere[1].nom, pere[1].prenom, pere[1].age);
  }
}
