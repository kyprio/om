using System;

public class Personne {
  // attribut
  private string leNom;
  private string lePrenom;
  private DateTime laDate_naiss;
  
  // une personne est potentiellement parent :
  private Personne[] Famille = new Personne[10];

  // constructeur
  public Personne() {}
  public Personne(string nom, string  prenom) {
    leNom = nom;
    lePrenom = prenom;
  }
  
  // indexeur
  // tableau d'instances de la classe
  public Personne this[int index] {
    get { return Famille[index]; }
    set { Famille[index] = value; }
  }

  // accesseur 
	public string nom {
		get { return leNom; }
		set { leNom=value; }
	}
	public string prenom {
		get { return lePrenom ;}
		set { lePrenom = value;}
	}
  public DateTime date_naiss {
    get { return laDate_naiss;}
    set { laDate_naiss = value;}
  }

  // methode
  public int age {
    get { return DateTime.Now.Year - laDate_naiss.Year; }
  }
  
  // toString
  public override string ToString() { return (lePrenom + " " + leNom); }

}
