class ClasseA {
  // attribut simple
  private int _x;
  
  // attribut et getter setter simple
  public string y{get;set;}
  
  // constructeur
  // constructeur avec valeur par defaut
  public ClasseA(int x = 50)
  { this._x = x;}
  
  // methode getter setter
  // les termes "get" "set" et "value" sont des mot clés
  public int x {
    get { return this._x; }
    set { _x = value; }
  }

}
