class main
{
  static void Main(string[] args)
  {
    // attribut
    ClasseA a1 = new ClasseA();
    ClasseA a2 = new ClasseA(10);
    a1.y = "choux";
    a2.y = "potiron";

    // execution
    System.Console.WriteLine("a1.x = {0}, a1.y = {1}" , a1.x, a1.y );
    System.Console.WriteLine("a2.x = {0}, a2.y = {1}" , a2.x, a2.y );
  }
}
