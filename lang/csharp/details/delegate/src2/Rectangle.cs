class Rectangle
{
private uint _aire;
public uint Longueur {get; set;}
public uint Largeur {get; set;}
public uint Aire { get { return (_aire); } }
public Rectangle(uint longueur, uint largeur)
{
Longueur = longueur;
Largeur = largeur;
_aire = Longueur * Largeur;
}
public static int CompareRectangle(Rectangle r1, Rectangle r2)
{ int valcomp=r1.Aire.CompareTo(r2.Aire);
if (valcomp==0)
if (r1.Longueur>r2.Longueur) valcomp=1;
return valcomp;
}
public override string ToString()
{
return ("Long:"+Longueur+" larg:"+Largeur+" Aire:"+Aire);
}
}
