using System.Collections.Generic;
using System;

class Program
{ static void Main(string[] args){
List<Rectangle> maList = new List<Rectangle>();
maList.Add(new Rectangle(10, 20));
maList.Add(new Rectangle(10, 10));
maList.Add(new Rectangle(1, 2));
maList.Add(new Rectangle(20, 10));
// Le délégué Comparaison pointera vers Rectangle.CompareRectangle

maList.Sort(Rectangle.CompareRectangle);
foreach (Rectangle rect in maList) Console.WriteLine(rect);
// exemple de methode anonyme ( delégué sur methode non nommée)
maList.Sort(
delegate(Rectangle r1,Rectangle r2)
{return(r1.Aire.CompareTo(r2.Aire)); }
);
}
}
