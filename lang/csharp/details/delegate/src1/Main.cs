using System;
class main {

  // déclaration du délégué
  public delegate int Operation(int x);

  // methodes nommées ayant la même signature que le délégué
  static int Double(int x){ return x*2; }
  static int Triple(int x){ return x*3; }

  static void Main() {
      // instanciation des délégués
      Operation opDouble = Double;
      Operation opTriple = Triple;
      // instanciation d'une déléguée d'une méthode anonyme
      Operation opSquare = delegate(int x) {return x*x;};
      // Instantiate delegate with lambda expression
      Operation opCube = s => s * s * s;

      // appel des objets dérivés
      Console.WriteLine("opDouble(4) : " + opDouble(4));
      Console.WriteLine("opTriple(4) : " + opTriple(4));
      Console.WriteLine("opSquare(4) : " + opSquare(4));
      Console.WriteLine("opCube(4) : " + opCube(4));

  }
}
