public class main {
    // enum est une constante de donnée indexée
    enum Day { Sun, Mon, Tue, Wed, Thu, Fri, Sat };

    // enuma avec modification des index par defaut
    // chaque élement est incrémenté de +1 sur l'élément précédent
    enum Nombre { un, deux, dix = 10, onze, douze, vingt =20 , vingtetun };

    static void Main()
    {
        System.Console.WriteLine("(int)Day.Sun -> " +  (int)Day.Sun);
        System.Console.WriteLine("(int)Nombre.dix -> " +  (int)Nombre.dix);
        System.Console.WriteLine("(int)Nombre.onze -> " +  (int)Nombre.onze);
    }
}
