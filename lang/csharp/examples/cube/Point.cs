using System;
using static  System.Math;
namespace cube {
class Point {
  // attribut simple
  public double x{ get; set; }
  public double y{ get; set; }
  public double z{ get; set; }
  
  // constructeur
  // constructeur avec valeur par defaut
  public Point(double x = 0, double y = 0, double z = 0) {
    this.x = x;
    this.y = y;
    this.z = z;
  }
  public Point(Point pt1) {
    this.x = pt1.x;
    this.y = pt1.y;
    this.z = pt1.z;
  }

  // toString
  public override string ToString() {
    return "x = " + x.ToString() + " y = " + y.ToString() + " z = " + z.ToString() ;
  }

  // surchage opérateur
  public static Point operator +(Point pt1, Point pt2) {
    Point ptr = new Point( pt1.x + pt2.x ,pt1.y + pt2.y , pt1.z + pt2.z );
    return ptr;
  }
  public static bool operator ==(Point pt1, Point pt2) {
    return ( pt1.x == pt2.x && pt1.y == pt2.y && pt1.z == pt2.z ) ;
  }
  public override bool Equals(Object obj ) {
    Point pt2 = obj as Point;
    return ( x == pt2.x && y == pt2.y && z == pt2.z ) ;
  }
  public static bool operator !=(Point pt1, Point pt2) {
    return ( pt1.x != pt2.x || pt1.y != pt2.y || pt1.z != pt2.z ) ;
  }

  public override int GetHashCode() {
    return (int)x^(int)y^(int)z; 
  }


 // opération de positionnement
  public void translation(double alpha, double beta, double gamma){
    x = x + alpha;
    y = y + beta;
    z = z + gamma;
  }

  public void rotation(double alpha, double beta, double gamma){
    const double PI = 3.14;
    alpha = alpha * PI/180;
    beta = beta * PI/180;
    gamma = gamma * PI/180;
    // selon x
    y = (short)(Math.Cos(alpha)*y - Math.Sin(alpha)*z);
    z = (short)(Math.Sin(alpha)*y + Math.Cos(alpha)*z);
    // selon y
    x = (short)(Math.Cos(beta)*x+Math.Sin(beta)*z);
    z = (short)(-Math.Sin(beta)*x+Math.Cos(beta)*z);
    // selon z
    x = (short)(Math.Cos(gamma)*x - Math.Sin(gamma)*y);
    y = (short)(Math.Sin(gamma)*x + Math.Cos(gamma)*y);
  } 
}
}
