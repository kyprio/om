class main {

  // methode affichage
  public static void affichageTableau(params int[] tab) {
    System.Console.Write("tableau : ");
    foreach (int e in tab) {System.Console.Write(e + " "); }
    System.Console.WriteLine("");
  }

  // methode d'alteration
  public static void Changed(int[] tab) {
    tab[0] = 888; 
    affichageTableau(tab);
  }

  static void Main(string[] args) {   

    // attribut
    int[] tab1 = {0, 1, 2};
    
    // execution
    // passage du tableau (par default en référence)
    System.Console.WriteLine("initialisation tab1[]");
    affichageTableau(tab1); 
    System.Console.WriteLine("modification  par passage en référence tab1[]");
    Changed(tab1);
    System.Console.WriteLine("après modification  par passage en référence tab1[]");
    affichageTableau(tab1) ;
  }   
}
