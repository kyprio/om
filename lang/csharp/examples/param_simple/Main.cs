using System;
class main
{
  // methode récupérant une valeur
  static int  valeur(int a) {
  a = a * 2;
  return a;
  }
  
  // methode récupérant une référence
  static int reference(ref int a) {
    a = a * 2;
    return (a);
  }

  static void Main(string[] args) {   

    int a = 10;
    Console.WriteLine("valeur de a");
    Console.WriteLine(valeur(a));
    Console.WriteLine(valeur(a));
    Console.WriteLine("reference de a");
    Console.WriteLine(reference(ref a));
    Console.WriteLine(reference(ref a));
  }   
}
