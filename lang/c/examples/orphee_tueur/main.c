#include <stdio.h>
#include <signal.h>
#include <stdlib.h>
#include <sys/wait.h>
#include <sys/types.h>
#include <unistd.h> // pid
int main()
{
  // execution

  pid_t pid_enf;
  printf("process[%d][%d]\n",getpid(),getppid());
 
  switch(pid_enf=fork()) {
    case -1:
      perror("fork");
      return(EXIT_FAILURE);
    case 0:
      printf("[%d][%d] je suis enfant\n",getpid(),getppid());
      kill(getppid(), SIGKILL);
      break;
    default:
      printf("[%d][%d] je suis parent de %d\n",getpid(),getppid(),pid_enf);
      wait(NULL);
      break;
  }

  printf("je suis %d\n",getpid());
  return 0;
}
