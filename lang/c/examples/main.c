#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <unistd.h>

int main ()
{
  char chaine[20];
  printf("Tapez une phrase : ");
  ( fgets(chaine, sizeof chaine, stdin) ) ? printf(" ") : printf("erreur lors de la saisie");

  int fd[2];
  pid_t pid_enf;
  char readbuffer[80];

  // création du pipe avant l'enfant
  pipe(fd);

  // création de l'enfant et test
  switch(pid_enf = fork()) {
    case -1:
      // SI pb de création ALORS quitter
      perror("erreur de création de l'enfant");
      return(1);

    case 0 :
      // SI enfant ALORS écrire le pipe
      // fermeture de l'ouverture inutilisée
      close(fd[0]);
      printf("[%d] envoit : %s",getpid(),chaine);
      write(fd[1], chaine, (strlen(chaine)+1));
      return(0);

    default:
      // SI parent ALORS lire le pipe comme un fichier standard
      // fermeture de l'ouverture inutilisée
      close(fd[1]);
      read(fd[0], readbuffer, sizeof(readbuffer));
      printf("[%d] recoit : %s",getpid(),readbuffer);
      break;
  }

  return(0);
}
