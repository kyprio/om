#include <stdio.h>
#include <string.h>

static void search(char *chaine) {
    char *p = strchr(chaine, '\n');
    if ( p ) { *p = 0 ; }
}
int main (void)
{
    char chaine[20];
    printf("Tapez une phrase : \n");
    // fgets renvoi un valeur positive qu'il convient de traiter un minima
    ( fgets(chaine, sizeof chaine, stdin) ) ? printf("success") : printf("error");
    search(chaine);
    printf("Vous avez tape : '%s'\n", chaine);
    return 0;
}
