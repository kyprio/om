#include <stdio.h>
#include <unistd.h> // pid
int main()
{
  // variable
  pid_t pid_enf;
  // execution
	pid_enf = fork();

  if (pid_enf == 0) {
    printf("Je suis enfant %d\n", getpid());
  } else {
    printf("Je suis parent %d de %d\n", getpid(), pid_enf );
  }

  return 0;
}
