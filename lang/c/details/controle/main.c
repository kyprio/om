// include des libs
#include <stdio.h>

int main()
{

  // attribut
  int a,b, c;
  a = 30;
  b = 20;
 
  // test posibles
  // == != <= >=

  // test court
  (a == b) ? printf("a==b \n") : printf("a!=b \n");
  
  // test court et affectation variable
  c = (a == b) ? 30 : 20 ;
  printf("c = %d \n",c);

  // IF ELSE
  if (a == b) {
    printf("a==b \n") ;
  } else {
    printf("a!=b \n");
  }

  // IF ELSE IF
  if (a == b) {
    printf("a==b \n") ;
  } else if ( a <= b ){
    printf("a<=b \n");
  } else if ( a >= b ){
    printf("a>=b \n");
  }

  // SWITCH
  switch(a) {
    case 10 : 
      printf("a = 10\n");
      break;
    case 20 :
      printf("a = 20\n");
      break;
    case 30 :
      printf("a = 30\n");
      break;
  }


  // fin execution
  return 0;
}

