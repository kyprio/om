// include des libs
#include <stdio.h>
#include <math.h>

int main()
{
  // calcul simple
  // + - * / %
  int a = 10 * 6 / 3 + 20 - 2 ;
  printf("\n%-15s = %d \n", "10*6/3+20-2", a);

  // calcul au niveau binaire
  // cf wiki mathematique.md
  // & | ^ ET logique OU logique OU exclusif
  // >>= <<= décalage au niveau binaire
  int z;
  z = 4&6; // 2
  z = 6|4; //
  printf("\n%-15s = %d \n", "z", z);
  
  // incrementation
  int i,j;
  i = 0; // i = 0 ; j = (null)
  i++; //  i = i + 1 = 1
  j = i++; // j = i = 1 ; i = i+1 = 2
  j = ++i; // i = i+1 = 3 ; j = i
  printf("\n%-15s = %d \n", "i", i);
  printf("%-15s = %d \n", "j", j);
  
  // incrementation avancé
  // += -= *= /=
  // &= |= ^=
  // >>= <<=

  // representation décimal
  int m = 11 ;
  m = 0b1011; // binaire
  m = 0xB; // hexadécimal
  printf("\n%-15s = %X \n", "m hexa", m);
  printf("%-15s = %o \n", "m octale", m);
  printf("%-15s = %d \n", "m decimal", m);

  // calcul comlexe
  // via math.h
  double valeur = 42.42;
  printf("Valeur :               : %23.2f \n", valeur);
  printf("Arrondie inférieur     : %23.2f \n", ceil(valeur));
  printf("Arrondie supérieur     : %23.2f \n", floor(valeur));
  printf("Racine                 : %23.2f \n", sqrt(valeur));
  printf("Carré                  : %23.2f \n", pow(valeur,2));
  printf("Puissance              : %23.2f \n", pow(valeur,2.1));
  printf("Exponenetielle         : %23.2f \n", exp(valeur));
  printf("Log                    : %23.2f \n", log(valeur));
  printf("Log népérien           : %23.2f \n", log(valeur));
  printf("Log base 10            : %23.2f \n", log10(valeur));
  
  printf("\n");
  printf("Valeur de pi M_PI      : %22.20f \n", M_PI);
  printf("Valeur de pi/4 M_PI_4  : %22.20f \n", M_PI_4);

  printf("\n");
  printf("Cosinus pi/4           : %f \n",cos(M_PI_4));
  printf("Sinux pi/4             : %f \n",sin(M_PI_4));
  printf("Tangeante pi/4         : %f \n",tan(M_PI_4));
  printf("aCosinus pi/4          : %f \n",acos(M_PI_4));
  printf("aSinux pi/4            : %f \n",asin(M_PI_4));
  printf("aTangeante pi/4        : %f \n",atan(M_PI_4));

  // fin execution
  return 0;
}

