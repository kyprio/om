// include des libs
#include <stdio.h>
// include du type bool
#include <stdbool.h>


int main()
{
  // attribut et types

  // void : type ne renvoyant rien (utilise pour les fonctions procédures)

  int int1 = 1;
  unsigned int int2 = 1;
  short short1 = 1;
  unsigned short short2 = 1;
  long long1 = 1;
  unsigned long long2 = 1;
  long long longlong1 = 1;
  unsigned long long longlong2 = 1;
  
  float float1 = 1.444444;
  double double1 = 1.444444;

  char char1 = 'a' ; // encodage en ASCII par defaut
  unsigned char char2 = 'a' ;
  char string1[20] = "Liste des variables";
  const char author[10] = "Kyprio"; // constante

  bool bool1 = true ; // true = 1 ; false = 0
  bool bool2 = 1 ;
  // execution

printf("_ %-20s _\n", string1); // %-20s string sur zone fixe de 20 caractères

printf("\n%-10s : %s (%lu bits) \n", "author", author, sizeof(author));
printf("%-10s : %d (%lu bits) \n", "int1", int1, sizeof(int1));
printf("%-10s : %u (%lu bits) \n", "int2",int2, sizeof(int2));
printf("%-10s : %d (%lu bits) \n", "short1",short1, sizeof(short1));
printf("%-10s : %d (%lu bits) \n", "short2",short2, sizeof(short2));
printf("%-10s : %ld (%lu bits) \n", "long1",long1, sizeof(long1));
printf("%-10s : %ld (%lu bits) \n", "long2",long2, sizeof(long2));
printf("%-10s : %lld (%lu bits) \n", "longlong1",longlong1, sizeof(longlong1));
printf("%-10s : %lld (%lu bits) \n", "longlong2",longlong2, sizeof(longlong2));
 
printf("\n%-10s : %lf (%lu bits) \n", "float1",float1, sizeof(float1));
printf("%-10s : %lf (%lu bits) \n", "double1",double1, sizeof(double1));

printf("\n%-10s : %c (%lu bits) \n", "char1",char1, sizeof(char1));
printf("%-10s : %d (%lu bits) \n", "char1",char1, sizeof(char1));
printf("%-10s : %c (%lu bits) \n", "char2",char2, sizeof(char2));
printf("%-10s : %d (%lu bits) \n", "char2",char2, sizeof(char2));

printf("\n%-10s : %s (%lu bits) \n", "bool1", bool1 ? "true" : "false", sizeof(bool1));
printf("%-10s : %s (%lu bits) \n", "bool2", bool2 ? "true" : "false", sizeof(bool2));

  // fin execution
  return 0;
}

