#  HELLO WORLD

###      Hello world basique

    #include <stdio.h>
    
    int main(int argc, char *argv[])
    {
      printf("hello, world\n");
      return 0;
    }



###      Hello world avance

# ECRITURE ET EXECUTION DU CODE
###      shebang

n/a

###      extension des fichiers

programme.c : contient le code source et la déclaration des fonctions

programme.h : contient les prototypes des fonctions déclaréés

###      compilation et permissions

paquets néccessaires : build-essential

####        compilation rapide

    gcc main.c fic1.c -o nom_programme

####        compilation détaillée


assemblage des instructions pré-processeur :

    gcc -c main.c -o main.o
    gcc -c fic1.c -o fic1.o

compilation des fichiers .o (-01 à -06 = niveau d'optimisation)

    gcc -06 main.o fic1.o -o nom_programme


###      execution

#  SPECIFICITES

#  SYNTAXE

###      Instuctions

####        ligne par ligne

    instruction1;
    instruction2;

####        enchainement sur une ligne

    instruction1; instruction2;

####        instruction découpée sur plusieurs ligne

###      Commentaires

####        en une ligne

    // commentaire en une ligne

####        multilignes

    /* commentaire
    multiligne */

###      include
###      execution code externe
####        fichier
####        commande d'un autre langage
###      gestion des interprétations des valeurs
####        textes simples
####        variables
####        commandes
####        intepretation complète de l'instruction avant execution
###      sortie
####        sans code de retour
####        avec code de retour
###      options debug

#  MEMOIRE

##    VARIABLES

###      Appel variable

####        comme valeur

  printf(var1);

####        comme commande ou instruction

###      Creation variables

####        declaration et typage

    int a;
    unsigned short i;
    signed short i;

| type | taille minimale | description |
|------|-----------------|-------------|
| char | 1 octet         | texte       |
| short| 2 octets        | short int   |
| int  | 2 octets        | entier -32768 à 32767 |
| long | 4 octets        | long int    |
| long long | 8 octets   | long long int |
| signed ... |               | par defaut pour int/log/short |
| unsigned ... |             | unsigned int / unsigned short ... |
| float |                 | reel (non recommandé pour les calculs |
| double |               | reel 0.1 (recommandé pour les calculs) |
| long double |          | reel O.11 |
| bool |                 | true or false |

####        creation manuelle simple

    a = 40;

    int a = 40;

    int a=0, b=2, c=20;

    char i = 'a';
    char i = 97 ;

####        creation à vide

    int var = 0;

####        creation manuelle a partir de texte et de variable
####        creation par retour de commande
####        creation par saisie utilisateur
####        creation avec valeur par defaut
####        creation variable de variable
####        suppression
###      Portee
####        globale
####        export vers shell enfant
####        locale
###      methodes communes aux variables
####        renvoi de valeur par defaut sans creation de variable
####        renvoi d'une valeur si test sur variable
###      Variables environnement et shell/processus courant
####        liste variables environnement
####        liste variables shell/processus courant
###      Parametres
####        liste des variables parametres
####        fonctions et options parametre
####        parcours des arguments
##    CONSTANTES
###      Creation constante

    const int max = 10;

##    REFERENCES
##    POINTEURS

#  TYPES
##    STRING
####        creation
####        conversion
####        attributs
####        methodes
####        operateur
##    INTEGER
####        appel
####        creation
####        conversion
####        attributs
####        methodes
#####          calcul
#####          incrementation decrementation
####        operateur
##   FLOAT DOUBLE
####        appel
####        creation
####        conversion
####        attributs
####        methodes
####        operateur
##   BOOLEAN
####        appel
####        creation
####        conversion
####        attributs
####        methodes
####        operateur
##   TABLEAU
###      liste
####        appel
####        parcours des valeurs
####        creation
####        conversion
####        attributs
####        methodes
####        operateur
###      dictionnaire clés valeur
####        appel
####        parcours des valeurs
####        creation
####        conversion
####        attributs
####        methodes
####        operateur
###      tableau de tableau
####        appel
####        parcours des valeurs
####        creation
####        conversion
####        attributs
####        methodes
####        operateur
###      matrice
####        appel
####        parcours des valeurs
####        creation
####        conversion
####        attributs
####        methodes
####        operateur
##    FICHIER
####        appel en mémoire
####        parcours des lignes
####        ouverture
####        lecture
####        fermeture
####        ecriture
####        attributs
####        methodes

#  STRUCTURE CONDITIONNELLES
##    TEST ET CODE DE RETOUR
###      code de retour
###      syntaxe de la structure test
####        test simple sans consequence
####        test simple AND - si vrai alors
####        test simple OR - si faux alors
####        test complet
####        combinaison de test AND - test 1 si vrai alors test 2
####        combinaison de test OR - test 1 si faux alors test 2
####        inverse du test
###      operateurs de comparaison et de test
####        chaine de caractere
####        nombre
####        boolean
####        variables objets
####        fichiers
##    IF
##    IF ELSE
##    IF ELSE IF
##    SWITCH CASE

#  BOUCLES ET PARCOURS
##    FOR
###      boucle simple
###      parcours d'une sequence
##    FOREACH
##    WHILE
##    DO WHILE
#    UNTIL

#  FONCTIONS
 
###      liste fonctions de base

####        affichage

#####          affichage texte

    printf("texte");

#####          affichage décimal

    printf("%d", a);
    printf("%d %d", a, b);

####        saisie utilisateur

###      appel
####        sans paramètre
####        avec paramère non nommé
####        avec paramètre nommé
###      creation
####        creation fonction courte
####        creation fonction
#####          renvoyant un valeur
#####          ne renvoyant aucune valeur
####        attendant des parametres non nommés
####        attendant des parametres nommés
####        attendant un nombre non défini de parametres nommé ou non nommé
###      creation prototype

pré-déclaration de fonction dans le fichier.h sans son corp qui est place dans le fichier.c

    int nom_fonction(type var1, type var2);

# EXCEPTIONS
##    TRY CATCH

# PROGRAMMATION ORIENTEE OBJET
##    CLASSES
###      initialisation objet
###      suppression
###      appel
###      creation nouvelle classe
####        heritage
####        constructeur
####        attribut
####        methode
####        surcharge d'attributs
####        surcharge de methode
