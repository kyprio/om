SET SERVEROUTPUT ON 

/* CREATION TYPE TABLEAU  */
DECLARE
  /* Création d'un type tableau d'entier à 10 case mémoire max */
  TYPE numberTab IS VARRAY (10) OF NUMBER;


  /* déclaration d'objet tableau */
  /* 10 cases mémoires sont réservées mais aucune n'est utilisée */
  t numberTab;
  a numberTab;

BEGIN

  /* instanciation sans allocation de valeur/case mémoire*/
  a := numberTab();
  t := numberTab();

  /* instanciation et affectation des valeurs directement */
  a := numberTab(1,2,3,4,5,6,7,8,9,10);
  
  /* Allocation de 4 case mémoire sur les 10 maximum réservées */
  t.EXTEND(4);
  /* affectation des valeurs de 1 à 4 */
  t(1) := 30;
  t(2) := 20;
  t(3) := 20;
  t(4) := 40;

  DBMS_OUTPUT.PUT_LINE(a(1));
  DBMS_OUTPUT.PUT_LINE(t(1));

  /* parcours de tableau cf controle LOOP */
  FOR i IN t.FIRST .. t.LAST LOOP
    DBMS_OUTPUT.PUT_LINE(t(i));
  END LOOP;

END;
/
