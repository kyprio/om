SET SERVEROUTPUT ON 

DECLARE
  
  -- déclaration de variable prenant le type primitif des champs de la table (typpage implicite)
  x TABLE1.champ1%type;
  -- déclaration de variable avec typage explicite
  i TABLE1.champ2 NUMBER := 50 ;

BEGIN

  -- déclaration et affectation du résultat (unique) dans une variabel chp3
  SELECT champ3 INTO y
  FROM TABLE1
  WHERE champ2 = cond_chp2 AND id < i;
  DBMS_OUTPUT.PUT_LINE(y || x);

END;
/
