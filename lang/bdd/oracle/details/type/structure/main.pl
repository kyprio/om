SET SERVEROUTPUT ON 

/* GENERATION D'UNE STRUCTURE FROM SCRATCH */

DECLARE

-- UNE STRUCTURE CONTIENT PLUSIEURS TYPES

  -- creation d'un type structuré
  TYPE point IS RECORD
  (
    /* liste des types contenus */
    x NUMBER,
    y NUMBER
  );

  -- declaration d'une variable structurée
  p point ;
BEGIN
  p.x := 1;
  p.y := 1;
 
  DBMS_OUTPUT.PUT_LINE('p.x : ' || p.x);
  DBMS_OUTPUT.PUT_LINE('p.y : ' || p.y);

END;
/


/* GENERATION D'UNE STRUCTURE DEPUIS UNE LIGNE DE TABLE */

DECLARE

  ligne TABLE1%rowtype ;

BEGIN

  SELECT * INTO ligne
  FROM TABLE1
  WHERE champ1 = 'valeur';
  
  DBMS_OUTPUT.PUT_LINE(ligne.id);

END;
/
