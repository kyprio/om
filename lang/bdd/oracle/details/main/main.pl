/* OPTION : ici active la sortie sur terminal*/
SET SERVEROUTPUT ON 

/* DECLARATION DES TYPES ET OBJETS*/
DECLARE

  c varchar2(15) := 'Hello World !';

/* INSTANCIATIONS ET INSTRUCTIONS */
BEGIN

  DBMS_OUTPUT.PUT_LINE(c);
  DBMS_OUTPUT.PUT_LINE(' texte : ' || c || 'fin');

/* FIN */
END;
/

