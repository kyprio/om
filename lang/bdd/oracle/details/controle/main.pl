SET SERVEROUTPUT ON 

DECLARE 
a NUMBER :=10;
BEGIN
dbms_output.put_line('a=10');


/* IF */
dbms_output.put_line('IF'); 
IF( a > 100 ) THEN
  dbms_output.put_line('a>100'); 
END IF;

/* IF ELSE */
dbms_output.put_line('IF ELSE'); 
IF( a > 100 ) THEN
  dbms_output.put_line('a>100'); 
ELSE
  dbms_output.put_line('a<=100'); 
END IF;

/* IF ELSIF ELSE */
dbms_output.put_line('IF ELSIF ELSE'); 
IF( a > 100 ) THEN
  dbms_output.put_line('a>100'); 
ELSIF(a = 100) THEN
  dbms_output.put_line('a=100'); 
ELSE
  dbms_output.put_line('a<=100'); 
END IF;

/* SWITCH CASE */
dbms_output.put_line('CASE'); 
CASE a
	WHEN 10 THEN
  	dbms_output.put_line('a=10'); 
	WHEN 100 THEN
  	dbms_output.put_line('a=100'); 
	ELSE
  	dbms_output.put_line('a!=10 a!=100'); 
END CASE;


/* DO WHILE LOOP */
dbms_output.put_line('LOOP'); 
LOOP
  dbms_output.put_line(a); 
	a := a + 1;
	EXIT WHEN a = 15;
END LOOP ;

/* WHILE */
dbms_output.put_line('WHILE'); 
WHILE a < 100 LOOP
  dbms_output.put_line(a); 
	a := a + 10;
END LOOP ;

/* FOR SUR SEQUENCE FIXE */
dbms_output.put_line('FOR'); 
FOR i IN 1 .. 10 LOOP
  dbms_output.put_line(i); 
END LOOP;

/* FOR SUR INVERSE SEQUENCE FIXE */
dbms_output.put_line('FOR'); 
FOR i IN REVERSE 1 .. 10 LOOP
  dbms_output.put_line(i); 
END LOOP;

/* FOR parcours de tableau */
/* cf ../type/creation/ */

END;
/
