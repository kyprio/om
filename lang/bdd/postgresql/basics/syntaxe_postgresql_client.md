# UTILISATION DU CLIENT POUR POSTGRESQL


- help

```
\h
\h SELECT // aide spécifique
```

- fermer

```
\q
```

- liste les bases

```
\l
```

- se connecte à une base

```
\c base
```

- liste les tables

```
\lt
\lt+ // + verboses
```

- liste les index

```
\di
```

- liste les colonnes

```
\d tables;
```

