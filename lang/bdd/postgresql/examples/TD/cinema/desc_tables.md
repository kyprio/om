## artiste
     Column      |         Type          | Modifiers 
-----------------+-----------------------+-----------
 nom             | character varying(20) | not null
 prenom          | character varying(15) | 
 annee_naissance | numeric(4,0)          | 
Indexes:
    "artiste_pkey" PRIMARY KEY, btree (nom)

### cinema
     Column     |         Type          | Modifiers 
----------------+-----------------------+-----------
 nom_cinema     | character varying(10) | not null
 arrondissement | numeric(2,0)          | 
 adresse        | character varying(30) | 
Indexes:
    "cinema_pkey" PRIMARY KEY, btree (nom_cinema)

### film
     Column      |         Type          |          Modifiers          
-----------------+-----------------------+-----------------------------
 id_film         | numeric(10,0)         | not null default 0::numeric
 titre           | character varying(30) | 
 annee           | numeric(4,0)          | 
 nom_realisateur | character varying(20) | 
Indexes:
    "film_pkey" PRIMARY KEY, btree (id_film)

### role
   Column   |         Type          |          Modifiers          
------------+-----------------------+-----------------------------
 nom_role   | character varying(20) | not null
 id_film    | numeric(10,0)         | not null default 0::numeric
 nom_acteur | character varying(20) | not null
Indexes:
    "role_pkey" PRIMARY KEY, btree (id_film, nom_acteur)

### salle
   Column   |         Type          |          Modifiers          
------------+-----------------------+-----------------------------
 nom_cinema | character varying(10) | not null
 no_salle   | numeric(2,0)          | not null default 0::numeric
 climatise  | character(1)          | 
 capacite   | numeric(4,0)          | 
Indexes:
    "salle_pkey" PRIMARY KEY, btree (nom_cinema, no_salle)

### seance
   Column    |         Type          |          Modifiers          
-------------+-----------------------+-----------------------------
 nom_cinema  | character varying(10) | not null
 no_salle    | numeric(2,0)          | not null default 0::numeric
 no_seance   | numeric(2,0)          | not null default 0::numeric
 heure_debut | numeric(4,2)          | 
 heure_fin   | numeric(4,2)          | 
 id_film     | numeric(10,0)         | not null default 0::numeric
Indexes:
