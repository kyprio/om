# TD SQL BDD CINEMA

Exercice 1 Sélections simples

1. Les titres des films. En SQL, ordonner les réponses
```
select titre from film order by titre;
```
2. Noms et années de naissance des artistes nés avant 1950.
```
select nom, annee_naissance from artiste where annee_naissance < 1950;
```
3. Les cinémas du 12ème arrondissement.
```
select nom_cinema from cinema where arrondissement = 12;
```
4. Les artistes dont le nom commence par ’H’ (commande LIKE).
```
select nom,prenom from artiste where nom like 'H%';
    nom    | prenom 
-----------+--------
 Hitchcock | Alfred
 Hunt      | Greg
 Hudson    | Hugh
(3 rows)
```
5. Quels sont les artistes dont on ignore la date de naissance ? Attention : cela signifie que la valeur n’existe pas.)
```
select nom, prenom from artiste where annee_naissance IS NULL;
  nom   | prenom 
--------+--------
 Novak  | Kim
 Hudson | Hugh
```
6. Combien de fois (Bruce) Willis a-t-il joué le rôle de McLane ?
```
select count(*) from role where nom_role = 'McLane' and nom_acteur = 'Willis';
```

## Jointures

1. Qui a joué Tarzan (nom et prénom) ?

```
select nom, prenom from Artiste, Role where Role.Nom_acteur = Artiste.Nom AND Nom_Role='Tarzan';
```

2. Nom des acteurs de Vertigo.

```
select role.nom_acteur from role join film on role.id_film = film.id_film where film.titre='Vertigo';
```

3. Quels films peut-on on voir au Rex, et à quelle heure ?

```
 select distinct nom_cinema, titre, heure_debut from seance join film on film.id_film=seance.id_film where nom_cinema='Rex';
 nom_cinema |        titre         | heure_debut 
------------+----------------------+-------------
 Rex        | Jurassic Park        |        9.99
 Rex        | Reservoir Dogs       |        9.99
 Rex        | Une journee en enfer |        9.99
 Rex        | Shining              |        9.99
 Rex        | Metropolis           |        9.99
 Rex        | Psychose             |        9.99
 Rex        | Vertigo              |        9.99
(7 rows)
```

4. Titres des films dans lesquels a joué Woody Allen; donner aussi le rôle.
```
select film.titre, nom_role  from role, film  where nom_acteur='Allen' and role.id_film=film.id_film;
   titre    | nom_role 
------------+----------
 Annie Hall | Jonas
 Manhattan  | Davis
(2 rows)

```

5. Quel metteur en scène a tourné dans ses propres films ? Donner le nom, le rôle et le titre des films.
```
select role.nom_acteur, role.nom_role, film.titre from role join film on role.id_film = film.id_film where film.nom_realisateur=role.nom_acteur ;
 nom_acteur | nom_role |     titre      
------------+----------+----------------
 Allen      | Davis    | Manhattan
 Allen      | Jonas    | Annie Hall
 Tarantino  | Mr Brown | Reservoir Dogs
 Eastwood   | Munny    | Impitoyable
(4 rows)
```

6. Quel metteur en scène a tourné en tant qu’acteur ? Donner le nom du metteur en scène, le rôle joué, et le titre des films ou le metteur en scène a joué. NB : un metteur en scène peut avoir tourné dans des films qu’il n’a pas réalisés et aussi dans des films qu’il a réalisés.
```
select nom_realisateur, titre, nom_role from film join role on film.nom_realisateur=role.nom_acteur;
 nom_realisateur |      titre       | nom_role 
-----------------+------------------+----------
 Truffaut        | Le dernier metro | Bernard
 Allen           | Manhattan        | Davis
 Allen           | Annie Hall       | Davis
 Allen           | Manhattan        | Jonas
 Allen           | Annie Hall       | Jonas
 Tarantino       | Pulp Fiction     | Mr Brown
 Tarantino       | Reservoir Dogs   | Mr Brown
 Eastwood        | Impitoyable      | Munny
(8 rows)
```

7. Où peut-on voir Shining ? (Nom et adresse du cinéma, horaire).
```
# requete avec where
select DISTINCT Cinema.nom_cinema, Cinema.adresse from Cinema, Seance, Film where Seance.Id_film = Film.Id_film and Seance.nom_cinema = Cinema.Nom_cinema and Film.titre = 'Shining';

# requete en join
select distinct cinema.nom_cinema, cinema.adresse, seance.heure_debut from film                                                                           
join seance on film.id_film=seance.id_film 
join cinema on cinema.nom_cinema=seance.nom_cinema 
where film.titre='Shining';

 nom_cinema |      adresse      | heure_debut 
------------+-------------------+-------------
 Rex        | 22 Bd Poissoniere |        9.99
 Kino       | 3 Bd Raspail      |        9.99
 Nations    | 3 Rue de Reuilly  |        9.99
(3 rows)
```

8. Dans quels films le metteur en scène a-t-il le même prénom que l’un des interprètes ? (titre, nom du metteur en scène, nom de l'interprète). Le metteur en scène et l'interprète ne doivent pas être la même personne.
```
select titre, nom_realisateur, realisateur.prenom as prenom_realisateur, nom_role, nom_acteur, acteur.prenom as prenom_acteur 
from film 
join role on film.id_film=role.id_film 
join artiste  acteur on role.nom_acteur=acteur.nom 
join artiste realisateur on film.nom_realisateur=realisateur.nom 
where realisateur.prenom=acteur.prenom and realisateur.nom!=acteur.nom;
    titre     | nom_realisateur | prenom_realisateur | nom_role | nom_acteur | prenom_acteur 
--------------+-----------------+--------------------+----------+------------+---------------
 Broken Arrow | Woo             | John               | Deakins  | Travolta   | John
 Volte-Face   | Woo             | John               | Archer   | Travolta   | John
(2 rows)
```
9. Ou peut-on voir un film avec Clint Eastwood ? (Nom et adresse du cinéma,horaire).

```
select cinema.nom_cinema, cinema.adresse, seance.heure_debut 
from cinema, seance, film 
where Seance.id_film = film.id_film and seance.nom_cinema = cinema.nom_cinema and film.nom_realisateur = 'Eastwood';
 nom_cinema |     adresse      | heure_debut 
------------+------------------+-------------
 Halles     | Forum des Halles |        9.99
(1 row)
```

10. Quel film peut-on voir dans le 12e arrondissement, dans une salle clima-tisée ? (Nom du cinéma, No de la salle, horaire, titre du film).
```
# join avec where
select distinct cinema.nom_cinema, salle.no_salle, seance.heure_debut, film.titre
from  film , seance, cinema , salle
where salle.climatise = 'O'
AND   cinema.arrondissement = 12
AND   seance.id_film = film.id_film
AND   seance.nom_cinema = cinema.nom_cinema
AND   salle.nom_cinema = cinema.nom_cinema
AND   seance.no_salle     =   salle.no_salle;

 nom_cinema | no_salle | heure_debut |  titre  
------------+----------+-------------+---------
 Nations    |        1 |        9.99 | Vertigo
(1 row)

```

11. Liste des cinémas (adresse, arrondissement) ayant une salle de plus de 150places et passant un film avec (Bruce) Willis.
```
select distinct seance.nom_cinema, cinema.arrondissement, cinema.adresse 
from role 
join film on film.id_film=role.id_film  
join seance on seance.id_film=role.id_film 
join cinema on seance.nom_cinema=cinema.nom_cinema 
join salle on ( salle.nom_cinema=seance.nom_cinema and salle.capacite > 150) 
where role.nom_acteur='Willis';
------------+----------------+--------------
 Kino       |             15 | 3 Bd Raspail
(1 row)
```

12. Liste des cinémas (nom, adresse) dont toutes les salles ont plus de 100places.

```
select cinema.nom_cinema, cinema.adresse, cinema.arrondissement 
from cinema 
where 100 < ALL ( select salle.capacite from salle where cinema.nom_cinema = salle.nom_cinema ) ;

 nom_cinema |   adresse    | arrondissement 
------------+--------------+----------------
 Kino       | 3 Bd Raspail |             15
(1 row)

```
## Négation

1. Quels acteurs n’ont jamais mis en scène de film ?
```
select artiste.nom 
from artiste 
where artiste.nom NOT IN (select film.nom_realisateur from film );
   nom    
----------
 Stewart
 Novak
 Willis
 Lambert
 Keitel
 Travolta
 Cage
 DiCaprio
 Cruise
 Depp
 Ricci
(11 rows)
```
2. Les cinémas (nom, adresse) qui ne passent pas de film de Tarantino.

```
SELECT cinema.nom_cinema, cinema.adresse
FROM  cinema
WHERE NOT EXISTS (SELECT * 
FROM seance, film 
WHERE nom_realisateur = 'Tarantino' 

AND   film.id_film = seance.id_film
AND   seance.nom_cinema = cinema.nom_cinema);
           
```
## Division

Les noms des cinémas qui passent tous les films réalisés par Kubrick.
1.7.1 Fonctions de groupe
1. Total des places dans les salles du Rex.
```
select sum (capacite)
from   salle
where  nom_cinema = 'Rex';
```
2. Année du film le plus ancien et du film le plus récent.
```
select min(annee), max(annee)
from film;
```
3. Total des places offertes par le cinéma.
```
select nom_cinema, sum (capacite)
from   salle
group by  nom_cinema;
```
4. Nom des cinémas ayant plus d’une salle climatisée.
```
select salle.nom_cinema, count(*)
from  salle
where salle.climatise = 'O'
group by salle.nom_cinema
having count (*) > 1;
 nom_cinema | count 
------------+-------
 Kino       |     2
 Rex        |     2
(2 rows)

```
5. Les artistes (nom, prénom) ayant joué au moins dans trois films depuis 1985, dont au moins un passe à l’affiche à Paris (donner aussi le nombre de films).
```
select nom, prenom , count(*)
from artiste, role, film
where nom = role.nom_acteur
and exists  (select * from seance, film, role
where seance.id_film = film.id_film
and role.id_film = film.id_film
and role.nom_acteur = artiste.nom)
and role.id_film = film.id_film
and film.annee > 1985
group by nom, prenom
having count (*) >= 3;
   nom    | prenom | count 
----------+--------+-------
 Willis   | Bruce  |     4
 Travolta | John   |     3
(2 rows)


```
