# ELASTICSEARCH

L'ensemble des commandes s'execute via CURL ou autre outils HTTP

# CRUD

Create data
Read data
Update data
Delete data

## CREATE

### CREATE ENTRY

ajoute une entree
```
POST /zenika/_doc/1
{
"code" : "S10_1949",
"name" : "1952 Alpine Renault 1300",
"scale" : "1:10",
"provider" : "Classic Metal Creations",
"stock" : 7305,
"price" : 98.58,
"currency" : "EUR"
}
```

```
PUT /library
```

ajoute plusieurs entree

### CREATE TABLE

cree une table

    CREATE TABLE VIN     
    ( NV Integer,
      CRU Char(20),
      MIL Integer)

cree une table avec contrainte sur champ

    CREATE TABLE VIN
    ( NV     Integer UNIQUE NOT NULL,
      CRU    Char(20),
      MIL    Integer,
      DEG    Integer BETWEEN 5 AND 15 )

## READ

### READ ENTRY

#### READ ENTRY BASIC

liste toutes les informations d'une table

    SELECT    *
    FROM    VINS

liste les entrées ayant CRU == ...

    SELECT    NV, MIL, DEG
    FROM    VINS
    WHERE    CRU = 'Chablis'

liste les entrées ayant CRU commence par ...

    SELECT    *
    FROM    VINS
    WHERE CRU LIKE 'B%' OR CRU LIKE 'b%'


liste en triant les informations final

    SELECT    CRU
    FROM    VINS
    WHERE    MIL=1985 AND DEG>9
    ORDER BY    CRU

liste unique

    SELECT    DISTINCT    CRU
    FROM VINS

liste d'entrée sur plusieurs tables

    SELECT V.NV, P.NVT
    FROM VINS V, PRODUCTEURS P
    WHERE V.NV=P.NV

liste d'entrée géré par bloc imbriqué

    SELECT    DISTINCT NOM
    FROM    VITICULTEURS VT
    WHERE    NVT    IN     
    ( SELECT    NVT
      FROM    PRODUCTIONS P
      WHERE    NV      IN
      (SELECT NV
       FROM VINS V
       WHERE    V.CRU = 'Bordeaux'
       )
     )

### READ TABLE

## UPDATE

### UPDATE ENTRY

### UPDATE TABLE

modifie en ajoutant un champ

    ALTER TABLE VIN
      ADD COLUMN DEG Integer

## DELETE

### DELETE ENTRY

### DELETE TABLE

supprimer une table

    DROP TABLE table1
