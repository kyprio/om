# COMMANDE
CREATE DATABASE mabdd;
USE mabdd;
CREATE TABLE matable ( id INT PRIMARY KEY, nom VARCHAR(20) );
INSERT INTO matable VALUES ( 1, 'Will' );
INSERT INTO matable VALUES ( 2, 'Marry' );
INSERT INTO matable VALUES ( 3, 'Dean' );
SELECT id, nom FROM matable WHERE id = 1;
UPDATE matable SET nom = 'Willy' WHERE id = 1;
SELECT id, nom FROM matable;
DELETE FROM matable WHERE id = 1;
SELECT id, nom FROM matable;
DROP DATABASE mabdd;
SELECT count(1) from matable; retourne le nombre d'enregistrements de la table
