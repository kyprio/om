# UTILISATION DU CLIENT POUR MONGODB

- help

```
help
```

- fermer

```
exit
```

- liste les bases

```
show databases
show dbs // idem
```

- se connecte à une base

```
use base
```

- liste les collections

```
show collections
```
