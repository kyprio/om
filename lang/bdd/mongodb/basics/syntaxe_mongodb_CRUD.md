# MONGODB

# CRUD

Create data
Read data
Update data
Delete data

##    CREATE

ajoute un document

    db.collection1.insert({var1:"valeur1", var2:"valeur2"})

ajoute plusieurs document

    db.collection1.insert( [ {var1:"valeur1"} , {var1:"valeur2"}, {var1:"valeur3"} ])

idem en ignorant les erreurs si une insertion plante
   
    db.collection1.insert( [ {var1:"valeur1"} , {var1:"valeur2"}, {var1:"valeur3"} ], {ordered:false} )

ajoute ou écrase un document

    db.collection1.insert({var1:"valeur1", var2:"valeur2"})

ajoute le contenu d'un fichier texte < 16Mo

    db.collection1.insert( {nom:"valeur",contenu:'ligne1 \nligne \\n 2\nligne3'} )
    db.collection1.insert( {nom:"valeur",contenu: cat('fichier') } ) // idem

ajoute un fichier > 16Mo

    monfiles put fichier

##    READ

###      liste des opérateurs 

    $gt >
    $gte >=
    $lt < 
    $lte <=
    $ne =! (texte et nombre)
    /texte/
    $in:['valeur','valeur2'] contient l'une des valeurs
    $all:['valeur1','valeur2'] contient toutes les valeurs (pour les array)

###      liste des commandes de recherches sur texte

liste tous les documents d'une collection

    db.collection1.find()
    db.collection1.find().pretty() // idem en formatage JSON

Liste uniquement certains champs sans critère de recherche

    db.collection1.find( {} , {_id:0,nom:1,prenom:1})
    db.collection1.find( {} , {_id:false,nom:true,prenom:true}) // idem

Compte les documents renvoyés

    db.collection1.find().count()

Recherche sur des nombres

    db.employee.find({salaire:{$gte:1500}})

Recherche sur des chaines de caractères

    db.employee.find({nom:'valeur exacte'});
    db.employee.find({nom: /texte/});
    db.employee.find({nom: /^debut/});
    db.employee.find({nom: /fin$/});

Recherche parmi une liste de valeur

    db.employe.find({ville: {$in:['toulouse','paris']}}) // contient au moins ...
    db.employe.find({competence: {$all:['php','mysql']}}) // contient au minimum ...

####        liste les de receherche suf lcommandes sur fichier


###      filtres avancés

####        jointure

source : 
- https://docs.mongodb.com/master/reference/operator/aggregation/lookup/#pipe._S_lookup
- https://www.sitepoint.com/using-joins-in-mongodb-nosql-databases

exemple de collection

```
> db.fichemetier.find({},{_id:0})
{ "intitule" : "adminsys", "salaire" : "1500" }
{ "intitule" : "dev", "salaire" : "1400" }
{ "intitule" : "manager", "salaire" : "1200" }

> db.personne.find({},{_id:0})
{ "nom" : "TOTO", "metier" : "adminsys" }
{ "nom" : "TATA", "metier" : "menage" }
{ "nom" : "TITI", "metier" : "dev" }
```

```
db.personne.aggregate([
  { "$lookup": {
    "localField": "metier",
    "from": "fichemetier",
    "foreignField": "intitule",
    "as": "fonction"
  } },
  { "$unwind": "$fonction" },
  { "$project": {
    "_id":0,
    "nom": 1,
    "fonction.salaire": 1,
  } }
]);
```

$lookup : associe un collection via un champ commun et le nomme fonction
$unwind : déconstruit l'arrayList créée par le $lookup pour une meilleur présentation
$project : selectionne les champs à renvoyer


renvoit

```
{ "nom" : "TOTO", "fonction" : { "salaire" : "1500" } }
{ "nom" : "TATA", "fonction" : { "salaire" : "1200" } }
{ "nom" : "TITI", "fonction" : { "salaire" : "1400" } }

```

##    UPDATE

###      syntaxe générale

    db.collection1.update( {filtre}, {$operation1, $operation2}, { options } )

###      modifie une valeur - $set

modifie la valeur du sous-champ par valeur (du premier document trouvé)

    db.collection1.update( {champ1:"valeur1"} , {$set:{'champ1.souschamp2':"valeur"}} )

idem pour tous les documents trouvés

    db.collection1.update( {champ1:"valeur1"} , {$set:{'champ1.souschamp2':"valeur"}}, {multi:true} )

idem ou créé un nouveau document

    db.collection1.update( {champ1:"valeur1"} , {$set:{'champ1.souschamp2':"valeur"}}, {upsert:true} )

idem pour tous les documents trouvés ou créé un nouveau document

    db.collection1.update( {champ1:"valeur1"} , {$set:{'champ1.souschamp2':"valeur"}}, {upsert:true, multi:true} )
    db.collection1.update( {champ1:"valeur1"} , {$set:{'champ1.souschamp2':"valeur"}}, {true,true} ) // idem

###      suppresion de champ - $unset

supprime un champ (:1 désigne le ou les champs à désactiver)

    db.collection1.update( {champ1:"valeur1"} , {$unset:{'champ1.souschamp2'}} )

ajout un champ au document qui ne l'ont pas

    db.collection1.update({champ:{$exists:false}},{$set:{champ:"valeur"}},{multi:true})


###      incrementation - $in

    db.collection1.update( {champ1:"valeur1"} , {$inc:{'salaire':200}} )

##    DELETE

supprime un document

    db.collection1.remove({champ1:"valeur1"}})

supprime tous les documents d'une collection
    
    db.collection1.remove({})

