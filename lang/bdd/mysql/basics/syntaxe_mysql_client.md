# UTILISATION DU CLIENT POUR MYSQL/MARIADB

- help

```
help
```

- fermer

```
exit
```

- liste les bases

```
SHOW DATABASES;
```

- se connecte à une base

```
use base;
```

- liste les tables

```
show tables;
```

- liste les colonnes

```
DESC table;
```
