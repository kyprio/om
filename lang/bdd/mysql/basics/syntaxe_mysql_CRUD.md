# NOM LANGAGE

# CRUD

Create data
Read data
Update data
Delete data

## CREATE

ajoute un document

   INSERT INTO table VALUES ( 'valeur_unique', 'valeur2 ); # si pas de colonne auto-incrementée
   INSERT INTO table VALUES ( '', 'valeur1', 'valeur2 ); # si colonne ID auto-incrementée

ajoute plusieurs document

    INSERT INTO table ( colone1, colonne2 ) VALUES ( 'valeur1, 'valeur2 ), ('valeur11', 'valeur22');

## READ

### liste des opérateurs

    = égal
    <=> NULL est null (IS NULL/ IS NOT NULL)
    < inférieur
    <= inférieur ou égal
    > supérieur
    <= supérieur ou égale
    <> != différent
    LIKE égal (à utiliser avec des jokers % et _)
    NOT LIKE différent " "
    BETWEEN...AND encadrement entre x et y
    NOT BETWEEN...AND encadrement inverse entre x et y

### liste des commandes de recherches

liste toutes les lignes d'une table

    SELECT * FROM table;

Liste uniquement certaines colonnes sans critère de recherche

    SELECT nom, prenom FROM table;

Compte les lignes renvoyés

    SELECT COUNT(nom) FROM table;

Recherche sur des nombres

   SELECT nom FROM table WHERE salaire > 1300;

Recherche sur des chaines de caractères

    SELECT nom FROM table WHERE prenom = 'texte';
    SELECT nom FROM table WHERE prenom LIKE 'texte';
    SELECT nom FROM table WHERE prenom LIKE 'debut%';
    SELECT nom FROM table WHERE prenom LIKE '%fin%';

Recherche parmi une liste de valeur

    SELECT nom FROM table WHERE prenom IN ('valeur1', 'valeur2');

## UPDATE

###      syntaxe générale

    UPDATE table ACTION1 operation1, operation2 WHERE filtre;

###      modifie une valeur - SET

modifie la valeur d'une colonne selon le critère de recherche

    UPDATE table set colonne1='valeur WHERE id=5;





# DELETE
