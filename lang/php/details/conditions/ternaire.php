//ternaire = condition condensée
//si $variable=='valeur' alors $variable2='valeur1'
//sinon $variable2='valeur2'
//type de condition pratique pour les booléens (true false)
$variable2=($variable1=='valeur') ? 'valeur1' : 'valeur2';

