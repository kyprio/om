//TANT QUE $variable affiche quelquechose ALORS instructions
//boucle utile uniquement si l'on est sûr que variable est périssable
//sinon  cf. for
$variable='valeur';
while($variable<='valeur_differente')
{
	echo 'instruction sur '.$variable.'<br />';
	$variable++;
}
