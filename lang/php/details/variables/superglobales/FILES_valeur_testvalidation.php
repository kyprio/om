//TEST et validation du fichier uploadé
//qui est replacé dans un dossier définitif
//dans du code PHP
//test existence du fichier temporaire et check si aucune erreur d'envoi
if( isset( $_FILES['monfichier'] )AND $_FILES['monfichier']['error'] == 0 )
{
//Test taille fichier ( 8Mo en PHP max )
	if( $_FILES['monfichier']['size'] <= 1000000 )
	{	
//Test extension du fichier en 3etapes:
//	récupération de lextension du fichier
		$infosfichier = pathinfo( $_FILES['monfichier']['name'] );
		$extension_upload = $infosfichier['extension'];
//	creation dune liste dextensions autorisées
		$extensions_autorisees = array( 'jpg', 'jpeg', 'gif','png' );
//	comparaison entre lextention du fichier et les extensions autorisées
		if( in_array( $extension_upload,	$extensions_autorisees ) )
		{
//Validation et stockage du fichier dans le serveur si tous les critères sont remplis
//	récupération du nom temporaire puis déplacement en utilisant son nom d'origine comme nom définitif.
			$emplacement = 'uploads/'.basename( $_FILES['monfichier']['name'] );
			move_uploaded_file( $_FILES['monfichier']['tmp_name'], $emplacement );
			echo 'Instruction envoi réussi. Fichier placé vers <br />/'.$emplacement;
		}
		else
		{
			echo 'erreur d\'extension<br />';
		}
	}
	else
	{
		echo 'erreur de taille<br />';
	}
}
else
{
	echo 'erreur d\'envoi<br />';
}
