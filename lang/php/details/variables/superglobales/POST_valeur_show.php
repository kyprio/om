//Afficher les valeurs stockées dans $_POST du formulaire en toute sécurité
//dans du code PHP
//modification des variables avec des valeurs sûrs
$_POST['pseudo'] = strip_tags( $_POST['pseudo'] );
$_POST['password'] = htmlspecialchars( $_POST['password'] );
$_POST['message'] = strip_tags( $_POST['message'] );
//verification si des valeurs existent
$existe = ( strlen( $_POST['pseudo'] ) ) + ( strlen( $_POST['password'] ) ) + ( strlen( $_POST['message'] ) );
if( $existe >=3 )
{
	echo 'instruction <br />
		Pseudo : '.$_POST['pseudo'].'<br />
		Mot de passe : '.$_POST['password'].'<br />
		Message : '.$_POST['message'].'<br />
		Choix fait : '.$_POST['choix'].'<br />
		Case coché : '.$_POST['case1'].'<br />
		Oui ou non : '.$_POST['casechoix'].'<br />
		valeur caché : '.$_POST['hidden'].'<br />';
}
else
{
	echo 'erreur de saisie<br />';
}
