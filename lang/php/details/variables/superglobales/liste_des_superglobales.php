#Les superglobantes sont de type array associative
$_GET = valeurs transmises dans l'URL après .php?. Les clefs sont à définir dans le lien et séparées par &amp; .
$_POST = valeurs transmises par les formulaires
$_FILES['monfichier1']['clef'] = valeurs transmises par les formulaires enctype="multipart/form-data" en POST. Clefs possibles : name nom du fichier envoyé, type (ex. image/gif ), size taille en octect, tmp_name chemin et nom complet temporaire du fichier sur le serveur, error retourne un code d'erreur si erreurs sinon 0.
$_SESSION = valeurs transmises durant la navigation d'un visiteur sur le site. il faut ouvrir la variable avant le code HTML. Les clefs sont à définir à partir du numero unique PHPSESSID ( PHP SESSION ID ) de chaque client transmis automatiquement de page en page.

$_COOKIE = valeurs transmises par les cookies stockés par le navigateur du visiteur pendant une durée déterminée. Chaque cookie 
contient une seule clef avec une seule valeur.
$_SERVER = valeurs transmises par le serveur sur les visiteurs. Clefs possibles : REMOTE_ADDR adresse IP du client qui a visité la page.
$_ENV = valeurs transmises par le serveur sur son environnement.
