<!--CREER un formulaire texte pour $_POST
dans du code HTML-->
<form method="post" action="cible.php">
	<p>
<!--clef pseudo: $_POST['pseudo']-->
	pseudo<br />
	<input type="text" name="pseudo" /><br />
<!--clef mot de passe $_POST['password']-->
	mot de passe<br />
	<input type="password" name="password" /><br />
<!--clef message $_POST['message'] -->
	zone de texte<br />
	<textarea name="message"></textarea><br />
<!--clef choix sur liste déroulante $_POST['choix']-->
	choix
	<select name="choix">
		<option value="choix1">Choix 1</option>
		<option value="choix2">Choix 2</option>
		<option value="choix3" selected="selected">Choix 3</option>
	</select><br />
<!--clef case pour case à cocher $_POST['case']-->
	case à cocher<br />
	<input type="checkbox" name="case1" checked="checked" />
	<label for="case1">Texte pour la case 1</label><br />
<!--clef case-radio pour case à choisir $_POST['casechoix']-->
	Choix Oui ou Non<br />
	<input type="radio" name="casechoix" value="oui" id="oui" checked="checked" />
	<label for="oui">Oui</label>
	<input type="radio" name="casechoix" value="non" id="non" />
	<label for="non">Non</label><br />
<!--clef invisible pour champ caché $_POST['hidden']-->
	Champ invisible...
	<input type="hidden" name="hidden" value="valeur" /><br />

<!--SUBMIT bouton pour envoyer le formulaire-->
	<input type="submit" value="Valider" />
<!--LIEN bouton de lien -->
	<input type="button" onClick="self.location.href='page1.php'" value="Rafraichir" />
	</p>
</form>
