//fonction fopen() et autres
//ouverture avec le paramètre lire et écrire
$fichier = fopen('fichier', 'r+');
//lecture de la première ligne
$ligne = fgets($fichier);
//instruction sur la ligne 
$ligne++;
//remise du curseur php au début
fseek($fichier,0);
//écriture de la nouvelle valeur
fputs($fichier, $ligne);
//fermeture
fclose($fichier);
echo 'lecture '.$ligne.'<br />';
