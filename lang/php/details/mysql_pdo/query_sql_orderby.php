//QUERY : requete MySql avec l'option ORDER BY
//ORDER BY clef1_table DESC pour décroissant par rapport aux valeurs du champ
$reponse = $bdd->query( 'SELECT * FROM table1 ORDER BY clef1_table DESC' ) or die( print_r( $bdd->errorInfo() ) );
while ( $donnees = $reponse->fetch() )
{
	echo $donnees['clef1_table'].' et '.$donnees['clef2_table'].'<br />';
}
//termine la requète
$reponse->closeCursor();
