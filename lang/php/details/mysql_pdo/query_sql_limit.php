//QUERY : requete MySql avec l'option LIMIT
//affiche après 0 donc 1, et affiche un total de 20 entrees
$reponse = $bdd->query( 'SELECT * FROM table1 LIMIT 0,20' ) or die( print_r( $bdd->errorInfo() ) );
while ( $donnees = $reponse->fetch() )
{
	echo $donnees['clef1_table'].' et '.$donnees['clef2_table'].'<br />';
}
$reponse->closeCursor();
