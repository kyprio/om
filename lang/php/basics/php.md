#  HELLO WORLD

###      Hello world basique

    <?php
    echo "Hello world";
    ?>
    
par convention les fichiers uniquement php ne ferme pas sa balise

    <?php
    echo "Hello world";

###      Hello world avance

Hello world intégré à une page html

    <html>
      <head>
        <title>Exemple</title>
      </head>
      <body>
    <?php
    $var = "valeur";
    echo $var;
    ?>
    </body>
    </html>

# ECRITURE ET EXECUTION DU CODE

###      shebang
    
    #!/usr/bin/php

###      extension des fichiers

    script.php

###      compilation et permissions

    chmod +x script.sh

###      execution

    ./script.php

    php script.php

#  SPECIFICITES

Langage de scripting avancé et de programmation orienté objet interprété par le moteur php.

#  SYNTAXE

###      Instuctions

    instruction1;
    instruction2;

####        ligne par ligne

####        enchainement sur une ligne

    instruction1 ; instruction 2 ; instruction 3 ;

####        instruction découpée sur plusieurs ligne

###      Commentaires

####        en une ligne

    // commentaire sur une ligne

    # commentaire sur une ligne

####        multiligne

    /* commentaire sur
    plusieurs
    ligne */

###      include

Inclus et exécute le fichier...

    include ’vars.php’; // renvoit une alterte en cas d'echec
    require 'vars.php'; // renvoit une erreur fatale en cas d'échec

    include once 'vars.php' // vérifie si le fichier a déjà été récupéré
    require once 'vars.php' // idem

###      execution code externe

####        fichier

cf include

####        commande d'un autre langage

    exec(commande bash);

###      gestion des interprétations des valeurs

####        textes simples

    'texte simple'

####        variables

    "texte et $var"

####        commandes
####        intepretation complète de l'instruction avant execution
###      sortie
####        sans code de retour
####        avec code de retour
###      options debug

#  MEMOIRE
##    VARIABLES
###      Appel variable
####        comme valeur
####        comme commande ou instruction
###      Creation variables
####        declaration et typage
####        creation manuelle simple
####        creation à vide
####        creation manuelle a partir de texte et de variable
####        creation par retour de commande
####        creation par saisie utilisateur
####        creation avec valeur par defaut
####        creation variable de variable
####        suppression
###      Portee
####        globale
####        export vers shell enfant
####        locale
###      methodes communes aux variables
####        renvoi de valeur par defaut sans creation de variable
####        renvoi d'une valeur si test sur variable
###      Variables environnement et shell/processus courant
####        liste variables environnement
####        liste variables shell/processus courant
###      Parametres
####        liste des variables parametres
####        fonctions et options parametre
####        parcours des arguments
##    CONSTANTES
###      Creation constante
###      Manipulation constantes
##    REFERENCES
##    POINTEURS

#  TYPES
##    STRING
####        creation
####        conversion
####        attributs
####        methodes
####        operateur
##    INTEGER
####        appel
####        creation
####        conversion
####        attributs
####        methodes
#####          calcul
#####          incrementation decrementation
####        operateur
##   FLOAT DOUBLE
####        appel
####        creation
####        conversion
####        attributs
####        methodes
####        operateur
##   BOOLEAN
####        appel
####        creation
####        conversion
####        attributs
####        methodes
####        operateur
##   TABLEAU
###      liste
####        appel
####        parcours des valeurs
####        creation
####        conversion
####        attributs
####        methodes
####        operateur
###      dictionnaire clés valeur
####        appel
####        parcours des valeurs
####        creation
####        conversion
####        attributs
####        methodes
####        operateur
###      tableau de tableau
####        appel
####        parcours des valeurs
####        creation
####        conversion
####        attributs
####        methodes
####        operateur
###      matrice
####        appel
####        parcours des valeurs
####        creation
####        conversion
####        attributs
####        methodes
####        operateur
##    FICHIER
####        appel en mémoire
####        parcours des lignes
####        ouverture
####        lecture
####        fermeture
####        ecriture
####        attributs
####        methodes

#  STRUCTURE CONDITIONNELLES
##    TEST ET CODE DE RETOUR
###      code de retour
###      syntaxe de la structure test
####        test simple sans consequence
####        test simple AND - si vrai alors
####        test simple OR - si faux alors
####        test complet
####        combinaison de test AND - test 1 si vrai alors test 2
####        combinaison de test OR - test 1 si faux alors test 2
####        inverse du test
###      operateurs de comparaison et de test
####        chaine de caractere
####        nombre
####        boolean
####        variables objets
####        fichiers
##    IF
##    IF ELSE
##    IF ELSE IF
##    SWITCH CASE

#  BOUCLES ET PARCOURS
##    FOR
###      boucle simple
###      parcours d'une sequence
##    FOREACH
##    WHILE
##    DO WHILE
#    UNTIL

#  FONCTIONS
###      liste fonctions de base
####        affichage
####        saisie utilisateur
###      appel
####        sans paramètre
####        avec paramère non nommé
####        avec paramètre nommé
###      creation
####        creation fonction courte
####        creation fonction
#####          renvoyant un valeur
#####          ne renvoyant aucune valeur
####        attendant des parametres non nommés
####        attendant des parametres nommés
####        attendant un nombre non défini de parametres nommé ou non nommé

# EXCEPTIONS
##    TRY CATCH

# PROGRAMMATION ORIENTEE OBJET
##    CLASSES
###      initialisation objet
###      suppression
###      appel
###      creation nouvelle classe
####        heritage
####        constructeur
####        attribut
####        methode
####        surcharge d'attributs
####        surcharge de methode
