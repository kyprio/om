# MARKDOWN EXTRA

# H1
## H2
### H3
#### H4
##### H5
###### H6

<!--
This is a comment
-->

<!--horizontal rule-->
--- 

texte body
with line break

**strong**
_emphasis_
~~strikethrough~~

> blockquotes
>> with another blockquotes

* unordered list
+ unordered list
- unordered list
  + with another list

1. ordered list
2. ordered list


'command line'

    command multi lines (4x spaces)
    command multi lines (4x spaces)

```
command multi lines
command multi lines
```

```html
<html>
<body>
<p>syntax highlighting</p>
</body>
<html>
```


| Tables | Description |
| ------ | ----------- |
| data   | path to data files to supply the data that will be passed into templates. |

| Tables | Description |
| ------:| -----------:|
| type   | right aligned text |


[Links](http://url.com)
[Links](http://url.com "Info!")


# Table of Contents
  * [Chapter 1](#chapter-1)
  * [Chapter 2](#chapter-2)
will jump to the section nameds:
## Chapter 1 <a id="chapter-1"></a>
Content for chapter one.
## Chapter 2 <a id="chapter-2"></a>
Content for chapter one.


![Minion](http://octodex.github.com/images/minion.png)
![Alt text](http://octodex.github.com/images/stormtroopocat.jpg "The Stormtroopocat")
![Alt text](http://octodex.github.com/images/stormtroopocat.jpg "The Stormtroopocat")[footnote1]
[footnote]: référence à l'image

