#!/bin/bash

if ! which play > /dev/null  ; then
  echo Please install sox !
  exit 1
fi

notename=( 'A3' 'A#3' 'B3' 'C4' 'C#4' 'D4' 'D#4' 'E4' 'F4' 'F#4' 'G4' 'G#4' 'A4' 'A#4' 'B4' 'C5' 'C#5' 'D5' 'D#5' 'E5' 'F5' 'F#5' 'G5' 'G#5' ) 
noteindex=( 'A' 'A#' 'B' 'C' 'C#' 'D' 'D#' 'E' 'F' 'F#' 'G' 'G#' )
I=0
m=3
M=4
V=7

function n { play -qn ${sound:=synth 2 pluck} $1 & }
function s { sleep $( echo "$1 * 0.10" | bc ); }
function p {
  if [[ "$1" =~ "-" ]] ; then
    c "$1" $2
  else
    echo $1
    n $1
    s $2
  fi
}
function a { 
  for i in $1; do
      p $i $2
  done
}
function c {
  for i in ${1//-/ } ; do
    echo -n "$i "
    n $i
  done
  echo
  s $2
}
function cs {
  note=$( echo $1 | sed 's/[Mm]//')
  quality=$( echo $1 | sed 's/[A-G#]*//')
  for (( i=0; i<=$((${#noteindex[@]} - 1)); i++ )); do
    if [ "$note" == "${noteindex[$i]}" ]; then
      break
    fi
  done
  
  if [ "$quality" == "m" ]; then
    cs  "${notename[$(($i+$I))]} ${notename[$(($i+$m))]} ${notename[$(($i+$V))]}" $2
  elif [ "$quality" == "M" ]; then
    cs  "${notename[$(($i+$I))]} ${notename[$(($i+$M))]} ${notename[$(($i+$V))]}" $2
  fi
}
