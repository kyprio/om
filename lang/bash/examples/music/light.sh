#!/bin/bash

source player.sh

function intro {
# INTRO
a "B2-B3-B4-A4
A4-B4
A4-B4
A4-B4
A2-A3-B4-A4
A4
A4
A4
E4-A4
A4
A4
A4
C4-A4
A4
A4
A4
B3-A4
A4
A4
A4
E3-B3-A4
A4
A4
A4
E3-B3-G#4
G#4
G#4
G#4
A2-A3-A4
A4
A4
A4
A4-C5
A4-C5
" 16
}

function riffAmSimple {
# RIFF CHORD Am
for i in 1 2 3 4; do
a "
A2-A4 A3 E4 
A2 A3 E4 
A2 A3 D4-A4 
A2 A3 D4 
A2 A3 B3 C4 
" 2
done
}

function riffAmNormal {
# RIFF CHORD Am with decoration
a "
A2-A4-E5 A3 E4 
A2 A3 E4 
A2 A3 D4-A4-A5
A2 A3 D4 
A2 A3 B3 C4 
A2-A4 A3 E4
A2 A3 E4 
A2 A3 D4-A4-A5
A2 A3 D4 
A2 A3 B3 C4 
A2-A4 A3 E4 
A2 A3 E4 
A2 A3 D4-A4 
A2 A3 D4 
A2 A3 B3 C4 
A2-A4 A3 E4 
A2 A3 E4 
A2 A3 D4-A4 
A2 A3 D4 
A2 A3 B3 C4 
" 2
}
function riffF {
# RIFF CHORD F
a "
F2-A4 A3 E4 
F2 A3 E4 
F2 A3 D4-A4
F2 A3 D4 
F2 A3 B3 C4
F2-A4 A3 E4 
F2 A3 E4 
F2 A3 D4-F5-A4
F2 A3 D4 
F2 A3 B3 C4
" 2
}
function bridge {
# BRIDGE
a "
A2-A4 A3 E4
A2 A3 E4
A2 A3 E4-A4
A2 A3 E4
A3
A2-A4 A3 E4
A2 A3 E4
A2 A3 E4-A4
A2 A3 E4
A3
D3 A3 F4
D3 A3 F4
D3 A3 F4
D3 A3 F4
D3
" 2
}

#intro
#riffAmSimple
#riffF
#riffAmNormal
bridge
