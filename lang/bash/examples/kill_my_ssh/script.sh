#!/bin/bash

export file="$PWD/log"

# ecriture d'un fichier log 
echo "CHILD_PID = " > $file

# lancement de l'enfant
nohup bash child.sh &

# écriture du pid du child
sed -i "s/CHILD_PID = .*/CHILD_PID = $!/" $file
echo "CHILD_PID = $!"
