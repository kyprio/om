#!/bin/bash

oldppid="$PPID"
lastppid="$PPID"
pid="$$"
logname=$LOGNAME
uid=$UID

while "true" ; do
  lastppid="$PPID"
  
  # recupération de pts d'une connexion ssh du même nom que l'UID correspondant à l'enfant
  newparent=$(ps -aux | grep $LOGNAME | grep "pts" | grep "sshd:" | grep -v grep | awk -F "$LOGNAME" '{print $3}')
  pidnewparent=$(ps -aux | grep $LOGNAME | grep "pts" | grep "sshd:" | grep -v grep | awk  '{print $2}')
  newparent=${newparent#\@}
  newparent="/dev/"${newparent}

# log
  echo -e "
[$(date +%T)]
OLD_PPID = $oldppid
LAST_PPID = "$(ps -f |  awk -v pid=$$ '{if ($3 == pid) print $3;}')"

PID = $pid
LOGNAME_TO_SEEK = $logname
UID = $uid
NEWPARENT = "$newparent"
" >> $file

# envoi de message au tty de la session ssh trouvée
  echo "Deconnexion dans 10 secondes : " > $newparent
  sleep 1
  if [ "$newparent" != "/dev/" ]; then
    for i in $(seq 10); do
      echo $i  $pidnewparent > $newparent
      sleep 1
      # kill du pid
    done
    kill $pidnewparent
  fi
  sleep 10
done
