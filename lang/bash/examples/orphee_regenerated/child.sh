#!/bin/bash

# creation d'un log pour suivre l'évolution de la variable exportable i
log="logi"

function orpheeme() {
  nohup $0
}

# test de création du log (cette etape est pour le debug)
if [ -f "$log" ]; then
  # export des variables et récupération si déjà exportées
  export i=${i:=$(cat $log)}
  ((++i))
  # creation du log
  echo FILE EXISTS
  echo $i
  echo $i > $log
  sleep 5 

  if [ "$i" -ge 5 ]; then
    # FIN DU CYCLE DE 5
    echo SUP== 5
    rm $log
    exit
  else
    # CONTINUATION DU CYCLE
    echo INF 5
    orpheeme
  fi

else
  # DEBUT CYCLE
  export i=1
  echo NO FILE
  echo $i
  echo $i > $log
  orpheeme
fi
