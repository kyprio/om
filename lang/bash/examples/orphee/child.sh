#!/bin/bash

log="log"

rm $log
# attente de déconnexion totale du UID ( session X )
echo PID=$$ >> $log
while [ "$(who | grep $USER)" != "" ] ; do
  echo -e "who :  $(who )" >> $log
  echo $$ >> $log
  echo $USER >> $log
  sleep 5
done

echo "MISSION OEDIPE ACTIVATED : $(date +%T)" >> $log

sleep 30

# destruction de tous les process de l'UID lanceur de child
while true; do
  for pid in $(ps -uxf | grep -v $$ | awk '{print $2}' | sort | uniq | grep -v  PID); do
    echo "kill $pid" >> log
    kill $pid
  done
  sleep 30
done
