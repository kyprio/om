# README

## ENONCE AGENT

```
TD3: [A RENDRE] Processus auto-propulsé entre deux systèmes répartis physiquement

Description:

TD3: Processus auto-propulsé entre deux systèmes répartis physiquement

Description:

 Ecrire un script "agent" qui a la capacité seul de se propulser (Tout est inclut dans le seul script "agent" qu'il aura a exécuter),

de facon sécurisée et sans intervention humaine en ssh,

sur un systeme distant physiquement et effacer sa présence sur le systeme source.
```

## SOLUTION PROPOSEE

Solution fortement inspirée du script précedéent FOG (cf FOG pour plus d'explication)
Ici, le script contient une liste de noeud d'un cluster simulé sur lequel il se connecte en SSH sans mot de passe avec authentification par clés asymétrique SSL.

Fonctionnement en 3 phases en boucle pour un mega-camouflage :
- phase tâche principale "TO BE":
    * le script remplit sa tâche

- phase exportation "OR" : 
    * déplacement aléatoire sur un noed (machine) du cluster via ssh

- phase camouflage "NOT TO BE" :
    * le script se renomme

Pour s'assurer du débugage et arrêter les scripts cachés :
- tester la présence d'un fichier précis sur le noeud1 du cluster
- reprise de sa "forme" initiale si le fichier existe vers noeud1 dans le HOME

Une bonne idée de juger et vérifier le fonctionnement du script et de le lancer, d'attendre 10sec ( le temps d'arrivée à la phase d'exportation ) et de faire un `tail -f ~/nohup.out`.
Pour arreter, il faut créé un fichier exit dans le HOME, attendre 5sec, puis supprimer les fichiers généré "resultat" et "nohup"

