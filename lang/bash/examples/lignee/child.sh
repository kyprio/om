#!/bin/bash

i=$1
fifo="fifofile"

# SI 1er parent ALORS créé le fifo via test existance fifo ou non
if [ ! -e $fifo ] ; then
  mkfifo --mode=666 $fifo
  echo "FIFO EST $fifo"
fi

echo "PID : $$, PPID $PPID, i : $i" > $fifo

if [ ! $i -eq 1 ] ; then
  ((i--))
  ./$0 $i &
fi

# terminer le fifo
if [ $i -eq 1 ] ; then
  rm $fifo
  echo "FIN"
fi
