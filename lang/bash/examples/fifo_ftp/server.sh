#!/bin/bash
#set -x

echo "server start"

# repertoire des fifo
rootfifo="$(dirname $(realpath ${0}))/fifo"
mkdir -p ${rootfifo}

# fifo du server principal
fifosrv="${rootfifo}/fifosrv"
mkfifo --mode=666 $fifosrv

# redefinition de exit pour fermeture propre du server
trap ctrl_c INT
function ctrl_c() {
  kill ${childpid} 2> /dev/null
  echo "childs stopped"
  rm -rf ${rootfifo}
  echo "fifos removed"
  echo "server stopped"
  kill $$
}
function exit { ctrl_c ;}


childpid=""
export fifo=""
export srv=""
export cli=""
export fifocli=""

# mise en ecoute du serveur sur fifosvr
# traitement des lignes de type
# clientname>request
while request="$(cat $fifosrv)"; do
  fifo="${rootfifo}/$RANDOM" #fifo du child
  srv=$(basename $fifo) # petit nom du child
  cli="${request%>*}" # petit nom du client
  fifocli="${rootfifo}/${cli}" # fifo du client
  request="${request#${cli}>}" # commande envoyez par le client

  # ouverture d'un child si la commande est hello
  if [ "$request" == "hello" ]; then
      echo "connection from $cli redirect to $srv"
      ./child_server.sh &
      childpid="${childpid} $!" # liste des pid des enfants
  fi
done

