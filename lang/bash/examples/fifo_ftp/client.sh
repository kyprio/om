#!/bin/bash
#set -x

USAGE='
USAGE : 
? : help
hello   : open a connection to the server
bye     : end a communication [bye|quit|exit]
ls      : list directory on the server
lls     : list directory on the client
cd  <path>      : change directory on the server
lcd  <path>     : change directory on the client
put <filename>  : send file from the client to the server
rm <filename>   : remove a file on the server
lrm <filename>   : remove a file on the client
'
# initialisation des variables
rootfifo="$(dirname $(realpath ${0}))/fifo"
fifosrv="${rootfifo}/fifosrv" # variable réaffectée après première ouverture de connexion

# redefinition de exit pour fermeture propre du server
trap ctrl_c INT
function ctrl_c() {
  rm -f $fifocli
  echo existing
  kill $$
}
function exit { ctrl_c ;}

#nom du client pour création de son fifo
while [ -z "$cli" ] || [[ -e "${rootfifo}/${cli}" ]] ; do read -p "unique username : " cli ; done
fifocli=${rootfifo}/${cli}
mkfifo --mode=666 $fifocli

# fonction de requete vers le fifo du server (enfant/principal)
function request() {
  echo -e "${cli}>$@" > $fifosrv
}

echo "Starting communication (CTRL+C to end)"


# attente d'un hello
read -p "$cli>" cmd
while [ "$cmd" != "hello" ] ; do echo -e "$USAGE" ; read -p "$cli>" cmd ; done
# test si fifosrv du server principal existe
if [ -e "$fifosrv" ] ; then
  request "hello"
  reply=$(cat $fifocli)
  echo -e $reply
  fifosrv="${rootfifo}/${reply%>*}"
else
  echo "server not listening"
  exit
fi

# attente des commandes de l'utilisateur
while true; do
  read -p "$cli>" cmd

  case $cmd in
    "?"|"help"|"h")
      echo -e "$USAGE"
      continue
      ;;
    "hello")
      request "hello"
      ;;
    "bye"|"quit"|"exit")
      request "bye"
      cat $fifocli
      rm $fifocli
      exit
      ;;
    "ls")
      request "ls"
      ;;
    "lls")
      ls -c1
      continue
      ;;
    "cd"*)
      request "$cmd"
      ;;
    "lcd"*)
      ${cmd#l}
      continue
      ;;
    "put "[\.\/a-zA-Z0-9]*)
      # demande de placer un fichier du client vers serveur si existe
      file="${cmd#put }"
      if [ -e "$file" ]; then
        request "$cmd"
        cat $file > $fifosrv
      else
        echo "$file doesn't exist"
      fi
      unset file
      ;;
    "get "[\.\/a-zA-Z0-9]*)
      # demande de récupérer un fichier du serveur vers le client si le serveur envoit "OK"
      file=${cli}_${cmd#get }
      request "$cmd"
      if [[ "$(cat $fifocli)" =~ ">OK" ]]; then
        echo "the file doesn't exist"
        if $(cat $fifocli > ${file}); then
          echo "getting $file : done"
       else
          echo "getting $file : failed"
        fi
      else
        echo "the server can't send the file"
      fi
      unset file
      continue
      ;;
    "rm "[\.\/a-zA-Z0-9]*)
      request "$cmd"
      ;;
    "lrm "[\.\/a-zA-Z0-9]*)
      file="${cmd#lrm }"
      if [ -e "$file" ]; then
        ${cmd#l}
      else
        echo "$file doesn't exist"
      fi
      continue
      ;;
    [a-zA-Z0-9]*)
      echo command not found
      ;;
    *)
      continue
      ;;
  esac
  # recupération de la réponse du serveur
  cat $fifocli

done
