#!/bin/bash
#set -x

# creation d'un fifo unique pour la session de $cli
mkfifo --mode=666 $fifo

# fonction de reponse vers le fifo du client
function reply() {
  echo -e "${srv}>$@" > $fifocli
}

# première réponse au client
# celle-ci permet au client de connaitre le fifo du serveur enfant
reply "hello $cli"

# mise en ecoute du $fifo du server enfant
# traitement des lignes de type
# clientname>request
while request=$(cat $fifo); do
  request="${request#${cli}>}"

  case $request in 
    "hello")
      reply "hello again dumbass"
      ;;
    "bye")
      echo "deconnection from $cli"
      reply "bye $cli"
      rm -f $fifo
      exit
      ;;
    "ls")
      reply "$(ls -c1)"
      ;;
    "cd"*)
      $request
      reply "$(pwd)"
      ;;
    "put "[\.\/a-zA-Z0-9]*)
      file="${cli}_${request#put }"
      if $(cat $fifo > ${file}); then
        echo "$cli put $file"
        reply "puttin $file : done"
      else
        echo "$cli failed putting $file"
        reply "puttin $file : failed"
      fi
      ;;
    "get "[\.\/a-zA-Z0-9]*)
      file="${request#get }"
      if [ -e "$file" ]; then
        reply "OK"
        cat $file > $fifocli
        echo "$cli get $file"
      else
        reply "KO"
        echo "$cli failed getting $file"
        echo "$file doesn't exist"
      fi
      unset file
      ;;
    "rm "[\.\/a-zA-Z0-9]*)
      file=${request#rm }
      if $(rm $file); then
        echo $cli removed $file
        reply "removing $file : done"
      else
        echo $cli failed removing $file
        reply "removing $file : failed"
      fi
      ;;
    esac

done
