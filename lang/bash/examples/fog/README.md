# README

## ENONCE FOG

```
TD2:[A RENDRE] Un processus essaie de brouiller sa piste dans le systeme: le script "fog"

Description:

TD2: Un processus essaie de brouiller sa piste dans le systeme: le script "fog"

Description:

 Ecrire un script "fog" qui exécuté par un processus, va chercher automatiquement à "Brouiller"

tout ce qui permet de le suivre ou l'identifier, tout en continuant à faire sa tâche...

en tout cas ce qui est possible de modifier dans ses repèrages

en fonction de ce que vous connaissez ou supposez sur les possibilités Linux/Unix.

On peut comme piste, faire des essais sur le brouillage du pid ?, tâche?, variable d'Envir. ? .....

On essaie au maximum d'utiliser le scripting bash, au-delà il est toujours possible d'aller vers une prog. C Syst. /Linux
```

## SOLUTION PROPOSE

La tâche du script est :
- écrire dans un fichier le résultat de `ps aux` régulièrement

Le brouillage est :
- changement de nom du script dans son dossier initiale
- changement de PID en s'auto executant en tache de fond via `nohup ... &`

Pour s'assurer du débugage et arrêter les scripts cachés :
- tester la présence d'un fichier précis
- reprise de sa "forme" initiale si le fichier existe
