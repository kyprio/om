# FOR

for f in * ;  do
  command "${f}"
done

# FOR SEQUENCE
for i in {1..10..2}; do
  command "${i}"
done

for i in $(seq ${x} 2 ${y} ); do
  command "${i}"
done

# FOR LISTE

liste=('truc1' 'truc2')
for i in "${liste[@]}"; do
  echo i = $i
done

# FOR LISTE INDEX

liste=('truc1' 'truc2')
for (( i=0; i<=$((${#liste[@]} - 1)); i++ )); do
  echo "$i = ${liste[$i]}"
done

# FOR ARGUMENT

for i in $@; do
  echo $i
done

# FOR long param + court param
for arg in "$@" ; do 
  case "$arg" in
    --debug)
      DEBUG=1;;
    --os-packages-only)
      OS_PACKAGES_ONLY=1;;
    --no-self-upgrade)
      # Do not upgrade this script (also prevents client upgrades, because each
      # copy of the script pins a hash of the python client)
      NO_SELF_UPGRADE=1;;
    --no-bootstrap)
      NO_BOOTSTRAP=1;;
    --help)
      HELP=1;;
    --noninteractive|--non-interactive)
      NONINTERACTIVE=1;;
    --quiet)
      QUIET=1;;
    renew)
      ASSUME_YES=1;;
    --verbose)
      VERBOSE=1;;
    -[!-]*)
      OPTIND=1
      while getopts ":hnvq" short_arg $arg; do
        case "$short_arg" in
          h)
            HELP=1;;
          n)
            NONINTERACTIVE=1;;
          q)
            QUIET=1;;
          v)
            VERBOSE=1;;
        esac
      done;;
  esac 
done

