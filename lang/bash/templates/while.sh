# WHILE

while test ; do
  command
done

# WHILE READ COMMAND

while read line ; do
  command ${line}
done < <(ls /tmp)

# WHILE READ FILE (preserve spaces)

oIFS="echo -e $IFS"
IFS=''
while read line ; do
  command ${line}
done < file


# WHILE GETOPTS

while getopts ":a:hv" opt; do
  case "${opt}" in
    h)
      usage
      exit 0
      ;;
    v)
      om_version "${scriptVersion}"
      exit 0
      ;;
    a)
      var1=${OPTARG}
      echo var1=${var1}
      ;;
    \?)
      om_echoerror "Invalid option: -${OPTARG}"
     
      usage
      exit 4
      ;;
    :)
      om_echoerror "missing value to -${OPTARG}"
      om_echoerror"getopts error : ${OPTERR}"
      usage
      exit 4
      ;;
  esac
done
