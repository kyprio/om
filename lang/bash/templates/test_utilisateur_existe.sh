# test_utilisateur_existe en ROOT
if [[[ ! `getent passwd | grep sysadmin` ]]] ; then
  echo -e $yellow"Création de sysadmin"$white
  useradd -m sysadmin -s /bin/bash -p `mkpasswd -m sha-512 @sys@`
  usermod -G sysadmin,cdrom,floppy,sudo,audio,dip,video,plugdev,users,netdev sysadmin
else
  echo -e $yellow"ajout de Sysadmin à sudo"$white
  adduser sysadmin sudo
fi
