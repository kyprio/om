# ip
iface=`grep "static" /etc/network/interfaces | cut -d" " -f2`
ip=`grep -A 1 "static" /etc/network/interfaces | grep address | cut -d" " -f2`
subnet=`echo $iface| sipcalc - | grep "Network address" | awk -F "- " '{print $2}'`
netmask=`echo $iface | sipcalc - | grep "mask" | grep -vP "bits|hex" | awk -F "- " '{print $2}'`
netmaskbits=`echo $iface | sipcalc - | grep "mask (bits)" | awk -F "- " '{print $2}'`
old_begin=`echo $iface | sipcalc - | grep "Usable range" | awk -F "- " '{print $2}'`
old_end=`echo $iface | sipcalc - | grep "Usable range" | awk -F "- " '{print $3}'`
gw=`grep -A 3 "static" /etc/network/interfaces | grep "gateway" | cut -d" " -f2`
old_dn=`hostname -d` ; [[ -z "$old_dn" ]] && old_dn="labo.lan"
old_dns1="8.8.8.8"
old_dns2="8.8.4.4"



echo "iface : '$iface'"
echo "ip : '$ip'"
echo "subnet : '$subnet'"
echo "netmask : '$netmask'"
echo "netmaskbits : '$netmaskbits'"
echo "old_begin : '$old_begin'"
echo "old_end : '$old_end'"
echo "gw : '$gw'"
echo "old_dn : '$old_dn'"
echo "old_dns1 : '$old_dns1'"
echo "old_dns2 : '$old_dns2'"
