# info_reseau
ip=`sudo ifconfig eth0 | gawk -F ":" '/Bcast/ {print $2}' | cut -d " " -f 1`
mac=`sudo ifconfig eth0 | gawk '/HW/ {print $5}'`
netmask=`sudo ifconfig eth0 | gawk -F ":" '/Bcast/ {print $4}' | cut -d " " -f 1`
gw=$(sudo ip route | awk '/default/ { print $3 }')
echo $mac $netmask $ip $gw

