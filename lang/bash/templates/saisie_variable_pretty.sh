#!/bin/bash
source modele/design.sh 2> /dev/null

echo -e $yellow"\n## TEST ##\n"$white

DISTROS=$(whiptail --title "Checklist Box" --checklist \
"Quel est votre distribution linux ?" 15 60 4 \
"debian 7" "Wheezy" OFF \
"ubuntu 14.04 LTS" "Trusty Tahr" ON \
"Fedora 21" "Twenty One" OFF \
"Elementary OS" "Luna" OFF 3>&1 1>&2 2>&3)
 
[[ "$?" ]] && echo CHOISI $DISTROS



echo $UU
