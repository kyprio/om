#!/bin/bash
source $HOME/om/scripts/base_script.sh
set -o errexit
set -o nounset
set -o pipefail
#set -o xtrace

# GESTION VARIABLE
VAR_ENV1="variable d'environnement"
VAR_ENV2="${VAR_ENV2:-'valeur si non déclarée'}"
var1="variable interne"
readonly var2="variable non modifiable"
#[ -z "${var3=-var}" ] && echo '$var3'" n'est pas déclaré" || echo '$var3'" est déclaré $var3"


#var4="${var4=v4}"
#var3="${var3:-v3}"
[ -z "${var4=valeur4}" ] && echo non definie $var4 || echo définie $var4
echo $var4
[ -z "${var3:valeur3}" ] && echo non definie $var3 || echo définie $var3
echo $var3

