# IF

if [[ "test" == "test" ]] ; then  
  command
fi

# IF ELSE

if [[ "test" == "test" ]] ; then
  command
else
  command
  exit 1
fi

# IF ELIF

if [[ "test" == "test" ]] ; then
  command
elif [[ "test" == "test" ]] ; then
  command
else
  command
  exit 1
fi

