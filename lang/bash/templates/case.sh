# CASE

# CASE SIMPLE
case "${var}" in
  'val1') command ;;
  'val2') command ;;
  *) exit 1 ;;
esac

# CASE COMPLEX

case "${var}" in
  'val1'|'val2')
    command
    command
    ;;
  "[a-z]")
    command
    command
    ;;
  *)
    exit 1
    ;;
esac
