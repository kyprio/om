# whiptail_complet

#msgbox
whiptail --title "TITRE" --msgbox "Information" 20 70

# yesno
whiptail --title "TITRE" --yesno "Question ?" 20 70 && echo YES || echo NO
yesno=$(whiptail --title "TITRE" --yesno "Question ?" 20 70 3>&1 1>&2 2>&3)

# yesno button
whiptail --title "TITRE" --yesno "Question ?" --yes-button "Choix1" --no-button "Choix2" 20 70 && echo YES || echo NO
yesnobutton=$(whiptail --title "TITRE" --yesno "Question ?" --yes-button "Choix1" --no-button "Choix2" 20 70 3>&1 1>&2 2>&3)

radiolist=$(whiptail --title "radiolist" --radiolist "Choix unique" 20 70 3 "tag1" "Description 1" ON "tag2" "Description 2" OFF "tag3" "Description 3" OFF 3>&1 1>&2 2>&3)

checklist=$(whiptail --title "checklist" --checklist "Choix multiple" 20 70 3 "tag1" "Description 1" ON "tag2" "Description 2" OFF "tag3" "Description 3" OFF 3>&1 1>&2 2>&3)

inputbox=$(whiptail --title "Saisie" --inputbox "Saisir une valeur" 20 70 "valeur pre-saisie" 3>&1 1>&2 2>&3)

password=$(whiptail --title "MDP" --passwordbox "mdp" 20 70 3>&1 1>&2 2>&3)

