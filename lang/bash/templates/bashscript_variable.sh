  -X TIPS $Y # DESCRIPTION $B #mandatory
  [-X TIPS] $Y # DESCRIPTION (VALUE) $B #optional
VAR=''     # mandatory : DESCRIPTION #mandatory
VAR=''     # optional : DESCRIPTION #optional
VAR=''     # internal : DESCRIPTION #internal
X: #mandatory
X: #optional
    X) VAR="${OPTARG}" ;; #mandatory
    X) VAR="${OPTARG}" ;; #optional
while [[ -z "${VAR}" ]]; do om_readtext VAR "DESCRIPTION" ; done #mandatory
VAR="${VAR:-VALUE}" #optional
VAR=VALUE #internal
om_debugvar VAR #mandatory
om_debugvar VAR #optional
om_debugvar VAR #internal
