#!/bin/bash
#- version : 0.1 08/04/17 template_script.sh

# HEADER
##################################################

set -o errexit
set -o nounset
set -o pipefail
#set -o xtrace
#set -v


source "$HOME/om/scripts/base_script.sh"  2> /dev/null || (echo "base_script.sh manquant" ; exit 1)

readonly scriptName="$(basename ${0})"
readonly scriptVersion="VERSION" # DATE by kyprio for the project om

function usage {
  local returnCode="${1:-0}"
 
  om_echoinfo "${scriptName}\tINFORMATION_SCRIPT"

  om_echoreturn "${scriptName}
  -x $Y # run the script with interactive mode if necessary $B
  [-v] $Y # version $B
  [-h] $Y # help $B"
  om_echoreturn "${scriptName}
  "

  exit "${returnCode}"
}

om_header -t "SCRIPT_TITLE" -v "${scriptVersion}" -u TYPE_USER

[[ "${#}" -eq 0 ]] &&  (om_echoerror "At least, please run the script with the -x parameter" ; usage "4")

#om_inprogress

# PRECONFIGURATION
##################################################

om_echosubtitle "PRECONFIGURATION"

while getopts ":xhv" opt; do
  case "${opt}" in
    x) ;;
    h) usage ;;
    v) om_version "${scriptVersion}" ;;
    \?) om_echoerror "Invalid option: -${OPTARG}" ; exit 4 ;;
    :) om_echoerror "missing value to -${OPTARG}" ; exit 4 ;;
  esac
done

om_debugvar $USER

# EXECUTION
##################################################

om_step "EXECUTION"

# POSTCONFIGURATION
##################################################

om_echosubtitle "POSTCONFIGURATION"

om_theend
