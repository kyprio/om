#!/bin/bash

# HEADER
##################################################

# options shell

#set -o errexit
set -o nounset
set -o pipefail
#set -o xtrace

# include om function

source "$HOME/om/scripts/base_script.sh"  2> /dev/null || (echo "base_script.sh manquant" ; exit 1)
[ "0" == "1" ]
om_checkreturn "$?" 'On continu' 'information a propos erreur'

exit 0

# information script

readonly scriptName="$(basename ${0})"
readonly scriptVersion="0.1" # 04/01/2017 by kyprio for the project om

# presentation

om_header -t "TITRE_SCRIPT" -v "${scriptVersion}" -u user
om_inprogress

function usage {
  om_version "${scriptVersion}"
  om_echoinfo "${scriptName} -a argumentA [-b argumentB] [-v] [-h]"
  om_echoinfo "${scriptName} INFORMATION_SCRIPT"
}

# PRECONFIGURATION
##################################################

om_echosubtitle "PRECONFIGURATION"

# internal script variables

readonly var1="value"
readonly var2="value"

# attended and optional variables from user

var3=''
var4=''
while getopts ":a:bhv" opt; do
  case "${opt}" in
    h)
      usage
      exit 0
      ;;
    v)
      om_version "${scriptVersion}"
      exit 0
      ;;
    a)
      var3="${OPTARG}"
      echo "var3=${var3}"
      ;;
    b)
      var4="${OPTARG:=default_value}"
      echo "var4=${var4}"
      ;;
    "\?"|"*")
      echo "Invalid option: -${OPTARG}"
      exit 4
      ;;
    :)
      echo "missing value to -${OPTARG}"
      exit 4
      ;;
  esac
done

# mandatory variables

if [[ -z "${var3}" ]]; then
  om_readtext var3  "Enter var3" "?"
fi

# EXECUTION
##################################################

om_step "EXECUTION"



# POSTCONFIGURATION
##################################################

om_echosubtitle "POSTCONFIGURATION"

om_theend
