# BASH

#  HELLO WORLD

###      Hello world basique

    #!/bin/bash
    echo 'Hello World !'

###      Hello world avance

    #!/bin/bash
    var='Hellow World !'
    echo "${var}"

# ECRITURE ET EXECUTION DU CODE

###      shebang

    #!/bin/bash

###      extension des fichiers
    script.sh

###      compilation et permissions

    chmod +x script.sh

###      execution

    ./script.sh
    bash script.sh

#  SPECIFICITES

Langage de scripting interprété par le shell Bash
La syntaxe est rigide ; les espaces sont importants et ont du sens pour l'interpreteur bash notament dans les structures conditionnels

#  SYNTAXE

###      Instuctions

####        ligne par ligne

    instruction1
    instruction2

####        enchainement sur une ligne

    instruction1 ; instruction 2 ; instruction 3

####        instruction découpée sur plusieurs ligne

    instruction \
    sur plusieurs \
    lignes

####        bloc d'instruction

    ( instruction1 ; instuction2 )

###      Commentaires

####        en une ligne

    #ceci est un commentaire
    instruction1 # ceci est un commentaire

####        multiligne
n/a

###      include

    source script.sh
    . script.sh

###      execution code externe

####        fichier

    ./script.sh
    
    bash script.sh
    php script.php

####        commande d'un autre langage

    mysql -u user -ppassword -e "SQL QUERY"

###      gestion des interprétations des valeurs

####        textes simples

    'texte simple'

####        variables

    "texte et ${var}"
    'texte et'"${var}"

####        commandes

    "$(execution de commade)"
    `execution de commande`

####        intepretation complète de l'instruction avant execution

    var='var1'
    eval $var='valeur' # declare var1='valeur'
    
    var='nomVar'
    nomVar='valeur'
    eval echo "${var} = \${${var}}" # affiche nomVar = valeur
    
###      sortie

####        sans code de retour

    exit
    exit 0
    
####        avec code de retour

    exit 1
    exit "${number only}"
    
###      options debug

    set -o errexit  # quitte a la moinde erreur
    set -o nounset  # quitte si une variable n'est pas définie
    set -o pipefail # quitte si une commande renvoit une erreur dans un enchainement de commande entre pipe
    set -o xtrace   # affiche ce que fait l'interpreteur (-x)
    set -v          # affiche la ligne d'instruction du script avant de l'interpreter et l'executer

    bash -vx script.sh

#  MEMOIRE

##    VARIABLES

###      Appel variable

####        comme valeur

    echo "${var}"
    echo "var a pour valeur : $[var}"
    echo $var
    
####        comme commande ou instruction

    "${var}"

###      Creation variables

####        declaration et typage

en Bash les variables sont typées automatiquement
mais on peut imposer le type d'une variable
pour permettre des opérations sur celle-ci (ex:calcul)
ou eviter des caracteres non desires depuis une saisie utilisteur
le typage avec la commande declare est définitif

    declare var    # string
    declare -i var # integer definitivement
    declare -a var # array definitivement
    
    declare -i num=10 # num=10
    num="texte${num}"  # num=(string)
    echo ${num}        # num=0

####        creation manuelle simple

    var='valeur'
    var=10

####        creation à vide

    var=''
    
####        creation manuelle a partir de texte et de variable

    var="${var2}"
    var="${var2} et texte"

####        creation par retour de commande

    var="$(commande)"

####        creation par saisie utilisateur

    read var
    
    read var -p "valeur de var : "
    
    read

####        creation avec valeur par defaut

    ${var:='default si pleine'}
    ${var='default si pleine ou vide'}
    
####        creation variable de variable

cf interpretation complete avant execution

####        suppression

    unset var
    
###      Portee
####        globale

en Bash une variable est toujours accessible dans l'ensemble du script
y compris les variables définies dans un fichier inclus

####        export vers shell enfant

    export var
    var='valeur'
    
    export var='valeur'
    
####        locale

    local var
    var='valeur'
    
    local var='valeur'
    
###      methodes communes aux variables

####        renvoi de valeur par defaut sans creation de variable

    ${var:-'default si pleine'}
    ${var-'default si pleine ou vide'}

cas courant d'utilisation si attente d'une saisie utilisateur :

    var="{$1:-'defaut'}"
    
####        renvoi d'une valeur si test sur variable

    ${var:?renvoit erreur et ce message si var non pleine} 
    ${var?renvoit erreur et ce message si var ni pleine ni vide}
    
    ${var+renvoi ce message si var pleine}
    ${var:+renvoi ce message si var existe pleine ou vide}
    
###      Variables environnement et shell/processus courant

####        liste variables environnement

    env          # liste les variables environnement
 
    $HOME        # chemin du repertoire personnel de l'utilisateur
    $SHELL       # chemin absolu du shell utilise
    $OLDPWD      # chemin du repertoire precedent
    $PATH        # liste des chemins de recherche des commandes executables
    $PPID        # PID du processus pere du shell
    $PS1         # invite principale du shell
    $PS2         # invite secondaire du shell
    $PS3         # invite de la structure shell "select"
    $PS4         # invite de l'option shell de debogage "xtrace"
    $PWD         # chemin du repertoire courant
    $RANDOM      # nombre entier aleatoire compris entre 0 et 32767
    $REPLY       # variable par defaut de la commande "read" et de la structure shell "select"
    $SECONDS     # nombre de secondes ecoulees depuis le lancement du shell
    $BASH_SOURCE # dossier d\'execution du script en cours

####        liste variables shell/procesus courant

    $$   # PID du shell courant
    $!   # PID du dernier travail lance en arriere plan
    $?   # code retour de la derniere commande
    $IFS # délimiteur de champ (valeur par defaut IFS=$' \t\n')
  
###      Parametres

####        liste des variables parametres

    ${0}   # nom du script lance
    ${1}   # {1} = parametres positionnels 1(valeurs possibles: 1-9)

    ${#}   # nombre de parametres positionnels
    ${*}   # ensemble des parametres positionnels
    ${@}   # " " (interpretation légèrement différente)
    "${*}" # " " entre guillement groupe
    "${@}" # " " entre guillement pour chaque element
    
####        fonctions et options parametre

    shift   # decalage de 1 vers les valeurs affectés au variable de parametre pour acceder au dela de 9 parametre en utilisant la même variable
    shift 2 # décalage de 2
    
####        parcours des arguments

    for i in $@; do
      echo $i
    done
    
    while getopts ":a:hv" opt; do
      case "${opt}" in
        h)
          usage
          exit 0
          ;;
        v)
          om_version "${VERSION}"
          exit 0
          ;;
        a)
          var1=${OPTARG}
          echo var1=${var1}
          ;;
        \?)
          echo "Invalid option: -${OPTARG}"
          usage
          exit 1
          ;;
        :)
          echo "missing value to -${OPTARG}"
          usage
          exit 1
          ;;
      esac
    done



##    CONSTANTES

###      Creation constante

    readonly conts='valeur'
    
    declare -r const='valeur'

##    REFERENCES
n/a
##    POINTEURS
n/a

#  TYPES

##    STRING

####        appel

    echo "${var}"

####        creation

    var='string string'

    declare var='sting sting'
        
####        conversion

    var="${int1}"
    
####        attributs

    ${#var} # longueur de la chaine

####        methodes

#####          majuscule minuscule

    ${var^}  # Majuscule premiere lettre
    ${var^^} # Majuscule toutes les lettres
    
    ${var,}  # minuscule premiere lettre
    ${var,,} # minuscule toutes les lettres
    
#####          substitution

    ${var/motif/remplacement}  # remplacement du 1er motif
    ${var//motif/remplacement} # remplacement de tous les motifs
    
####        operateur

#####          concaténation

    var="${var1}${var2}"
    var+="${var2}"      
    
##    INTEGER
####        appel

    ${var}
    
####        creation

    var=10

    var=$((5+5))       # calcul la valeur var=10
    
    declare -i var=5+5 # force le typage et calcul la valeur var=10
    
####        conversion
n/a
####        attributs

cf string

####        methodes

cf string

#####          incrementation decrementation

    ((var++))  # incrementation
    ((var--))  # decrementation
    ((var+=2)) # incrementation de 2
    ((var-=2)) # decrementation de 2
    ((var*=2))
    ((var/=2))
    ((var%=2))
    
####        operateur

    +   -    *    /    %    **

    echo $((var+2)) # 3
    

##   FLOAT DOUBLE
####        appel
cf int
####        creation

    num=1.50 
    
    num=$( echo "scale=2; 3/2" | bc) # num=1.50
    
####        conversion
n/a
####        attributs
cf string
####        methodes

####        operateur

    echo $( echo "scale=2; 3/2" | bc) # 1.50

##   BOOLEAN

####        appel

    "${bool}"

####        creation

    bool=true
    bool=false
    
####        conversion
n/a
####        attributs

    ${#tabl[@]} = 
cf string
####        methodes
cf string
####        operateur
cf string
##   TABLEAU

###      liste

####        appel

    echo "${tableau[0]}" # element de l'indice 0
    echo "${tab[*]}"     # tous les elements du tableau espace d'un espace

####        parcours des valeurs

    IFS=$'\n'; echo "${tab[*]}"

    for e in "${tab[@]}"; do 
      echo $e;
    done

    for (i=0; i<=$(${#tab[@]} - 1); i++ ); do
      echo "$i = ${tab[$i]}"
    done
    
####        creation

    tab=('valeur0' 'valeur1')
    tab[2]='valeur'        # creation/modification du 3eme element
    tab2=(${tab1[@]})      # copie le contenu de tab1 a tab2 
   
####        conversion

    tab=( $(command) ) # creation depuis une sortie de commande
    var=(${var})       # converti var en tableau 

####        attributs

    ${#tableau 

####        methodes

    unset ${tableau[2]} # supprime l'element 2
    ${tableau[2]:0:4}   # quatres premieres lettres en partant de 0 de l'element 2
    ${tableau[@]:2:3}   # elements a partir de l'indice 2 jusqu\'a 3
    ${tableau[@]/A/B}   # remplace l'element A par B

####        operateur

    ${#tableau[2]} = longueur de l\'element a l\'indice 2

###      dictionnaire clés valeur
n/a
####        appel
####        parcours des valeurs
####        creation
####        conversion
####        attributs
####        methodes
####        operateur
###      tableau de tableau
n/a
####        appel
####        parcours des valeurs
####        creation
####        conversion
####        attributs
####        methodes
####        operateur
###      matrice
n/a
####        appel
####        parcours des valeurs
####        creation
####        conversion
####        attributs
####        methodes
####        operateur

##    FICHIER

####        appel en mémoire

    file="$(cat fichier)"

####        parcours des lignes
    
affichage simple ligne par ligne

    for l in $(cat fichier) ; do
      echo ${l}
    done

Traitement ligne par ligne

    IFS=''
    while read line ; do
      command ${line}
    done < file
    IFS=$' \t\n'
    
recherche de motif

    grep motif fichier

substitution

    sed 's/motif/remplacement/g'fichier
    
####        ouverture
n/a
####        lecture

    cat fichier
    
    cat fichier1 fichier2
    
####        fermeture
n/a
####        ecriture


    echo 'texte'> fichier   # overwrite
    
    echo 'texte' >> fichier # append
    
####        attributs
n/a
####        methodes
n/a

#  STRUCTURE CONDITIONNELLES
##    TEST ET CODE DE RETOUR
###      code de retour

    $?

    commande_OK ; echo $? # 0
    comande_ECHEC ; echo $? # 1 ou >1

###      syntaxe de la structure test

####        test simple sans consequence

    [ ]
    
    [ "${var}" == 'valeur' ]
    
    
    test
    
    test "${var}" == 'valeur' # syntaxe non recommandée
    

    [[ ]]

    [[ "${var}" == 'valeur' ]] # structure évoluée et tolère de nombreux opérateur mais incompatible avec les enchainements standards
    echo $?                      # 0 si ok, sinon 1 ou autre

    
####        test simple AND - si vrai alors

    &&
    
    [ "${var}" == 'valeur' ] && echo VRAI

####        test simple OR - si faux alors

    ||
    
    [ "${var}" == 'valeur' ] || echo FAUX

[ $string == *" mot_contenu "* ]      # teste en interprétant les regex pour vérifié qu\'un mot est contenu dans un liste $string

####        test complet

    [ "${var}" == 'valeur' ] || echo FAUX && echo VRAI

    [ "$(commande)" == 'valeur' ] # teste la valeur de retour de la commande
    [ ((calcul)) == valeur ]        # teste la valeur d\'un calcul

####        combinaison de test AND - test 1 si vrai alors test 2

     [ -a ]
     
    [ "${var}" == 'valeur' -a "${var2}" == 'valeur2' ] || echo FAUX && echo VRAI
    
####        combinaison de test OR - test 1 si faux alors test 2

    [ -o ]

    [ "${var}" == 'valeur' -o "${var2}" == 'valeur2' ] || echo FAUX && echo VRAI
    
####        inverse du test

    [ ! test ]      # inverse le test à effectuer

###      operateurs de comparaison et de test

####        chaine de caractere

    "${var1}" == "${var2}" # identique
    "${var1}" =! "${var2}" #  différentes
    -z "{$var}"            # test si la variable est vide
    [ "${var}" ]           # idem non recommandé
    -n "{$var}"            # test si la variable est pleine
    [ ! "${var}" ]         # idem non recommandé

####        nombre

    [ "${nombre1}" -eq "${nombre2}" ]  # egal
    [[ "${nombre1}" = "${nombre2}" ]]  # idem
    
    [ "${nombre1}" -ne "${nombre2}" ]  # différent
    [[ "${nombre1}" != "${nombre2}" ]] # idem
    
    [ "${nombre1}" -lt "${nombre2}" ]  # <
    [[ "${nombre1}" < "${nombre2}" ]]  # <
    
    [ "${nombre1}" -le "${nombre2}" ]  # <=
    [[ "${nombre1}" <= "${nombre2}" ]] # <=
    
    [ "${nombre1}" -gt "${nombre2}" ]  # >
    [[ "${nombre1}" > "${nombre2}" ]]  # >
    
    [ "${nombre1}" -ge "${nombre2}" ]  # <=
    [[ "${nombre1}" >= "${nombre2}" ]] # <=

####        boolean
####        variables objets
####        fichiers

    -e "${fichier}" # existe
    -d "${fichier}" # est un répertoire
    -f "${fichier}" # est un fichier
    -L "${fichier}" # est un lien symbolique
    -s "${fichier}" # est écrit (non vide)
    -r "${fichier}" # est lisible (read)
    -w "${fichier}" # est modifiable (write)
    -x "${fichier}" # est exécutable (execute)
    
    "${fichier1}" -nt "${fichier2}" # vérifie si fichier1 est plus récent que fichier2
    "${fichier1}" -ot "${fichier2}" # vérifie si fichier1 est plus vieux que fichier2

##    IF

    if [ "test" == "test" ] ; then
      commande_si_vrai
    fi

##    IF ELSE

    if [[ "test" == "test" ]] ; then
      command
    else
      command
      exit 1
    fi

##    IF ELSE IF

    if [[ "test" == "test" ]] ; then
      command
    elif [[ "test" == "test" ]] ; then
      command
    else
      command
      exit 1
    fi

##    SWITCH CASE

syntaxe courte : 

    case "${var}" in
      'val1') command ;;
      'val2') command ;;
      *) exit 1 ;;
    esac

syntaxe longue : 

    case "${var}" in
      'val 1'|val2)
        commande_si_val 1_ou_val2
        command
        ;;
      [a-zA-Z-0-9]*)
        commande_si_alphanumérique
        command
        ;;
      *)
        exit 1
        ;;
    esac

Ne pas utiliser de double guillements sauf pour interpréter une variable

#  BOUCLES ET PARCOURS
##    FOR

###     boucle simple
    for f in * ;  do
      command "${f}"
    done

###      parcours d'une sequence

    for i in {1..10..2}; do
      command "${i}"
    done

    for i in $(seq ${x} 2 ${y} ); do
      command "${i}"
    done

##    FOREACH
n/a
##    WHILE

    while [ instruction_a_tester ]; do
      commande_si_vrai
    done

    while read line ; do
      command ${line}
    done < <(ls /tmp)
    
##    DO WHILE
n/a
##    UNTIL

    until [ instruction_a_tester ] ; do
      commande_si_faux
    done
    
#  FONCTIONS

###      liste fonctions de base

####        affichage

    echo 'texte'
    echo "texte ${var}"
    echo "$(retour de commande)"
    echo -e "\n ${var}"  # saut de ligne puis valeur de $var

####        saisie utilisateur
    
    read var
    read var1 var2
    read var -p 'prompt'
    read -n 1 variable   # se limite a 1 caractere
    read -t 5 variable   # avec timeout
    read -s variable     # pour les mot de passe

###      appel

####        sans parametre

    ma_fonction #  appel fonction déclarée normalement
    
    ${fnc}      # appel fonction courte déclarée dans une variable

####        avec parametre non nommé

    ma_fonction 'param 1' 'param 2' # $1='param 1' $2='param 2'
    
    ma_fonction -p -a # utilisable avec getopt
    ma_fonction -pa   # idem
    
####        avec paramètre nommé

    ma_fonction -a 'valeur' -b 'valeur2' # utilisable ave getopt

###      creation

####        creation fonction courte

    fnc='command'

####        creation fonction

#####          renvoyant un valeur

    function hello_world {
      echo 'Hello world'
    }  

    hellow_world() {
      echo 'Hello world'
    }
    
#####          ne renvoyant aucune valeur

    function affiche_rien {
      touch fichier
    }
    
####        attendant des parametres non nommés

    function hello {
      echo "${1}"
    }
    
    function hello {
      if [ "${1}" == 'a' ]; then
        echo 'Hello world'
      fi
    }
    

####        attendant des parametres nommés

avec getopts, a est nommé b n'est pas nommé

    function hello {
      while getopts ":a:b" opt; do
        case "${opt}" in
          a)
            var1=${OPTARG}
            echo "var1 = ${var1}"
            ;;
          b)
            echo 'B non nommé'
            ;;
          \?)
            echo "Invalid option: -${OPTARG}"
            usage
            exit 1
            ;;
          :)
            echo "missing value to -${OPTARG}"
            usage
            exit 1
            ;;
        esac
      done
    }
    
####        attendant un nombre non défini de parametres nommé ou non nommé

    function hello {
       echo $1            # ${1}
       shift              # ${x+1}
       for i in ${@}; do  # de ${2} à ...
        echo $i           # ${2}
       done
    }

###      creation prototype

n/a
    
# EXCEPTIONS

##    TRY CATCH

cf IF ELSE

# PROGRAMMATION ORIENTEE OBJET
##    CLASSES
###      initialisation objet
###      suppression
###      appel
###      creation nouvelle classe
####        heritage
####        constructeur
####        attribut
####        methode
####        surcharge d'attributs
####        surcharge de methode
