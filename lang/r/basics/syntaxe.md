# R

## Analyse de données



| Par les maths | Business Intelligence |
| -------- | -------- |
| Stats Descriptions     |      |
| Regression linéaire |  |
| Méthode d'analyse (AFC, ACP) |  | 

ACP = 


AFP = 

Big Data = traitement de données hétérogènes (sans connaître la forme à l'avance). La plupart du temps ce traitement se fait sur des serveurs distribués car le volume des données peut être conséquent.



## Commandes Basique R

### Environnement de travail

- donner un repertoire courant

```setwd("/home/alexis")```

- recuperer le repertoire courant

```
getwd()
```


- Charger un fichier

```Nom_variable=read.table("nom_fichier",h=T,sep="\t")```

h = Header ( qui dans le cas est true car on a des entete dans notre fichier)

sep = separateur qui dans notre cas est separé par une tabulation

sep = \t


- Definir sur quoi on travaille
 
 ```attach(EtudePersonnes)```

### Statistiques simples

- moyenne des valeurs de la colonne poi

```mean(poi)``` 

- écart type (standard deviation):

```sd(tai)```

- variance

```var(tai)```

- médiane

```median(poi)```

- quartiles

```quantile(tai)```

### Graphiques

#### fusion de graphique dans une fenetre

afficher sur 1 ligne 2 graphique (qui sont définit après )

```par(mfrow=c(1,2))```

#### Boite à moustache

boite a moustache simple

```boxplot(poi)``` 

boite a moustache comparant poi/sexe, avec un titre, et des couleurs

```boxplot(poi~sexe,main='Param poid', col=c('blue','green'))```

#### histogramme

histogramme simple avec légendes

```hist(poi, main="Répartition poid", xlab="x poids", ylab="y effectifs", col="pink")```

histogramme avec filtre "poid" pour les hommmes

```hist(poi[sexe=="h"], main="Répartition poid", xlab="x poids", ylab="y effectifs", col="pink")```

comparaison

```
par(mfrow=c(2,2))
hist(poi[sexe=="h"], main="Répartition poid Homme", xlab="x poids", ylab="y effectifs", col="blue")
hist(poi[sexe=="f"], main="Répartition poid Femme", xlab="x poids", ylab="y effectifs", col="green")
hist(tai[sexe=="h"], main="Répartition taille Homme", xlab="x poids", ylab="y effectifs", col="red")
hist(tai[sexe=="f"], main="Répartition taille Femme", xlab="x poids", ylab="y effectifs", col="pink")
```

#### Diagramme à barre

``barplot(poi,main="Répartition poids", xlab="Poids (kg)", col="Black")```

#### Diagramme en secteur

> diagramme en camenbert / tarte (pie) / secteur

```pie(table(sexe),main="Population par sexe",col=c("blue","pink"))```

#### Courbe et nuage de point

comparaison pour chaque ligne, de 2 valeur numérique ( x=poid/y=taille)

```plot(x=poi,y=tai)```

comparaison plus complète (pch = forme des points)
```plot(x=poi,y=tai,main="Evolution du poid par rapport à la taille", xlab="Poids", ylab="Tailles",pch=21,col="red")```

