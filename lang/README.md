# LANG om/lang

Ensemble de fichier mémo sur des langages informatiques avec les bases, les points spécifiques, quelques modeles de syntaxes et de fonctions et des exemples concrets.
Le but étant de pouvoir apprendre un langage sous forme de fiches mémos. Il faut parfois avoir des connaissances théorique préalable sur le langage.

Etant donné que le projet Om est principalement tourné autour du Bash et des langages de scripting utilisés en système, ce sont les langages les plus éparpillés du projet. On retrouvera beaucoup d'exemple de script dans le dossier om/script/, tout comme le langage python et perl où les scripts systèmes les plus intérréssants doivent être placés dans le dossier om/scripts et non dans leur dossier respectif examples/. Les commandes bash sont répertoriés dans om/wiki/commands. Seul la syntaxe est décrite en détail dans son dossier om/lang/bash/

## Usage

Ya qu'à consulté les fichiers en commencant par basics si on découvre le langage. Le dossier template permettra d'insérer des éléments récurrent et nos codes sources comme les boucles de parcours de liste, les demande de saisie, etc...

arborescence d'un dossier :
- bascis
- details
- example
- template

## Contributing

1. Fork it!
2. Create your feature branch: `git checkout -b my-new-feature`
3. Commit your changes: `git commit -am 'Add some feature'`
4. Push to the branch: `git push origin my-new-feature`
5. Submit a pull request :D

## Credits

Kyprio CenKeneno

## License

GPLv3
