package classes;
public class Mouvement {
  // attribut
  private double montant ;
  private String libelle ;
  
  // Constructeur
  public Mouvement(String libelle, double montant) {
    this.libelle = libelle ;
    this.montant = montant ;
  }

  // methode
  @Override
  public String toString() { return libelle ;}
}
