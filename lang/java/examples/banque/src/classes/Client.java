package classes;
public class Client {
  // attribut
  private String nom;
  private String prenom;
  private String tel;
  

  // constructeur Client sans compte par defaut
  public Client(String nom, String prenom, String tel) {
    this.nom = nom;
    this.prenom = prenom ;
    this.tel = tel ;
  }      
  
  // toString
  public String toString() {
    return prenom + " " + nom + " " + tel ;
  }
}
