package classes;
import java.util.ArrayList;
public class Compte {
  // attribut
  private int id; 
  private static int num = 1;
  private double solde;
  private Client client;
  private ArrayList<Mouvement> listMouv ;
  
  // constructeur Compte sans solde par defaut
  public Compte(Client client) {
    this.id = num++;
    this.solde = 0 ; 
    this.client = client ;
    listMouv = new ArrayList<>();
  }
  public Compte(Client client, double solde) {
      this.id = num++;
      this.client = client ;
      if ( solde < 0 ) {this.solde = 0; }
      else { this.solde = solde ; } 
  }
  // toString
  @Override 
  public String toString() { 
  String liste = "";
  for(int i = 0; i < listMouv.size(); i++)
    {liste = liste + listMouv.get(i).toString();} 
  return Integer.toString(id) + " : " +  Double.toString(solde) + "  " + liste ;
  }
  // methodes
  public void debiter(double montant) { 
    this.solde = this.solde - montant ;
    Mouvement mouv = new Mouvement("Débit", montant);
    listMouv.add(mouv); 
  }
  public void crediter(double montant) {
    this.solde = this.solde + montant ;
    Mouvement mouv = new Mouvement("Credit" , montant);
    listMouv.add(mouv);
  }
}
