package view;
import classes.*;
public class Main {
  // main
  public static void main(String[] args) {
    // création objet
    Client client1 = new Client("MARTIN", "Bidule", "0192030403");
    Compte compte1 = new Compte(client1);
    Compte compte2 = new Compte(client1);

    // affichage objet
    System.out.println(client1);
    System.out.println(compte1);
    System.out.println(compte2);
    compte1.crediter(500);
    compte2.debiter(500);
    System.out.println(compte1);
    System.out.println(compte2);
    compte1.crediter(500);
    compte2.debiter(500);
    System.out.println(compte1);
    System.out.println(compte2);
  }
}
