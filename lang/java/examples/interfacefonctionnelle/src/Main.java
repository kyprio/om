import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Stream;



public class Main {

	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		// Utiliser des interfaces comme des types
		// les interfaces fonctionnelle est la represenation des lambda expression sous forme de refer de methode


		Traitement t1 = (Ressource r) ->  r.isStatut() ;
		lanceTraitement(t1, new Ressource());
		
//		//Stream<T> filter(Predicate<? super T> predicate)
//		Ressource r1 = new Ressource() ;
//		System.out.println(r1.isStatut());
//
//		System.out.println(t1.traite(new Ressource()));
//		Traitement t2 = (Ressource r)-> r.isStatut() ? false : true;
//
//		
//		// Predicat interface fonctionelle return un boolean
//		
//		Predicate<Livre> livredis = (Livre l)->l.isDispo() ;
//
//		Stream.of(new Livre(true),new Livre(false),new Livre(true))
//		.filter(livredis).forEach(System.out::println);
//		
//		// interface fontionnelle Function< type Parametre , type Resultat>
//		
//		Function<Integer, Integer> carre = x-> new Integer(x*x);
//		System.out.println(carre.apply(3));
	}
	public static void lanceTraitement(Traitement t , Ressource r)
	{
		if(t.traite(r)){
			System.out.println(" Traitement acheve");
		}else { System.out.println("Traitement en cours");} 
	}
}
