public class Livre {
 boolean dispo ;

public Livre(boolean dispo) {
	super();
	this.dispo = dispo;
}

public boolean isDispo() {
	return dispo;
}

public void setDispo(boolean dispo) {
	this.dispo = dispo;
}

@Override
public String toString() {
	return "Livre [dispo=" + dispo + "]";
}
 
}
