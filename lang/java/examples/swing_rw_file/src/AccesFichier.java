import java.io.*;             // Entrées-Sorties
import java.awt.*;            // interface graphique basique
import javax.swing.*;         // amélioration de l'interface graphique basique
import java.awt.event.*;


public class AccesFichier implements ActionListener {


  // attribut Graphique
  JTextArea textArea = new JTextArea(100,30); 
  JButton btnRead = new JButton("Read");
  JButton btnWrite = new JButton("Write");
  JFrame window = new JFrame("Read_and_Write_File");   
  final private String FS = File.separator; 

  // Methode IHM
  public void createIhm(){
    // Window
    window.setBounds(100,100,500,500);
    // textArea
    textArea.setLineWrap(true);          
    // main panel with scrollbar
    JScrollPane panel = new JScrollPane(textArea);  
    panel.setPreferredSize(new Dimension(50,200));
    window.getContentPane().add(panel, BorderLayout.CENTER); 
    // lower panel with the actions buttons           
    JPanel actionPanel = new JPanel();
    actionPanel.add(btnRead);
    actionPanel.add(btnWrite);
    window.getContentPane().add(actionPanel, BorderLayout.SOUTH); 

    // EVENEMENTS
    window.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    btnRead.addActionListener(this);
    btnWrite.addActionListener(this);
    window.setVisible(true);
  }
                                                        
  // Methode Read
  private void read (){
    String message;
    FileInputStream file_in;
    final int GRANULE = 1000;
    // OUVRIR UN DESCRIPTEUR DE FICHIER
    File fichier = new File("test.txt");
    // ASSOCIER UN FLUX DE LECTURE AU DECRIPTEUR DE FICHIER
    try {
      file_in = new FileInputStream (fichier);
    // LECTURE DE TEXTE ISO-LATIN1
      byte [] table = new byte [GRANULE];
      int nbr = file_in.read(table,0,GRANULE);
      message = new String(table,0,nbr,"ISO-8859-1");
    // FERMER LE FLUX DE LECTURE
      file_in.close();
    }
    // CAS DE FAUTE DE LECTURE
    catch (Exception e) {
      JOptionPane.showMessageDialog(window, "faute lecture fichier" + fichier.getName() + " : " + e.getMessage());
      return;
    }

    // AFFICHAGE DE TEXTE DANS LA ZONE DE TEXTE
    textArea.setText(message);
  }

  /* METHODE Write */
  public void write (){
  String message = textArea.getText();
// OUVRIR UN DESCRIPTEUR DE FICHIER
   File fichier = new File("test.txt");

try{

// OUVRIR UN FICHIER ASSOCIE AU DESCRIPTEUR DE FICHIER
// OU LE CRÉER S'IL N'EXISTE PAS 
// OUVRIR UN FICHIER ASSOCIER UN FLUX D'ECRITURE AU DESCRIPTEUR DE FICHIER 
      fichier.createNewFile();
      FileOutputStream file_out = new FileOutputStream (fichier);

// ECRITURE DU TEXTE EN ISO-LATIN1 
      file_out.write(message.getBytes("ISO-8859-1"));

// FERMER LE FLUX D'ECRITURE 
// LIBÉRATION DES RESSOURCES
      file_out.close();

// CAS DE FAUTE DE LECTURE 
} catch (Exception e) {
        JOptionPane.showMessageDialog(window, "faute lecture d'écriture " + fichier.getName() + ": " + e.getMessage());
        return;
   }
// AFFICHAGE DE TEXTE DANS LA ZONE DE TEXTE
        textArea.setText(message);
  }
// INTERFACE ACTION LISTENER                                      
  public void actionPerformed(ActionEvent e) {

    // BOUTON Read 
    if (e.getSource() == btnRead) {
      read();
    }

    // BOUTON Write
    if (e.getSource() == btnWrite) {
      write ();
    }
  }

}
