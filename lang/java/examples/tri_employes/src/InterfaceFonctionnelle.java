@FunctionalInterface
public interface InterfaceFonctionnelle {
  public boolean compare(int x, int y);
}
