import java.util.Scanner;
public class Main {
	
	public static void main(String[] args) {
  
  // création de la méthode de l'instance fonctionnelle à partir d'une fonction lambda
  InterfaceFonctionnelle compare = (int x, int y) -> x == y; 	
  
  System.out.println("Entier : ");
	int input1 = (new Scanner(System.in)).nextInt();
	int input2 = (new Scanner(System.in)).nextInt();

  //utilisation de la fonction de l'interface fonctionnelle définit par la fonction lambda testInt
  System.out.println( compare.compare(input1,input2) );
  }
}
