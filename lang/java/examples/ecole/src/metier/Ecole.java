package metier;

public class Ecole {
	// attribut
	private String nom ;
	private int siret ;
	
	// getter setter
	public String getNom() { return nom ; }
	public int getsiret() { return siret ; }

	
	// constructeur
	public Ecole(String nom, int siret) {
		this.nom = nom;
		this.siret = siret;
		
	}
	
	public String toString() {
		return "Ecole [nom = " + nom +
				"Siret = " + siret + "]" ;
				}
	

}
