package metier;
import metier.Personne ;
import metier.Ecole;

public class Etudiant extends Personne {
	private static int num = 1;
	private int id ;
	public Ecole ecole;
	
	public int getId() { return this.id ; }
	public Ecole getEcole() { return this.ecole ; }
	
	public Etudiant(String nom, String prenom, Ecole ecole, int age) {
	   super(nom, prenom, age);
	   this.id = num++ ;
	   this.ecole = ecole;}
	
	//toString
	public String toString() { return "Etudiant [id=" + id + "," +
			"ecole=" + ecole + 
			",getNom()=" + getNom() + 
			", getPrenom()=" + getPrenom() + 
			", getAge()=" + getAge() + "]";}
}
