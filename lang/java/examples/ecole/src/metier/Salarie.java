package metier;
import metier.Personne;
import metier.Entreprise;

public class Salarie extends Personne {
	// attribut
	private int salaire ;
	private Entreprise entreprise;
	
	// Getter Setter
	public int getSalaire() { return this.salaire ; }
	public Entreprise getEntreprise() {return this.entreprise ; }
	
	//Constructeur
	public Salarie(String nom, String prenom, Entreprise entreprise, int salaire ) {
		//constructeur class mère
	   super(nom, prenom);
	   System.out.println("Constructeur Salarie nom prenom entreprise salaire");
	   this.entreprise = entreprise;
	   this.salaire = salaire;}
	
	//toString
	public String toString() {
		return "Salarie [ entreprise= "+ entreprise + 
			",getNom()=" + getNom() + 
			", getPrenom()=" + getPrenom() + 
			", getSalaire()=" + getSalaire() + "]";}
}
