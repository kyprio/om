package metier;


public class Personne {
	// attribut
	private String nom;
	private String prenom;
	private int age;
	
	// getter setter	
	public String getNom() { return this.nom; }
	public String getPrenom() { return this.prenom;	}
	public int getAge() { return this.age;	}
	
	// constructeurs
    	// Personne pers1 = new Personne("Nom1", "Prenom1", 30);
	public Personne (String nom, String prenom, int age) {
	System.out.println("contructeur Personne nom prenom age");
		this.nom = nom;
		this.prenom = prenom;
		this.age = age;}
	   // Personne pers1 = new Personne("Nom1", "Prenom2");
	public Personne (String nom, String prenom) {
	System.out.println("contructeur Personne nom prenom");
		this.nom = nom;
		this.prenom = prenom;
		this.age=0;}
	
	// toString
	public String toString() { return "Personne [nom=" + nom + ", prenom=" + prenom + ", age=" + age + "]"; }
}
