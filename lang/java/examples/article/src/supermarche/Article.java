package supermarche;

public class Article {

  // attribut
  private int id ;
  private static int numId = 1 ;
  private String desc ;
  private int price ;
  private int stock ;
  
  // constructeur
  public Article(String desc, int price, int stock) {
    this.desc = desc ;
    this.id = numId++ ;
    this.price = price ;
    this.stock = stock ;
  }

  // getter setter
  public int getId() { return id ; }
  public String getDesc() { return desc ; }
  public int getStock() { return stock ; }
  public int getHt() { return price ; }
  public float getTtc() { return 1.20f * price; }

  // methode
  public void provision(int add) {
    stock += add ;
  }
  public boolean sell(int nbUnits) {
    if ( (stock - nbUnits ) < 0 ) {
      return false ; }
    else {
      stock -= nbUnits ;
      return true ;
    }
  }

}
