package view;
import supermarche.* ;

public class Main { 

  // main
  public static void main(String[] args) {
    // création des objets
    Article art1 = new Article("Cocotte", 100, 50 );

    System.out.println(art1.getStock());
    System.out.println(art1.getHt());
    System.out.println(art1.getTtc());
    System.out.println(art1.getDesc());

    art1.provision(40);
    

    System.out.println(art1.getStock());
  }
}
