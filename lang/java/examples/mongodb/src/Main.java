import com.mongodb.MongoClient;
import com.mongodb.MongoClientURI;
import com.mongodb.ServerAddress;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.MongoCollection;
import org.bson.Document;
import java.util.Arrays;
import com.mongodb.Block;
import com.mongodb.client.MongoCursor;
import static com.mongodb.client.model.Filters.*;
import com.mongodb.client.result.DeleteResult;
import static com.mongodb.client.model.Updates.*;
import com.mongodb.client.result.UpdateResult;
import com.mongodb.MongoCredential;
import java.util.ArrayList;
import java.util.List;

import java.util.Scanner;

public class Main {

  static String scanInputString(String message) {
    Scanner scan=new Scanner(System.in);
    System.out.println(message);
    String input=scan.nextLine();
    return input;
  }
  static int scanInputInt(String message) {
    Scanner scan=new Scanner(System.in);
    System.out.println(message);
    int input=scan.nextInt();
    return input;
  }

  public static void main( String[] args ) {
    // création des instances
    String url_db = scanInputString("url/ip db : ");
    int port = scanInputInt("port : ");
    String nm_db = scanInputString("nom db : ");
    MongoClient mongo = new MongoClient( url_db , port ); 
      
    // programme
    MongoDatabase database = mongo.getDatabase(nm_db); 
    for (String name : database.listCollectionNames()) {
      System.out.println(name);
    }
      
  }
}
