package view;
import geo.*;
public class Main {

// main
  public static void main(String[] args) {

    // Création des objets
    Coordinate coord1 = new Coordinate(55, 60);
    Coordinate coord2 = new Coordinate(45, 60);
    Coordinate coord3 = new Coordinate(145, 360);
    Town town1 = new Town("Bretigny", coord1 , 44);
    Town town2 = new Town("Plessi", coord2 , 54);
    Town town3 = new Town("Paris", coord3 , 74);
    Road road1 = new Road("66", town1, town2);
    Agglo agglo1 = new Agglo("Essonne", town1, 50);

    // Test des méthodes
    System.out.println("Perimetre" + agglo1.getPerimeter());
    System.out.println("Area" + agglo1.getArea());
    System.out.println("Road1" + road1.lengthRoad());
    System.out.println(agglo1.testTown(town1));
    System.out.println(agglo1.testTown(town2));
    System.out.println(agglo1.testTown(town3));

  }
}
