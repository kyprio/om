package geo;

public class Agglo {
  //attribut
  private String name;
  private Town mainTown;
  private int radius;

  // constructeur
  public Agglo(String name, Town mainTown, int radius) {
  this.name = name;
  this.mainTown = mainTown;
  this.radius = radius;
  }
  
  //getter setter
  public void setName(String name) { this.name = name ; }
  
  //methode
  public double getPerimeter() {
    return 2*3.14*radius;
  }
  public double getArea() {
    return 3.14*radius*radius;
  }
  public boolean testTown(Town town) {
    if ( town.getCoord().distanceTo(mainTown.getCoord() ) < 0 ) {
      return false; }
    else {
      return true ;
    }
    }
  
}
