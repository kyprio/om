package geo;

public class Road {
  //attribut
  private String name;
  private Town town1; 
  private Town town2;

  // constructeur
  public Road(String name, Town town1, Town town2) {
    this.name = name;
    this.town1 = town1;
    this.town2 = town2;
  }
  
  // getter setter
  public void setname(String name) {
    this.name = name;
  }
  
  // methode
  public double lengthRoad() {
    return this.town1.getCoord().distanceTo(town2.getCoord());
}
}

