package geo;

public class Town {

  // attribut
  private String name;
  private Coordinate coord;
  private int nbPeople;

  // constructeur
  public Town(String name, Coordinate coord, int nbPeople) {
    this.name = name;
    this.coord = coord ;
    this.nbPeople = nbPeople ;
  }

  // getter setter
  public void setName(String Name) { this.name = name ;}
  public String getName() { return name ;}
  public void setNbPeople(int nb) { this.nbPeople = nb ;}
  public int getNbPeople() { return nbPeople ;}
  public Coordinate getCoord() { return coord ;}

}
