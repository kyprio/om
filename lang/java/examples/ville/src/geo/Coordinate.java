package geo;

public class Coordinate {
  // attribut coordonnee
  private double x;
  private double y;

  // constructeur
  public Coordinate( double xcoord, double ycoord ) {
   x = xcoord;
   y = ycoord;
  }

  // methode
  public double distanceTo( Coordinate other ) {
    double dx = other.x - x;
    double dy = other.y - y;
    return Math.sqrt( dx*dx + dy*dy );
  }
  
  // getter setter
  public double getX() { return x ;}
  public void setX(double value) { x = value ;}
  public double getY() { return y ;}
  public void setY(double value) { y = value ;}
  public void setXY(double a, double b){
   this.x= a;
   this.y = b;
  }

  //toString
  public String toString(){
    return "("+x+", "+y+")";
  }

}
