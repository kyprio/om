package view;
import db.*;
import file.*;


import java.util.Scanner;
import java.util.ArrayList;
import java.util.List;

public class Console {
ConnectMongodb connect ;
Filetext file;
String content;
String path;
String version;
String nvVersion;
String tmpPath;

  // attribut

  // construteur
  public Console(ConnectMongodb connect) {
  this.connect = connect;
  }

  // methodes private

  // methodes publiques standard

    // scanInputString
  private String scanInputString(String message) {
    System.out.println(message);
    Scanner scan=new Scanner(System.in);
    String input=scan.nextLine();
    return input;
  }   
    // scanInputInt
  private int scanInputInt(String message) {
    System.out.println(message);
    Scanner scan=new Scanner(System.in);
    int input=scan.nextInt();
    return input;
    }   
  // methodes publiques MENU
    // menuHome
  public void menuHome() {
    String action = scanInputString("- (O)pen a file ?\nor\n- (L)oad a version from the database ?\n(E)xit").toLowerCase();
    switch (action) {
      case "o": menuOpen();
        break;
      case "l": menuLoad();
        break;
      case "e": return;
      default: System.out.println("command not found");
        menuHome();
        break;
    }
  }
    // meunOpen
  private void menuOpen() {
    System.out.println("Open");
      // path
    path = scanInputString("Path : ").toLowerCase();
    file = new Filetext(path);
    path = file.getFilePath();
      // test if path exists version0
    if ( connect.exists(path, "version0") ) {
      System.out.println(path + " : exists in DB");
    }
    else {
      System.out.println(path + " : does not exist in DB");
      connect.insertFiletext(file, "version0");
      System.out.println("Inserted as version0");
    }
      // content
    content = file.read();
    menuEdit();
  }

    // menuLoad
  private void menuLoad() {
    System.out.println("Load");
      // path
    List<String> listPath = connect.listPath();
    System.out.println("Which path to edit ?");
    for (int i = 0 ; i < listPath.size(); i++) {
      System.out.println("("+i+") : " +listPath.get(i));
    }
    path = listPath.get(scanInputInt("path number : "));
      // version
    System.out.println("Available versions for " + path);
    List<String> listVersion = connect.listVersion(path);
    System.out.println("Which version to edit ?");
    for (int i = 0; i < listVersion.size(); i++) {
      System.out.println("("+i+") : " + listVersion.get(i));
    }
    version = listVersion.get(scanInputInt("version number : "));
      // content
    content = connect.getContent(path, version);
    file = new Filetext(path);
    menuEdit();
  }
    // menuEdit
  private void menuEdit() {
    System.out.println("Editing");
      // temp
    tmpPath = file.createTmpFile();
    System.out.println("Creation of a temporary file.\nPath for the temporary file to edit : \n" + tmpPath);
    file.writeToTmp(content);
    String continu = scanInputString("when done : (S)ave modification or (C)ancel").toLowerCase();
    switch (continu) {
      case "s":
        file.writeMod();
        menuSave();
        break;
      default:
        System.out.println("Exit");
        return;
    }
      // write
  }
      // menuSave
  private void menuSave() {
    nvVersion = scanInputString("New name version : ");
    if ( connect.exists(path, nvVersion) ) {
      System.out.println(path + " : already exists in DB");
      menuSave();
    }
    else {
      connect.insertFiletext(file, nvVersion);
      System.out.println("Inserted as "+  nvVersion);
    }
  }
}
