package db;
import file.*;

import com.mongodb.Block;
import com.mongodb.MongoClient;
import com.mongodb.client.FindIterable;
import com.mongodb.client.DistinctIterable;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoCursor;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.model.CreateCollectionOptions;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import org.bson.Document;
import static com.mongodb.client.model.Filters.*;
import static com.mongodb.client.model.Projections.*;

public class ConnectMongodb {
  // attribut
  private MongoClient mongoclient ;
  private MongoDatabase db ;
  private MongoCollection<Document> coll ;
  private static ConnectMongodb instance ;

  // constructeur
  private ConnectMongodb(String hostname, int port, String nm_db, String nm_coll) {
    this.mongoclient  = new MongoClient(hostname, port); // cree la connection au serveur
    this.db = mongoclient.getDatabase(nm_db); // utiliser la connection pour utiliser une base
    this.coll = this.db.getCollection(nm_coll);
  }
  public static ConnectMongodb getInstance(String hostname, int port, String nm_db, String nm_coll) {
    if (instance == null ) {
      instance = new ConnectMongodb(hostname, port, nm_db, nm_coll); }
    return instance ;
  }

  // methodes private

    // printBlock
  private Block<Document> printBlock = new Block<Document>() {
       @Override
       public void apply(final Document document) {System.out.println(document.toString());}
  };
    // getBlock
  private Block<Document> getBlock = new Block<Document>() {
       @Override
       public void apply(final Document document) {System.out.println(document.getString("version"));}
  };



  // methodes publiques
    // createCollection
  public void createCollection(String nm_coll) {
    db.createCollection(nm_coll, new CreateCollectionOptions());
    }
    // dropCollection
  public void dropCollection(String nm_coll) {
    this.db.getCollection(nm_coll).drop();
  }
    // close
  public void close() { this.mongoclient.close(); }
    // listCollections
  public String listCollections() {
    String name = "# Liste des collections : ";
    for (String i : db.listCollectionNames()) {name = name + "\n" + i;}
    return name ;
    }
    // insertDocument
  public void insertDocument(String path, String version, String content) {
    Document document = new Document("path", path)
      .append("version", version)
      .append("content", content);
    coll.insertOne(document);
    System.out.println(path + " : " + version + " -> successfully inserted");
  }
    // insertFiletext
  public void insertFiletext(Filetext file, String version) {
    Document document = new Document("path", file.getFilePath())
      .append("version", version)
      .append("content", file.read());
    coll.insertOne(document);
    System.out.println(file.getFilePath() + " : " + version + " -> successfully inserted");
  }
    // removeDocument
  public void removeDocument (String path, String version) {
    Document document = new Document ("path", path)
      .append("version", version);
    coll.deleteOne(document);
    System.out.println("Document Deleted");
  }
    // ListPath
  public List<String> listPath(){
      MongoCursor<String> cursor = coll.distinct("path", String.class).iterator();
      List<String> listPath = new ArrayList<String>();
      while(cursor.hasNext()){
          listPath.add(cursor.next().toString());
      }
  
      return listPath;
  }
    // listVersion
  public List<String> listVersion(String path) {
    List<String> listVersion = new ArrayList<String>();
    FindIterable<Document> find =  coll.find(eq("path", path))
      .projection(fields(include("version"), excludeId()));
    MongoCursor<Document> cursor = find.iterator();
    try {
      while ( cursor.hasNext() ) {
        Document doc = cursor.next();
        listVersion.add(doc.getString("version"));
      }
    } 
    finally { cursor.close(); }
    return listVersion;
  }
    // exists
  public boolean exists(String path, String version) {
    List<String> listPathVersion = new ArrayList<String>();
    long find =  coll.count(and(eq("path", path),eq("version", version)));
    if ( find > 0 ) { return true;}
    else { return false ; }
  }
    // getContent
  public String getContent(String path, String version) {
    Document document = coll.find(and(eq("path", path),(eq("version" , version))))
                .projection(fields(include("content"), excludeId()))
                .first();
    return document.getString("content");
  }
}
