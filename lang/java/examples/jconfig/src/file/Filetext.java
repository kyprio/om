package file;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;
import java.util.Arrays;
import java.nio.file.StandardOpenOption;
import java.io.IOException;


public class Filetext extends File {

  // attribut
  final private String LS = System.getProperty("line.separator");
  private static final long serialVersionUID = 33333; // inutle mais néccessaire
  private Path filePath ;
  private Path tmpPath ;
  private final  Charset charset = Charset.forName("UTF-8");
  private List<String> fileLines = new ArrayList<>();
  private List<String> tmpLines = new ArrayList<>();
  private String fileContent ;
  private String tmpContent ;
  private String tmpFilename ;
  private String version;

  // construteur
  public Filetext(String path) {
    super(path);
    this.filePath = toPath();
    this.tmpFilename = super.getAbsolutePath().replace("_","__").replace("/","_");
    try { fileLines = Files.readAllLines(filePath, charset);}
    catch ( Exception e ) {System.out.println(e);}
    fileContent = String.join(LS,fileLines);
  }

  // methodes
    // createTmpFile
  public String createTmpFile() {
    try {this.tmpPath = Files.createTempFile(tmpFilename, ".tmp"); }
    catch ( Exception e ) {System.out.println(e);}
    return tmpPath.toString();
  }
    // writeToTmp
  public void writeToTmp(String content) {
    this.tmpLines = Arrays.asList(content.split(LS));
    try {Files.write(tmpPath, tmpLines, charset, StandardOpenOption.WRITE); }
    catch (Exception e) {System.out.println(e);}
  }
    // readTmp 
  public String readTmp() {
    try { fileLines = Files.readAllLines(tmpPath, charset);}
    catch ( Exception e ) {System.out.println(e);}
    tmpContent = String.join(LS,fileLines);
    return tmpContent;
  } 
    // write  or  overwrite // pour le test uniquement (public)
  public void write(String content) {
    try {
      fileLines = Arrays.asList(content.split(LS));
      Files.write(filePath, fileLines, charset); }
    catch ( Exception e ) {
      System.out.println(e);}
    }
    // writeMod
  public void writeMod() {
    try {
      fileLines = Arrays.asList(readTmp().split(LS));
      Files.write(filePath, fileLines, charset); }
    catch ( Exception e ) {
      System.out.println(e);}
    fileContent = String.join(LS,fileLines);
    }
    // deleteTmp
  public void deleteTmp() {
    try { Files.deleteIfExists(tmpPath); }
    catch ( Exception e) { System.out.println(e); }
  }
    // read
  public String read() {
    try { fileLines = Files.readAllLines(filePath, charset);}
    catch ( Exception e ) {
      System.out.println(e);}
    fileContent = String.join(LS,fileLines);
    return fileContent;
  } 
    // getFilePath
  public String getFilePath() {
    return getAbsolutePath();
  }
    // append or write
  public void append(String content) {
    if (Files.notExists(filePath))
      { write(content); }
    else 
      try {
        fileLines = Arrays.asList(content.split(LS));
        Files.write(filePath, fileLines, StandardOpenOption.APPEND); }
    catch ( Exception e ) {
      System.out.println(e);}
    }
    // edit
  public void edit() {
    createTmpFile();
    System.out.println("You can modify the temp file, then press ENTER");
    try {System.in.read();}
    catch ( Exception e ) {
      System.out.println(e);}
    writeMod();
    deleteTmp();
  }
    // exists
  public boolean exists() {return Files.exists(filePath);}

}
