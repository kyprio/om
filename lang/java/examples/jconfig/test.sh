#!/bin/bash

lib="$(ls $(pwd)/lib/* | tr '\n' ':')"
rm -rf bin 
mkdir -p bin 
javac -Xlint -g -deprecation -d bin src/*/*.java -classpath "${lib}"
rsync src/ -rv --exclude="*.java" bin/
cd bin

#echo -e "\n#### test.Main_db ####\n"
#java -cp "${lib}" test.Main_db
#echo -e "\n#### test.Main_file ####\n"
#java -cp "${lib}" test.Main_file
echo -e "\n#### test.Main ####\n"
java -cp "${lib}" test.Main


echo -e "\n#### FIN du test ####\n"
cd ..
rm -rf bin 
