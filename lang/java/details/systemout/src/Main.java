public class Main {
  
  // attribut retour de ligne
  static final String LS = System.getProperty("line.separator");

  public static void main(String[] args) {
  
  // mesage sans retour de ligne
  System.out.print("message sans retour de ligne");
  System.out.println("");

  // message puis retour à la ligne
  System.out.println("message puis retour à la ligne");

  // message sur multiligne via attribut LS
  System.out.println("Message" + LS + "multiligne");
  }
}
