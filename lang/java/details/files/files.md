# FILES

## DEFINITION

utilisation des classes File et Files

## CODE

Filetext.java
```
import java.io.File;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;
import java.util.Arrays;
import java.nio.file.StandardOpenOption;
import java.io.IOException;

public class Filetext extends File {

  // attribut
  final private String LS = System.getProperty("line.separator");
  private static final long serialVersionUID = 33333; // inutle mais néccessaire
  private Path filePath ;
  private Path tmpPath ;
  private final  Charset charset = Charset.forName("UTF-8");
  private List<String> lines = new ArrayList<>();
  private String fileContent ;
  private String tmpContent ;
  private String tmpFilename ;

  // construteur
  public Filetext(String path) {
    super(path);
    this.filePath = toPath();
    this.tmpFilename = super.getAbsolutePath().replace("_","__").replace("/","_");
    try { lines = Files.readAllLines(filePath, charset);}
    catch ( Exception e ) {System.out.println(e);}
    fileContent = String.join(LS,lines);
  }

  // methodes private
    // createTempFile
  private void createTempFile() {
    try {
      this.tmpPath = Files.createTempFile(tmpFilename, ".tmp");
      Files.write(tmpPath, lines, charset, StandardOpenOption.WRITE); }
    catch ( Exception e ) {System.out.println(e);}
    System.out.println("tmpPath :" + LS + tmpPath.toString());
  }
    // readTmp 
  private String readTmp() {
    try { lines = Files.readAllLines(tmpPath, charset);}
    catch ( Exception e ) {System.out.println(e);}
    tmpContent = String.join(LS,lines);
    return tmpContent;
  } 
    // write  or  overwrite // pour le test uniquement (public)
  private void write(String content) {
    try {
      lines = Arrays.asList(content.split(LS));
      Files.write(filePath, lines, charset); }
    catch ( Exception e ) {System.out.println(e);}
    }
    // writeMod
  private void writeMod() {
    try {
      lines = Arrays.asList(readTmp().split(LS));
      Files.write(filePath, lines, charset); }
    catch ( Exception e ) {System.out.println(e);}
    fileContent = String.join(LS,lines);
    }

  // methodes public
    // read
  public String read() {
    try { lines = Files.readAllLines(filePath, charset);}
    catch ( Exception e ) {System.out.println(e);}
    fileContent = String.join(LS,lines);
    return fileContent;
  } 
    // getFilePath
  public String getFilePath() {
    return getAbsolutePath();
  }
    // append or write
  public void append(String content) {
    if (Files.notExists(filePath))
      { write(content); }
    else 
    try {
      lines = Arrays.asList(content.split(LS));
      Files.write(filePath, lines, StandardOpenOption.APPEND); }
    catch ( Exception e ) {System.out.println(e);}
    }
    // edit
  public void edit() {
    createTempFile();
    System.out.println("You can modify the temp file, then press ENTER");
    try {System.in.read();}
    catch (Exception e ) {System.out.println(e);}
    writeMod();
  }
    // exists
  public boolean exists() {return Files.exists(filePath);}
}
```

Main.java
```
public class Main{
  public static void main(String[] args){
  // création des instances
  Filetext file = new Filetext("test.txt");

  // programme
  if (! file.exists()) { System.out.println("NO EXISTS"); }

  file.edit(); // OK
  System.out.println(file.read()); // OK
  System.out.println(file.getFilePath());
  file.append("appendu");
  
  }
}
```
