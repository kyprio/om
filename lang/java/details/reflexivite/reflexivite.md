# REFLEXIVITE

## DEFINITION

Utilisation d'un fichier de configuration.
- quel classe implements (type de BDD)
- information propreau serveur
- methodes à utiliser

## CODE

cf interface.md sauf class Main.java

### fichier de coniguration brut

config
```
DAO_1
Metier
```

### classes :

Main.java
```
import java.lang.reflect.Method; // introspection des methodes
import java.lang.reflect.InvocationTargetException;

import java.io.File; // ouverture des fichiers
import java.io.FileNotFoundException; // ouverture des fichiers
import java.util.Scanner; // lecture des fichiers
 
public class Main {
  public static void main(String[] args) 
throws FileNotFoundException, ClassNotFoundException, InstantiationException, NoSuchMethodException, IllegalAccessException, InvocationTargetException{  
  
    // recupération en attribut des ligne de fichier "config" brut
    Scanner sc = new Scanner(new File("config"));
    String DAOclassName = sc.next();
    String METIERclassName = sc.next();


    // introspection des classes récupéré sur "config"
    Class<?> daoclass = Class.forName(DAOclassName);
    InterfaceDAO dao = (InterfaceDAO) daoclass.getConstructor().newInstance();
    Class<?> metierclass = Class.forName(METIERclassName);
    Metier metier = (Metier) metierclass.getConstructor().newInstance();


    // introspection des methodes des classes
    Method setD = metierclass.getMethod("setDao", new Class<?>[] {InterfaceDAO.class});
    setD.invoke(metier, new Object[] {dao});

    // programme
    System.out.println(metier.traitement("Message"));
  }
}
```
