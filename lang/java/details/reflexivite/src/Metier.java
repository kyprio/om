public class Metier {
  InterfaceDAO dao;

  //constructeur
  public Metier() {}
  public Metier (InterfaceDAO dao) {
    this.dao = dao;
  }
  // methodes alternative constructeur
  public void setDao(InterfaceDAO dao){
    this.dao = dao;
  }
  // methodes
  public String traitement(String message) {
    return dao.getValue() + message;
  }
}

