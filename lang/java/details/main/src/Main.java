public class Main {
  public static void main(String[] args) {
  // creation des instances
  Classe1 instance1 = new Classe1();
  Classe1 instance2 = new Classe1("instance2");

  // programme
  System.out.println("# System.out.println(instance1);");
  System.out.println(instance1);
  System.out.println("# System.out.println(instance2);");
  System.out.println(instance2);
  System.out.println("# instance1.setNom(''instance1'');");
  instance1.setNom("instance1");
  System.out.println("# System.out.println(instance1.getNom());");
  System.out.println(instance1.getNom());
  }
}
