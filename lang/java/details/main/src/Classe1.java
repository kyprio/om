public class Classe1 {
  // attribut
  private String nom;
  private int num;

  // Contructeurs
  public  Classe1() {
  this.nom="instance_defaut";
  this.num=2017;
  }
  public  Classe1(String nom) {
  this.nom = nom;
  this.num = 1017;
  }
  
  // methodes getter setter
  public String getNom() {
    return nom;
  }
  public void setNom(String nom) {
    this.nom=nom;
  }
  
  // methodes réécrites
  @Override
  public String toString() {
    return nom + " " + num;
  }
}
