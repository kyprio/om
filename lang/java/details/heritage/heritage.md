# HERITAGE

## DEFINITION

notion de classe mère (abstraite ou non) et de classes filles héritant de l'ensemble des attributs et methodes de la classe mère.


classe abstraite = aucune instance ne peut être générée à partir d'une classe abstraite. Il faut passer par une classe fille. Similaire au classe interface.

## CODE

### classe mère

Mere.java

```
public abstract class Mere {
  // attributs
  private static int num = 1;
  private int id;

  // constructeur
  public Mere() {
    this.id = num++;
  }
  
  // methodes communes pré-définies
  public abstract String  methode1();
}
```

### classe fille

Fille.java

```
public class Fille extends Mere {

  // constructeur avec héritage
  public Fille() {
    super(); // appel du constructeur de la classe mère
  }

  // methodes commune à définir
  @Override
  public String methode1() {
    return "Methode1 Fille";
  }

  // methodes propre à la classe fille
  public String methode2() {
    return "Methode2 FIlle";
  }
}
```

### classe Main

Main.java

```
public class Main {
  public static void main(String[] args) {
    // creation des instances
    Fille f1 = new Fille();

    // programme
    System.out.println(f1.methode1());
    System.out.println(f1.methode2());

  }
}
```
