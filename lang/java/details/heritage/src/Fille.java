public class Fille extends Mere {

  // constructeur avec héritage
  public Fille() {
    super(); // appel du constructeur de la classe mère
  }

  // methodes commune à définir
  @Override
  public String methode1() {
    return "Methode1 Fille";
  }

  // methodes propre à la classe fille
  public String methode2() {
    return "Methode2 FIlle";
  }
}
