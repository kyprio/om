import java.util.ArrayList;
import java.util.List;

public class Main {

  public static void main(String[] args) {
    
    // nouvelle instance ArrayList contenant des String
    List <String> liste = new ArrayList<String>(); 
    
    // ajout d'élément String
    liste.add("mot0");
    liste.add("mot99");
    liste.add("mot1");
    
    // suppression d'un élément via son indice
    liste.remove(1);
    
    // affichage de liste tel quel
    //  [mot0, mot1]
    System.out.println("# affichage de liste tel quel");
    System.out.println(liste);

    
    // parcours de la liste sans index
    // mot0
    // mot1 
    System.out.println("# parcours de la liste sans index");
    for(String e : liste)
      System.out.println(e);

    // parcours de la liste avec index
    System.out.println("# parcours de la liste avec index");
    int i;
    for(i=0;i<liste.size();i++)
      System.out.println("Element[" +i+"] : " + liste.get(i));
  }
}

