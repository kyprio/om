# ARRAYLIST

## DEFINITION

objet permettant de lister plusieurs objet d'un même type


## CODE

Main.java

```
import java.util.ArrayList;

public class Main {

  public static void main(String[] args) {
    
    // nouvelle instance ArrayList contenant des String
    ArrayList <String> liste = new ArrayList <String> (); 
    
    // ajout d'élément String
    liste.add("mot0");
    liste.add("mot99");
    liste.add("mot1");
    
    // suppression d'un élément via son indice
    liste.remove(1);
    
    // affichage de "liste" sous la forme:
    //  [mot0, mot1]
    System.out.println(liste);

    
    // parcours de la liste sans index
    // mot0
    // mot1 
    for(String e : liste)
      System.out.println(e);

    // parcours de la liste avec index
    int i;
    for(i=0;i<liste.size();i++)
      System.out.println("Element[" +i+"] : " + liste.get(i));
  }
}
```
