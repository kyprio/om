# INTERFACE

## DEFINITION

class permettant de faire l'interface entre plusieurs class ayant les mêmes méthodes
L'interface est un contrat qui a être utilisé par les classes qui l'implémente.

## CODE


### Declaration classe de la classe interface

InterfaceDAO.java

```
public interface InterfaceDAO {
  // methode communes a déclarer
  public String getValue();
}
```

### Declaration classe implémantant la classe interface

DAO_1.java

```
public  class DAO_1 implements InterfaceDAO {
  @Override
  public String getValue() {
    return "DAO_1";
  }
}
```

DAO_2.java

```
public class DAO_2 implements InterfaceDAO {
  @Override
  public String getValue() {
    return "DAO_2";
  }
}
```

### Déclaration d'une clase utilisant ces classes interfaces

Metier.java

```
public class Metier {
  InterfaceDAO dao;

  //constructeur
  public Metier (InterfaceDAO dao) {
    this.dao = dao; 
  }

  // methodes alternative constructeur
  public void setDao(InterfaceDAO dao){
    this.dao = dao;
  }
  // methodes
  public String traitement(String message) {
    return dao.getValue() + message;
  }
}
```

### Déclaration du main

Main.java

```
public class Main {
  public static void main(String[] args) {
  
  //declaration des objets
  DAO_1 dao1 = new DAO_1();
  Metier m = new Metier(dao1);

  // programme
  System.out.println(m.traitement("Message"));
  }
}
```
