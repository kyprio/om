import java.util.Scanner;
public class Main {
	
	public static void main(String[] args) {
  
  // création de la méthode de l'instance fonctionnelle à partir d'une fonction lambda
  InterfaceFonctionnelle testInt = (int i) -> i > 0; 	
  
  System.out.println("Entier : ");
	int input = (new Scanner(System.in)).nextInt();

  //utilisation de la fonction de l'interface fonctionnelle définit par la fonction lambda testInt
  System.out.println( testInt.isPositive(input) );
  }
}
