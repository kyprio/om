@FunctionalInterface
public interface InterfaceFonctionnelle {
  public boolean isPositive(int i);
}
