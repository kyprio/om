import com.mongodb.Block;
import com.mongodb.client.model.CreateCollectionOptions;
import com.mongodb.client.model.ValidationOptions;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoCursor;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.result.DeleteResult;
import com.mongodb.client.result.UpdateResult;
import com.mongodb.MongoClient;
import com.mongodb.MongoClientURI;
import com.mongodb.ServerAddress;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import org.bson.Document;
import static com.mongodb.client.model.Filters.*;
import static com.mongodb.client.model.Updates.*;

public class ConnectMongodb {
  // attribut
  private MongoClient mongoclient ;
  private MongoDatabase db ;
  private MongoCollection<Document> coll ;
  private static ConnectMongodb instance ;
  
  // constructeur
  private ConnectMongodb(String hostname, int port, String nm_db, String nm_coll) {
    this.mongoclient  = new MongoClient(hostname, port); // cree la connection au serveur
    this.db = mongoclient.getDatabase(nm_db); // utiliser la connection pour utiliser une base
    this.coll = this.db.getCollection(nm_coll);
  }
  public static ConnectMongodb getInstance(String hostname, int port, String nm_db, String nm_coll) {
    if (instance == null ) {
      instance = new ConnectMongodb(hostname, port, nm_db, nm_coll); }
    return instance ;
  }

  // methodes
    // creation collection vide (erreur si existante)
  public void createCollection(String nm_coll) {
    db.createCollection(nm_coll, new CreateCollectionOptions()); }
    // getCollection
  //public MongoCollection<Document> getCollection(String nm_coll) {
  //  MongoCollection<Document> coll = this.db.getCollection(nm_coll);
  //  return coll;}
    // dropCollection
  public void dropCollection(String nm_coll) { this.db.getCollection(nm_coll).drop(); } 
    // close connexion
  public void close() { this.mongoclient.close(); }
    // liste collections 
  public String listCollections() {
    String name = "# Liste des collections : ";
    for (String i : db.listCollectionNames()) {name = name + "\n" + i;}
    return name ; }

}
