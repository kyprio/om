public class Main {
  public static void main(String[] args) {
  // création des instances
  ConnectMongodb connect = ConnectMongodb.getInstance("let.kyprio-libre.fr", 1317, "jconfig", "file");
  
  // programme
  // connect.close(); // OK
  // connect.createCollection("collection1"); // OK
  // connect.getCollection("file"); // OK
  // connect.dropCollection("collection1"); // OK
  // connect.dropCollection("collection2"); // OK
  System.out.println(connect.listCollections()); // OK
  }
}
