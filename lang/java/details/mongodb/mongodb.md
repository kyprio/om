# MONGOD

## NOTIONS

Les connexions au BDD se font par l'intermédiaire des classes interfaces que l'on récupère en chargeant les drivers java .jar java de la base de donnée récupérés sur le site officiel de la bdd.
MongoDB évolue très vite les méthodes peuvent avoir évoluées.

## DEPENDANCES

sous **java 1.8** pour un serveur Mongodb en 3.4.10
- mongodb-driver-core-3.4.3.jar
- bson-3.4.3.jar
- mongodb-driver-3.4.3.jar

Ces librairies sont à chargés lors de la compilation et de l'execution de Main.

## CODE

arborescence :
- src/
    - Main.java
    - ConnectMongodb.java
- lib/
    - mongodb-driver-core-3.4.3.jar
    - bson-3.4.3.jar
    - mongodb-driver-3.4.3.jar
- bin/
- test.sh
   
test.sh (nettoyage/compilation/test/nettoyage)
```
#!/bin/bash

lib="$(ls $(pwd)/lib/* | tr '\n' ':')" # "lib1.jar:lib2.jar:"
rm -rf bin 
mkdir -p bin 
javac -Xlint -g -deprecation -d bin src/*.java -classpath "${lib}"
cd bin 
java -cp "${lib}" Main
cd ..
rm -rf bin 
```

Main.java
```
public class Main {
  public static void main(String[] args) {
  // création des instances
  ConnectMongodb connect = ConnectMongodb.getInstance("let.kyprio-libre.fr", 1317, "jconfig", "file");
  
  // programme
  // connect.close(); // OK
  // connect.createCollection("collection1"); // OK
  // connect.getCollection("file"); // OK
  // connect.dropCollection("collection1"); // OK
  // connect.dropCollection("collection2"); // OK
  System.out.println(connect.listCollections()); // OK
  }
}
```

ConnectMongodb.java
```
import com.mongodb.Block;
import com.mongodb.client.model.CreateCollectionOptions;
import com.mongodb.client.model.ValidationOptions;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoCursor;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.result.DeleteResult;
import com.mongodb.client.result.UpdateResult;
import com.mongodb.MongoClient;
import com.mongodb.MongoClientURI;
import com.mongodb.ServerAddress;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import org.bson.Document;
import static com.mongodb.client.model.Filters.*;
import static com.mongodb.client.model.Updates.*;

public class ConnectMongodb {
  // attribut
  private MongoClient mongoclient ;
  private MongoDatabase db ;
  private MongoCollection<Document> coll ;
  private static ConnectMongodb instance ;
  
  // constructeur
  private ConnectMongodb(String hostname, int port, String nm_db, String nm_coll) {
    this.mongoclient  = new MongoClient(hostname, port); // cree la connection au serveur
    this.db = mongoclient.getDatabase(nm_db); // utiliser la connection pour utiliser une base
    this.coll = this.db.getCollection(nm_coll);
  }
  public static ConnectMongodb getInstance(String hostname, int port, String nm_db, String nm_coll) {
    if (instance == null ) {
      instance = new ConnectMongodb(hostname, port, nm_db, nm_coll); }
    return instance ;
  }

  // methodes
    // creation collection vide (erreur si existante)
  public void createCollection(String nm_coll) {
    db.createCollection(nm_coll, new CreateCollectionOptions()); }
    // getCollection
  //public MongoCollection<Document> getCollection(String nm_coll) {
  //  MongoCollection<Document> coll = this.db.getCollection(nm_coll);
  //  return coll;}
    // dropCollection
  public void dropCollection(String nm_coll) { this.db.getCollection(nm_coll).drop(); } 
    // close connexion
  public void close() { this.mongoclient.close(); }
    // liste collections 
  public String listCollections() {
    String name = "# Liste des collections : ";
    for (String i : db.listCollectionNames()) {name = name + "\n" + i;}
    return name ; }
    // METHODES A FAIRE
    // public void insertFile(String path, String version, String content) { }
        // insertion de document de type {path:"/chemin/access", version:"version0", content:"texte multiligne"}
    // public void removeFile(String path, String version) { }
        // suppression des documents correspondant au critère "path" et "version" envoyé en paramètre
    // public ?? (String ou tableau...) listFilePath () { }
        // renvoit la liste ordonnées et unique des "path" stocké dans la collection file
    // public ?? listFilePathVersion (String path) {}
        // renvoit la liste des versions associé à "path" envoyé en paramètre
    // public boolean existVersion(String path, String version) { }
        // test si un fichier existe déjà sous une version particulière
        // on l'utilisera à la fois pour savoir si une version0 existe d'un fichier qui est ouvert
        // on l'utilisera aussi lorsque l'utilisateur souhaitera enregistrer son nom de version et qu'il faudra s'assurer que celle-ci n'existe pas déjà
```

