import java.util.Scanner;
public class Main {
  
  // methodes input pour Int
  static int scanInputInt() {
    Scanner scan=new Scanner(System.in);
    System.out.println( "Entier : ");
    int input=scan.nextInt();
    return input;
    }   

  // methode input pour String
  static String scanInputString() {
    Scanner scan=new Scanner(System.in);
    System.out.println( "Chaine : ");
    String input=scan.nextLine();
    return input;
    }   
  
  public static void main(String[] args) {
  System.out.println("Votre entier : " + scanInputInt());
  System.out.println("Votre chaine sans espace : " + scanInputString());
  }
}

