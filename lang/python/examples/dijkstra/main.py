#!/usr/bin/python
# -*-coding:utf8 -*

#import scipy
from random import randint, getrandbits
import numpy as np
from string import ascii_lowercase
import networkx as nx
import matplotlib.pyplot as plt

# GESTION DE L'ALEATOIRE
########################

# initialisation d'une matrice carré à n noeuds servant 
nbNd = int( input('Nombre de noeuds : ') )
graph = np.zeros((nbNd, nbNd)) - 1 # distance entre noeud dans matrice
#graphPos = [] # position des noeuds dans tableau

# création aléatoire des liens entre les noeuds
# 0 pour lien vers lui meme
# 0 pour aucun lien
# n>0  pour un lien
for y in range(nbNd):
  # lien vers lui-meme
  graph[y][y]=0
  for x in range(nbNd):
    if y > x:
      # chance qu'il n'y ai aucun lien entre x et y
      if bool(getrandbits(int( nbNd/ 14 + 1))) :
        # aucun lien entre x et y
        graph[x][y]=graph[y][x]=0
      else :
        # lien entre x et y
        graph[x][y]=graph[y][x]=randint(1,20)

# correction de l'absence totale de lien pour certain point
#print('avant correction')
#print(graph)
for y in range(nbNd):
  valid=0
  for x in range(nbNd):
    if y > x:
      valid += graph[x][y]
  if valid == 0 : 
    n = randint(0,x)
    m = randint(0,x-1)
    graph[x][n] = graph[n][x] = randint(1,20)
    graph[x-1][m] = graph[m][x-1] = randint(1,20)
    #print('graph[' + str(x) + '][' + str(y) + '] == 0 corrigé -> ' + str(graph[x][y]) )
#print('après correction')
#print(graph)
    

# creation du dictionnaire de nom des noeuds
#dic = {x:i for i, x in enumerate(al, 0)}
listNd = []
for x in ['X','Y','Z'] :
  for y in list(ascii_lowercase):
    listNd.append(x+y)
print('La liste des noeuds actuels est entre : \n'+ listNd[0] + '...' + listNd[int(nbNd/2)] + '... et ' + listNd[nbNd - 1])
  
startNd=raw_input('Chemin le plus court en le noeud ? ')
endNd=raw_input('et le noeud ? ')

def generer_liens(matrice, listNd):                                                      
  ''' Génération des liens en utilisant la matrice et la liste de noms
  de noeuds. Retourne une liste de tuples sous la forme : 
  [(source, dest, {'poids': poids_lien}, ...)]
  '''
  line_nb = 0
  column_nb = 0
  liens = []
  for line in matrice:
    source = listNd[line_nb]
    for column in line:
      dest = listNd[column_nb]
      if column != 0:
        liens.append((source, dest, {'weight' : int(column)}))
      column_nb += 1
    line_nb += 1
    column_nb = 0

  return liens

def generer_liens_PLC(bestPath):
    PLC = []
    for noeud in range(len(bestPath)-1):
        PLC.append((bestPath[noeud], bestPath[noeud+1]))
    return PLC

#G = nx.from_numpy_matrix(a)
a = np.array(graph.tolist())
G = nx.from_numpy_matrix(a)
#print(nx.dijkstra_path(G, listNd.index(startNd), listNd.index(endNd)))
bestPath=[]
for i in nx.dijkstra_path(G, listNd.index(startNd), listNd.index(endNd)) :
  bestPath.append(listNd[i])
#print(bestPath)

draw = nx.Graph()                                                                  
liens = generer_liens(a,listNd)										
draw.add_edges_from(liens)
pos = nx.random_layout(draw)
#dessiner les noeuds                                                            
nx.draw_networkx_nodes(draw,pos,node_size=300, node_color='blue', alpha=0.3)

#label du noeud                                                                 
nx.draw_networkx_labels(draw,pos,font_size=10,font_family='sans-serif', font_weight='bold')            
										
#dessiner edges (liens entre les noeuds)                                        
nx.draw_networkx_edges(draw,pos,edgelist=liens, width=1, edge_color='green', alpha=0.3) 

PLC = generer_liens_PLC(bestPath)
#dessiner plus court chemin
nx.draw_networkx_edges(draw,pos,edgelist=PLC, width=2, edge_color='red', alpha=0.6) 
										
#pour le label, on récupère le poids du lien                                    
labels = nx.get_edge_attributes(draw,'weight')                                     
#on affiche le label au-dessus des liens                                        
nx.draw_networkx_edge_labels(draw,pos,edge_labels=labels)

plt.title('Meilleur chemin entre ' + startNd + ' et ' + endNd + " : " + str(bestPath))
plt.axis('off')                                                                 
plt.show()    
