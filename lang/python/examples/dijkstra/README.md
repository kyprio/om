# README

Explication de code

Pour établir des liens entre plusieurs noeuds d'un réseau, il est courant d'utiliser des matrices carré comportant autant de colonnes et ligne que de noeud (n noeuds) :

```
     A    B    C    D
A [  0.  15.   0.  14.]
B [ 15.   0.   0.  13.]
C [  0.   0.   0.   5.]
D [ 14.  13.   5.   0.]
```

On peut ainsi appelé le lien entre 2 point par :
```
graph[x][y]
```
exemple pour la valeur du lien entre A et B par :
```
graph[0][2]
```

On remarque que sur ce type de matrice, la valeur d'un lien entre A et B et la même qu'entre B et A, soit
```
graph[x][y] = graph[y][x]
```
Il y a un mirroir des valeur avec la diagonale descendante de la matrice.


On remarque qu'un lien d'un noeud à lui même vaut 0. 

Lorsqu'il n'y a pas de lien entre deux noeuds, on mets, par convention, soit 0, soit -1. Cela dépends du developpeur.

## GESTION DE L'ALEATOIRE

### Création d'un graph à n noeud

Ainsi pour généré aléatoirement des matrices de n noeud, il suffit de créé une matrice carré de n ligne et n colonnes.

graph = numpy.zeros((n,n))

### Parcours et remplissage de la matrice

Pour remplir cette matrice par des valeurs de lien entre les noeuds façon aléatoire, il suffit de parcourir la matrice ligne par ligne, colonne par colonne ou plus précisement "ligne ,colonne par colone, par ligne".

```
for x in range(n):
  for y in range(n):
    action()
```

Et comme la matrice est en mirroir. il suffit de n'affecter qu'une valeur sur la première partie du graph. Pour cela, il faut par exemple, affecter une valeur uniquement au point de coordonnée x < y.

```
if x > y :
  graph[x][y] = valeur_du_lien
```
Il peut être utile, suivant les modules utilisés par la suite, reproduire tout de même la partie en mirroir :
```
if x > y :
  graph[x][y] = graph[y][x] = valeur_du_lien
```

Lorsque x = y, cela correspond à un lien d'un noeud vers lui-même. Il convient alors de lui attribué la valeur 0
```
if x = y :
  graph[x][y] = 0
```

### Choix aléatoire de l'existance ou non d'un lien

Avant d'affecter une valeur de lien entre deux noeud, on peut établir de façon aléatoire, si oui ou non, un lien existera bien entre ces deux noeuds. Pour effectuer ce test, on peut voir si dans le module random (module générant des valeurs aléatoires), il existerait une méthodes renvoyant un booléen VRAI ou FAUX aléatoire. Comme il n'en existe pas voici comment l'on peut outrepasser l'obstacle : générer un nombre binaire 0 ou 1 et forcer l'interprétation booléen de la valeur renvoyé :
```
if bool(getrandbits(1)) :
  graph[x][y] = valeur_du_lien
else:
  graph[x][y] = 0
```
Dans ce cas, si if est FAUX, alors il n'y aura pas de lien entre les noeuds x y.

### Valeur aléatoire du lien

Si le test booléen précédent est VRAI, alors il convient d'affecter une valeur aléatoirement définit. Comme dans tout ce qui est aléatoire, il faut définir des limites, on définit arbitraitement que le lien prendra une valeur allant au maximum jusqu'à 20. Cette valeur est puremet arbitraire et peut être augmenté. En revanche la valeur du lien est forcement positive.

```
graph[x][y] = randint(1,20)
```

Il aurait été possible de coupler le premier test avec l'affectation d'une valeur aléatoire de lien en incluant 0 dans les nombres possiblement généré.

```
graph[x][y] = randint(0,20)
```

L'inconvénient d'une telle solution, est le nombre de chance très minimum qu'un noeud ne soit pas du tout relié avec un autre noeud. Cela aurait géré une multitude de réseaux en étoile où tous les noeuds auraient été reliés aux autres noeuds. Ce qui aurait produit un graphique très lourd à lire.

### Correction de de l'absence totale de lien pour certain point

Le script actuel peut générer des lignes et des colonnes à 0. Ce qui correspond à des noeuds isolés dans le graph, non accessible. Cela pose un problème pour le principe même du routage. Pour cela un petit hack consiste à boucler à nouveau sur la matrice et de sommer chaque valeur d'une ligne. Si cette valeur est de 0 alors un noeud est isolé. Il suffit d'affecter une valeur à l'un des éléments de la ligne. Pour plus de simplification, on choisit le dernier élément en mémoire.

```
for x in range(nbNd):
  valid=0
  for y in range(nbNd):
    if y > x:
      valid += graph[x][y]
  if valid == 0 :
    graph[x][y] = graph[y][x] = randint(1,20)
```

### Positionnement aléatoire

Dans ce script il n'est pas néccessaire de placer les points dans un graph de façon aléatoire étant donné que le module graphique utilisé gère déjà cela. Si cela n'avait pas été le cas 2 solution ont été envisager, dont une a été implémenté et mise en commentaire.

La première solution non implémentée consiste à placer les noeuds sur un cercle. Ainsi, aucun lien n'aurait traversé un noeud tier.

La seconde solution implémentée et mise en commentaire consiste à positionné aléatoirement les noeuds de la matrice sur un graphique en 2D. Et de stocké autant de coupe de valeur x,y que de noeud, dans une liste.

```
graphPos = []
for y in range(nbNodes):
        graphPos.append((randint(0,20),randint(0,20)))
```

### Creation d'un dictionnaire de nom de noeuds

En vue de pouvoir calculer un chemin entre deux noeuds, il convient préalablement des nommer de façon intelligible les noeuds.

La première solution consiste à affecter une lettre de l'alphabet à chaque noeud, mais cela limite le nombre de noeud à 26.

La seconde consiste à affecter 2 lettres par noeuds de type aa, ab, ac ...dr, mais l'inconvénient est que cela pertube l'esprit et que l'utilisateur risque de confondre ab en vecteur entre a et b. Cela aurait en revanche permit un très grand nombre de noeuds (26x26 noeuds).

La troisième solution consiste à réduire la seconde solution aux seules lettres x y z pour généré des noms tel que xa, xb, xc, ..., ya, yb, ... za, ..., zz. Ce qui permet d'avoir 3x26 =oeuds(78 noeuds).


