#!/usr/bin/python3 -B
# -*-coding:utf8 -*

def notparity(data):
  for i in data:
    if i%2 != 0 :
      yield i

liste=[1,2,3,4,5,6,7,8,9,10]
for n in notparity(liste):
  print(n)
