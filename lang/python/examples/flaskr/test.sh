#!/bin/bash

python3 -m venv venv
. venv/bin/activate

pip install -e .
export FLASK_APP=flaskr
export FLASK_ENV=development

flask init-db
flask run

# test
#pip install -e .[test]
#pytest
#coverage run -m pytest
#coverage report
#coverage html
