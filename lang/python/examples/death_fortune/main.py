#!/usr/bin/python3 -B
# -*-coding:utf8 -*

import csv
import numpy as np
import matplotlib.pyplot as plt

#variables
in_csv="data.csv"

liste = []
with open(in_csv, newline='') as csvf :
  reader = csv.reader(csvf, delimiter=',', quotechar='"')
  csvf.readline() # suppression header
  for col in reader:
    #print(' : '.join(col))
    row = { 'age': int(col[0]),
      'sex':col[1], 
      'cp':col[2],
      'trestbps':col[3],
      'chol':int(col[4]),
      'fbs':int(col[5]),
      'restecg':col[6],
      'thalach':col[7],
      'exang':col[8],
      'oldpeak':col[9],
      'slope':col[10],
      'ca':col[11],
      'thal':col[12],
      'target':col[13]  }
    liste.append(row)

with open(in_csv, newline='') as csvf :
  countrow=sum(1 for line in csvf)
  print('Nombre de ligne traitées : ',countrow)
# creation des x et y

x = []
y = []
for i in liste :
  if i['age'] >= 75 :
    x.append(int(i['age']))
    y.append(int(i['chol']))

plt.scatter(x, y)
plt.show()

