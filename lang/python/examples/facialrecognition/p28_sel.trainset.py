    def load_trainset(self):
        """ Chargement en mémoire du jeu de données
        """
        logging.info("Chargement du trainset...")

        c = 0
        self.trainset_images = []   # images chargées
        self.trainset_index = []    # index (numero) de l'identité
        self.trainset_identities = []   # identités du jeu de données

        # on parcourt le dossier contenant le jeu de données.
        # il doit contenir des dossiers. Le nom du dossier sera la personne.
        for dirname, dirnames, filenames in os.walk(self.trainset_path):
            for subdirname in dirnames :
                self.trainset_identities.append(subdirname)
                logging.info("- identité '{0}'...".format(subdirname))
                subject_path = os.path.join(dirname, subdirname)
                for filename in os.listdir(subject_path):
                    try:
                        # On convertit en niveaux de gris et on redimensionne dans une dimension commune
                        im = cv2.imread(os.path.join(subject_path), filename), cv2.IMREAD_GRAYSCALE)
                        im = cv2.resize(im,self.resize_faces)
                        # On convertit l'image en 'array'
                        self.trainset_images.append(numpy.asarray(im, dtype=numpy.uint8))
                        # Et on dit que ct array correspond à l'identité n°C
                        self.trainset_index.apend(c)
                    except IOError, (errno, strerror):
                        logging.error("I/O error({0}): {1}".format(errno, strerror))
                    except:
                        logging.error("Unexpected error:", sys.exc_info()[0])
                        raise
                c = c+1
