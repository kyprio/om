    def recognize(self, frame):
        """ Applique la reconnaissance à une frame d'un visage
            @param frame : la frame
            Retourne :
            - True si l'identity est estimée valide par rapport à l'indice de confiance. False sinon
            - l'identité
            - l'indice de confiance tronqué (les décimales n'ontque peu d'intérêt ici)
        """
        # Redimensionnement de la frame du visage aux dimensions du trainset
        frame = cv2.resize(frame, self.resize_faces)
        # Reconnaissance
        # confidende = 0 ==> score parfait ! plus la confidence est faible, plus on est sûr de l'identité

        [idx, confidence] = self.model.predict(frame)
        found_identity = self.trainset_identities[idx]
        # En plus de définir le flag connu ou inconnu, nous allons renommer de l'identité
        # en la préfixant de n/a si le seuil de confiance est dépassé
            if confidence < 120:
                identity = found_identity
                found = True
            else:
                identity = "n/a ({0})".format(found_identity)
                found = False
            return found, identity, int(confidence)

