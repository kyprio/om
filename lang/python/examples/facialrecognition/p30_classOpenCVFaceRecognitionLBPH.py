class OpenCVFaceRegognitionLBPH(OpenCVGenericRecognition):

    def train(self):
        """ Entrainement du jeu de données
            Methode à surcharger
        """
        logging.info("Entrainement du trainset...")

        # les autres algorithmes :
        #self.model = cv2.createEigenFaceRecognizer()
        #self.model = cv2.createFisherFaceRecognizer()

        # Celui que l'on va utiliser :
        self.model = cv2.createLBPHFaceRecognizer()
        self.model.train(numpy.asarray(self.trainset_images), numpy.asarray(self.trainset_index))
