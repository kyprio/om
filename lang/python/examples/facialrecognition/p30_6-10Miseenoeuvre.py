from ocv_detection import OpenCVFaceFrontalDetection

# On intancie la classes de reconnaissance avec en paramètre le jeu de données
reco = OpenCVFaceRecognitionLBPH("./samples/faces/", archive_folder = "../../archives/")
# On charge le jeu de données en mémoire
reco.load_trainset()
# On lance l'entrainement
reco.load_trainset()
# On lance l'entrainement
reco.train()

# Utilisation de mon jeu de test n°6
dir = "./test/jeu6"
# On boucle sur chaque image du jeu de test
for filename in os.listdir(dir):
    filepath = os.path.join(dir, filename)
    # pour une image, je lance la détection des visages
    detect = OpenCVFaceFrontalDetection(filepath, archive_folder = "../../archives/", debug = True)
    detect.find_items()
    detect.extract_items_frames()
    # et on boucle sur la frame de chaque visage (convertie en niveaux de gris)
    for item in detect.get_items_frames(grayscale = True):
    # On récupère le statut (connu ou non), l'identité théorique et l'indicede confiance
    known, identity, confidence = reco.recognize(item["frame"])
    # On génère un label qu'on va écrire sur l'image
        label = "{0} ({1})".format(identity,confidence)
        logging.info("Trouvé : {0}".format(label))
        x = item["x"]
        y = item["y"]
        detect.add_label(label, x, y)
        # Et on archive à la fois les visages et l'image complète
        detect.archive_items_frames()
        detect.archive_with_items()
