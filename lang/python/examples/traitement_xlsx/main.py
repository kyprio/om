#!/usr/bin/python3 -B
# -*-coding:utf8 -*

import xlrd # python3-xlrd
import matplotlib.pyplot as plt
filexlsx = 'data.xlsx'
workbook = xlrd.open_workbook(filexlsx)
sheet = workbook.sheet_by_index(0)

# information pratique
nrows = sheet.nrows
ncols = sheet.ncols
print(filexlsx)
print("nb ligne : ",nrows)
print("nb colonnes : ",ncols)

# parcours des en-têtes sur 1ère ligne
for c in range(ncols) :
  print('entete',c,':',sheet.cell_value(0,c))

# mise sous liste
data = [[ sheet.cell_value(r,c) for c in range(ncols)]for r in range(nrows)]

col1=[]
col2=[]
# parcours des lignes 1 à nrows
for r in range(1,nrows):
  col1.append(data[r][0])
  col2.append(data[r][1])
import matplotlib.pyplot as plt
 
fig = plt.figure()
ax = fig.add_subplot(1,2,1, projection='2d')
ax.scatter(df["taille_en_pieds_carre"], df["nb_chambres"], df["prix"], c='r', marker='^')
 
ax.set_xlabel('surface en pieds_carre')
ax.set_ylabel('nb_chambres')
ax.set_zlabel('prix en $')
 
plt.show()
