#!/usr/bin/python3 -B
# -*-coding:utf8 -*

import sys
import argparse
import os.path

def usage() :
  print('Usage: main_argparse.py [-v|--verbose] [-o|--output=filename]')
  print('  filename = input file')
  print('  -h | --help = help')
  print('  -v | --verbose = verbose mode')
  print('  -o | --output=filename = output file')
  exit()

def getargs(args):
  # création du parser
  parser = argparse.ArgumentParser(description='get arguments')
  # arguments facultatifs avec paramètre
  parser.add_argument('-o','--output', dest='output', default='output', help='output file')
  parser.add_argument('-d','--logdir', dest='logdir', help='input logdir')
  parser.add_argument('-f','--logfile', dest='logfile', help='input logfile')
  return parser.parse_args()

def logdirdefault() :
  # tester si existe :
  defdirlog = ['/var/log/apache2/','/var/log/httpd/','/var/log/httpd24']
  for i in defdirlog :
    if os.path.isdir(i):
      return i
  # dirlog not found
  print("dirlog not found")
  exit(1)

if __name__ == '__main__' :
  # argument
  args = getargs(sys.argv)
  print(args)

  # test dirlog par defaut
  logdir=logdirdefault()
  print(logdir)
