from flask import Flask
from flask_bootstrap import Bootstrap
from flask_debugtoolbar import DebugToolbarExtension
from test_template.config import Config
from test_template.db import db
from test_template.models import *


# instanciate Flask extentions
toolbar = DebugToolbarExtension()
Bootstrap = Bootstrap()
# db already instanciated and imported from db.py

# Create the app
def create_app(test_config=None):
  # Instantiate the App and config (def instance dir = ./instance)
    # Instantiate the Flask app
    app = Flask(__name__, instance_relative_config=True)
    app.config.from_object(Config)

    # init_app each Extension
    db.init_app(app)
    toolbar.init_app(app)
    Bootstrap.init_app(app)
    # creation of the schema in .db file
    with app.app_context():
      # Extensions now knows which "app" to use
      # prevent error like : "No application found. Either work inside a view function or push an application context."
      db.create_all()
      
    # register the database commands
    from test_template.main.routes import main
    from test_template.users.routes import users
    app.register_blueprint(main)
    app.register_blueprint(users)
    
    return app
