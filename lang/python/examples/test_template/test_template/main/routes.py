from flask import Blueprint, render_template

main = Blueprint('main', __name__ )

@main.route('/')
def home():
  list_modules = ['pdo','mysql']
  return render_template('index.html', list_modules = list_modules )
