#!/usr/bin/python3
# -*-coding:utf8 -*

# SCHEMA : 
# LED1+ = 7 GPIO4
# LED- = resistance+
# resistance- = ground

import RPi.GPIO as GPIO
import time

# creation de la fonction blink
def blink(pin, n=50):
    
    #initialisation du mode de lecture des numero de pin
    GPIO.setmode(GPIO.BCM)
    # initialisation du mode IN ou OUT de la variable pin
    GPIO.setup(pin, GPIO.OUT)

    for i in range(n):
        # allume pendant 1sec
        GPIO.output(pin, GPIO.HIGH)
        time.sleep(1)
        # eteint pendant 1sec
        GPIO.output(pin, GPIO.LOW)
        time.sleep(1)


# appel de la fonction sur GPIO4
if __name__ == "__main__":
    blink(4)

