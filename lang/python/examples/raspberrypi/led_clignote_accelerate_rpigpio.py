#!/usr/bin/python3
# -*-coding:utf8 -*

# SCHEMA : 
# LED1+ = 7 GPIO4
# LED- = resistance+
# resistance- = ground

import RPi.GPIO as GPIO
import time
import numpy

# creation de la fonction blink
def blink(pin, n=50):
    
    #initialisation du mode de lecture des numero de pin
    GPIO.setmode(GPIO.BCM)
    # initialisation du mode IN ou OUT de la variable pin
    GPIO.setup(pin, GPIO.OUT)
    # initialisation de la valeur de temps variable
    w = [ numpy.cos(x) / 4 + 0.25 for x in numpy.linspace(0, 2 * numpy.pi, n) ]

    for i in range(n):
        # allume pendant 1sec
        GPIO.output(pin, GPIO.HIGH)
        time.sleep(w[i])
        # eteint pendant 1sec
        GPIO.output(pin, GPIO.LOW)
        time.sleep(w[i])


# appel de la fonction sur GPIO4
if __name__ == "__main__":
    blink(4)

