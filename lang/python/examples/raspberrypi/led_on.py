#!/usr/bin/python3
# -*-coding:utf8 -*

# SCHEMA : 
# LED+ = 7 GPIO4
# LED- = resistance+
# resistance- = ligne ground
# ligne ground = 9 Ground

import RPi.GPIO as GPIO

GPIO.setmode(GPIO.BCM)
GPIO.setwarnings(False)
GPIO.setup(4,GPIO.OUT)

GPIO.output(4,GPIO.HIGH)
