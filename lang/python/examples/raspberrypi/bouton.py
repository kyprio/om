#!/usr/bin/python3
# -*-coding:utf8 -*

# SCHEMA : 
# BUTTON+ = 5 GPIO3
# BUTTON- = 9 ground

from gpiozero import LED, Button
from signal import pause

button = Button(3)

button.when_pressed = print("ON")
button.when_released = print("OFF")

pause()
