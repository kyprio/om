#!/usr/bin/python3
# -*-coding:utf8 -*

# SCHEMA : 
# LED1+ = 7 GPIO4
# LED2 = 15 GPIO22
# LED- = resistance+
# resistance- = ligne ground
# ligne ground = 9 Ground

from gpiozero import LED
from time import sleep

led1 = LED(4)
led2 = LED(22)

while True:
    led1.on()
    led2.off()
    print("led1")

    sleep(1)
    
    led1.off()
    led2.on()
    print("led2")
    
    sleep(1)
