#!/usr/bin/python3
# -*-coding:utf8 -*

import donnees

"""Liste des fonctions néccessaire au pendu"""

def choix_mot(liste):
	"""fonction qui va piocher parmi une liste donnée de mots et va rapidement analyser ce mot"""
	from random import randrange
	choix=liste[randrange(liste.__len__())]
	nb=len(choix)
	cherche="*".center(nb,'*')
	return choix, nb, cherche
	
def presentation(nb,cherche, choix):
	"""Presentation du mot à deviner"""
	print("\nVous devez découvrir ce qu'il y a derrière ces étoiles.\n\
Vous avez",donnees.chances,"coups !\n\
Voici le mot recherché: \n".format(nb))
	print(cherche)

def verif_erreur(liste):
	"""Saisie d'une lettre et verification"""
	erreur=True
	encore=""
	while erreur :
		print("Devinez {}une lettre :".format(encore))
		lettre=input()
		try :
			if len(lettre) != 1 or not lettre.lower() in liste :
				raise ValueError
				erreur=True
			else:
				erreur=False
		except ValueError :
			print("\nErreur\nIl faut inscrire impérativement 1x lettre.")
			print("Elle doit faire partie de l'alphabet :",liste,"\n")
		encore="à nouveau "
	return lettre

def majuscule(lettre):
	"""mise en majuscule et sans accent"""
	if lettre in "éêè":
		lettre="e"
	elif lettre in "à" :
		lettre="a"
	return lettre.upper()

def verif_mot(cherche, lettre, setting, choix, nb,chances):
	"""Verification lettre dans mot"""
	cherche=list(cherche)
	if lettre in setting:
		print("Déjà dit, faut être toc toc pour se répéter pour si peu de lettre à trouver !")
	elif lettre in choix:
		print("Bien trouvé !\n")
		i=0
		while i <= nb:
			indice=choix.find(lettre,i)
			if indice == -1 : break
			cherche[indice] = lettre
			print 
			i=1+indice
	else:
		chances-=1
		print("Eh non ! ca fait une chance en moins.\nchance={}".format(chances))
	setting.add(lettre)
	cherche=''.join(cherche)
	print(cherche)
	return cherche, chances

def ton_nom(chances):
  """Enregistre le score du joueur dans un dico avec son pseudo"""
  pseudo=input("C'est quoi ton blaze de merde ?!")
  

# if pseudo in dico :
#   dico[pseudo] = dico[pseudo] + chances
#   print("déjà client")
# else :
#   dico[pseudo] = chances
# print("Vous, {}, avez {} points.".format(pseudo,dico[pseudo]))



  import pickle
  liste={pseudo:chances,"Sam":2}
  with open("score","wb") as liste_score:
    mon_pickler=pickle.Pickler(liste_score)
    mon_pickler.dump(liste)
