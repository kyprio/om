#!/usr/bin/python3
# -*-coding:utf8 -*

""" programme du pendu regroupant touts les fichiers"""

	#IMPORTATION DES FONCTIONS ET OBJETS
import fonctions
import donnees

continuer="C"

while continuer == "C":

		#CHOIX ALEATOIRE D'UN MOT, MISE EN PLACE DES VARIABLES
	mot_choisi, nb_lettre, mot_cherche = fonctions.choix_mot(donnees.liste)
		#PRESENTATION DU MOTS A DEVINEZ
	fonctions.presentation(nb_lettre,mot_cherche,mot_choisi)

	while mot_choisi != mot_cherche and donnees.chances > 0 :
			#SAISIE D'UNE LETTRE PAR LE JOUEUR ET VERIFICATION
		lettre=fonctions.verif_erreur(donnees.lettres_acceptees)
			#MISE EN FORME DE LA LETTRE EN MAJUSCULE SANS ACCENT
		lettre=fonctions.majuscule(lettre)
			#VERIFICATION LETTRE DANS MOT_CHOISI
		mot_cherche, donnees.chances = fonctions.verif_mot(mot_cherche, lettre, donnees.deja_dit, mot_choisi, nb_lettre, donnees.chances)
		print(donnees.deja_dit)
		#RESULTAT ET SCORE	
	if mot_choisi == mot_cherche :
		print("\nCool ! il vous restait encore {} coups et vous avez pourtant trouvé le mot !".format(donnees.chances))
	elif donnees.chances == 0:
		print("Eh ben fallait bien essayé, et vous le savez à présent...\nVous êtes nul !")
	else :
		print("ya un truc qui cloche")
	donnees.joueur=fonctions.ton_nom(donnees.chances)
		#CONTINUER ?
	continuer=input("Tu veux continuer ?\nC pour Continuer\n(rien) ou N pour arrêter : ").upper()
	donnees.deja_dit=set()
	donnees.chances=8
