#!/usr/bin/python3
# -*-coding:utf8 -*


chiffres = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ"
nombres = { c : n for n, c in enumerate(chiffres)}


def chiffre(n):
	return chiffres[n]
def nombre(ch):
	return nombres[ch]
def convertir(b=2):
	rep=input("quelle nombre à convertir en entier? : ")
	nb_chiff = len(rep)
	somme = 0
	b_puiss_i = 1
	for i in range(nb_chiff):
		c_i = rep[nb_chiff - i -1 ]
		somme = somme + nombre(c_i) * b_puiss_i
		_puiss_i = b_puiss_i * b
	return somme

