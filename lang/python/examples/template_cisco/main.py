#!/usr/bin/python3 -B
# -*-coding:utf8 -*

from jinja2 import Template
import yaml

# variables

data = yaml.load(open('data.yml'))
with open('output', 'w') as output :
  with open('template.tpl') as tpl :
    out=str(Template(tpl.read()).render(data))
    print(out)
    output.write(out)
