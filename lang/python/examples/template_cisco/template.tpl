# DEBUT
# OSPF partout

# FRAMERELAYS
#############
{% for framerelay in framerelays %}
# {{framerelay.hostname}}
enable
conf terminal
no ip domain lookup
hostname {{framerelay.hostname}}
frame-relay switching 
{% for map in framerelay.dlci_maps %}interface {{map.serial_x}}
no ip address
encapsulation frame-relay
frame-relay lmi-type ansi
clock rate 64000
frame-relay intf-type dce
frame-relay route {{map.dlci}} interface {{map.serial_y}} {{map.dlci}}
no shutdown
no keepalive
exit
{% endfor %}end
copy running-config startup-config
{% endfor %}

# ROUTER MP
###########
{% for router in routersMP %}
# {{router.hostname}}
enable
conf terminal
no ip domain lookup
hostname {{router.hostname}}
interface Serial 1/0
ip address {{router.ip}} {{router.mask}}
encapsulation frame-relay
frame-relay lmi-type ansi
{%for map in router.dlci_maps %}frame-relay map ip {{map.ip}} {{map.dlci}} broadcast
{% endfor %}no shutdown
no keepalive
exit
router eigrp {{router.eigrp_id}}
network {{router.eigrp_network}} {{router.eigrp_mask}}
no auto-summary
end
copy running-config startup-config
{% endfor %}

# ROUTER PP
###########
{% for router in routersPP %}
# {{router.hostname}}
enable
conf terminal
no ip domain lookup
hostname {{router.hostname}}
interface {{router.interface}} 
no ip address
encapsulation frame-relay
frame-relay lmi-type ansi
no shutdown
no keepalive
exit
{% for ssinterface in router.ssinterfaces %}interface {{ssinterface.name}} point-to-point
ip address {{ssinterface.ip}} {{ssinterface.mask}}
frame-relay interface-dlci {{ssinterface.dlci}}
bandwidth 56
no shutdown
no keepalive
exit
router eigrp {{router.eigrp_id}}
{% for eigrp in router.eigrp_networks %}network  {{eigrp.network}} {{eigrp.mask}}
{% endfor %} no auto-summary
{% endfor %}end
copy running-config startup-config
{% endfor %}

# ROUTER CHAP bidirectionnelle
##############################
{%for router in routersCHAP %}
# {{router.hostname}} suite CHAP
enable
conf terminal
hostname {{router.hostname}}
username {{router.username}} password {{router.password}}
interface {{router.interface}} 
ip address {{router.ip}} {{router.mask}}
no shutdown
no keepalive
encapsulation ppp
ppp authentication chap {{router.pasword_chap}}
exit
router eigrp {{router.eigrp_id}}
network {{router.eigrp_network}} {{router.eigrp_mask}}
end
copy running-config startup-config
{% endfor %}

# ROUTER PAP bidirectionnelle
#############################
{%for router in routersPAP %}
# {{router.hostname}} suite PAP
enable
conf terminal
hostname {{router.hostname}}
username {{router.username_local}} password {{router.password_local}}
interface {{router.interface}} 
ip address {{router.ip}} {{router.mask}}
no shutdown
no keepalive
encapsulation ppp
ppp authentication pap {{router.pasword_chap}}
ppp pap sent-username {{router.username_remote}} password {{router.password_remote}}
exit
router eigrp {{router.eigrp_id}}
network {{router.eigrp_network}} {{router.eigrp_mask}}
end
copy running-config startup-config
{% endfor %}

# FIN
