#! /usr/bin/python3

# On va utiliser Glob pour pouvoir lister les fichiers avec notre extension txt
from os import chdir
from os import chmod
from glob import glob

rep=''
while rep == '' :
  rep=input("entrez votre repertoire: ")

print('Conteu de ', rep,' : ')

chdir(rep)
for i in glob("*.txt"):
  print(i)
  chmod(i, 0o744)

exit()

print("Votre repertoire est : " ,rep, "Et la liste des fichiers dont les droits seront changés sont les suivants: ")
# On va utiliser notre repertoire dans la variable rep
# On y parcours tous les fichiers en .txt
for file in glob.glob("*.txt"):
# Pour chaque fichier en .txt, on applique les droits 744 puis on les affiche au passage
    print(file)

