#!/usr/bin/python3 -B
# -*-coding:utf8 -*

import os
from sys import argv

# variable
limite = int(argv[1])
userHome = {}
etcpasswd = "/etc/passwd"

# méthode

def CompterDossier(path) :
  count=0
  for root, dirs, files in os.walk(path):
    for name in files:
      count+=1
      #print(os.path.join(root,name))
    for name in dirs:
      count+=1
      #print(os.path.join(root,name))
    return count

#execution

with open(etcpasswd) as f:
  for i in f.read().splitlines():
    e=i.split(':')
    if int(e[2]) >= 1000 and int(e[2]) <= 50000 :
      userHome[e[0]] = e[5]

for user, home in userHome.items():
  print("{} : {} ".format(user,home))
  if CompterDossier(home,limite) > limite :
    print("Limite dépassée")
  else :
    print("limite pas encore atteinte")
