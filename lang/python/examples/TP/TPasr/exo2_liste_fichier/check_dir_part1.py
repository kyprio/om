#! /usr/bin/python3

import os, argparse
# On va utiliser Parser pour donner l'argument du nombre de fichier limite dans l'arbo.

parser = argparse.ArgumentParser()
parser.add_argument("x", type=int, help="le nombre limite de fichier")
args=parser.parse_args()
x=args.x

my_dir=input("entrez votre repertoire: ")

## Petite condition qui dit que si aucun repertoire specifié, alors on utilise le repertoire courant

if not my_dir:
    my_dir=os.getcwd()
# On met à 0 l'indice i
i=0
#On va chercher dans le path renseigne, le nombre total de fichier
# root est le repertoire courant exploré, dirs les repertoires à l'interieur si existant, et files, les fichiers à l'interieur si existant
for root, dirs, files in os.walk(my_dir):
    for n in files:
        i+=1

if i > x:
    print(-1)
else:
    print(0)
