#! /usr/bin/python3

import os, argparse, re, sys

# On créer un dico vide qui sera utilisé pour stocker le path des utilisateurs

list_path=[]

# On ouvre le fichier /etc/passwd en lecture, puis on va splitter dans fiels la ligne en supprimant les ":" par des espaces.
# A partir de là, on a des espaces entre les arguments, et on peut travailler en colomne ( on veut celle qui correspond à un groupe avec ID 1000)
# Si la colomne des groupes (la 4e car on part de 0) correspond à 1000, on va prendre le path de l'utilisateur ( qui correspond à colomne 5 et l'ajouter à la liste)


for line in open("/etc/passwd", 'r'):
    fields=line.strip().split(":")
    groups="1001"
    if fields[3] == groups:
        list_path.append(fields[5])

#On va utiliser Parser pour donner l'argument du nombre de fichier limite dans l'arbo.

parser = argparse.ArgumentParser()
parser.add_argument("x", type=int, help="le nombre limite de fichier")
args=parser.parse_args()
x=args.x
## On met à 0 l'indice i
i=0
##On va chercher dans le path des utilisateurs, le nombre total de fichier de tous les repertoires (et sous répertoire) et les aditionner.
## root est le repertoire courant exploré, dirs les repertoires à l'interieur si existant, et files, les fichiers à l'interieur si existant

for home_path in list_path:
    for root, dirs, files in os.walk(home_path):
        for n in files:
            i+=1

if i > x:
    print(-1)
else:
    print(0)
