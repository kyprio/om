#!/usr/bin/python3 -B
# -*-coding:utf8 -*

import os
from sys import argv

# variable
limite = int(argv[1])
path = argv[2]
etcpasswd = "/etc/passwd"

# méthode

def CompterDossier(path) :
  count=0
  for root, dirs, files in os.walk(path):
    for name in files:
      count+=1
      #print(os.path.join(root,name))
    for name in dirs:
      count+=1
      #print(os.path.join(root,name))
    return count

#execution

print(path)
if CompterDossier(path) > limite :
  print("Limite dépassée")
  exit(1)
else :
  print("limite pas encore atteinte")
  exit(0)
