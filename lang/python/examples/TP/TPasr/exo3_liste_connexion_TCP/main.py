#!/usr/bin/python3 -B
# -*-coding:utf8 -*

# Paramètre entrant : IP distante (tester en local 127.0.1.1)

import sys 

# PREPARATION

# variables
tcp="/proc/net/tcp"
#udp="/proc/net/udp"
ipdec=sys.argv[1]

# convertion nombre
def hex2dec(s):
  return str(int(s,16))
def dec2hex(s):
  return str(format(int(s),'02x'))

# convertion format ip
def hex2ip(s):
  ip = [(hex2dec(s[6:8])),(hex2dec(s[4:6])),(hex2dec(s[2:4])),(hex2dec(s[0:2]))]
  return '.'.join(ip)
def ip2hex(s):
  ipliste = s.split('.')
  iphex = ""
  for i in reversed(ipliste) :
    iphex += dec2hex(i)
  return iphex.upper()

# fichier vers liste
def file2liste(fichier):
  # fichier vers liste
  with open(fichier) as f :
    liste=f.read().splitlines()
  return liste

# EXECUTION

iphex = ip2hex(ipdec)
listeconn = file2liste(tcp)
found=[]
for l in listeconn:
  if iphex in l.split(' ')[4] :
    found.append(l)

print("TCP")
print("IPsource:port -> IPdestination:port")
for e in found:
  li = e.split()
  # extraction des colonnes de tcp et conversion
  ipsrc = hex2ip(li[1].split(':')[0])
  portsrc = hex2dec(li[1].split(':')[1])
  ipdst = hex2ip(li[4].split(':')[0])
  portdst = hex2dec(li[4].split(':')[1])
  print('{}:{} -> {}:{}'.format(ipsrc,portsrc,ipdst,portdst))
