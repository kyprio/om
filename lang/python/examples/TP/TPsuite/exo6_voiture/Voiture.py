#!/usr/bin/python3
# -*-coding:utf8 -*

# définition de classe

class Voiture:
 
 # Constucteur et attribut
  def __init__(self, marque='Renault', couleur='rouge'):
    self.marque=marque
    self.couleur=couleur
    self._pilote='personne'
    self._vitesse=0


  # méthodes de type setter getter
  def choix_conducteur(self,nom):
    self._pilote = nom

  # methodes
  def accelerer(self,taux,duree):
    if self._pilote != 'personne' :
      self._vitesse= taux * duree


  # override du toString python
  def __str__(self):
    return "Marque = {}, Couleur = {}, Pilote = {} , vitesse = {}".format(self.marque, self.couleur, self._pilote, self._vitesse)
  def affiche_tout(self):
    return self.__str__()
