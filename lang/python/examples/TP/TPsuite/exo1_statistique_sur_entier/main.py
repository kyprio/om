#!/usr/bin/python3
# -*-coding:utf8 -*

import numpy

liste = [ int(i) for i in input("Liste d'entier :").split() ]
print(type(liste[0]))

max = min = liste[0]
somme = 0
for i in liste :
  somme += i
  if i < min:
    min = i
  if i > max:
    max = i

print("min : " ,min)
print("max : " ,max)
print("somme : ", somme)
print("moyenne : ", numpy.average(liste))
print("ecart : ", numpy.std(liste))
