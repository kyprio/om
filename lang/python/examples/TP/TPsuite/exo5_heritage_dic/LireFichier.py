#!/usr/bin/python3 -B
# -*-coding:utf8 -*

import configparser

class LireFichier(dict):
  
  # Contructeur
  def __init__(self, fichier, subsection):
    dict.__init__(self)
    with open('fichier.ini') as f : 
      for l in f.read().splitlines():
        if l.startswith(subsection):
          self[l.split('.')[1].split('=')[0]] = l.split('=')[1]
