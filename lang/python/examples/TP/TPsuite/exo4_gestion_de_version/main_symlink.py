#!/usr/bin/python3
# -*-coding:utf8 -*

# commande utile
import os 
from os import listdir as ls
from os import chdir as cd
from os import getcwd
import shutil
from os import symlink as ln
import re
  
# VARIABLES
dossier='dossier'
projet=os.path.realpath('projet_en_cours')
pwd=getcwd()


# EXECUTION

if os.path.exists(projet):
    shutil.rmtree(projet)
os.mkdir(projet)

cd(dossier)
ls_file=ls()

# récupération de la dernière version
version=0
for i in ls_file:
  resultat = re.sub(r'.*-v([0-9]{3}).*',r'\1',i)
  if version <= int(resultat) :
    version = int(resultat)

# création des liens
for last_file in ls_file:
  if str(version).zfill(3) in last_file :
    # chemin complet et suppresion de pattern "version"
    ln_file = projet+"/"+ re.sub('<-v[0-9]{3}>','',last_file)
    # chemin complet
    last_file = os.path.realpath(last_file)
    ln(last_file,ln_file)
