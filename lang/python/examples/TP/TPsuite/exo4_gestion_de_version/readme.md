#  main_createfile.py

test la présence de **dossier** sinon le créé
test la présence de fichier et récupère la version la plus grande, sinon l'initialise à 001
prend en paramètre une de fichier, et les place dans dossier avec la nouvelle version

# main_symlink.py

test la présence de **projet_en_cours** sinon le créé
liste les fichiers puis récupère la dernière version des fichiers dans dossier
filtre la liste des fichiers en ne traitant que ceux qui ont la dernière version
pour chacun d'entre eux, génère un lien symbolique placé **projet_en_cours**
