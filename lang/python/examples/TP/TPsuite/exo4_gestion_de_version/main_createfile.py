#!/usr/bin/python3
# -*-coding:utf8 -*

# commande utile
from os import listdir as ls
from os import chdir as cd
from os import getcwd
import os
import re
  
# VARIABLES
dossier='dossier'
pwd=getcwd()


# FONCTIONS

# fonction récupération de la version en cours des fichiers présents
def getLastVersion(directory) :
  cd(directory)
  ls_file=ls()
  cd(pwd)
  version = 0
  print("\nListe fichiers existants : ")
  for i in ls_file:
    print(i)
    resultat = re.sub(r'.*-v([0-9]{3}).*',r'\1',i)
    if version <= int(resultat) :
      version = int(resultat)
  return version

# fonction de création des nouveaux fichiers avec version définies
def createNewFile(directory,new_files,new_version) :
  cd(directory)
  print("\nListe fichiers à créer : ")
  for i in new_files :
    fichier=i.split('.',1)
    try : 
      filename=fichier[0]+"-v"+new_version+"."+fichier[1]
    except :
      filename=fichier[0]+"-v"+new_version
    print(filename)
    with open(filename,'w') as fw :
      fw.write("")
  cd(pwd)

# EXECUTION

if not os.path.exists(dossier):
    os.mkdir(dossier)

# récupération de la version en cours (001 par defaut)
try :
  last_version = str(getLastVersion(dossier)).zfill(3)
  new_version = str(int(last_version)+1).zfill(3)
except :
  last_version = '000'
  new_version = str(int(last_version)+1).zfill(3)

print("\ndernière version trouvée : ", last_version)
print("prochaine version utilisée : ", new_version)

# demande la liste des nouveaux fichiers
new_files = [ str(i) for i in input("\nListe des fichiers à créer (séparés par un espace): ").split() ]

# création des fichiers avec la nouvelle version
createNewFile(dossier,new_files,new_version)
