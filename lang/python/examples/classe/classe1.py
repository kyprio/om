#!/usr/bin/python3 -B

class classe1 :
 
  def __init__(self,nom,prenom):
    self.__nom = nom
    self.prenom = prenom

  @property
  def nom(self) :
    return self.__nom

  @nom.setter
  def nom(self,nom):
    self.__nom = nom

  def __str__(self):
    return "nom {}, prenom {}".format(self.__nom,self.prenom)
