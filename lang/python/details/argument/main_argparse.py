#!/usr/bin/python3 -B
# -*-coding:utf8 -*

import sys
import argparse

def usage() :
  print('Usage: main_argparse.py [-v|--verbose] [-o|--output=filename]')
  print('  filename : input file')
  print('  -h | --help : help')
  print('  -v | --verbose : verbose mode')
  print('  -o | --output=filename : output file')
  exit()

def getargs(args):
 
 # création du parser
  parser = argparse.ArgumentParser(description='get arguments')
  
  # liste arguments obligatoires sans paramètre
  parser.add_argument('input', action='store', help='input file')
  # arguments facultatifs avec paramètre
  parser.add_argument('-v','--verbose', dest='verbose',default=False, action='store_true', help='verbose mode')
  parser.add_argument('-o','--output', dest='output', default='output', help='output file')
  return parser.parse_args()

if __name__ == '__main__' :
  args = getargs(sys.argv)
  print(args)
