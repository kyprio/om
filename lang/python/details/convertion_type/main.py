#!/usr/bin/python3 -B
# -*-coding:utf8 -*

# hex to int (base 16)
a = int('ff',16) ) # 255
print(a)

# int to hex
a = hex(255) # 0xff
print(a)
a = format(255,'x') # ff
print(a)
a = format(0,'02x') # 00
print(a)

# bin to int (base2)
a = int('11111111',2) # 255
print(a)

# int to bin
a = bin(255) # 0b11111111
print(a)
a = format(255,'b') # 11111111
print(a)
