#!/usr/bin/python3
# -*-coding:utf8 -*

"""
la fonction yield s'utilise dans une boucle
elle ne renvoit pas objet fini comme return
mais un objet de type generator contenant uniquement l'élement en cours
elle continuera alors le traitement de la boucle 
puis renverra à nouveau son nouvelle élément en cours
"""
"""
range(début,limite max non depassée, pas)
"""
def reverse(data):
  for index in range(len(data)-1, -1,-1):
    yield data[index] # renvoit le caractère de l'index

for char in reverse('4321') :
  print(char)
