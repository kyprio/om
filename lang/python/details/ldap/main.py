#!/usr/bin/python3
# -*-coding:utf8 -*

import ldap

# variables de connexion ldap
LDAP_HOST = 'ldap://172.16.1.1'
LDAP_ADMIN = 'cn=admin,dc=edu,dc=librescargot,dc=fr'
LDAP_PASSWORD = 'secret'
LDAP_BASEDN = 'dc=edu,dc=librescargot,dc=fr'
LDAP_grp = 'users'

class Ldap():

# définition de classe
  def lastuid():
    #initialisation connection au ldap
    conn = ldap.initialize(LDAP_HOST)
    #authentification compte admin ldap
    conn.simple_bind_s(LDAP_ADMIN,LDAP_PASSWORD)
    res = conn.search_s(LDAP_BASEDN, ldap.SCOPE_SUBTREE, 'objectclass=posixaccount', ['uidNumber'])
    lastUid = 0 
    for i in res:
      uidtemp = int(i[1].get('uidNumber')[0])
      if uidtemp > lastUid:
        lastUid = uidtemp
    print(lastUid) 
