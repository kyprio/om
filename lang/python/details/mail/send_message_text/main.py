#!/usr/bin/python3 -B
# -*-coding:utf8 -*

# import
import smtplib
from email.mime.text import MIMEText
from email.header import Header

# contenu mail
text="""\
Contenu du mail
Cordialement
"""

# instanciation objets email
email = MIMEText(text, _charset='utf-8')

# information mail
#email['Subject'] = Header('Objet','utf-8')
email['Subject'] = Header(input('Objet : '),'utf-8')
#email['From'] = 'expediteur@mail.com'
email['From'] = input('mail expéditeur : ')
#email['To'] = 'destinataire@mail.com'
email['To'] = input('mail destinataire : ')

# SMTP
#smtpserver = smtplib.SMTP('smtp.atoutlibre.fr')

# SMTP_SSL
#smtpserver = smtplib.SMTP_SSL('smtp.mail.com:465')
smtpserver = smtplib.SMTP_SSL(input('serveur smtp-ssl et port : '))
#smtpserver.login('username','mdp')
smtpserver.login(input('login : '),input('mot de passe : '))

# envoie mail
print('Envoie mail...')
smtpserver.send_message(email)
print('Email envoyé')
smtpserver.quit()
