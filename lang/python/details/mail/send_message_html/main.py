#!/usr/bin/python3 -B
# -*-coding:utf8 -*

# import
import smtplib
from email.mime.text import MIMEText
from email.header import Header
from email.mime.multipart import MIMEMultipart

# Contenu mail format text & html
text = "ConHi!\nHow are you?\nHere is the link you wanted:\nhttp://www.python.org"
html = """\
<html>
  <body>
  <h1>Title</h1>
    <p>How are you?<br>
       Here is the <a href="http://www.python.org">link</a> you wanted.
    </p>
    <h2>title</h2>
  </body>
</html>


"""
# instanciation objets email multipart
email = MIMEMultipart('alternative') # 'alternative'/'multipart'

# information mail
#email['Subject'] = Header('Objet','utf-8')
#email['From'] = 'expediteur@mail.com'
#email['To'] = 'destinataire@mail.com'
#email['Subject'] = Header(input('Objet : '),'utf-8')
#email['From'] = input('mail expéditeur : ')
#email['To'] = input('mail destinataire : ')
email['Subject'] = Header('Objet','utf-8')
email['From'] = 'librescargot@mailer.fr'
email['To'] = 'librescargot@gmail.com'

# part1 text / part2 html (la dernière partie est privilégiée)
part1 = MIMEText(text, 'plain')
part2 = MIMEText(html, 'html')
email.attach(part1)
email.attach(part2)

# SMTP
#smtpserver = smtplib.SMTP('smtp.mailer.fr')

# SMTP_SSL
#smtpserver = smtplib.SMTP_SSL('smtp.mail.com:465')
#smtpserver.login('username','mdp')
#smtpserver = smtplib.SMTP_SSL(input('serveur smtp-ssl et port : '))
#smtpserver.login(input('login : '),input('mot de passe : '))
smtpserver = smtplib.SMTP_SSL('smtp.mailer.fr:465')
smtpserver.login('librescargot','mdpmdp')

# envoie mail
print('Envoie mail...')
smtpserver.send_message(email)
print('Email envoyé')
smtpserver.quit()
