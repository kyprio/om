#!/usr/bin/python3
# -*-coding:utf8 -*

import re

# variables
series= 'Serie1 : Acteur serie1 ; Serie2 : Acteur serie2 ; Serie1 : Acteur2 serie1 ; Serie2 : Acteur2 serie2'
pattern = r'([A-Za-z0-9 ]+)[;|$]' # motif encadrant(motif à récupéré)motif encadrant
key = r'Serie1 : '

# recherche de plusieurs termes d'un motif définit de type clés valeur en regex sur un string
# on renvoit uniquement le (pattern)
pattern = r'([A-Za-z0-9 ]+)[;|$]'
key = r'Serie1 : '
match = re.findall(key + pattern, series)
print("recherche clés valeur sur string")
for i in match :
  print(i)

# recherche si pattern existe ou non
print("recherche si clés valeur existe")
if re.search(key+pattern, series):
  print("{} existe".format(key))
