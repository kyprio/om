#!/usr/bin/python3
# -*-coding:utf8 -*

from Compte import Compte

# définition de classe

class CompteCourant(Compte):
 
  type_compte='CompteCourant'
  @classmethod # methode statique
  def showType(cls):
    return cls.type_compte

 # Constucteur de la classe
 # avec valeur de paramètre par défaut
  def __init__(self, nom = 'Dupont', solde = 1000):
    # constucteur de la classe mère
    Compte.__init__(self, nom, solde)
    # attribut private (accessible uniquement pour la classe)
    self.__plafond = -500
 
 # Methode de classe
  
  def depot(self, somme):
    self._solde = self._solde + somme 

  def retrait(self, somme):
    if (self._solde - somme) >= self.__plafond : 
      self._solde = self._solde - somme
    else :
      print("retrait impossible : Attention au plafond")

  def setPlafond(self, plafond):
    if plafond <= 0 :
      self.__plafond = plafond
    else :
      print("erreur car plafond > 0")

  # override du toString python
  def __str__(self):
    return "CompteCourant %s / solde : %s / plafond : %s" %(self.nom, self._solde, self.__plafond)
