#!/usr/bin/python3 -B
# -*-coding:utf8 -*

from Compte import Compte
from CompteCourant import CompteCourant
from PEL import PEL

# MAIN
# variables
c = Compte()
cc = CompteCourant()
pel = PEL('PEL DUPONT', 450)

# execution

# Compte
print("\nCOMPTE")
print(c)

# CompteCourant
print("\nCOMPTE COURANT")
print(cc) # solde : 1000 / plafond : -500
cc.solde=500 # solde : 500
cc.retrait(50) # solde : 450
cc.depot(5) # solde : 455
cc.setPlafond(-100) # : plafond : -100
cc.retrait(600) # retrait impossible à cause plafond
print(cc) # solde 455 : plafond -100

# PEL
print("\nPEL") # solde : 450
pel.solde=40 # impossible
pel.retrait(40) # impossible
pel.depot(50) # solde : 500
print(pel)


# Methode statique
print(Compte.showType())
print(CompteCourant.showType())
