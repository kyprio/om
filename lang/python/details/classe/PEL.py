#!/usr/bin/python3
# -*-coding:utf8 -*

from Compte import Compte

# définition de classe

class PEL(Compte):
 
 # Constucteur de la classe
 # avec valeur de paramètre par défaut
  def __init__(self, nom = 'Dupont', solde = 1000):
    # constucteur de la classe mère
    Compte.__init__(self, nom, solde)
  
  # override setter
  @Compte.solde.setter # passe la méthode publique suivante en setter
  def solde(self,solde):
    if solde < self._solde :
      print("Diminution du solde impossible sur compte PEL")
    else :
      self._solde = solde
 
 # Methode de classe
  
  def depot(self, somme):
    self._solde = self._solde + somme
  def retrait(self, *args):
    print("retrait impossible sur compte PEL")

  # override du toString python
  def __str__(self):
    return "Compte PEL : %s / solde : %s" %(self.nom, self._solde)
