#!/usr/bin/python3
# -*-coding:utf8 -*

# définition de classe

class Compte:

  type_compte='Compte'
  @classmethod # methode statique
  def showType(cls):
    return cls.type_compte
 
  # Constucteur de la classe
  # avec valeur de paramètre par défaut
  def __init__(self, nom ='anonyme', solde =1000):
    self.nom = nom # public
    self._solde = solde # _ protected (héritable) / __ private (non héritable)
  

  # getter setter

  @property # passe la méthode publique suivante en propriété (getter)
  def solde(self):
    return self._solde
  @solde.setter # passe la méthode publique suivante en setter
  def solde(self,solde):
    self._solde = solde

  # override du toString python
  def __str__(self):
    return "compte: %s /  solde : %s" %(self.nom, self._solde)
