#!/usr/bin/python3 -B
# -*-coding:utf8 -*

# paquet python3-scipy

from scipy.io import loadmat, savemat
import scipy

A = scipy.matrix( [ [1, 0, 2],[0, 3, 0] ] )
B = scipy.matrix( [ [1, 1, 1, 1],[2, 2, 2, 2],[3,3,3,3] ] )
print("Matrice A \n" ,A)
print("Matrice B \n" ,B)

# save des matrices vers fichier
savemat('matrice_save', {'A': A, 'B': B})
# load des matrices du fichiers
m = loadmat('matrice_save')
print("Matrice save A \n" ,m['A'])
print("Matrice save B \n" ,m['B'])
