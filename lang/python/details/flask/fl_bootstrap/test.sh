#!/bin/bash

python3 -m venv venv
. venv/bin/activate

# RUN APP
pip install wheel
pip install -e .
export FLASK_APP=fl_bootstrap
export FLASK_ENV=development
export LC_ALL=C.UTF-8
export LANG=C.UTF-8

# INIT-DB
#flask init-db
flask run

# UNIT TEST
#pip install -e .[test]
#pytest
#coverage run -m pytest
#coverage report
#coverage html
