#!/usr/bin/python3 -B
# -*-coding:utf8 -*

from flask import Flask, render_template
# from mocks import Article # pour test en dehors de la base
from datetime import datetime
# python3-flask-sqlalchemy # wrapper MySQL/PostgreSQL/sqlite
from flask_sqlalchemy import SQLAlchemy

from forms import RegistrationForm
from wtforms import Form, BooleanField, StringField, PasswordField, validators

# mise en module
app = Flask(__name__)

# ORM et base de donnée
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///database/db.sqlite3'
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False # désactivation du suivi des modifications
db = SQLAlchemy(app)

# Schéma des objets de la base
class Article(db.Model):
  """ utilisation en ligne de commande python3 :
  from app import Article, db
  # ajout vers base
  a = Article()
  a.title = "titre"
  a.content = "Contenu"
  db.session.add(a)
  db.session.commit()
  print(a.id) # test de création de l'objet dans la base
  # requete select
  db.session.delete(a)
  db.session.commit()
  Article.query.all()
  Article.query.count()
  Article.query.get(3)
  Article.query.filter_by(title='TitreC').first()
  Article.query.order_by(Article.title.desc()).all()
  # modification dans base
  q = Article.query.get(1)
  q.title='modif titre'
  db.session.commit()
  """

  __tablename__ = 'articles' # facultatif
  id = db.Column(db.Integer, primary_key=True)
  title = db.Column(db.String(255))
  content = db.Column(db.Text)
  
  # redéfinition de la requete Article.query.all()
  def __repr__(self):
    return '<Article {}>'.format(self.title)

# VARIABLES DU SITE
page_title="Flask demo"

# FONCTIONS

@app.errorhandler(404)
def not_found(error):
    return render_template('errors/404.html'), 404

# creation de fonction avancé
@app.context_processor
def utility_processor():
  # gestion du pluriel
  def pluralize(count, singular, plural=None):
    if not isinstance(count, int):
      raise ValueError('{} must be an integer'.format(count))
    if plural is None:
      plural = singular + 's'
    word = singular if count == 1 else plural
    return "{} {}".format(count, word)
  return dict(pluralize=pluralize, now=datetime.now())

# PAGES

@app.route('/')
def home():
  page_title="Home"
  # génération de la page et envoit des variables à utiliser
  return render_template('pages/home.html', page_title=page_title)

@app.route('/contact')
def contact ():
  page_title="Contact"
  return render_template('pages/home.html', page_title=page_title)

# index de tous les articles
@app.route('/blog')
def articles_index():
  page_title="Blog"
  #articles=Article.all() # pour mocks.py
  articles=Article.query.all()
  return render_template('articles/index.html', page_title = page_title, articles=articles)

# affichage d'un article particulier
@app.route('/blog/articles/<int:id>')
def articles_show(id):
  #article=Article.find(id) # pour mocks.py
  article=Article.query.get(id)
  #page_title = article['title'] # pour mocks.py
  page_title = article.title
  return render_template('articles/show.html', page_title = page_title, article=article)

@app.route('/register', methods=['GET', 'POST'])
def register():
    form = RegistrationForm(request.form)
    if request.method == 'POST' and form.validate():
        user = User(form.username.data, form.email.data,
                    form.password.data)
        db_session.add(user)
        flash('Thanks for registering')
        return redirect(url_for('login'))
    return render_template('register.html', form=form)

# appel du module
if __name__ == '__main__':
  db.create_all() # création de la base
  app.run(debug=True,port=5000)
