# README

## DEFINITION

"templates" est le dossier contenant l'ensemble des modèles de pages web.

Il existe plusieurs modèles qui s'imbrique les un dans les autres et qui forme la structure principal du site :
- **layouts** : modèle de page complète où les block de contenu variable ( généralement le block body ) sont signalé sous forme de "block". Les pages enrichisse un layout via le "extends" et redéfinissent le contenu des block de contenu variable via l'appel du nom du block.
- **pages** : contient les modèles des pages du site (Home/Contacts/Articles/...)
- **errors** : contient les pages d'erreurs

Il existe ensuite les autres dossiers dont leur fonction dépends directement de l'activité du site et qui contiennent véritablement le coeur du site ( exemple : posts/tutos/articles) :
- **articles** : modèles de pages d'articles

Il existe enfin des fichiers personnalisés et en dehors de toutes conventions :
- page_template.html.tpl : template d'une page modèle type héritant des différents fichiers templates
