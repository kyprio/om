# test dans le cas où l'on n'utilise pas de BDD
from flask import abort

class Article():

  ARTICLES = [ 
    {'id' : 1, 'title':'article', 'content':'This is my 1st article'},
    {'id' : 2, 'title':'article', 'content':'This is my 2nd article'},
    {'id' : 3, 'title':'article', 'content':'This is my 3rd article'},
  ]

  @classmethod
  def all(cls):
    """ Return all articles"""
    return cls.ARTICLES
  
  @classmethod
  def find(cls, id):
    """ Return one articles"""
    try:
      return cls.ARTICLES[id - 1]
    except IndexError:
      abort(404)
