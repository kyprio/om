# REAMDE

## DEFINITION

projet test Flask comportant :
- gestion de template
- base de donnee SQLite (stockage sur fichiers plats)
- création de fonction avancé

Les dossiers sont :
- **templates** : gestion des templates
- **static** : éléments statiques
- **database** : uniquement dans le cas de SQLite pour stocker la bdd

## DEPENDANCES

python3-flask
python3-flask-sqlalchemy
sqlite3

```
pip3 freeze
Flask==1.0.2
Flask-SQLAlchemy==2.3.2
```

## UTILISATION

Utilisation avec Database par default :
```
from app import Article, db
# ajout vers base
a = Article()
a.title = "titre"
a.content = "Contenu"
db.session.add(a)
db.session.commit()
print(a.id) # test de création de l'objet dans la base
# requete select
db.session.delete(a)
db.session.commit()
Article.query.all()
Article.query.count()
Article.query.get(3)
Article.query.filter_by(title='TitreC').first()
Article.query.order_by(Article.title.desc()).all()
# modification dans base
q = Article.query.get(1)
q.title='modif titre'
db.session.commit()
```
