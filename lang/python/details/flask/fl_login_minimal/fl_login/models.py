from fl_login import db
from flask_login import UserMixin
from datetime import datetime

class User(UserMixin, db.Model):
  id = db.Column(db.Integer, primary_key=True)
  username = db.Column(db.String(20), unique=True, nullable=False)
  password = db.Column(db.String(60), nullable=False)
  # toString de la requete table.query.all()
  def __repr__(self):
    return f"User('{self.username}', '{self.email}'')"
