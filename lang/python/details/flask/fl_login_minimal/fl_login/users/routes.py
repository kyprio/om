from flask import Blueprint, render_template, flash
from fl_login.users.forms import UserForm
from fl_login.users.utils import *
from fl_login import db
from fl_login.models import User
from flask_login import login_user, logout_user, login_required, current_user

users = Blueprint('users', __name__ )

@users.route('/login')
def login():
  user = User.query.filter_by(username='user1').first()
  login_user(user)
  flash('you are logged in', 'success')
  return render_template('index.html')

@users.route('/logout')
@login_required
def logout():
  logout_user()
  flash('you are log out', 'success')
  return render_template('index.html')

@users.route('/current')
@login_required
def current():
  flash('current user is : ' + current_user.username, 'success')
  return render_template('index.html')
