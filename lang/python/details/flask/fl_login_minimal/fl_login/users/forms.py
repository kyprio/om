from flask_wtf import FlaskForm
from wtforms import StringField, PasswordField, SubmitField
from wtforms.validators import InputRequired

class UserForm(FlaskForm):
  username = StringField('Username', validators = [ InputRequired() ] )
  password = PasswordField('Password', validators = [ InputRequired() ] )
  submit_button = SubmitField('Add')
