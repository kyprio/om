#!/usr/bin/python3 -B
# -*-coding:utf8 -*

# cf https://pythonhosted.org/Flask-Uploads/
# THANKS TO PRETTY PRINTED YOUTUBE CHANNEL

from flask import Flask, render_template, request
# import du module upload et du type ALL (cf type plus bas)
from flask_uploads import UploadSet, configure_uploads, ALL

app = Flask(__name__)

# specifie le type d'upload parmi :
# - DEFAULT, (TEXT, DOCUMENT, DATA and IMAGES)
# - ALL
# - TEXT(.txt)
# - IMAGES(.jpg, jpe, .jpeg, .png, .gif, .svg, .bmp)
# - AUDIO (.wav, .mp3, .aac, .ogg, .oga, .flac)
# - DOCUMENTS (.rtf, .odf, .ods, .gnumeric, .abw, .doc, .docx, .xls, .xlsx)
# - DATA (.csv, .ini, .json, .plist, .xml, .yaml, and .yml)
# - SCRIPTS (.js, .php, .pl, .py .rb, and .sh)
# - ARCHIVES (.gz, .bz2, .zip, .tar, .tgz, .txz, and .7z)
# - EXECUTABLES (.so, .exe and .dll)
files = UploadSet('files', ALL)
# configuration app.config['...'] depuis config.py
app.config.from_object('config')
configure_uploads(app, files)

@app.route('/', methods=['GET', 'POST'])
@app.route('/upload', methods=['GET', 'POST'])
def upload():
    if request.method == 'POST' and 'media' in request.files:
        filename = files.save(request.files['media'])
        print(filename)

    return render_template('pages/upload.html', page_title='Upload')

if __name__ == '__main__':
  app.run(debug=True)
