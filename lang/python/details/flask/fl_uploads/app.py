#!/usr/bin/python3 -B
# -*-coding:utf8 -*
from flask import Flask, render_template, flash, request
# import du module upload et du type ALL (cf type plus bas)
from flask_uploads import UploadSet, configure_uploads, ALL

# cf https://pythonhosted.org/Flask-Uploads/

# App config.
app = Flask(__name__)

# specifie le type d'upload parmi :
# - DEFAULT, (TEXT, DOCUMENT, DATA and IMAGES)
# - ALL
# - TEXT(.txt)
# - IMAGES(.jpg, jpe, .jpeg, .png, .gif, .svg, .bmp)
# - AUDIO (.wav, .mp3, .aac, .ogg, .oga, .flac)
# - DOCUMENTS (.rtf, .odf, .ods, .gnumeric, .abw, .doc, .docx, .xls, .xlsx)
# - DATA (.csv, .ini, .json, .plist, .xml, .yaml, and .yml)
# - SCRIPTS (.js, .php, .pl, .py .rb, and .sh)
# - ARCHIVES (.gz, .bz2, .zip, .tar, .tgz, .txz, and .7z)
# - EXECUTABLES (.so, .exe and .dll)
files = UploadSet('files', ALL) # 'files' corresponds à l'url auto générée pour visionner le fichier
app.config['UPLOAD_DIR'] = 'static/uploads'
configure_uploads(app, files)

@app.route('/', methods=['GET', 'POST'])
@app.route('/upload', methods=['GET', 'POST'])
def upload():
  # test si un post a été effectué du nom de archive
  if request.method == 'POST' and 'file' in request.files:
    filename = files.save(request.files['file'])
    return filename
  return render_template('pages/upload.html',page_title='Upload')



@app.errorhandler(404)
def not_found(error):
	return render_template('errors/404.html'),404

if __name__ == '__main__':
  app.run(debug=True)
