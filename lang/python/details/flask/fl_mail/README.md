# README

```
. test.sh
```

```
. untest.sh
```

## FLASK BOOTSTRAP

Vérifier la version utilisé des feuille de style bootstrap pour utiliser les bonnes classes

## FLASK MAIL

https://pythonhosted.org/Flask-Mail/

Configuration Gmail SMTP : Compte Google, Securite, Accès moins sécurisé des applications = Activé

```
    MAIL_SERVER = 'smtp.gmail.com'
    MAIL_PORT = 465
    MAIL_USE_TLS = False
    MAIL_USE_SSL = True
    MAIL_USERNAME = 'mail@gmail.com'
    MAIL_PASSWORD = 'Mdp'
    MAIL_DEFAULT_SENDER = 'mail@gmail.com'
    MAIL_MAX_EMAILS = None
    MAIL_ASCII_ATTACHMENTS = False
```
