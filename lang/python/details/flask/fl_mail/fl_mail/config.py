import os

class Config :
    SECRET_KEY=os.environ.get('SECRET_KEY')
    SQLALCHEMY_DATABASE_URI='sqlite:///fl_mail.db'
    SQLALCHEMY_TRACK_MODIFICATIONS=False
    DEBUG_TB_INTERCEPT_REDIRECTS=False
    MAIL_SERVER = 'smtp.gmail.com'
    MAIL_PORT = 465
    MAIL_USE_TLS = False
    MAIL_USE_SSL = True
    MAIL_USERNAME = 'cookyourapp@gmail.com'
    MAIL_PASSWORD = 'superpassword'
    MAIL_DEFAULT_SENDER = 'cookyourapp@gmail.com'
    MAIL_MAX_EMAILS = None
    MAIL_ASCII_ATTACHMENTS = False
