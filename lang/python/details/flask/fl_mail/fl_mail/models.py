from fl_mail import db
from datetime import datetime

class User(db.Model):
  id = db.Column(db.Integer, primary_key=True)
  username = db.Column(db.String(20), unique=True, nullable=False)
  email = db.Column(db.String(120),  nullable=False)
  password = db.Column(db.String(60), nullable=False)
  # back reference de post permettant les requete de type user.posts[1] 
  posts = db.relationship('Post', backref='author', lazy=True)
  # toString de la requete table.query.all()
  def __repr__(self):
    return f"User('{self.username}', '{self.email}'')"

class Post(db.Model):
  id = db.Column(db.Integer, primary_key=True)
  title = db.Column(db.String(100), nullable=False)
  date_posted = db.Column(db.DateTime, nullable=False, default=datetime.utcnow)
  content = db.Column(db.Text, nullable=False)
  # FK
  user_id = db.Column(db.Integer, db.ForeignKey('user.id'), nullable=False)
  # toString de la requete table.query.all()
  def __repr__(self):
    return f"Post('{self.title}', '{self.date_posted}')"
