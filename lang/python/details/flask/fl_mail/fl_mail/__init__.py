from flask import Flask
from flask_mail import Mail
from flask_bootstrap import Bootstrap
from flask_debugtoolbar import DebugToolbarExtension
from fl_mail.config import Config
from fl_mail.db import db
from fl_mail.models import *


# instanciate Flask extentions
toolbar = DebugToolbarExtension()
Bootstrap = Bootstrap()
Mail = Mail()
# db already instanciated and imported from db.py

# Create the app
def create_app(test_config=None):
  # Instantiate the App and config (def instance dir = ./instance)
    # Instantiate the Flask app
    app = Flask(__name__, instance_relative_config=True)
    app.config.from_object(Config)

    # init_app each Extension
    db.init_app(app)
    toolbar.init_app(app)
    Bootstrap.init_app(app)
    Mail.init_app(app)
    # creation of the schema in .db file
    with app.app_context():
      # Extensions now knows which "app" to use
      # prevent error like : "No application found. Either work inside a view function or push an application context."
      db.create_all()
      
    # register the database commands
    from fl_mail.main.routes import main
    from fl_mail.users.routes import users
    from fl_mail.mails.routes import mails
    app.register_blueprint(main)
    app.register_blueprint(users)
    app.register_blueprint(mails)
    
    return app
