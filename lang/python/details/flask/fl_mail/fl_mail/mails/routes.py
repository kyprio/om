from flask import Blueprint, render_template, flash, redirect, url_for
from flask_mail import Message
from fl_mail.mails.forms import ContactUsMailForm
from fl_mail import Mail

mails = Blueprint('mails', __name__ )

@mails.route('/contact_us', methods=('GET','POST') )
def contact_us():
  form = ContactUsMailForm()
  if form.validate_on_submit() :
    title = form.title.data
    content = form.content.data
    flash('Mail sent, thank you for contacting us !', 'success')

    msg = Message(subject = title , body = content , recipients = ["cyprien.bougault@gmail.com"])
    Mail.send(msg)
    return redirect( url_for('main.home') )
  else :
    return render_template('layout/default.html', page_title = 'Contact Us', form = form)
