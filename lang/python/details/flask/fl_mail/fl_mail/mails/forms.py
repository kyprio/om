from flask_wtf import FlaskForm
from wtforms import StringField, SubmitField
from wtforms.validators import InputRequired, Email

class ContactUsMailForm(FlaskForm):
  title = StringField('Title', validators = [ InputRequired() ] )
  content = StringField('Content', validators = [ InputRequired() ] )
  submit_button = SubmitField('Send')
