#!/bin/bash

deactivate 2> /dev/null
rm -rf fl_mail.egg-info instance/* htmlcov venv
find -name '__pycache__' -exec rm  -rf {} \; 2> /dev/null
find -name '*.db' -exec rm  -rf {} \; 2> /dev/null

echo 'project cleaned'
