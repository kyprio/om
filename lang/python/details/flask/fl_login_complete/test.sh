#!/bin/bash

# CREATE VIRTUAL ENV
python3 -m venv venv
. venv/bin/activate

# MAKE INSTANCE DIRECTORY
mkdir -p instance

# CLEAN FILES
find -name '*.db' -exec rm  -rf {} \; 2> /dev/null

# INSTALL DEPENDENCIES FROM SETUP.PY
pip install wheel
pip install -e .

# CONFIG APP
export FLASK_APP='fl_login'
export FLASK_ENV='development'
export SECRET_KEY='supersecret'
export LC_ALL=C.UTF-8
export LANG=C.UTF-8

# RUN APP
flask run
