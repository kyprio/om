from flask_wtf import FlaskForm
from wtforms import StringField, PasswordField, SubmitField, BooleanField
from wtforms.validators import InputRequired, Length, Email

class LoginForm(FlaskForm):
  username = StringField('Username', validators = [ InputRequired()] , render_kw={'autofocus' : True, 'placeholder' : 'username' } )
  password = PasswordField('Password', validators = [ InputRequired()] , render_kw={'autofocus' : True })
  remember = BooleanField('Remerber me')
  submit_button = SubmitField('Login')

class RegisterForm(FlaskForm):
  username = StringField('Username', validators = [ InputRequired(), Length(min=4, max=15, message='Username between 4 and 15 characters') ], render_kw={'placeholder' : 'username'} )
  email = StringField('Email', validators = [ InputRequired(), Length(max=50, message='Email max 80 characters') ], render_kw={'placeholder':'email@mail.com'} )
  password = PasswordField('Password', validators = [ InputRequired(), Length(min=4, max=80, message='password between 4 and 80 characters') ], render_kw={'placeholder':'your strongest password'} )
  submit_button = SubmitField('Register')
