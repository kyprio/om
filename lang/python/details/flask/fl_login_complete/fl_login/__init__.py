from flask import Flask
from flask_bootstrap import Bootstrap
from flask_debugtoolbar import DebugToolbarExtension
from flask_login import LoginManager
from fl_login.config import Config
from fl_login.db import db
from fl_login.models import *


# instanciate Flask extentions
toolbar = DebugToolbarExtension()
Bootstrap = Bootstrap()
login_manager = LoginManager()
# db already instanciated and imported from db.py

# Create the app
def create_app(test_config=None):
  # Instantiate the App and config (def instance dir = ./instance)
    # Instantiate the Flask app
    app = Flask(__name__, instance_relative_config=True)
    app.config.from_object(Config)

    # init_app each Extension
    db.init_app(app)
    toolbar.init_app(app)
    Bootstrap.init_app(app)
    login_manager.init_app(app)
    # creation of the schema in .db file
    with app.app_context():
      # Extensions now knows which "app" to use
      # prevent error like : "No application found. Either work inside a view function or push an application context."
      # create db
      db.create_all()
      # force non_loggedinuser to go to users.login
      login_manager.login_view = 'users.login'
      
    # register the database commands
    from fl_login.main.routes import main
    from fl_login.users.routes import users
    app.register_blueprint(main)
    app.register_blueprint(users)
    
    return app
