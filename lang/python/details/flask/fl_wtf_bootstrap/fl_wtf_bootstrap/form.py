from flask import Blueprint
from flask import render_template
from flask_wtf import FlaskForm
from wtforms import StringField, PasswordField, SelectField, RadioField, SubmitField
from wtforms.validators import InputRequired, Length

bp = Blueprint('form', __name__, url_prefix='/form')

# Formulaire 
class TestForm(FlaskForm):
  username = StringField('username', validators=[InputRequired(), Length(min=3, max=20, message="Must be between 3 and 20")], render_kw={"placeholder":"username"})
  password = PasswordField('password', validators=[InputRequired(), Length(min=6, max=16, message="Must be between 6 and 16")], render_kw={"placeholder":"password"} )
  select = SelectField('Select', default="php", choices=[('php', 'PHP'), ('nodejs', 'NodeJS') ])
  radio = RadioField('Radio', default="php", choices=[('php', 'PHP'), ('nodejs', 'NodeJS') ])
  submit_button = SubmitField('Submit')

@bp.route('/form_wtf_only', methods=('GET', 'POST'))
def form_wtf_only():
  form = TestForm()
  # test GET or POST du formulaire
  if form.validate_on_submit(): 
    return render_template('success.html', username=form.username.data, password=form.password.data, select=form.select.data, radio=form.radio.data) 
  else :
    return render_template('form_wtf_only.html', page_title='Form WTF', form=form)

@bp.route('/form_wtf_flaskbootstrap', methods=('GET', 'POST'))
def form_wtf_flaskbootstrap():
  form = TestForm()
  # test GET or POST du formulaire
  if form.validate_on_submit(): 
    return render_template('success.html', username=form.username.data, password=form.password.data, select=form.select.data, radio=form.radio.data) 
  else :
    return render_template('form_wtf_flaskbootstrap.html', page_title='Form WTF combined With Flask Bootstrap', form=form)

@bp.route('/form_wtf_quick', methods=('GET', 'POST'))
def form_wtf_quick():
  form = TestForm()
  # test GET or POST du formulaire
  if form.validate_on_submit(): 
    return render_template('success.html', username=form.username.data, password=form.password.data, select=form.select.data, radio=form.radio.data) 
  else :
    return render_template('form_wtf_quick.html', page_title='Form WTF combined With Flask Bootstrap Quickform', form=form)
