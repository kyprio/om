import io

from setuptools import find_packages, setup

setup(
    name='fl_wtf_bootstrap',
    version='1.0.0',
    description='fl_wtf_bootstrap',
    packages=find_packages(),
    include_package_data=True,
    zip_safe=False,
    install_requires=[
        'flask', 'flask_bootstrap','flask_wtf','wtforms',
    ],
    extras_require={
        'test': [
            'pytest',
            'coverage',
        ],
    },
)
