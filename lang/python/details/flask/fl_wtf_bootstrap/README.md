# README

```
. test.sh
```

```
. untest.sh
```

## FLASK WTF BOOTSTRAP

utilisation de Flask WTF, WTForms et Flask Bootstrap.

- Flask WTF permets d'intégrer wtforms à Flask
- WTForms contient des objets prêt à l'emploi automatisant la création de formulaire HTML et la validation du contu côté backend. Il est possible d'utiliser les MACRO jinja2 pour faciliter la création de plusieurs formulaire.
- Flask Bootstrap créer dynamiquement des template Jinja2 prêt à être hérité et appelant les fichiers style et scripts de Bootstrap ( vérifier les version des fichiers pour la mise en forme ). Il contient plusieurs fichier template dont un pour WTF permettant de faciliter la création de formulaire. Pour autant cela n'empèche pas d'avoir à utiliser les class Bootstrap ou d'ajouter un style por un affichage plus précis.

## FLASK BOOTSTRAP

Vérifier la version utilisé des feuille de style bootstrap pour utiliser les bonnes classes
