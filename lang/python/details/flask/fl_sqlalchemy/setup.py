import io

from setuptools import find_packages, setup

setup(
    name='fl_sqlalchemy',
    version='0.0.1',
    description='fl_sqlalchemy',
    packages=find_packages(),
    include_package_data=True,
    zip_safe=False,
    install_requires=[
        'flask', 'flask_bootstrap', 'flask_debugtoolbar', 'flask_wtf', 'wtforms', 'flask_sqlalchemy',
    ],
)
