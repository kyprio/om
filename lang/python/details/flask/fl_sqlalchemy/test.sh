#!/bin/bash

python3 -m venv venv
. venv/bin/activate

# instance directory
mkdir -p instance

# clean db
rm fl_sqlalchemy/fl_sqlalchemy.db 2> /dev/null

# RUN APP
pip install wheel
pip install -e .
export FLASK_APP='fl_sqlalchemy'
export FLASK_ENV='development'
export SECRET_KEY='supersecret'
export LC_ALL=C.UTF-8
export LANG=C.UTF-8

# INIT-DB
flask run
