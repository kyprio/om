#!/bin/bash

deactivate 2> /dev/null
rm -rf fl_sqlalchemy.egg-info instance/* htmlcov venv
find -name '__pycache__' -exec rm  -rf {} \; 2> /dev/null

echo 'project cleaned'
