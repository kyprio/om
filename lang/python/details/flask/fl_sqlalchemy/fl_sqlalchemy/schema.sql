-- Initialize the database.
-- Drop any existing data and create empty tables.

DROP TABLE IF EXISTS sqla_user;
DROP TABLE IF EXISTS sqla_post;

CREATE TABLE sqla_user (
  id INTEGER PRIMARY KEY AUTOINCREMENT,
  username TEXT UNIQUE NOT NULL,
  password TEXT NOT NULL
);

CREATE TABLE sqla_post (
  id INTEGER PRIMARY KEY AUTOINCREMENT,
  user_id INTEGER NOT NULL,
  created TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  title TEXT NOT NULL,
  body TEXT NOT NULL,
  FOREIGN KEY (user_id) REFERENCES sqla_user (id)
);
