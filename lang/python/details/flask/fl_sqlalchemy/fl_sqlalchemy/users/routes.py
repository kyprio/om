from flask import Blueprint, render_template, flash
from fl_sqlalchemy.users.forms import UserForm
from fl_sqlalchemy import db
from fl_sqlalchemy.models import User

users = Blueprint('users', __name__ )

@users.route('/add_user', methods=('GET','POST') )
def add_user():
  form = UserForm()
  if form.validate_on_submit() :
    user = User( username = form.username.data , password = form.password.data, email = form.email.data )
    db.session.add(user)
    db.session.commit()
    flash('Account created : ' + form.username.data, 'success')
    return render_template('index.html',page_title = 'Add User')
  else :
    return render_template('add_user.html', page_title = 'Add User', form = form)

@users.route('/list_users')
def list_users():
  users = User.query.all()
  return render_template('list_users.html', page_title = 'List Users', users = users )
