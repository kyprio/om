from flask import Flask
from flask_bootstrap import Bootstrap
from flask_debugtoolbar import DebugToolbarExtension
from fl_sqlalchemy.config import Config
from fl_sqlalchemy.db import db
from fl_sqlalchemy.models import *

# instanciate Flask extentions
toolbar = DebugToolbarExtension()
Bootstrap = Bootstrap()
# db already instanciated and imported from db.py

# CF README pour plus de detail sur DB

# Create the app
def create_app():
  # Instantiate the App and config (def instance dir = ./instance)
    # Instantiate the Flask app
    app = Flask(__name__, instance_relative_config=True)
    app.config.from_object(Config)

    db.init_app(app)
    toolbar.init_app(app)
    Bootstrap.init_app(app)

    # creation of the schema in .db file
    with app.app_context():
      # Extensions now knows which "app" to use
      # prevent error like : "No application found. Either work inside a view function or push an application context."
      db.create_all()

    # register the database commands
    from fl_sqlalchemy.main.routes import main
    from fl_sqlalchemy.posts.routes import posts
    from fl_sqlalchemy.users.routes import users
    app.register_blueprint(main)
    app.register_blueprint(posts)
    app.register_blueprint(users)

    return app
