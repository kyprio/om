import os

class Config :
    SECRET_KEY=os.environ.get('SECRET_KEY')
    #SQLALCHEMY_DATABASE_URI='sqlite:////tmp/fl_sqlalchemy.db' # chemin absolu sqlite:////
    SQLALCHEMY_DATABASE_URI='sqlite:///fl_sqlalchemy.db' # chemin relatif sqlite:/// par rapport au __init__.py
    SQLALCHEMY_TRACK_MODIFICATIONS=False
