from flask_wtf import FlaskForm
from wtforms import StringField, PasswordField, SubmitField
from wtforms.validators import InputRequired

class PostForm(FlaskForm):
  title = StringField('Title', validators = [ InputRequired() ] )
  content = StringField('Content', validators = [ InputRequired() ] )
  user_id = StringField('Owner username', validators = [ InputRequired() ] )
  submit_button = SubmitField('Add')
