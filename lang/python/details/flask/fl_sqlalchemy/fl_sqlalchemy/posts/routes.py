from flask import Blueprint, render_template, flash
from fl_sqlalchemy.posts.forms import PostForm
from fl_sqlalchemy import db
from fl_sqlalchemy.models import Post, User

posts = Blueprint('posts', __name__ )

@posts.route('/add_post', methods=('GET','POST') )
def add_post():
  form = PostForm()
  if form.validate_on_submit() :
    user_id = User.query.filter_by(username=form.user_id.data).first().id
    post = Post( title = form.title.data , user_id = user_id , content = form.content.data )
    db.session.add(post)
    db.session.commit()
    flash('Post created : ' + form.title.data, 'success')
    return render_template('index.html',page_title = 'Add Post')
  else :
    return render_template('add_post.html', page_title = 'Add Post', form = form)

@posts.route('/list_posts')
def list_posts():
  posts = Post.query.all()
  return render_template('list_posts.html', page_title = 'List posts', posts = posts )
