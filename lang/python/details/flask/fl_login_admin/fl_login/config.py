import os

class Config :
    SECRET_KEY=os.environ.get('SECRET_KEY')
    SQLALCHEMY_DATABASE_URI='sqlite:///fl_login.db'
    SQLALCHEMY_TRACK_MODIFICATIONS=False
    DEBUG_TB_INTERCEPT_REDIRECTS=False
