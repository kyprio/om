from flask import redirect, url_for
from flask_login import current_user
from flask_admin import AdminIndexView
from flask_admin.contrib.sqla import ModelView

# AdminIndexView est le backend complet d'admin url.com/admin
class MyAdminIndexView(AdminIndexView):
  # la vue des objet de la base sont accessible pour : ...
  # override du comportement des méthodes du AdminIndexView
  def is_accessible(self):
    return current_user.is_authenticated

  # comportement si page est inaccessible pour l'utilisateur
  def inaccessible_callback(self, name, **kwargs):
    return redirect(url_for('main.home'))

# ModelView correspond à une vue du modele de BDD
class MyModelView(ModelView):
  # override du comportement des méthodes de ModelView

  # la vue des objet de la base sont accessible pour : ...
  def is_accessible(self):
    return current_user.is_authenticated

  # comportement si page est inaccessible pour l'utilisateur
  def inaccessible_callback(self, name, **kwargs):
    return redirect(url_for('main.home'))
