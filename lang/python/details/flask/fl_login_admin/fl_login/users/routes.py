from flask import Blueprint, render_template, flash, redirect, url_for
from fl_login.users.forms import *
from fl_login.users.utils import *
from fl_login import db
from fl_login.models import User
from flask_login import login_user, logout_user, login_required, current_user
from werkzeug.security import generate_password_hash, check_password_hash

users = Blueprint('users', __name__ )

@users.route('/login', methods=['GET','POST'])
def login():
  form = LoginForm()
  if form.validate_on_submit():
    user = User.query.filter_by(username = form.username.data).first()
    if user :
      if check_password_hash(user.password, form.password.data):
        # load user into flask_login.current_user cookie
        login_user(user, remember=form.remember.data)
        return redirect(url_for('main.dashboard'))
    flash('Invalid username or password', 'error')
  return render_template('login.html', form=form)

@users.route('/register', methods=['GET','POST'])
def register():
  form = RegisterForm()
  if form.validate_on_submit():
    hashed_password = generate_password_hash(form.password.data, method='sha256')
    user = User(username=form.username.data, email=form.email.data, password=hashed_password)
    db.session.add(user)
    db.session.commit()
    flash('User registered, now you can log you in', 'success')
    return redirect(url_for('users.login'))
  return render_template('register.html', form=form )

@users.route('/logout')
# force user to be logged in cf__init__.py Login_manager.login_view
@login_required
def logout():
  # unload user from flask_login.current_user cookie
  logout_user()
  return redirect(url_for('main.index'))
