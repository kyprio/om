from fl_login.models import User
from fl_login import login_manager

# load the whole user to Flask Login 'current_user'
# so that : current_user.username or current_user.id ...
# this is a special Flask_login function to override
@login_manager.user_loader
def load_user(user_id):
  return User.query.get(int(user_id))
