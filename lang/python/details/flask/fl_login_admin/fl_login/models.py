from fl_login import db
from flask_login import UserMixin, current_user
from datetime import datetime
from flask_admin import AdminIndexView
from flask_admin.contrib.sqla import ModelView

class User(UserMixin, db.Model):
  id = db.Column(db.Integer, primary_key=True)
  username = db.Column(db.String(15), unique=True, nullable=False)
  email = db.Column(db.String(50), unique=True, nullable=False)
  password = db.Column(db.String(80), nullable=False)
  # toString de la requete table.query.all()
  def __repr__(self):
    return f"User('{self.username}', '{self.email}'')"
