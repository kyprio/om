import io

from setuptools import find_packages, setup

setup(
    name='fl_login',
    version='0.0.1',
    description='fl_login',
    packages=find_packages(),
    include_package_data=True,
    zip_safe=False,
    install_requires=[
        'flask', 'flask_bootstrap', 'flask_debugtoolbar', 'flask_wtf','wtforms','flask_sqlalchemy', 'flask_login', 'flask_admin',
    ],
)
