#!/usr/bin/python3 -B
# -*-coding:utf8 -*

from jinja2 import Template
import yaml


# variables

data = yaml.full_load(open('data.yml'))
with open('template.tpl') as tpl :
  print(Template(tpl.read()).render(data))
