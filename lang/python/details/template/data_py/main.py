#!/usr/bin/python3 -B
# -*-coding:utf8 -*

from jinja2 import Template


# variables

from data import data
with open('template.tpl') as tpl :
  print(Template(tpl.read()).render(data))
