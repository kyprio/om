data = {
  'id': 1,
  'name': 'R1',
  'vlans': {
    '10': 'Users',
    '20': 'Voice',
    '30': 'Managements'
  },
  'ospf': [
    {
      'network': '10.0.1.0 0.0.0.255',
      'area': 0
    },
    {
      'network': '10.0.2.0 0.0.0.255',
      'area': 2
    },
    {
      'network': '10.1.1.0 0.0.0.255',
      'area': 0
    }
  ]
} 
