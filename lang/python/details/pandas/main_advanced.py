#!/usr/bin/python3
# -*-coding:utf8 -*

import pandas as pd
import re

print('--- import')
df = pd.read_csv('https://raw.githubusercontent.com/KeithGalli/pandas/master/pokemon_data.csv', delimiter=',')
print(df.head())
print()


print('--- Parsing/filtering value in column into new dataframe')
# df has not changed it-self, it has just returned a modified version
grass_df = df.loc[df['Type 1' ] == 'Poison']
print(grass_df.head())
#print()

print('--- Parsing with condition into new dataframe')
mega_df = df.loc[(df['Type 1'] == 'Grass') & (df['Type 2'] == 'Poison') & (df['HP'] > 70)]
print(mega_df.head())
print()

print('--- reset index')
# inplace -> modify the df it-self rather than just return a modified result
mega_df.reset_index(drop=True, inplace=True )
print(mega_df.head())
print()

print('--- filtering a pattern : str.contains')
print(df.loc[df['Name'].str.contains('Mega')])
# invert
#print(df.loc~[df['Name'].str.contains('Mega')])
print()

print('--- filtering multiple pattern : str.contains + regex')
# using module re
# re.I -> ignore case
print(df.loc[df['Type 1'].str.contains('fire|grass', flags=re.I, regex=True)])
print()

print('--- filtering with regex : str.contains + regex')
print(df.loc[df['Name'].str.contains('^pi[a-z]*', flags=re.I, regex=True)])

print('--- replace value with pattern : =')
# Fire -> Flamer
df.loc[df['Type 1'] == 'Fire', 'Type 1'] = 'Flamer'
print(df.head())
print()
# rollback
df.loc[df['Type 1'] == 'Flamer', 'Type 1'] = 'Fire'

print('--- replace one value from another columns : =')
# all pokemon Fire -> legendary
df.loc[df['Type 1'] == 'Fire', 'Legendary'] = True
print(df.head())
print()

print('--- replace several values from another columns : =')
df.loc[df['HP'] > 50, ['Generation', 'Legendary']] = ['HP Gen','HP Leg']
print(df.head())

# rollback
df = pd.read_csv('https://raw.githubusercontent.com/KeithGalli/pandas/master/pokemon_data.csv', delimiter=',')

print('--- average on data : groupby')
# which type of pokemon has the highest defense
print(df.groupby(['Type 1']).mean().sort_values('Defense', ascending=False))


print('--- count with one columns')
# how many pokemon are they for each type 1
df['count'] = 1
print(df.groupby(['Type 1']).count()['count'])
#print(df.groupby(['Type 1']).sum()
print()

print('--- count with several columns')
# usefull with concatenate with loading file by chunk
print(df.groupby(['Type 1', 'Type 2']).count()['count'])
print()
