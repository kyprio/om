#!/usr/bin/python3
# -*-coding:utf8 -*

import pandas as pd

df = pd.read_csv('https://raw.githubusercontent.com/KeithGalli/pandas/master/pokemon_data.csv', delimiter=',')

print('--- show first rows : head()')
print(df.head())
print()

print('--- show headers/columns : columns')
print(df.columns)
print()

print('--- size of the dataframe')
print('number of row : ' + str(df.shape[0]))
print('number of columns : ' + str(df.shape[1]))

print("--- print specific columns (5 first row) : ['<column_name>'] or [['<1>','<2>']]")
#print(df['Name'][0:5])
print(df[['Name', 'HP', '#']][0:5])
print()

print('--- print specific row (multiline view if one index : iloc)')
print(df.iloc[2])
print()
print(df.iloc[0:3])
print()
## read a specific location (2nd row, 1st column)
print(df.iloc[2,1])

print('--- alternative to print row : iterrows()')
for index, row in df.iterrows():
  #print(index, row)
  print(index, row['Name'])
print()

print("--- parsing : loc[df['<column_name>'] == '<value>']")
print(df.loc[df['Type 1'] == "Fire"])

print('--- stats on the all df : describe')
print(df.describe())
print()

print('--- sort the DataFrame : sort_values')
#df.sort_values('Name', ascending=True)
df.sort_values(['Type 1' , 'Type 2'], ascending=[0,1])
print(df.head())
print()

print('--- change : sum columns : creating new columns')
df['Total'] = df['HP'] + df['Attack'] + df['Defense'] + df['Sp. Atk'] + df['Sp. Def'] + df['Speed']
print(df.head())
print()
# alternative with iloc starting from th 4th to the 10th (excluded) columns
#print('--- change : sum colums : creating new columns with iloc and sum')
# ":" means all the rows,
# sum(axis=1 -> horizontaly, 0 -> vertically)
#df['Total'] = df.iloc[:, 4:10].sum(axis=1)

print('--- delete columns : drop')
#df.drop(columns=['Total'])
print(df.head())
print()

print('re order columns using a list')
cols = list(df.columns.values)
# order : 
# 0:4 -> column 0 1 2 3
# [cols[-1]] -> last columns ( Total )
# 4:12 -> column 4 ... 12 (11th column + 1 excluded)
df = df[cols[0:4] + [cols[-1]]  + cols[4:12]]
print(df.head())
print()

print('filter one columns and uniq : unique')
print( df['Type 1'].unique() )


print('write csv to csv file without number of line ( index ) : to_csv')
df.to_csv('modified.csv', index=False, sep=',')
df.to_csv('modified.csv', index=False, sep='\t')
#print('write csv to exel file without number of line ( index ) : to_csv')
#df.to_excel('modified.xlsx, index=False')
