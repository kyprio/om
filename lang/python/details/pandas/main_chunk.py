#!/usr/bin/python3
# -*-coding:utf8 -*

import pandas as pd
import re

print('--- increase limit to display for panda : display.max_rows')
pd.set_option('display.max_rows', None)
#print('--- loading big file : chunk')
# chunk 100 row
#i = 0
#for df in pd.read_csv('https://raw.githubusercontent.com/KeithGalli/pandas/master/pokemon_data.csv', delimiter=',', chunksize=100):
#  print('CHUNK' + str(i))
#  i += 1
#print()

print('--- loading big file and concatenate the groupby to diminish the result size : chunksize + concat')

print('    create empty data frame with the same columns that df')
stat = pd.DataFrame(columns = ['#', 'Name', 'Type 1', 'Type 2', 'HP', 'Attack', 'Defense', 'Sp. Atk', 'Sp. Def', 'Speed', 'Generation', 'Legendary', 'Count'])

print('    load chunk by chunk')
for df in pd.read_csv('https://raw.githubusercontent.com/KeithGalli/pandas/master/pokemon_data.csv', delimiter=',', chunksize=100):
  print('    groupby into results')
  df['Count'] = 1
  results = df.groupby(['Type 1']).count()['Count']
  print(results)
  
  print('    concat result with stat')
  stat = pd.concat([stat, results])

print('    groupby again after concat')
stat = stat.groupby(['Type 1']).count()['Count']

print('    print stat')
print(stat)

print('    print shape of stat')
print(str(stat.shape))


# POUR CORRIGER CA, IL FAUT PEUT ETRE REGARDER DU COTE DE "SUM"
