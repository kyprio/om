#!/usr/bin/python3 -B
# -*-coding:utf8 -*

import random # aléatoire
import tempfile # fichier temporaire
import uuid # nom fichier aléatoire

# si mode DEBUG, le nombre aléatoire est toujours le même
#DEBUG = True
DEBUG = False
if DEBUG:
  random.seed(1)

# entier aléatoire 
print(random.randint (0,10))

# reel aléatoire
print(random.uniform(5 , 25) )

# liste
liste=['user1','user2','user3']
print(random.choice(liste))

# nom fichier temporaire
tempfile = tempfile.NamedTemporaryFile()
print(tempfile.name)

# nom fichier aléatoire
filename = str(uuid.uuid4())
print(filename)
