#!/usr/bin/python3 -B
# -*-oding:utf8 -*

# convertion hex

def hex2dec(s):
  return str(int(s,16))

def dec2hex(s):
  return str(format(int(s),'02x'))

# convertion format ipv4

def hex2ip(s):
	# exemple hex2ip('0101007f') >>> 127.0.1.1
  ip = [(hex2dec(s[6:8])),(hex2dec(s[4:6])),(hex2dec(s[2:4])),(hex2dec(s[0:2]))]
  return '.'.join(ip)

def ip2hex(s):
  # exemple ip2hex('127.0.1.1') >>> 0101007F
  ipliste = s.split('.')
  iphex = ""
  for i in reversed(ipliste) :
    iphex += dec2hex(i)
  return iphex.upper()
