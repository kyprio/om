#!/usr/bin/python3
# -*-coding:utf8 -*

import os

# variables
filename = input("Veuillez entrer le nom du fichier : ")
content = """ligne1
ligne2
ligne3"""

# écriture dans un fichier
print("ecriture fichier")
with open(filename, 'w') as fw :
  fw.write(content)

# lecture complete
print("lecture simple read")
with open(filename) as fr :
  print(fr.read())

# lecture ligne par ligne sans saut de ligne
print("ligne par ligne sans saut de ligne read().splitlines()")
with open(filename) as fic:
  for line in fic.read().splitlines():
	  print(line)

# lecture d'un fichier selon un numéro de caractère
# et gestion des exceptions
print("lecture numéro de caractère")
try:
  with open(filename) as f :
    # placement du curseur
    f.seek(6)
    # lecture de 2 caratère
    print(f.read(1))
except FileNotFoundError as e:
  print('Le fichier (} n\'existe pas !'.format(e.filename))
  exit(1)

# fichier vers liste
liste=[]
print("fichier vers liste")
with open(filename) as fl :
  liste=fl.read().splitlines()
print(liste)

# first 2 line
print("head -2 ")
with open(filename) as head :
  for  i in range(2):
    print(head.readline().rstrip())
