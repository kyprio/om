#!/usr/bin/python3 -B
# -*-coding:utf8 -*

# reading .ods
from ODSReader import ODSReader

doc = ODSReader(u'data.ods', clonespannedcolumns=True)
table = doc.getSheet(u'Sheet1')

# information pratique
nrows = len(table)
ncols = len(table[0])
print("nb ligne : ",nrows)
print("nb colonnes : ",ncols)

# parcours des en-têtes sur 1ère ligne
for j in range(len(table[0])):
  print('entete ',j,':',table[0][j])

# parcours des lignes 1 à nrows
for r in range(1, nrows):
  print('')
  for c in range(ncols):
    print (table[r][c])
