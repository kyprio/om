#!/usr/bin/python3 -B
# -*-coding:utf8 -*

import xlrd # python3-xlrd

filexlsx = 'data.xlsx'
workbook = xlrd.open_workbook(filexlsx)
sheet = workbook.sheet_by_index(0)

# Correspondance de code-type de cellule (facultatif)
celltype = dict()
celltype[0] = 'XL_CELL_EMPTY'
celltype[1] = 'XL_CELL_TEXT'
celltype[2] = 'XL_CELL_NUMBER'
celltype[3] = 'XL_CELL_DATE'
celltype[4] = 'XL_CELL_BOOLEAN'
celltype[5] = 'XL_CELL_ERROR'
celltype[6] = 'XL_CELL_BLANK'


# print cellule ligne 1, colonnes 3
print(sheet.cell_value(1,3))

# information pratique

nrows = sheet.nrows
ncols = sheet.ncols
print(filexlsx)
print("nb ligne : ",nrows)
print("nb colonnes : ",ncols)

# parcours des en-têtes sur 1ère ligne
for c in range(ncols) :
  print('entete',c,':',sheet.cell_value(0,c))

# mise sous liste
data = [[ sheet.cell_value(r,c) for c in range(ncols)]for r in range(nrows)]
print("C3 data[1][2] :",data[1][2]) # C2
print("type interne de C3 :",sheet.cell_type(1,2))
print("type interne de C3 :",celltype[sheet.cell_type(1,2)])

# parcours des lignes 1 à nrows
for r in range(1,nrows):
  print('')
  for c in range(ncols):
    print(data[r][c])
