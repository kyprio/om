#!/usr/bin/python3
# -*-coding:utf8 -*

import os

#Change the current directory
print("\n# os.chdir() os.getcwd()")
os.chdir('dossier')
os.chdir('..')
print(os.getcwd())


#Return a list of the entries in the directory given by path.
print("\n# os.listdir()")
print(os.listdir('dossier'))
#print(os.listdir())

# liste uniquement les fichiers *.log
print("\n# os.listdir() et file.endswith()")
for file in os.listdir('dossier'):
  if file.endswith("log") :
    print(os.path.join('dossier',file))

#Walk through a directory recursively and gets all directories, sub directories, and files
print("\n# os.walk()")
for racine, dirs, fichiers in os.walk('dossier'):
  print("Racine: {}".format(racine))
  print("Repertoires: {}".format(dirs))
  print("Fichiers: {}".format(fichiers))

# afficher de chemin par concaténation avec gestion des séparateurs /
print("\n# os.path.join()")
print(os.path.join("/dossier","fichier"))


# realpath
print("\n# os.path.realpath()")
print(os.path.realpath('dossier'))
