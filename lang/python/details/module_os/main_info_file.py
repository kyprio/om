#!/usr/bin/python3
# -*-coding:utf8 -*

import os

os.chdir('dossier')

# test si fichier ou dossier
print("\n# os.path.isfile() os.path.isdir()")
if os.path.isfile('a.log') :
  print("a.log is a file")
if os.path.isdir('dossier1'):
  print("dossier1 is a directory")

# size (bytes)
print("\n# os.path.getsize()")
print(os.path.getsize('dossier1'))

# test read write execute os.access
print("\n# os.access()")
if os.access('a.log',6):
  print("a.log est rw")
else :
  print("a.log n'est pas rw")

