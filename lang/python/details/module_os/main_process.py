#!/usr/bin/python3
# -*-coding:utf8 -*

import os

# PID UID
print("\n# os.getgid() os.getguid()")
print(os.getpid())
print(os.getuid())

# umask
print("\n# os.umask()")
old=os.umask(2) # set the umask to 2 and return the previous umask.
print(old)
os.umask(old)

# information OS
print("\n#os.uname()")
print(os.uname())

# Change the root directory to the current dir ( /home/user -> ./dossier)
# only root or sudo
print("\n# os.chroot()")
#os.chroot('/home/kyprio/om/lang/python/details/module_os')

# executer une commande # deprecated -> subprocess.check_call(['ls','-l','-a'])
print("\n#os.system('commande')")
os.system('ls -la')
