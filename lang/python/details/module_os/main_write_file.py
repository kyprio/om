#!/usr/bin/python3 -B
# -*-coding:utf8 -*

import os
import shutil


# mkdir and makedirs (recursive)
print("\n# os.mkdir() os.makedirs()")
os.mkdir("dossier2")
os.makedirs("dossier3/dossier")

# rmdir rmtree
print("\n# os.rmdir() shutil.rmtree()")
os.rmdir("dossier2")
shutil.rmtree("dossier3")
