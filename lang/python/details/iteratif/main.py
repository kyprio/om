#!/usr/bin/python3
# -*-coding:utf8 -*

# map permet d'appliquer une fonction à 0 ou un paramètre à une liste d'élement
# et de renvoyé un iteratif
#
# liste=[2,4,6]
# fl=lambda a: a*a
# result = map(fl,liste)
# for i in result :
#   print(i)
#
for i in map(lambda a: a*a, [2,4,6]):
  print(i)
