#!/usr/bin/python3 -B
# -*-coding:utf8 -*


# VARIABLES
line1 = "texte texte"

# ajout saut de ligne
line1 = line1+"\n"
print(line1)

# suppression saut de ligne
line1 = line1.rstrip()
print(line1)

# remplacer un motif simple
line1=line1.replace('texte','truc')
print(line1)

# test si fin de ligne par motif
if line1.endswith("truc") :
  print("fini par truc")

