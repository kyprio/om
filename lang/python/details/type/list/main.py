#!/usr/bin/python3
# -*-coding:utf8 -*

# liste

liste = [] # equivalent de liste = list()
liste = [1,2] #  [1,2]

liste2 = liste # passage de ref
liste.append(3) # [1,2,3]

print("liste2 : {}".format(liste2)) # [1,2,3]
print("taille liste2 : ",len(liste2))

print(liste[0]) # element index 0
print(liste[-1]) # dernier element index 
listestr="Hello World".split() # convertion str vers liste via séparateur par defaut ' '
print(listestr)
text="fichier.txt.toto"
print(text.split('.',2))#  -> split sur le 2ème '.'


# methodes utile
#len(liste) # taille de la liste
#liste.sort #  trier la liste
#liste.append # ajout d'un élément à la fin de la liste
#liste.Reverse # inverser la liste
#liste.index # rechercher un élément dans la liste
#liste.remove # retirer un élement de la liste
#liste.pop # retirer le dernier élément de la liste
