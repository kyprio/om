#!/usr/bin/python3
# -*-coding:utf8 -*

# dict
#dic = {} # equivalent de dic = dict()
#dic['un'] = '1' # {'un': '1'}
#dic['deux'] = '2' # {'deux': '2', 'un': '1'}
dic = {
  'un':'1',
  'deux':'2'
  }
dic2={ 'un':'1', 'deux':'2' }
print('dic',dic)
print('dic2',dic2)
print(dic.keys()) # dict_keys(['deux', 'un'])
print(dic.values()) # dict_values(['2', '1'])
# test de clés dan dic
if "1" in dic  : # equivalent de dic.keys()
  print("La clef 'un' existe pour dic")
# parcours de dic
for clef, valeur in dic.items():
  print (clef, valeur)
