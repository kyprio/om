#!/usr/bin/python3 -B
# -*-coding:utf8 -*

import yaml
from data import data

out_yml='data.yml'

with open(out_yml, 'w') as f:
  yaml.dump(data, f, default_flow_style=False)
