#!/usr/bin/python3 -B
from lxml import etree

tree = etree.parse('data.xml')
tab = ""
for node in tree.xpath('//series/nom') :
  print('{} :'.format(node.get('titre')))
  tab = "  "
  print('{}lang : {}'.format(tab,node.get('lang')))
  print('{}personnages :'.format(tab))
  tab="    "
  for perso in node.xpath('personnage'):
    print('{}- {}'.format(tab,perso.text))

