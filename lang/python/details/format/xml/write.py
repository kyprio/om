#!/usr/bin/python3 -B
# -*-coding:utf8 -*

# creation de fichier xml

# root = etree.Element('racine') # racine
# subel1 = etree.SubElement(root, "sous-element1") # sous-element de root
# subel1.set('cles' , 'valeur') # attribut cles-valeur de subel1
# subsubel1(subel1, "sous-sous-element1") # sous-element de subel1
# subsubel1.text = "texte" # valeur text de subsubsel1
# subsubsubel1(subsubel1,"sous-sous-sous-élément1") = balise indépendante
#
#<racine>
#  <sous-element1 cles="valeur">
#    <sous-sous-element1>texte</sous-sous-element1>
#    <sous-sous-sous-élement1/>
#  </sous-element1>
#</racine>

from lxml import etree 

# variable

out_xml="data.xml"
out_dtd="data.dtd"
doctype='''<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE series SYSTEM "data.dtd">
'''
content_dtd='''<!DOCTYPE series
[
<!ELEMENT series(nom)>
<!ELEMENT nom (personnage)*>
  <!ATTLIST nom
    titre CDATA #REQUIRED
    lang (en | fr) #REQUIRED>
<!ELEMENT personnage (#PCDATA)>
]>
'''

root = etree.Element('series') 
serie = etree.SubElement(root, 'nom')
serie.set('lang', 'fr')
serie.set('titre', 'Docteur_who') 
personnages = ['Docteur Who', 'Rose Tyler', 'Mickey Smith'] 

for perso in personnages:
  personnage = etree.SubElement(serie, 'personnage')
  personnage.text = perso 

try:
  with open(out_xml, 'w') as f:
    f.write(doctype)
    f.write(etree.tostring(root, pretty_print=True).decode('utf-8'))
except IOError: 
  print('Pb écriture ', out_xml)
  exit(1) 
try:
  with open(out_dtd, 'w') as fd:
    fd.write(content_dtd)
except IOError: 
  print('Pb écriture ', out_dtd)
  exit(1) 
