#!/usr/bin/python3 -B
# -*-coding:utf8 -*
import json
from data import data

out_json='data.json'

try:
  with open(out_json, 'w') as f :
    f.write(json.dumps(data, indent=4))
except:
  print('Problème de création de', out_json)
  exit(1)
