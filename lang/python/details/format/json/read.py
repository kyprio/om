#!/usr/bin/python3 -B
# -*-coding:utf8 -*

import json

# variables
in_json='data.json'

try:
  with open(in_json,'r') as f:
    data = json.load(f)
except:
  print('Erreur lors de la lecture du fichier')
  exit(1) 

for series in data.keys() :
  print(series)
  tab="  "
  for nom in data[series].keys():
    print(tab,nom," : ",', '.join(data[series][nom]['personnages']))
