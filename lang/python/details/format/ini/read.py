#!/usr/bin/python3 -B
# -*-coding:utf8 -*

# import
import configparser

# variable
in_ini='data.ini'

# lecture

config = configparser.ConfigParser()
config.read(in_ini)

# affichage

for section in config.sections():
  print('\n[{}]'.format(section))
  for key in config[section]:
    print('{} => {}'.format(key, config[section][key]))
