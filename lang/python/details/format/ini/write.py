#!/usr/bin/python3 -B
# -*-coding:utf8 -*

# import et jeu de données
from data import data
import configparser

# variable
out_ini='data.ini'

config = configparser.ConfigParser()
config['personnages'] = {'chapeau_melon' : 'John Steed, Emma Peel', 'amicalement_votre' : 'Brett Sinclair, Danny Wilde'}
config['saisons'] = { 'chapeau_melon': 6, 'amicalement_votre': 1}

with open(out_ini , 'w') as f:
  config .write(f)
