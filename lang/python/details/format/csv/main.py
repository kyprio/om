#!/usr/bin/python3 -B
# -*-coding:utf8 -*

import csv

#variables
in_csv="data.csv"

with open(in_csv, newline='') as csvf :
  reader = csv.reader(csvf, delimiter=',', quotechar='"')
  for row in reader:
    print(' : '.join(row))
