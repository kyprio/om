#!/usr/bin/python3
# -*-coding:utf8 -*

# attribut
string1 = "texte"
string2 = "texte \"bonjour\""
string3 = "texte"

# IF ELSE
if string1 == string2 :
  print("string 1 = string2")
elif string1 == string3 :
  print("string 1 = string3")
else :
  print("string1 != string2 et string3")

# IF ELSE 1 LIGNE
print("IF ELSE 1 LIGNE")
print("string1 = string2") if string1 == string2 else print("string1 != string2")

# WHILE
# test condition puis execute
# affiche 10 9 8 7 6 5 4 3 2 1 0
num = 10
print("num AVANT while : " + str(num));
while num >= 0 :
  print("num dans while : " + str(num));
  num -= 1

print("num APRES while : " + str(num));

# FOR
# affiche 10 9 8 7 6 5 4 3 2 1 0
for i in range(10,-1,-1) :
#for i in range(11) :
  print("i dans for : ",i)
# affiche une liste
liste = [ 'zero','un','deux' ]
for i in liste:
  print(i)
