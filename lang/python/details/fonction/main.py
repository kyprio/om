#!/usr/bin/python3
# -*-coding:utf8 -*


# fonction avec renvoi de valeur
print("\n# fonction avec renvoi")
def renvoi(a):
    return a+a
print(renvoi(5))

# fonction sans renvoi avec modification de variable
print("\n# fonction modification variable")
def modifvariable(liste, i, j):
  x = liste[i]
  liste[i] = liste[j]
  liste[j] = x
liste = ['a', 'b', 'c']
print(liste)
modifvariable(liste, 1, 2)
print(liste)

# fonction avec plusieurs arguments
print("\n# fonction plusieurs arguments")
def multiargs(*args, **kwargs):
  print('args=%s'%str(args))
  print('kwargs=%s'%str(kwargs))
multiargs()
multiargs('1','2',b=4,a=3)

# execution

# fonction lambda
print("\n# fonction lambda")
fl= lambda x,y: x*y
print(fl(5,2))
