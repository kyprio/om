#!/usr/bin/python3 -B
# -*-coding:utf8 -*

# tri liste entier en string

liste = '5; 12; 24; 6; 1; 8;4'
liste_triee=sorted(map(int, liste.split(';')))
print(list(liste_triee))

# unique sur liste
# set permet d'identifié les valeurs valeurs uniques
liste = [1, 1,4,5,5,3,2,6,5,7]
liste_unique = list(set(liste))
print(liste_unique)

# dictionnaire ordonné
import collections
dico = collections.OrderedDict()
dico['Castle'] = 'Richard'
dico['Becktett'] = 'Kate'
for name, firstname in dico.items():
  print(firstname,name)
