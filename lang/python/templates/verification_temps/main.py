#!/usr/bin/python3 -B
# -*-coding:utf8 -*

import datetime 

date = input('Indiquez un jour au format (jj/mm/aaaa): ')

try:
  (day, month, year) = map(int, date.split('/'))
  datetime = datetime.datetime(year, month, day)
except ValueError:
  print('La date n\'est pas valide !') 
