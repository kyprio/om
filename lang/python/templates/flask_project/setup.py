import io

from setuptools import find_packages, setup

setup(
    name='flask_project',
    version='0.0.1',
    description='flask_project',
    packages=find_packages(),
    include_package_data=True,
    zip_safe=False,
    install_requires=[
        'flask', 'flask_bootstrap', 'flask_debugtoolbar', 'flask_wtf','wtforms','flask_sqlalchemy',
    ],
)
