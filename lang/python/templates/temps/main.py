#!/usr/bin/python3 -B
# -*-coding:utf8 -*

class Duree:
	"""Classe contenant des durées sous la forme d'un nombre de minutes
	et de secondes"""

	def __init__(self, min=0, sec=0):
		"""Constructeur de la classe"""
		self.min = min # Nombre de minutes
		self.sec = sec # Nombre de secondes
	def __repr__(self):
		a = self.min
		b = self.sec / 60
		return str(a+b)
	def __str__(self):
		"""Affichage un peu plus joli de nos objets"""
		return "{0:02}:{1:02}".format(self.min, self.sec)

	def __add__(self, objet_a_ajouter):
		"""L'objet à ajouter est un entier en secondes"""
		nouvelle_duree= Duree()
		# on va copier self dans l'objet créé pour avoir la même durée
		nouvelle_duree.min = self.min
		nouvelle_duree.sec = self.sec
		# on ajoute la durée
		nouvelle_duree.sec += objet_a_ajouter
		# si le nombre de secondes >= 60, on le transfere en minute
		if nouvelle_duree.sec >= 60:
			nouvelle_duree.min += nouvelle_duree.sec // 60
			nouvelle_duree.sec = nouvelle_duree.sec % 60
		# on renvoie la nouvelle durée
		return nouvelle_duree
	def __iadd__(self, objet_a_ajouter):
		self.sec += objet_a_ajouter
		if self.sec >= 60:
			self.min += self.sec //60
			self.sec = self.sec % 60
		return self
