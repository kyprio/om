#!/usr/bin/python3 -B
# -*-coding:utf8 -*

class ModifString():

  def dropSpecialChar(s):
    # retire les caractères spéciaux sauf -
    return ''.join(e for e in s if e.isalnum() or e == '-')

  def replaceAccent(s):
    import unicodedata
    # Retire les accents de string
    return ''.join(c for c in unicodedata.normalize('NFD', s) if unicodedata.category(c) != 'Mn')

