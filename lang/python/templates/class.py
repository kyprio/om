#!/usr/bin/python3
# -*-coding:utf8 -*

# définition de classe
class {{classe}}:

  # Constucteur de la classe
  def __init__(self, nom='anonyme'):
    self.nom = nom # public

  @classmethod # methode statique
  def methodestatique(cls):
    return "toto"
  
  # override du toString
  def __str__(self):
    return '{}'.format(self.nom)
